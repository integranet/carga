Ext.Loader.setConfig(
    {
        enabled: true
    }
);

Ext.application({    
    name: 'CrudNovEq',
    appFolder: 'app',
     controllers: ['ConMainNov','ConNovedades','ConActNovedades']
});