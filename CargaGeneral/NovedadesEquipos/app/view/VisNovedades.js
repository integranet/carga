Ext.define('CrudNovEq.view.VisNovedades', {
    extend: 'Ext.panel.Panel',
    closable:true,
    alias:  'widget.visnovedades',
    height: 483,
    width: 661,
    layout: {
        type: 'border'
    },
    xtype: 'dirnov',
    title: 'Novedades de Equipos',
    initComponent: function() {
        var me = this;        
        Ext.applyIf(me, {
            items: [              
                {
                    xtype: 'container',
                    layout: {
                        type: 'border'
                    },
                    region: 'center',
                    items: [
                        {
                            xtype: 'gridpanel',
                            store: 'StoNovedades',
                            name:'gridnovedades',
                            title:'',
                            region: 'center',
                            autoScroll: true,
                            flex: 6,
                            autoHeight: true,
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'id_nvdad_eqpo',
									hideable: false,
                                    text: 'Id',
                                    flex:0.5
                                },{
                                    xtype: 'gridcolumn',
                                    dataIndex: 'cdgo_eqpo',
									hideable: false,
                                    text: 'Cod. Equipo',
                                    flex:1
                                },{
                                    xtype: 'gridcolumn',
                                    dataIndex: 'desc_nvdad',
                                    hideable: false,
                                    text: 'Descripción',
                                    flex:3
                                },{
                                    xtype: 'gridcolumn',
                                    dataIndex: 'id_tpo_nvdad',
                                    hideable: false,
                                    text: 'Tipo Novedad',
                                    flex:2,
                                    renderer: function(value) {
                                         if(value=='A'){
                                            return 'Activo'
                                         }else if(value=='I'){
                                            return 'Inactivo'
                                         }else if(value=='C'){
                                            return 'Mantenimiento Correctivo'
                                         }else if(value=='P'){
                                            return 'Mantenimiento Preventivo'
                                         }else{
                                            return 'Restringido' 
                                         }
                                     }
                                },{
                                    xtype: 'gridcolumn',
                                    dataIndex: 'fcha_rgstro',
                                    hideable: false,
                                    text: 'Fecha Registro',
                                    flex:1.2
                                },{
                                    xtype: 'gridcolumn',
                                    dataIndex: 'fcha_hra_cierre',
                                    hideable: false,
                                    text: 'Fecha Cierre',
                                    flex:1
                                },/*{
                                    xtype: 'gridcolumn',
                                    dataIndex: 'fcha_hra_fin',
									hideable: false,
                                    text: 'Fecha Final',
                                    flex:1
                                },*/{
                                    xtype: 'gridcolumn',
                                    dataIndex: 'id_estdo_nvdad',
									hideable: false,
                                    text: 'Estado',
                                    flex:1,
                                    renderer: function(value) {
                                         if(value==97){
                                            return 'Abierta'
                                         }else if(value==98){
                                            return 'Cerrada'
                                         }else if(value==110){
                                            return 'Anulada'
                                         }else{
                                            return 'Eliminada'
                                         }
                                     }
                                }                              
                            ],
                            viewConfig: {
                                stripeRows: true,
                                enableTextSelection: true
                            },                            
                            plugins:[
                                Ext.create('Core.ux.FilterRow',{
                                    remoteFilter:true
                                })
                            ],
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    dock: 'top',
                                    items: [
                                        {
                                            xtype: 'button',
                                            text: 'Adicionar',
                                            iconCls: 'add',
                                            action:'addnov'
                                        },{
                                            xtype: 'button',
                                            text: 'Modificar',
                                            iconCls: 'edit',
                                            action:'editnov'
                                        },{
                                            xtype: 'button',
                                            text: 'Eliminar',
                                            iconCls: 'delete',
                                            action: 'delnov',
                                            hidden:true
                                        },{
                                            xtype: 'button',
                                            text: 'Actualizar',
                                            iconCls: 'refresh',
                                            action: 'actgridnov'
                                        }
                                    ]
                                }
                            ]
                        }                    
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});