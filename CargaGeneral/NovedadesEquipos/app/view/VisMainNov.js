Ext.define('CrudNovEq.view.VisMainNov', {
  extend: 'Ext.container.Viewport',
    autoRender:true,
    alias:'widget.vismainnov',
    layout: {
        type: 'border'
    },
    initComponent: function() {
        var me = this;
        Ext.applyIf(me, {
            items: [              
               {
                            xtype: 'tabpanel',
                            region:'center',
                            activeTab: 0,
                            name:'tpanel'                           
                }               
            ]
        });

        me.callParent(arguments);
    }

});