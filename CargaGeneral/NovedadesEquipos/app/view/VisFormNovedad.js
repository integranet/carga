Ext.define('CrudNovEq.view.VisFormNovedad', {
    extend: 'Ext.window.Window',
    alias:'widget.visformnovedad',
    modal:true,
    closable:true,
    height: 326,
   //autoHeight:true,
   // padding: 15,
   width: 460,
   layout: {
          type: 'border'
    },
    title: '',
    EsNuevo:false,
    initComponent: function() {
        var me = this;
        var fechaActual = new Date();
        Ext.applyIf(me, {
            items: [
             {
                xtype:'form' ,
                name:'formNovedad',
                region:'center',
                 layout: {
                      type: 'vbox'
                },
                items:[

              {
                xtype:'container',
                region:'north',
                padding: '10 5 3 10',
                items:[
                {
                    xtype: 'textfield',
                    width: 300,                   
                    name:'id_nvdad_eqpo',                    
                    fieldLabel: 'IdNovedad',
                    hidden:true                    
                },{
                    xtype: 'datefield',
                    width: 300,
                    name:'fcha_rgstro',                                     
                    fieldLabel: 'Fecha Registro',
                    value:fechaActual,
                    readOnly:true,
                    hidden:true
                },{
                    xtype: 'combobox',
                    width: 300,
                    name:'cdgo_eqpo',
                    allowBlank:false,                 
                    fieldLabel: 'Cod. Equipo',
                    emptyText:'Seleccione el equipo',
                    editable:false,                                                        
                    triggerAction: 'all',
                    store:'StoEquipos',
                    displayField: 'Valor',
                    valueField: 'IdValorParametro'
                }

                  ]
           },/*/{
                    xtype: 'textareafield',
                    width: 450,  
                    height:155,                  
                    maxLength:300,
                    name:'desc_nvdad',
                    readOnly:true,
                    allowBlank:false,                 
                    fieldLabel: 'Descripción'
                },*/
            {
                xtype:'container',
                region:'center',
                padding: '10 5 3 10',
                items:[
                 {
                xtype:'container',
                region:'center',
                padding: '0 0 5 105',
                items:[
                        {                               
                                xtype: 'button', 
                                action:'clickAddDescripcion', 
                                iconCls: 'add',     
                                text: 'Descripcion'
                        },{
                            xtype:'label',
                            text:' Pulse aqui para agregar descripcion'
                        }
                ]
              },             
                {
                        xtype: 'htmleditor',
                        name:'desc_nvdad',
                        allowBlank:false,      
                        fieldLabel: 'Descripción',
                        enableLinks: false,
                        enableAlignments:false,
                        enableFont: false, 
                        enableLists: false,
                        enableSourceEdit: false,
                        enableColors: false,
                        enableFontSize: false,
                        enableFormat:false,
                        readOnly:true                       
                 }
                 ]
           }  ,
                
            {
                xtype:'container',
                region:'south',
                padding: '10 10 3 10',
                items:[
                 {
                    xtype: 'combobox',
                    width: 300,
                    name:'id_tpo_nvdad',
                    allowBlank:false,                 
                    fieldLabel: 'Tipo Novedad',
                    emptyText:'Seleccione el tipo de novedad',
                    editable:false,                                                        
                    triggerAction: 'all',
                    store:'StoTipoNovedad',
                    displayField: 'Valor',
                    valueField: 'IdValorParametro'
                },/*{
                    xtype: 'datefield',
                    width: 300,
                    name:'fcha_hra_ini',    
                    value:fechaActual,                                 
                    fieldLabel: 'Fecha Inicial'
                },{
                    xtype: 'datefield',
                    width: 300,
                    name:'fcha_hra_fin', 
                    value:fechaActual,                                
                    fieldLabel: 'Fecha Final'
                },*/
                {
                    xtype: 'combobox',
                    width: 300,
                    name:'id_estdo_nvdad',
                    allowBlank:false,                 
                    fieldLabel: 'Estado',                                
                    emptyText:'Seleccione el estado',
                    editable:false,                                                        
                    triggerAction: 'all',
                    store:'StoEstadoNov',
                    displayField: 'Valor',
                    valueField: 'IdValorParametro'
                }

                ]
            }               
             ]
           }
        ],
            buttonAlign: 'center',
            buttons:[  
               {
                    xtype: 'button',
                    action:'clickAceptar',
                    iconCls: 'aceptar',
                    text: 'Aceptar'
                },
                {
                    xtype: 'button', 
                    action:'clickCancelar',  
                    iconCls: 'cancel',                
                    text: 'Cancelar'
                }
                
            ]
        });

        me.callParent(arguments);
    }

});