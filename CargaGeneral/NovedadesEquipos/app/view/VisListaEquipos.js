Ext.define('CrudNovEq.view.VisListaEquipos', {
    extend: 'Ext.window.Window',
    alias:'widget.vislistaequipos',
    autoShow:true,
    autoScroll: true,
    height: 250,
    width: 544,
    modal:true,
    title: 'Equipos',
    layout:{
        type:'border'
    },
    
    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'gridpanel',
                    height:'100%',
                    //width:'100%',
                    name:'gridequipos',
                    action:'dobleclick',
                    store: 'StoEquipos',
                    autoScroll: true,
                    title: '',
                    region:'center',
                    columns: [
                         {
                            xtype: 'gridcolumn',
                            dataIndex: 'IdValorParametro',
                            flex: 1,
                            text: 'Codigo',
                            hidden:true
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'Valor',
                            flex: 5,
                            text: 'Equipo'
                        }
                    ],
                    viewConfig: {

                    },
                    plugins:[
                        Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })
                    ]
                }
            ],
             buttons:[
                {
                    text:'Aceptar',
                    action:'clickAceptar',
                    iconCls:'aceptar'
                },{
                    text:'Cancelar',
                    action:'clickCancelar',
                    iconCls:'cancel'
                }

            ]
        });

        me.callParent(arguments);
    }

});