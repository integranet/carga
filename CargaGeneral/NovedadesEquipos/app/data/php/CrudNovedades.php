<?php

if(isset($_POST['action']) && !empty($_POST['action'])) {
      $action = $_POST['action'];
      switch($action) {
          case 'insnov' : InsertarNovedad();break;
          case 'updnov' : ActualizaNovedad();break;
          case 'delnov' : EliminarNovedad();break;
        
          // ...etc...
      }
 
  }
   
function cambiaMDY($fecha){
    list($dia,$mes,$anio)=explode("/",$fecha);
    return $mes."/".$dia."/".$anio;
}
function cambiaYMD($fecha){
    list($dia,$mes,$anio)=explode("/",$fecha);
    return $anio."/".$mes."/".$dia;
}
 /*Funcion que cambia el estado de las novedades a inactivas*/
function EliminarNovedad() {

   //include("conexion/config.php");
   include("conexion/conexion.php");
   
    $con = new Conexion($db_nombre, $db_usuario, $db_password);

   if (isset($_POST['id_nvdad_eqpo'])) {
      $idnovedad = $_POST['id_nvdad_eqpo'];
   }

      $sqldelnov = "update nvddes_eqpos set id_estdo_nvdad=111 where id_nvdad_eqpo=" .$idnovedad;
     
     if($con -> ejecutarConsulta($sqldelnov)){
         echo json_encode(array(
                 'success' => true,
                 'razon' => 'Novedad eliminada exitosamente'
          ));        
     }else{
          echo json_encode(array(
                 'success' => false,
                 'razon' => 'Error al realizar operacion'
          ));           
      } 
      
       $con -> cerrarConexion();

}


/*Funcion para insertar novedades en tabla novedades equipos*/
function InsertarNovedad() {
       
      //include("conexion/config.php");
      include("conexion/conexion.php");

 
       $con = new Conexion($db_nombre, $db_usuario, $db_password);
    
       if (isset($_POST['cdgo_eqpo'])) {
           $codigo = $_POST['cdgo_eqpo'];
        }
        if (isset($_POST['desc_nvdad'])) {
            $descripcion = utf8_decode($_POST['desc_nvdad']);
        } 
        if (isset($_POST['id_tpo_nvdad'])) {
           $tiponovedad = $_POST['id_tpo_nvdad'];
        }
        if (isset($_POST['id_estdo_nvdad'])) {
           $estadonovedad = $_POST['id_estdo_nvdad'];
        }
       /*  if (isset($_POST['fcha_hra_ini'])) {
            $fecini = new DateTime(trim(utf8_encode(cambiaYMD($_POST['fcha_hra_ini']))));
            $fechaini=date_format($fecini, 'Y-m-d H:i:s'); 
        } */
        
       $fecregistro=date("Y-m-d H:i:s");
     
       $sqlinsnov = "insert into nvddes_eqpos(cdgo_eqpo,desc_nvdad,id_tpo_nvdad,fcha_rgstro,id_estdo_nvdad) ".
        "values('" . $codigo . "','" . $descripcion . "','".$tiponovedad."','".$fecregistro."',".$estadonovedad.")";
       if($con -> ejecutarConsulta($sqlinsnov)){
               $sqlupdesteq=" update eqpos_aprjos set estdo='".$tiponovedad."' where cdgo_eqpo='" . $codigo . "'";
               if($con -> ejecutarConsulta($sqlupdesteq)){
                  echo json_encode(array(
                         'success' => true,
                         'razon' => 'Novedad agregada exitosamente'
                   )); 
                }else{
                      echo json_encode(array(
                                 'success' => false,
                                 'razon' => 'Error al realizar operacion'
                      ));          
                }  
        }else{
            echo json_encode(array(
                       'success' => false,
                       'razon' => 'Error al realizar operacion'
            ));          
        }  

        $con -> cerrarConexion();

}

/*Actualiza parametros*/
  function ActualizaNovedad(){

        //include("conexion/config.php");
        include("conexion/conexion.php");
  
        $con = new Conexion($db_nombre, $db_usuario, $db_password);
        
       if (isset($_POST['id_nvdad_eqpo'])) {
           $idnovedad = $_POST['id_nvdad_eqpo'];
        }     
        if (isset($_POST['cdgo_eqpo'])) {
           $codigo = utf8_decode($_POST['cdgo_eqpo']);
        }
        if (isset($_POST['desc_nvdad'])) {
            $descripcion = utf8_decode($_POST['desc_nvdad']);
        } 
        if (isset($_POST['id_tpo_nvdad'])) {
           $tiponovedad = $_POST['id_tpo_nvdad'];
        }
        if (isset($_POST['id_estdo_nvdad'])) {
           $estadonovedad = $_POST['id_estdo_nvdad'];
        }
     /*  if (isset($_POST['fcha_hra_ini'])) {
            $fecini = new DateTime(trim(utf8_encode(cambiaYMD($_POST['fcha_hra_ini']))));
            $fechaini=date_format($fecini, 'Y-m-d H:i:s');           
        } */
         if($estadonovedad==98){
              $fechacierre=date("Y-m-d H:i:s");

              $sqlactnov = "update nvddes_eqpos set cdgo_eqpo = '".$codigo."', desc_nvdad = '" . $descripcion . "'," .
              " id_tpo_nvdad='".$tiponovedad."',fcha_hra_cierre='".$fechacierre."',id_estdo_nvdad = ".$estadonovedad." ".
              " where id_nvdad_eqpo = " . $idnovedad;
         }else{
                 $sqlactnov = "update nvddes_eqpos set cdgo_eqpo = '".$codigo."', desc_nvdad = '" . $descripcion . "'," .
                " id_tpo_nvdad='".$tiponovedad."',id_estdo_nvdad = ".$estadonovedad." ".
                " where id_nvdad_eqpo = " . $idnovedad;
         }   

                     
        if($con -> ejecutarConsulta($sqlactnov)){
            if($estadonovedad==98){
                  $sqlupdesteq=" update eqpos_aprjos set estdo='A' where cdgo_eqpo='" . $codigo . "'";
            }else{
                 $sqlupdesteq=" update eqpos_aprjos set estdo='".$tiponovedad."' where cdgo_eqpo='" . $codigo . "'";
            }
            
               if($con -> ejecutarConsulta($sqlupdesteq)){
                    echo json_encode(array(
                     'success' => true,
                     'razon' => 'Novedad modificada exitosamente'
                  ));        
                }else{
                      echo json_encode(array(
                                 'success' => false,
                                 'razon' => 'Error al realizar operacion'
                      ));          
                }                       
        }else{
             echo json_encode(array(
                     'success' => false,
                     'razon' => 'Error al realizar operacion'
               ));                
        }
        
           $con -> cerrarConexion();
  }


?>