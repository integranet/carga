Ext.define('CrudNovEq.store.StoTipoNovedad',{
	extend: 'CrudNovEq.store.storeDinamico',
    url: 'app/data/php/SelectDiscreta.php',
    model: 'CrudNovEq.model.ModParametros',
    autoLoad:true,
    extraParams:{
       Discreta:'TIPONOVEDAD'
    }
});