Ext.define('CrudNovEq.store.StoEstadoNov',{
	extend: 'CrudNovEq.store.storeDinamico',
    url: 'app/data/php/SelectParametro.php',
    model: 'CrudNovEq.model.ModParametros',
    autoLoad:true,
    extraParams:{
       IdParametro:27
    }
});