Ext.define('CrudNovEq.store.StoNovedades',{
	extend		: 'Ext.data.Store',
	autoLoad	: true,
	pageSize	: 20,
	model		: 'CrudNovEq.model.ModNovedades',
	proxy		: {
	    SimpleSortMode:true,
		type: 'ajax',
		api: {

		   read: 'app/data/php/CargaGrillaNovedades.php'
		   /* read    : 'app/data/productos.json'*/
		   
		},
		actionMethods: {
			 read    : 'POST'
		},
		
		reader: {
			type: 'json',
			root: 'data',
			successProperty: 'success'
			
		}
		
	}  
});