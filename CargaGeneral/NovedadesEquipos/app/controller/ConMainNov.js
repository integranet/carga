Ext.define('CrudNovEq.controller.ConMainNov',{
	 extend: 'Ext.app.Controller',
	 views:['VisMainNov','VisNovedades','VisListaEquipos'],
   	 models: ['ModNovedades','ModParametros'],
	 stores: ['StoNovedades','StoEstadoNov','StoTipoNovedad','StoEquipos'],
	 init:function (){
	 		document.getElementById('Idbody').innerHTML='';
	 		
	 		//definimos el entorno:
	 		entorno={};
	 		//definimos los controladores:
	 		entorno['Controladores']={};
	 		entorno['Controladores']['ConPrincipal']=this;
	 		//entorno['Controladores']['ConNovedades']=this.getController('ConNovedades');
	 	
	 		//Definimos las Vistas:
	 		//vista principal (Viewport):
	 		entorno['Vistas']={}
	 		entorno['Vistas']['VisMain']=Ext.widget('vismainnov');
	 		
	 		//vista ventana Configuracion de Tablas y Campos:
	 		entorno['Vistas']['VisNovedades']=Ext.widget('visnovedades');
        	entorno['Vistas']['VisMain'].down('tabpanel[name=tpanel]').add(entorno['Vistas']['VisNovedades']);
		    entorno['Vistas']['VisMain'].down('tabpanel[name=tpanel]').setActiveTab(entorno['Vistas']['VisNovedades']);
		//	entorno['Vistas']['VisCrudPrincipal'].down('tabpanel').setActiveTab(entorno['Vistas']['VisCrudEmpresas']);       	
		    //vista de Busqueda Querys y Campos:
			/*entorno['Vistas']['VisBusquedaSql']=null;
            entorno['Vistas']['VisBusquedaCampos']=null;*/
          

            /*Variables del sistema*/
            entorno['Usuario']={};  
            entorno['Usuario']['Nombre']="";
           /* entorno['Editores']={};
            entorno['Arreglo']={};                 
            entorno['Consulta']['Codigo']="";
            entorno['Consulta']['Titulo']="";
            entorno['Arreglo']['Columnas']="";
            /*Se usa para determinar la accion a ejecutar al momento de guardar una
            consulta desde la vista de Sql Dinamico*/
          /*  entorno['SqlDinamico']={};                    
            entorno['SqlDinamico']['EsNuevo']=false;*/
          
            /*Stores*/
            entorno['Stores']={};
		/*	entorno['Vistas']['VisColaMaquinas']=Ext.widget('viscolamaquinas');	           
			entorno['Vistas']['VisColaAuxiliares']=Ext.widget('viscolaauxiliares');	

			entorno.Vistas.VisPrincipal.down('tabpanel[name=tpanel]').add(entorno['Vistas']['VisColaMaquinas']);          
			entorno.Vistas.VisPrincipal.down('tabpanel[name=tpanel]').setActiveTab(entorno['Vistas']['VisColaMaquinas']); 
			entorno.Vistas.VisPrincipal.down('tabpanel[name=tpanel]').add(entorno['Vistas']['VisColaAuxiliares']);          
			*/ 
            this.cargarParametros();
            this.cargaInfoSesion();
            this.cargarDiscretas();
         	this.control({

	        })
  },

   
    buscarParametro:function(value){

            for (var i = 0; i < entorno.parametros.length; i++) {
                if(entorno.parametros[i].IdValorParametro==value){
                    return entorno.parametros[i].Valor;
                }
            }
        return value;
    },

    cargarParametros:function(){
    	
    	Ext.Ajax.request({
					url: 'app/data/php/CargarParametros.php',
					
					success: function(response) {

						var resp = Ext.decode(response.responseText);
						entorno['parametros']=resp.data;
					
					},
					failure: function() {
											
					}
		});
    },  

     cargarDiscretas:function(){    	
    	Ext.Ajax.request({
					url: 'app/data/php/SelectDiscreta.php',
					params:{
                        Discreta:'TIPONOVEDAD'
					},
					success: function(response) {
						var resp = Ext.decode(response.responseText);
						entorno['discretas']=resp.data;					
					},
					failure: function() {
											
					}
		});
    },

    cargaInfoSesion:function(){
    	  Ext.Ajax.request({
            url : 'app/data/php/TraerInfoSesion.php',
            success : function(response) {
                 var resp = Ext.decode(response.responseText);
                 entorno['Usuario']['Nombre']=resp.usuario;
                
            },
            failure: function(){
                 Ext.Msg.alert('Error','Ocurrio un error al cargar la opcion, vuelva a intentar');
            }
  }); 
    }


	       
});