Ext.define('CrudNovEq.controller.ConActNovedades',{
	extend: 'Ext.app.Controller',

	 init:function (){
	 	me=this;
	 	   
         	this.control({
         		'visformnovedad combobox[name=cdgo_eqpo]':{
	            	expand: this.MostrarListaEquipos
	            },	
	            'vislistaequipos gridpanel[action=dobleclick]':{
	            	itemdblclick: this.SeleccionarEquipo
	            },
	            'vislistaequipos button[action=clickAceptar]':{
	            	click: this.SeleccionarEquipo
	            },
	             'vislistaequipos':{
                      close:this.AlCerrarVentanaEquipos
                }, 
	            'vislistaequipos button[action=clickCancelar]':{
	            	click: this.CerrarVentanaEquipos
	            },
                'visformnovedad button[action=clickAceptar]':{
                     click:this.ValidarNovedad
	            },
	            'visformnovedad button[action=clickAddDescripcion]':{
                     click:this.AgregarComentario
	            },
	            'visformnovedad':{
                      close:this.AlCerrarFormNovedad
                }, 
	            'visformnovedad button[action=clickCancelar]':{
                    click:this.CerrarFormNovedad
	            }

	        });
	    
     },
        //Funcion que devuelve la fecha actual
        FechaHoy:function()
		{
		    var fechaActual = new Date();
		 
		    dia = fechaActual.getDate();
		    mes = fechaActual.getMonth() +1;
		    anno = fechaActual.getFullYear();
		   
		 
		    if (dia <10) dia = "0" + dia;
		    if (mes <10) mes = "0" + mes;  
		 
		    fechaHoy = dia + "/" + mes + "/" + anno;
		   
		    return fechaHoy;
		},

       HayNovedadesEnEquipo:function(codigo){
       	        var me=this;
                Ext.Ajax.request({
                          url :'app/data/php/TraeTotalNovAbiertas.php',
                          params:{
                              codigo:codigo
                          },
                          success: function(response) {
                              var resp = Ext.decode(response.responseText);
                               if(resp.cantidad==0){
                                    me.GuardarNovedad();
			                    }else{
			                       	   Ext.Msg.alert('Error', 'El equipo seleccionado cuenta con una novedad en estado abierto, cierrela para agregar una nueva');
			                    }                             
                          },
                          failure: function() {
                                Ext.Msg.alert('Error', 'No es posible conectar con el servidor, intente nuevamente');                  
                          }
                }); 
       },

    	MostrarListaEquipos: function(el,e){
		
		el.collapse();	
       if(entorno['Vistas']['VisListaEquipos']==null){
	       // mask = Ext.Msg.wait( "Espere por favor", "Cargando", null );
	        entorno['Vistas']['VisListaEquipos'] = Ext.create('CrudNovEq.view.VisListaEquipos');
			entorno['Vistas']['VisListaEquipos'].down('gridpanel[action=dobleclick]').store.proxy.extraParams.search='{"IdValorParametro":"","Valor":""}';
			entorno['Vistas']['VisListaEquipos'].down('gridpanel[action=dobleclick]').store.load(function(){
				//	mask.close();
			});
	    
      }
	},

	SeleccionarEquipo: function(){
	 
	    var selrecords= entorno['Vistas']['VisListaEquipos'].down('gridpanel[action=dobleclick]').getSelectionModel().getSelection();
	   
	    if(selrecords.length === 0){
				Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(18));
	    }else{	
	    	  entorno.Vistas['VisFormNovedad'].down('combobox[name=cdgo_eqpo]').setValue(selrecords[0].get('Valor'));
	          this.CerrarVentanaEquipos();
	    }
	
	},


   AlCerrarVentanaEquipos:function(){
        entorno.Vistas.VisListaEquipos=null;
   },

   CerrarVentanaEquipos:function(){
        entorno['Vistas']['VisListaEquipos'].close();
        entorno.Vistas.VisListaEquipos=null;
    },



     ValidarNovedad:function(){
            var frmNovedad=entorno.Vistas['VisFormNovedad'].down('form[name=formNovedad]').getForm();
          
            if(frmNovedad.isValid()){ 
            	var codEquipo=entorno.Vistas['VisFormNovedad'].down('combobox[name=cdgo_eqpo]').getValue();
                var descripcion=entorno.Vistas['VisFormNovedad'].down('htmleditor[name=desc_nvdad]').getValue();
                if(descripcion!=""){
                	if(entorno.Vistas.VisFormNovedad.EsNuevo){
                	     this.HayNovedadesEnEquipo(codEquipo);
                	 }else{
                	 	 this.GuardarNovedad();
                	 }
                    
                 }else{
                 	 Ext.Msg.alert('Error', 'No se ha añadido ninguna descripcion para la novedad');
                 }  

        }else{
             Ext.Msg.alert('Error', 'Algunos campos en el formulario no son válidos');
        }

    },

    GuardarNovedad:function(){
    	  var frmNovedad=entorno.Vistas['VisFormNovedad'].down('form[name=formNovedad]').getForm();
          var accion="";
    	  if(entorno.Vistas.VisFormNovedad.EsNuevo){
              accion='insnov';
          }else{
              accion="updnov";  
          }
          var me=this;
          frmNovedad.submit({
                          url :'app/data/php/CrudNovedades.php',
                          method : 'POST',
                          waitMsg : 'Guardando novedad...',
                           params: {
                              action:accion
                          },    
                          success : function(request, result) {
                               if (this.result.success) {

                                     entorno.Vistas.VisFormNovedad.show();
                                      var msg = Ext.Msg.alert('Operación Exitosa', 'Novedad guardada exitosamente');
                                      Ext.defer(function () {
                                        msg.toFront();
                                      }, 50); 
                                      entorno['Vistas']['VisNovedades'].down('gridpanel[name=gridnovedades]').store.load();
                                      me.CerrarFormNovedad();                                               
                                      
                                 }else{
                                      var msg = Ext.Msg.alert('Error', 'Ocurrió un error al guardar la pregunta');
                                      Ext.defer(function () {
                                        msg.toFront();
                                      }, 50); 
                                 } 
                                                                     
                          },
                          failure : function(request, result) {
                          
                          }
            });
    },

     AgregarComentario:function(){
     	var me=this;
		Ext.Msg.prompt('Descripcion', 'Por favor, Ingrese la descripcion de la novedad:', function(btn, text) {
		    if(btn == 'ok') {		
		       if(Ext.isEmpty(text)){
                      var msg = Ext.Msg.alert('Error', 'Por favor, Ingrese la descripcion');
		              Ext.defer(function () {
		                msg.toFront();
		              }, 50); 
		       }else{
                   fechaActual=me.FechaHoy();
                   var descripcion=entorno.Vistas.VisFormNovedad.down('htmleditor[name=desc_nvdad]').getValue();
                   if(descripcion!=""){
                   	  descripcion+="<br>";
                   }
                   entorno.Vistas.VisFormNovedad.down('htmleditor[name=desc_nvdad]').setValue(descripcion+fechaActual+" - Agregado por: "+ entorno['Usuario']['Nombre']+'<br>'+text);
		       }  
		    }
		}, null,true,null);
     },

   	AlCerrarFormNovedad: function(){        
        entorno.Vistas.VisFormNovedad=null;
    },

    CerrarFormNovedad: function(){       
        entorno.Vistas.VisFormNovedad.close(); 
        entorno.Vistas.VisFormNovedad=null;
    }
    
});
