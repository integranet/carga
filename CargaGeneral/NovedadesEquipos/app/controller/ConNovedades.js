Ext.define('CrudNovEq.controller.ConNovedades',{
	extend: 'Ext.app.Controller',

	 init:function (){
	 	me=this;
	 	   
         	this.control({
              
                'visnovedades button[action=addnov]': {
	                click: this.addnov     
                 },
	            'visnovedades button[action=editnov]': {
                     click: this.editnov
                 },
                 'visnovedades gridpanel[name=gridnovedades]': {
	                 itemdblclick: this.editnov
	             },
	            'visnovedades button[action=delnov]': {
	                click: this.delnov
	             },	           
	            'visnovedades button[action=actgridnov]': {
	                click: this.ActualizaGrillaNovedades
	            }
	        });
	    
     },
    
	addnov:function(){
		  if(entorno.Vistas.VisFormNovedad==null){
                entorno.Vistas['VisFormNovedad'] = Ext.create('CrudNovEq.view.VisFormNovedad',{
                    EsNuevo:true,
                    title:'Agregar Novedad'
                });              
                entorno.Vistas.VisFormNovedad.show();
         }     
       
	},
	editnov:function(){
        var gridNovedades = entorno['Vistas']['VisNovedades'].down('gridpanel[name=gridnovedades]');
        var selrecords= gridNovedades.getSelectionModel().getSelection();
         if(selrecords.length == 0){
            Ext.Msg.alert('Error', entorno['Controladores']['ConPrincipal'].buscarParametro(18));
         }else{
            var idestado=selrecords[0].data.id_estdo_nvdad;
            if(idestado==97){
                if(entorno.Vistas.VisFormNovedad==null){
                    entorno.Vistas['VisFormNovedad'] = Ext.create('CrudNovEq.view.VisFormNovedad',{
                        EsNuevo:false,
                        title:'Modificar Novedad'
                    });
                     entorno.Vistas['VisFormNovedad'].down('form[name=formNovedad]').getForm().loadRecord(selrecords[0]);
    	             entorno.Vistas.VisFormNovedad.show();
    	        }
            }else{
                   Ext.Msg.alert('Error', entorno['Controladores']['ConPrincipal'].buscarParametro(112));
            }
	    }
	},

	delnov:function(){
         var gridNovedades = entorno['Vistas']['VisNovedades'].down('gridpanel[name=gridnovedades]');
         var delrecords= gridNovedades.getSelectionModel().getSelection();
         if(delrecords.length == 0){
            Ext.Msg.alert('Error', 'Por favor, seleccione la novedad a eliminar');
         }else{
             this.EliminarNovedad(delrecords[0].data.id_nvdad_eqpo);
         }

	},

     EliminarNovedad:function(idnovedad){
          //  this.ExistePregEnBaseEvaluacion(delrecords[0].data.IdPregunta);
          Ext.Msg.show({
                    title : 'Confirmación',
                    msg : '¿Está seguro de eliminar la novedad seleccionada?',
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.WARNING,
                    scope : this,
                    width : 450,
                    fn : function(btn, ev){                      
                        if (btn == 'yes') {  
                                  var me=this;                            
                                  Ext.Ajax.request({
                                              url :'app/data/php/CrudNovedades.php',
                                              params:{
                                                  action:'delnov',
                                                  id_nvdad_eqpo:idnovedad
                                              },
                                              success: function(response) {
                                                  var resp = Ext.decode(response.responseText);
                                                  if(resp.success){
                                                      entorno['Vistas']['VisNovedades'].down('gridpanel[name=gridnovedades]').store.load();
                                                      Ext.Msg.alert('Operacion Exitosa', 'La novedad seleccionada se ha eliminado satisfactoriamente');
                                                
                                                  }else{
                                                      Ext.Msg.alert('Error', 'Ocurrió un error al eliminar la pregunta');
                                                  }
                                              },
                                              failure: function() {
                                                    Ext.Msg.alert('Error', 'No es posible conectar con el servidor, intente nuevamente');                  
                                              }
                                  }); 
                        }
                    }
            });         
    },

	ActualizaGrillaNovedades:function(){
          entorno['Vistas']['VisNovedades'].down('gridpanel[name=gridnovedades]').store.load();
    }





});