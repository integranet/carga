Ext.define('equipo.model.Equipo',{
    extend: 'Ext.data.Model',
   fields: [
     'cdgo_eqpo',
     'cdgo_oprdor_prtrio',
     'nmbre_clnte',
     'arrnddo',
      'nmbre_tpo_eqpo',
      'mrca',
      'clor',
      'chsis',
      'mtor',
      'srie',
      'mdlo',
      'cpcdad',
      'sgro',
      'fcha_vncmnto',
      'plca',
      'fcha_ultma_insp',
      'emprsa_inspccion',
      'nmbre_inspctor',
      'en_el_prto',
      'estdo',
      /*horas disponibilidad*/
      'hora0',
      'hora1',
      'hora2'
      ]
});