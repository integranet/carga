Ext.define('equipo.model.Parametro',{
    extend: 'Ext.data.Model',
   fields: [
        'IdValorParametro'
        ,'IdParametro'
        ,'Valor'
        ,'Orden'
      ]
});