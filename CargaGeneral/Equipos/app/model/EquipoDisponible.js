Ext.define('equipo.model.EquipoDisponible',{
    extend: 'Ext.data.Model',
   fields: [
     'cdgo_eqpo'
     ,'cdgo_tpo_eqpo'
     ,'cdgo_oprdor_prtrio'
     ,'nmbre_clnte'
     ,'fcha'
     ,'nmbre_tpo_eqpo'
     ,'cpcdad'
     ,'estdo'
     ,'hra_incial'
     ,'hra_fnal'
     ,'nmbre_oprrio'
     ,'cdgo_oprrio'
    ]
});