Ext.define('equipo.store.stoEquipoDisponible',{
	extend: 'equipo.store.storeDinamico',
    url: 'app/data/php/SelectEquiposDisponible.php',
    model: 'equipo.model.EquipoDisponible', 
    autoLoad: {start: 0, limit: 15},
    extraParams: {hoy: true, estado:true},
    pageSize: 15,
    groupDir:'DESC',
    groupField: 'fcha',
});