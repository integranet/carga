Ext.define('equipo.store.stoEquipo',{
	extend: 'equipo.store.storeDinamico',
    url: 'app/data/php/SelectEquipos.php',
    model: 'equipo.model.Equipo', 
    autoLoad: {start: 0, limit: 15},
    pageSize: 15,
    remoteSort: false,
});