Ext.define('equipo.store.stoComboFiltroEquipo',{
	extend: 'equipo.store.storeDinamico',
    url: 'app/data/php/SelectValorParametro.php',
    model: 'equipo.model.Parametro', 
    autoLoad: true,
    extraParams: {IdParametro: 14},
    pageSize: 15,
    
});