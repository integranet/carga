Ext.define('equipo.store.stoCliente',{
	extend: 'equipo.store.storeDinamico',
    url: 'app/data/php/selectCliente.php',
    model: 'equipo.model.Cliente', 
    //autoLoad: {start: 0, limit: 15},
    //pageSize: 15,
    remoteSort: true,
});