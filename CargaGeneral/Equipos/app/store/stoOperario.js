Ext.define('equipo.store.stoOperario',{
	extend: 'equipo.store.storeDinamico',
    url: 'app/data/php/SelectOperario.php',
    model: 'equipo.model.Operario', 
    
    pageSize: 15,
    remoteSort: true,
});