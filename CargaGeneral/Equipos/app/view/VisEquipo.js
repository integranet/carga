Ext.define('equipo.view.VisEquipo', {
    extend: 'Ext.panel.Panel',
    autoRender:true,
    iconCls:'machine',
    title:'Equipos',
    region:'center',
    alias:'widget.visequipo',
    layout: {
        type: 'border'
    },

    initComponent: function() {
        var me = this;
        var fe = Ext.create('Ext.grid.feature.GroupingSummary', {
                groupHeaderTpl: 'Fecha: {name} ({rows.length} Equipo{[values.rows.length > 1 ? "s" : ""]})',
                hideGroupedHeader: true
            });
        Ext.applyIf(me, {
            items: [
               
                {
                    xtype: 'gridpanel',
                    region:'north',
                    store:'stoEquipo',
                    flex:1,
                    name:'gridAparejo',
                     plugins: [
                        Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })
                    ],
                    columns: [
                        { 
                            text: 'Equipo',
                            flex:0.5,
                            dataIndex: 'cdgo_eqpo' 
                        },
                        { 
                            text: 'Cod Operador',
                            flex:0.5,
                            dataIndex: 'cdgo_oprdor_prtrio' 
                        },
                        { 
                            text: 'Nombre Operador',
                            flex:1.5,
                            dataIndex: 'nmbre_clnte' 
                        },
                        { 
                            text: 'Arrendado',
                            flex:0.3,
                            dataIndex: 'arrnddo' 
                        },
                        { 
                            text: 'Tipo Equipo',
                            flex:1,
                            dataIndex: 'nmbre_tpo_eqpo' 
                        },
                        { 
                            text: 'Marca',
                            flex:0.5,
                            dataIndex: 'mrca' 
                        },
                        { 
                            text: 'Color',
                            flex:0.5,
                            dataIndex: 'clor' 
                        },
                        { 
                            text: 'Chasis',
                            flex:0.5,
                            dataIndex: 'chsis' 
                        },
                        { 
                            text: 'Motor',
                            flex:0.5,
                            dataIndex: 'mtor' 
                        },
                        { 
                            text: 'Serie',
                            flex:0.5,
                            dataIndex: 'srie' 
                        },
                        { 
                            text: 'Modelo',
                            flex:0.5,
                            dataIndex: 'mdlo' 
                        },
                        { 
                            text: 'Seguro',
                            flex:0.5,
                            dataIndex: 'sgro' 
                        },
                        { 
                            text: 'Fecha Venc.',
                            flex:1,
                            dataIndex: 'fcha_vncmnto' 
                        },
                        { 
                            text: 'Placa',
                            flex:0.5,
                            dataIndex: 'plca' 
                        },{ 
                            text: 'Ult. Insp.',
                            flex:0.5,
                            dataIndex: 'fcha_ultma_insp' 
                        },{ 
                            text: 'Empresa Insp',
                            flex:0.5,
                            dataIndex: 'emprsa_inspccion' 
                        },{ 
                            text: 'Nombre Inspector',
                            flex:0.5,
                            dataIndex: 'nmbre_inspctor' 
                        },{ 
                            text: 'En Puerto',
                            flex:0.5,
                            dataIndex: 'en_el_prto' 
                        },{ 
                            text: 'Estado',
                            flex:0.3,
                            dataIndex: 'estdo' 
                        }
                        
                    ],
                    bbar: Ext.create('Ext.PagingToolbar', {
                            store: 'stoEquipo',
                            displayInfo: true,
                            displayMsg: 'Equipos {0} - {1} de {2}',
                            emptyMsg: "No hay equipos",
                            items:[
                              /*   {
                                    xtype:'label',
                                    text:'',
                                    flex:1
                                },
                                {
                                    xtype:'button',
                                    iconCls:'icon-up'

                                },
                                {
                                    xtype:'button',
                                    iconCls:'icon-down'

                                },
                                {
                                    xtype:'label',
                                    text:'',
                                    flex:1
                                }*/
                            ]
                        })   
                },
               
                {
                    xtype: 'gridpanel',
                    region:'center',
                    flex:1,
                    features:[fe],
                    title:'Equipos disponibles',
                    iconCls:'machine',                                  
                    store:'stoEquipoDisponible',
                    name:'gridDisponible',
                    bbar: Ext.create('Ext.PagingToolbar', {
                            store: 'stoEquipoDisponible',
                            displayInfo: true,
                            displayMsg: 'Equipos {0} - {1} de {2}',
                            emptyMsg: "No hay equipos",
                            items:[
                                { xtype: 'tbseparator' },
                                {
                                    xtype:'checkbox',
                                    boxLabel  : 'Solo hoy',
                                    name      : 'isToday',
                                    checked:'true'
                                },
                                { xtype: 'tbseparator' }, 
                                {
                                    xtype:'checkbox',
                                    boxLabel  : 'Solo Activos',
                                    name      : 'isActivo',
                                    checked:'true'
                                }, { xtype: 'tbseparator' },
                                {
                                    xtype:'button',
                                    action:'clickCancelar',
                                    iconCls:'icon-cancelar',
                                    text:'Cancelar disponibilidad'
                                },
                                {
                                    xtype:'button',
                                    action:'clickEditarOperario',
                                    iconCls:'icon-edit',
                                    text:'Cambiar operario'
                                }
                            ]
                        }),  
                    plugins: [
                        Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })
                    ],
                    columns: [
                        { 
                            text: 'Equipo',
                            dataIndex: 'cdgo_eqpo' 
                        },
                        { 
                            text: 'Tipo',
                            flex:1,
                            dataIndex: 'cdgo_tpo_eqpo' 
                        },
                        { 
                            text: 'Capacidad',
                            flex:1,
                            dataIndex: 'cpcdad' 
                        },
                        { 
                            text: 'Codigo. Oper. Port.',
                            flex:1,
                            dataIndex: 'cdgo_oprdor_prtrio' 
                        },{ 
                            text: 'Oper. Port.',
                            flex:1,
                            dataIndex: 'nmbre_clnte' 
                        }
                        ,{ 
                            text: 'Cod. Operario',
                            flex:1,
                            dataIndex: 'cdgo_oprrio' 
                        },
                        { 
                            text: 'Operario',
                            flex:1,
                            dataIndex: 'nmbre_oprrio' 
                        },
                        { 
                            text: 'No. Asignacion',
                            flex:1,
                            dataIndex: '' 
                        },
                        { 
                            text: 'Estado',
                            flex:1,
                            dataIndex: 'estdo' 
                        },
                        { 
                            text: 'Fecha',
                            flex:1,
                            dataIndex: 'fcha' 
                        },
                        { 
                            text: 'Hora Inicial',
                            flex:1,
                            dataIndex: 'hra_incial' 
                        },
                        { 
                            text: 'Hora Final',
                            flex:1,
                            dataIndex: 'hra_fnal' 
                        }
                        
                    ]
                    
                           
                }
               
            ]
        });

        me.callParent(arguments);
    }

});