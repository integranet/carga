Ext.define('equipo.view.VisFormEquipo', {
  extend: 'Ext.window.Window',
    autoRender:true,
    width:400,
    height:375,
    modal:true,
    incremento:60,
    update:false,
    title:'Adicion de equipos disponibles',
    alias:'widget.visformequipo',
    layout: {
        type: 'border'
    },
    FnHoraFinal:{},
    FnHoraInicial:{},
    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                { 
                    xtype:'form',
                    region:'center',
                    bodyPadding:10,
                    items:[
                        {
                            xtype: 'textfield',
                            fieldLabel:'Equipo',
                            labelWidth:60,
                            width:350,
                            hidden:me.update,
                            name:'cdgo_eqpo',
                            
                       },{
                            xtype: 'textfield',
                            fieldLabel:'Tipo Equipo',
                            labelWidth:60,
                            width:350,
                            hidden:me.update,
                            name:'nmbre_tpo_eqpo',
                            
                            
                       },{
                            xtype: 'textfield',
                            labelWidth:60,
                            width:350,
                            hidden:me.update,
                            fieldLabel:'Capacidad',
                            name:'cpcdad',
                            
                       },{
                            xtype:'combobox',
                            fieldLabel:'Opera. Port.',
                            name:'cdgo_oprdor_prtrio',
                            store:'stoCliente',
                            labelWidth:60,
                            hidden:me.update,
                            forceSelection: true,
                            width:350,
                            allowBlank:false,
                            blankText:'Campo obligatorio',
                            displayField: 'nmbre_clnte',
                            valueField: 'cdgo_clnte'
                           
                       },{
                            xtype:'combobox',
                            fieldLabel:'Operario',
                            labelWidth:60,
                            width:350,
                            store:'stoOperario',
                            forceSelection: true,
                            name:'cdgo_oprrio',
                            allowBlank:false,
                            blankText:'Campo obligatorio',
                            displayField: 'nmbre_oprrio',
                            valueField: 'cdgo_oprrio'

                       },{
                            xtype: 'datefield',
                            fieldLabel: 'Fecha',
                            tooltip:'Formato de fecha: Día/Mes/Año',
                            //minValue: 0,
                            format:'d-m-Y',
                            editable: false,
                            hidden:me.update,
                            width:350,
                            labelWidth:60,
                            allowBlank: false,
                            // disabled:true,
                            minValue: new Date(entorno['Controladores']['ConPrincipal'].buscarParametro("FechaActualServidor")),
                            value: new Date(entorno['Controladores']['ConPrincipal'].buscarParametro("FechaActualServidor")),
                            //hideTrigger: true,
                            name: 'fcha'
                        },{
                            xtype: 'timefield',
                            name: 'hra_incial',
                            format:'H:i',
                            labelWidth:60,
                            hidden:me.update,
                            width:350,
                            editable:false,
                            allowBlank:false,
                            blankText:'Campo obligatorio',
                            fieldLabel: 'Hora Inicial',
                            minValue: '0:00:00',
                            // validator:me.FnHoraInicial,
                            maxValue: '23:59:00',
                            increment: me.incremento*1
                            
                        },
                        {
                            xtype: 'datefield',
                            fieldLabel: 'Fecha final',
                            tooltip:'Formato de fecha: Día/Mes/Año',
                            //minValue: 0,
                            format:'d-m-Y',
                            editable: false,
                            readOnly:true,
                            hidden:me.update,
                            width:350,
                            labelWidth:60,
                            allowBlank: false,
                            // disabled:true,
                            minValue: new Date(entorno['Controladores']['ConPrincipal'].buscarParametro("FechaActualServidor")),
                            value: new Date(entorno['Controladores']['ConPrincipal'].buscarParametro("FechaActualServidor")),
                            //hideTrigger: true,
                            name: 'fchaFinal'
                        },{
                            xtype: 'timefield',
                            name: 'hra_fnal',
                            format:'H:i',
                            labelWidth:60,
                            allowBlank:false,
                            editable:false,
                            hidden:me.update,
                            blankText:'Campo obligatorio',
                            validator:me.FnHoraFinal,
                            width:350,
                            fieldLabel: 'Hora Final',
                            minValue: '0:00:00',
                            maxValue: '23:59:00',
                            increment: me.incremento*1
                            
                       },
                       {
                            xtype:'label',
                            text:'Turno:    ???',
                            hidden:me.update,
                            name:'lbTurno'
                       }
                    ],
                    buttons:[
                        {
                            xtype:'button',
                            text:'Aceptar',
                            hidden:me.update,
                            iconCls:'icon-ok',
                            action:'clickDisponerEquipo'
                        },{
                            xtype:'button',
                            text:'Aceptar',
                            hidden:!me.update,
                            iconCls:'icon-ok',
                            action:'clickCambiarOperario'
                        },{
                            xtype:'button',
                            text:'Cancelar',
                            iconCls:'icon-cancelar',
                            action:'clickCancelar'
                        }
                    ]
                }

               
                
               
            ]
        });

        me.callParent(arguments);
    }

});