Ext.define('equipo.controller.ConEquipo',{
	extend: 'Ext.app.Controller',
	
    init:function (){
	 		
     	this.control({
        'visequipo checkbox[name=isToday]':{
           change:this.CargarDisponibles
        },
        'visequipo checkbox[name=isActivo]':{
           change:this.CargarActivos
        },
         'visequipo button[action=clickCancelar]':{
           click:this.CancelarDisponibles
        },
         'visequipo button[action=clickEditarOperario]':{
           click:this.cambiarOperario
        },
        'visequipo gridpanel[name=gridAparejo]':{
           itemdblclick:this.MostrarFormulario
        },
        'visformequipo button[action=clickCancelar]':{
            click:this.Cancelar
        },
        'visformequipo button[action=clickDisponerEquipo]':{
            click:this.Disponer
        },
        'visformequipo button[action=clickCambiarOperario]':{
            click:this.updateEquipoOperario
        },
        'visformequipo datefield[name=fcha]':{
            select:this.clearTime
        },
        'visformequipo timefield[name=hra_incial]':{
            select:this.ValidarHoraInicial
        }

          
      })
  	},

    clearTime:function( field, value, eOpts ){
        entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').clearValue();
        entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_incial]').clearValue();
        entorno['Vistas']['VisFormEquipo'].down('datefield[name=fchaFinal]').setValue(value);

    },

    MostrarFormulario:function(){
        me = this;
       var records = entorno['Vistas']['VisEquipo'].down("gridpanel[name=gridAparejo]").getSelectionModel().getSelection();
       if(records.length>=1){
           entorno['Vistas']['VisFormEquipo']=Ext.create('equipo.view.VisFormEquipo',{
             incremento: entorno['Controladores']['ConPrincipal'].buscarParametro(82),
             FnHoraFinal:me.ValidarHoraFinal,
             FnHoraInicial:me.ValidarHoraInicial
           });
           
           entorno['Vistas']['VisEquipo'].down("gridpanel[name=gridDisponible]").store.load();
           entorno['Vistas']['VisFormEquipo'].show();
           entorno['Vistas']['VisFormEquipo'].down('form').getForm().loadRecord(records[0]);
           

           entorno['Vistas']['VisFormEquipo'].down('combobox[name=cdgo_oprdor_prtrio]').store.proxy.extraParams.query=records[0].data.nmbre_clnte;
           entorno['Vistas']['VisFormEquipo'].down('combobox[name=cdgo_oprdor_prtrio]').store.load(function(){
            entorno['Vistas']['VisFormEquipo'].down('combobox[name=cdgo_oprdor_prtrio]').select(entorno['Vistas']['VisFormEquipo'].down('combobox[name=cdgo_oprdor_prtrio]').getStore().data.items[0]);
           });
           
       }
  	},

	  CargarDisponibles:function(el, newValue, oldValue, eOpts){
        entorno['Vistas']['VisEquipo'].down("gridpanel[name=gridDisponible]").store.proxy.extraParams.hoy=newValue;
       
        entorno['Vistas']['VisEquipo'].down("gridpanel[name=gridDisponible]").store.load();

    },

    CargarActivos:function(el, newValue, oldValue, eOpts){
        entorno['Vistas']['VisEquipo'].down("gridpanel[name=gridDisponible]").store.proxy.extraParams.estado=newValue;
       
        entorno['Vistas']['VisEquipo'].down("gridpanel[name=gridDisponible]").store.load();

    },

    Cancelar:function(){
        entorno['Vistas']['VisFormEquipo'].close();
        entorno['Vistas']['VisFormEquipo']=null;

    },

    Disponer:function(){
        var form= entorno['Vistas']['VisFormEquipo'].down('form').getForm();
        if(form.isValid()){
          
              Ext.Ajax.request({
                url: 'app/data/php/insertDisponibilidad.php',
                //waitMsg: 'Registrando',
                params:{
                  cdgo_oprdor_prtrio:entorno['Vistas']['VisFormEquipo'].down('combobox[name=cdgo_oprdor_prtrio]').getValue(),
                  cdgo_oprrio:entorno['Vistas']['VisFormEquipo'].down('combobox[name=cdgo_oprrio]').getValue(),
                  hra_incial:entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_incial]').getRawValue(),
                  hra_fnal:entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').getRawValue(),
                  cdgo_eqpo:entorno['Vistas']['VisFormEquipo'].down('textfield[name=cdgo_eqpo]').getValue(),
                  fcha:entorno['Vistas']['VisFormEquipo'].down('datefield[name=fcha]').getRawValue(),
                  fchaFinal:entorno['Vistas']['VisFormEquipo'].down('datefield[name=fchaFinal]').getRawValue()

                },
                success:function(response){
                  
                  resp = Ext.decode(response.responseText)
                    if(resp.sw){
                      
                        entorno['Vistas']['VisEquipo'].down("gridpanel[name=gridDisponible]").store.load();
                        entorno['Vistas']['VisFormEquipo'].close();
                        entorno['Vistas']['VisFormEquipo']=null;
                    }else{

                        var mens =entorno['Controladores']['ConPrincipal'].buscarParametro(resp.error);
                        var m = Ext.Msg.alert("informacion",mens);
                        m.toFront();
                    }
                },
                failure:function(){
                  
                        var mens =entorno['Controladores']['ConPrincipal'].buscarParametro(68);
                        var m = Ext.Msg.alert("informacion",mens);
                        m.toFront();
                       
                }
              }           
              );
        }
    },

    CancelarDisponibles:function(){
        var records = entorno['Vistas']['VisEquipo'].down("gridpanel[name=gridDisponible]").getSelectionModel().getSelection();
       if(records.length>=1){
           if(records[0].data.estdo!='I'){
              Ext.Ajax.request({
                url: 'app/data/php/updateEquipoDisponible.php',
                
                params:{
                  sw:0,
                  data:Ext.encode(records[0].data)

                },
                success:function(response){
                  
                  resp = Ext.decode(response.responseText)
                    if(resp.sw){
                      
                        entorno['Vistas']['VisEquipo'].down("gridpanel[name=gridDisponible]").store.load();
                        
                    }else{

                        var mens =entorno['Controladores']['ConPrincipal'].buscarParametro(68);
                        var m = Ext.Msg.alert("informacion",mens);
                        m.toFront();
                    }
                },
                failure:function(){
                  
                        var mens =entorno['Controladores']['ConPrincipal'].buscarParametro(68);
                        var m = Ext.Msg.alert("informacion",mens);
                        m.toFront();
                       
                }
              });
           }else{
              Ext.Msg.alert("informacion",entorno['Controladores']['ConPrincipal'].buscarParametro(80));
           }
           
           
       }else{
          Ext.Msg.alert("informacion",entorno['Controladores']['ConPrincipal'].buscarParametro(80));
       }
    },

    cambiarOperario:function(){
      var records = entorno['Vistas']['VisEquipo'].down("gridpanel[name=gridDisponible]").getSelectionModel().getSelection();
      if(records.length>=1){
          entorno['Vistas']['VisFormEquipo']=Ext.create('equipo.view.VisFormEquipo',{
             update:true,
             title:'Cambiar operario',
             height:110
          });
           
           
           entorno['Vistas']['VisFormEquipo'].show();
           
        
      }else{
          Ext.Msg.alert("informacion",entorno['Controladores']['ConPrincipal'].buscarParametro(80));
       }
    },

    updateEquipoOperario:function(){
      var records = entorno['Vistas']['VisEquipo'].down("gridpanel[name=gridDisponible]").getSelectionModel().getSelection();
      if(records.length>=1){
          var combo = entorno['Vistas']['VisFormEquipo'].down('combobox[name=cdgo_oprrio]');
          if(combo.isValid()){
              Ext.Ajax.request({
                url: 'app/data/php/updateEquipoDisponible.php',
                
                params:{
                  sw:1,
                  data:Ext.encode(records[0].data),
                  cdgo_oprrio:combo.getValue()
                },
                success:function(response){
                  
                  resp = Ext.decode(response.responseText)
                    if(resp.sw){
                      
                        entorno['Vistas']['VisEquipo'].down("gridpanel[name=gridDisponible]").store.load();
                        entorno['Vistas']['VisFormEquipo'].close();
                        entorno['Vistas']['VisFormEquipo']=null;
                        
                    }else{

                        var mens =entorno['Controladores']['ConPrincipal'].buscarParametro(resp.error);
                        var m = Ext.Msg.alert("informacion",mens);
                        m.toFront();
                    }
                },
                failure:function(){
                  
                        var mens =entorno['Controladores']['ConPrincipal'].buscarParametro(68);
                        var m = Ext.Msg.alert("informacion",mens);
                        m.toFront();
                       
                }
              });
          }
      }
    },

    ValidarHoraInicial:function(){
                               try {

                                    h1 =  entorno['Controladores']['ConPrincipal'].buscarParametro(90);
                                    h2 = entorno['Controladores']['ConPrincipal'].buscarParametro(91);
  
                                    hh1 = 1*h1.substring(0,2);
                                    mm1 = 1*h1.substring(3,5);
                                  
                                    hh2 = 1*h2.substring(0,2);
                                    mm2 = 1*h2.substring(3,5);
                                   
                                    var hi = entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_incial]').getValue().getHours();
                                    var mi = entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_incial]').getValue().getMinutes();
                                    entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').clearValue();
                                    
                                    if(hi<=hh1){
                                        if (hi==hh1){
                                            if(mi<mm1){
                                               
                                                 entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').getStore().data.items[hh1]);
                                                entorno['Vistas']['VisFormEquipo'].down('label[name=lbTurno]').setText("Turno:    Nocturno"+" ( "+h2+" a "+h1+" )");
                                            }else{
                                               
                                                entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').getStore().data.items[hh2]);
                                                entorno['Vistas']['VisFormEquipo'].down('label[name=lbTurno]').setText("Turno:    Diurno"+" ( "+h1+" a "+h2+" )");
                                            }
                                        }else{
                                           
                                            entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').getStore().data.items[hh1]);
                                            entorno['Vistas']['VisFormEquipo'].down('label[name=lbTurno]').setText("Turno:    Nocturno"+" ( "+h2+" a "+h1+" )");
                                        } 
                                        
                                    }else{
                                        if(hi>=hh2){

                                            if (hi==hh2){
                                                if(mi<mm2){
                                                   
                                                    entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').getStore().data.items[hh2]);
                                                    entorno['Vistas']['VisFormEquipo'].down('label[name=lbTurno]').setText("Turno:    Diurno"+" ( "+h1+" a "+h2+" )");
                                                }else{
                                                    entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').getStore().data.items[hh1]);
                                                    entorno['Vistas']['VisFormEquipo'].down('label[name=lbTurno]').setText("Turno:    Nocturno"+" ( "+h2+" a "+h1+" )");
                                                }
                                            }else{
                                               
                                                entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').getStore().data.items[hh1]);
                                                entorno['Vistas']['VisFormEquipo'].down('label[name=lbTurno]').setText("Turno:    Nocturno"+" ( "+h2+" a "+h1+" )");
                                            } 
                                            
                                        }else{
                                           
                                            entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').getStore().data.items[hh2]);
                                            entorno['Vistas']['VisFormEquipo'].down('label[name=lbTurno]').setText("Turno:    Diurno"+" ( "+h1+" a "+h2+" )");
                                        }
                                    }

                                  
                                   // return true;
                               }catch(e){
                                 
                                   // return true;
                                }
                                
                            },

    ValidarHoraFinal:function(value, field){
                               try {

                                var dia = entorno['Vistas']['VisFormEquipo'].down('datefield[name=fcha]').getValue();
                                entorno['Vistas']['VisFormEquipo'].down('datefield[name=fchaFinal]').setValue(dia);

                                    h1 =  entorno['Controladores']['ConPrincipal'].buscarParametro(90);
                                    h2 = entorno['Controladores']['ConPrincipal'].buscarParametro(91);
  
                                    hh1 = 1*h1.substring(0,2);
                                    mm1 = 1*h1.substring(3,5);
                                  
                                    hh2 = 1*h2.substring(0,2);
                                    mm2 = 1*h2.substring(3,5);

                                var hi = entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_incial]').getValue().getHours();
                                var hf = entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').getValue().getHours();
                                var mi = entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_incial]').getValue().getMinutes();
                                var mf = entorno['Vistas']['VisFormEquipo'].down('timefield[name=hra_fnal]').getValue().getMinutes();

                                    if(hi<=hh1){
                                        if (hi==hh1){
                                            if(mi<mm1){
                                                sw=false;
                                            }else{
                                                sw=true;
                                            }
                                        }else{
                                            sw=false;
                                        } 
                                        
                                    }else{
                                        if(hi>=hh2){

                                            if (hi==hh2){
                                                if(mi<mm2){
                                                    sw=true;
                                                }else{
                                                    sw=false;
                                                }
                                            }else{
                                                sw=false;
                                            } 
                                            
                                        }else{
                                            sw=true;
                                        }
                                    }

                                    if(sw){

                                        var text = ""+entorno['Controladores']['ConPrincipal'].buscarParametro(81)+" ( "+h1+" a "+h2+" )";

                                            if(hi<=hf){
                                                if(hi==hf){
                                                    if(mi<mf){
                                                        
                                                        return true;
                                                    }else{

                                                        return text;
                                                    }
                                                }else{
                                                   if(hf<=hh2){
                                                         if(hf==hh2){
                                                            if(mf<=mm2){
                                                                return true;
                                                            }else{
                                                                return text;
                                                            }
                                                         }else{
                                                            return true;
                                                         }
                                                        
                                                            
                                                    }else{
                                                            return text;
                                                    }
                                                }
                                            }else{
                                                return text;
                                            }
                                    }else{
                                            var text = ""+entorno['Controladores']['ConPrincipal'].buscarParametro(81)+" ( "+h2+" a "+h1+" )";
 
                                            if (hi>=hh2){

                                                if(hf>=hi){
                                                    if(hf==hi){
                                                        if(mi<mf){
                                                       
                                                            return true;
                                                        }else{

                                                            return text;
                                                        }
                                                    }else{
                                                     
                                                        return true;
                                                    }
                                                }else{
                                                    if(hf<=hh1){
                                                        if(hf==hh1){
                                                            if(mf<=mm1){
                                                              
                                                                entorno['Vistas']['VisFormEquipo'].down('datefield[name=fchaFinal]').setValue(dia.Add("D", 1));
                                                                return true;

                                                            }else{

                                                                return text;
                                                            }
                                                        }else{
                                                          
                                                            entorno['Vistas']['VisFormEquipo'].down('datefield[name=fchaFinal]').setValue(dia.Add("D", 1));
                                                            return true;
                                                        }
                                                    }else{
                                                        return text;
                                                    }
                                                }
                                            }else{

                                                if(hf<=hh1){
                                                        if(hf==hh1){
                                                            if(mf<=mm1){
                                                                if(hf>=hi){
                                                                     if(hf==hi){
                                                                        if(mi<mf){
                                                                         
                                                                            entorno['Vistas']['VisFormEquipo'].down('datefield[name=fchaFinal]').setValue(dia);
                                                                            return true;
                                                                        }else{

                                                                            return text;
                                                                        }
                                                                    }else{
                                                                      
                                                                        entorno['Vistas']['VisFormEquipo'].down('datefield[name=fchaFinal]').setValue(dia);
                                                                        return true;
                                                                    }
                                                                }else{
                                                                    return text;
                                                                }

                                                                
                                                            }else{

                                                                return text;
                                                            }
                                                        }else{
                                                          
                                                           if(hf>=hi){
                                                                     if(hf==hi){
                                                                        if(mi<mf){
                                                                         
                                                                            entorno['Vistas']['VisFormEquipo'].down('datefield[name=fchaFinal]').setValue(dia);
                                                                            return true;
                                                                        }else{

                                                                            return text;
                                                                        }
                                                                    }else{
                                                                      
                                                                        entorno['Vistas']['VisFormEquipo'].down('datefield[name=fchaFinal]').setValue(dia);
                                                                        return true;
                                                                    }
                                                                }else{
                                                                    return text;
                                                                }
                                                        }
                                                }else{
                                                    return text;
                                                }
                                            }

                                           
                                    }
                                    

                                }catch(e){
                                    return "error "+entorno['Controladores']['ConPrincipal'].buscarParametro(81);
                                }
                                
                            }


});