Ext.define('equipo.controller.ConPrincipal',{
	extend: 'Ext.app.Controller',
	views:['VisPrincipal','VisEquipo','VisFormEquipo'],
	stores: ['stoEquipoDisponible','stoEquipo','stoCliente','stoOperario'],
    init:function (){
	 		document.getElementById('Idbody').innerHTML="";
	 		
	 		//definimos el entorno:
	 		this.cargarParametros();

    
	 		
         	this.control({

	        })
  	},

  	IniciarEntorno:function(parametros){
  			entorno ={};

  			// Parametros SI:
  			entorno['parametros']=parametros;
  			
  			// Controladores del sistema:
  			entorno['Controladores']={};
  			entorno['Controladores']['ConPrincipal']=this;

  			// Vistas de Usuario:
  			entorno['Vistas']={};
  			
        entorno['Vistas']['VisPrincipal']=Ext.widget('visprincipal');
        entorno['Vistas']['VisEquipo']=Ext.widget('visequipo');
        
        entorno['Vistas']['VisPrincipal'].add(entorno['Vistas']['VisEquipo']);
       



  	},
	  
    cargarParametros:function(){
    	me=this;
    	Ext.Ajax.request({
					url: 'app/data/php/CargarParametros.php',
					
					success: function(response) {

						var resp = Ext.decode(response.responseText);
						
						me.IniciarEntorno(resp.data);
					
					},
					failure: function() {
											
					}
		});
    },

    buscarParametro:function(value){
    	for (var i = 0; i < entorno.parametros.length; i++) {
    		if(entorno.parametros[i].IdValorParametro==value){
    			return entorno.parametros[i].Valor;
    		}
    	}
    	return value;
    }



	       
});