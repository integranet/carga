<html>
<head>
  <meta charset="UTF-8"/>
   <title>Parametros del sistema</title>
    <link rel="stylesheet" type="text/css" href="../../ext4/resources/css/ext-all.css">
    <link rel="stylesheet" type="text/css" href="../resources/css/crudext.css">	
   <style type="text/css">

     #loading-mask{
         background-color:white;
         height:100%;
         position:absolute;
         left:0;
         top:0;
         width:100%;
         z-index:20000;
     }
  
    #loading{
        height:auto;
        position:absolute;
        left:45%;
        top:40%;
        padding:2px;
        z-index:20001;
    }
   
    #loading a {
        color:#225588;
    }
   
    #loading .loading-indicator{
        background:white;
        color:#444;
        font:bold 13px Helvetica, Arial, sans-serif;
        height:auto;
        margin:0;
        padding:10px;
    }
    #loading-msg {
        font-size: 10px;
        font-weight: normal;
    }

    </style>
</head>
<body id="Idbody">
<div id="loading-mask" style=""></div>
<div id="loading">
   <div class="loading-indicator"><img src="../resources/images/extanim32.gif" width="32" height="32" style="margin-right:8px;float:left;vertical-align:top;"/>Parametros del sistema<a href="#"></a><br /><span id="loading-msg">Cargando estilos e imagenes...</span></div>
</div>


<div id="viewport">


<div id="bd">
   
    <div class="left-column">
        <div id="sample-spacer" style="height:800px;"></div>

        <script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Cargando Nucleo...';</script>
         <script type="text/javascript" src="../../ext4/ext-all.js"></script>
         <script type="text/javascript" src="../resources/js/jquery-1.7.2.min.js"></script>
         <script type="text/javascript" src="../../ext4/locale/ext-lang-es.js"  charset="utf-8"></script>
            
        <script type="text/javascript" src="../resources/js/FilterRow.js"></script>
        <script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Cargando vistas...';</script>
         

        <script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Iniciando...';</script>
                 
        <script type="text/javascript" src="app.js"></script>
         <script language="JavaScript">

           
         
    </div>

    <div style="clear:both"></div>
</div>
</div>

</body>
</html>




