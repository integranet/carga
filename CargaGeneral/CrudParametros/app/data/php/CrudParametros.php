<?php

if(isset($_POST['action']) && !empty($_POST['action'])) {
      $action = $_POST['action'];
      switch($action) {
          case 'insertarpar' : InsertarParametro();break;
          case 'actualizarpar' : ActualizaParametro();break;
          case 'eliminarpar' : EliminarParametro();break;
          case 'insertarvp' : InsertarValorParametro();break;
          case 'actualizarvp' : ActualizaValorParametro();break;
          case 'eliminarvp' : EliminarValorParametro();break;
          // ...etc...
      }
 
  }
   
  
 /*Funcion que cambia el estado de los parametros a inactivo*/
function EliminarParametro() {
   //include("conexion/config.php");
   
   include("conexion/conexion.php");

    $con = new Conexion($db_nombre, $db_usuario, $db_password);

   if (isset($_POST['id'])) {
      $idpar = $_POST['id'];
   }

      $sqldeldet = "update vlor_prmtros set estdo_vlor_prmtro='I' where id_prmtro='" .  $idpar . "'";
      $result = $con -> ejecutarConsulta($sqldeldet); 

      $sqldelpar = "update prmtros set estdo_prmtro='I' where id_prmtro= '" .  $idpar . "'";
      $result = $con -> ejecutarConsulta($sqldelpar);   

       echo "{success:true,razon:Parametro eliminado exitosamente}";
       $con -> cerrarConexion();

}


/*Funcion para insertar nuevos parametros en tabla dir_parametros*/
function InsertarParametro() {
       
      //include("conexion/config.php");
      
      include("conexion/conexion.php");

      $con = new Conexion($db_nombre, $db_usuario, $db_password);
     
       if (isset($_POST['atributo'])) {
          $atributo = $_POST['atributo'];
        }
        if (isset($_POST['descripcion'])) {
            $descripcion = utf8_decode($_POST['descripcion']);
        }
      
 
        $sqlinspar = "insert into prmtros(atrbto,dscrpcion,estdo_prmtro)
        values('" . $atributo . "','" . $descripcion . "','A')";
      
   
         $con -> ejecutarConsulta($sqlinspar);

       echo "{success:true,razon:Parametro agregado exitosamente}";
        $con -> cerrarConexion();

}

/*Actualiza parametros*/
  function ActualizaParametro(){

        //include("conexion/config.php");
        
        include("conexion/conexion.php");
        
          $con = new Conexion($db_nombre, $db_usuario, $db_password);
             
        if (isset($_POST['id'])) {
            $idpar = $_POST['id'];
        }
        if (isset($_POST['descripcion'])) {
            $descripcion = utf8_decode($_POST['descripcion']);
        }
        if (isset($_POST['atributo'])) {
            $atributo = utf8_decode($_POST['atributo']);
        }
        if (isset($_POST['estado'])) {
            $idestadopar = $_POST['estado'];
        }

        $sqlactpar = "update prmtros set dscrpcion = '" . $descripcion . "', atrbto = '" . $atributo . "', 
         estdo_prmtro = '" . $idestadopar . "' where id_prmtro = " . $idpar;
                          
        if($con -> ejecutarConsulta($sqlactpar)){
                echo "{success:true,razon:Parametro modificado exitosamente}";
        }else{
               echo "{success:false,razon:Error al realizar operacion}";
        }
        
           $con -> cerrarConexion();
  }


/*Funcion para insertar nuevos valores de parametros en tabla dir_valorparametros*/
function InsertarValorParametro() {
        
        
        include("conexion/conexion.php");

          $con = new Conexion($db_nombre, $db_usuario, $db_password);

        if (isset($_POST['id'])) {
            $idpar = $_POST['id'];
        }
        if (isset($_POST['valor'])) {
            $valor = utf8_decode($_POST['valor']);
        }
        if (isset($_POST['orden'])) {
            $orden = utf8_decode($_POST['orden']);
        }

        $sqlinsvp = "insert into vlor_prmtros(vlor,id_prmtro,orden,estdo_vlor_prmtro)
        values('" . $valor . "','" . $idpar . "','" . $orden . "','A')";
        $con -> ejecutarConsulta($sqlinsvp);

        echo "{success:true,razon:Valor agregado exitosamente}";
         $con -> cerrarConexion();

}



/*Funcion que cambia el estado de los valores de parametros a inactivo*/
function EliminarValorParametro() {
        
        
        include("conexion/conexion.php");
         
           $con = new Conexion($db_nombre, $db_usuario, $db_password);

        if (isset($_POST['idvp'])) {
             $idvp = $_POST['idvp'];
        }
 
         $sqldelvp = "update vlor_prmtros set estdo_vlor_prmtro='I' where id_vlor_prmtro= '" .  $idvp . "'";
         $result= $con -> ejecutarConsulta($sqldelvp);


          echo "{success:true,razon:Valor de parametro eliminado exitosamente}";
           $con -> cerrarConexion();
}  
     
   
  /*Funcion que actualiza valores de parametros*/
  function ActualizaValorParametro(){
     
         
         include("conexion/conexion.php");

           $con = new Conexion($db_nombre, $db_usuario, $db_password);
        
        if (isset($_POST['idvp'])) {
           $idvp = $_POST['idvp'];
        }
        if (isset($_POST['valor'])) {
            $valor = utf8_decode($_POST['valor']);
        }
        if (isset($_POST['orden'])) {
            $orden = utf8_decode($_POST['orden']);
        }
        if (isset($_POST['estadovp'])) {
            $idestadovp = $_POST['estadovp'];
        }
      

         $sqlactvp = "update vlor_prmtros set vlor = '" . $valor . "', 
         orden='".$orden."', estdo_vlor_prmtro = '" . $idestadovp . "' where
         id_vlor_prmtro = " . $idvp;
                          
        if( $con -> ejecutarConsulta($sqlactvp)){
                echo "{success:true,razon:Valor de parámetro modificado exitosamente}";
        }else{
               echo "{success:false,razon:Error al realizar operacion}";
        }          

         
           $con -> cerrarConexion();
  }




  



?>