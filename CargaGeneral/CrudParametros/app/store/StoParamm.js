Ext.define('CrudParam.store.StoParamm',{
	extend		: 'Ext.data.Store',
	/*autoLoad	: true,*/
	pageSize	: 20,
	model		: 'CrudParam.model.ModParamm',
	proxy		: {
	    SimpleSortMode:true,
		type: 'ajax',
		api: {

			 read    : 'app/data/php/CargaGrillaParamm.php'
		    /*read    : 'app/data/paises.json'*/
		   
		},
		actionMethods: {
			 read    : 'POST'
		},
		extraParams: {
            idparametro: ''
       },
		reader: {
			type: 'json',
			root: 'dataparamm',
			successProperty: 'success'
			
		}
		
	}  
});