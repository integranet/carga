Ext.define('CrudParam.store.StoEstados',{
	extend		: 'Ext.data.Store',
	autoLoad	: true,
	pageSize	: 20,
	model		: 'CrudParam.model.ModParametros',
	proxy		: {
	    SimpleSortMode:true,
		type: 'ajax',
		
		api: {
			
		    read    : 'app/data/json/EstadosGeneralesParam.json'	   
		},
		actionMethods: {
			 read    : 'POST'
		},
		
		reader: {
			type: 'json',
			root: 'dataestado',
            /*root: 'dataemp',*/
			successProperty: 'success'
			
		}
		
	}  
});