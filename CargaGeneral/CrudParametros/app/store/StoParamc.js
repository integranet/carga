Ext.define('CrudParam.store.StoParamc',{
	extend		: 'Ext.data.Store',
	autoLoad	: true,
	pageSize	: 20,
	model		: 'CrudParam.model.ModParamc',
	proxy		: {
	    SimpleSortMode:true,
		type: 'ajax',
		api: {

		   read: 'app/data/php/CargaGrillaParamc.php'
		   /* read    : 'app/data/productos.json'*/
		   
		},
		actionMethods: {
			 read    : 'POST'
		},
		
		reader: {
			type: 'json',
			root: 'dataparamc',
			successProperty: 'success'
			
		}
		
	}  
});