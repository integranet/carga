Ext.define('CrudParam.controller.ConParametros',{
	 extend: 'Ext.app.Controller',
	 views:['VisParametros','VisParamc','VisParamm'],
   	 models: ['ModParamc','ModParamm','ModParametros'],
	 stores: ['StoParamc','StoParamm'],
	 init:function (){
	 	me=this;
	 		document.getElementById('Idbody').innerHTML="";
	 		
	 		VisViewport = Ext.create('CrudParam.view.VisParametros',{
             
        	});        	

        	//Control Sin Selector
        	VisViewport.down('gridpanel[name=gridparamc]').editingPlugin.on('edit',function(){
        		me.ActualizaGrillaParamc();});

        	VisViewport.down('gridpanel[name=gridparamm]').editingPlugin.on('edit',function(){
        		me.ActualizaGrillaVP();});
        	
	 		/*this.filtraGrillaEmpresa(VisViewport);*/
         	this.control({

              
                'dirparam button[action=salir]': {
	                click: this.salir     
                 },
	            'dirparam button[action=addparamc]': {
	                click: this.addparamc
	             },
	            'dirparam button[action=editparamc]': {
	                click: this.editparamc
	             },
	           
	            'dirparam button[action=delparamc]': {
	                click: this.delparamc
	            },

	            'dirparam button[action=actparamc]': {
	                click: this.ActualizaParamc
	            },

	            'dirparam button[action=addparamm]': {
	                click: this.addparamm
	            },
	            'dirparam button[action=editparamm]': {
	                click: this.editparamm
	            },
	            
	            'dirparam button[action=delparamm]': {
	                click: this.delparamm
	            },

	            'dirparam button[action=actparamm]': {
	                click: this.ActualizaParamm
	            },

	            'dirparam gridpanel[name=gridparamc]':{
	            	itemclick: this.MuestraValorParametro
	            }, 
	          
	            'visparamc button[text=Aceptar]': {
	                click: this.AdicionarParametro
	            },    

	           'visparamc button[text=Cancelar]': {
	                click: this.CerrarVentanaParamc
	            },       

	             'visparamm button[text=Aceptar]': {
	                click: this.AdicionarValorParametro
	            },              

	             'visparamm button[text=Cancelar]': {
	                click: this.CerrarVentanaParamm
	            },     

	        })
	    
     },
    
	salir:function(){
        console.log('Ha pulsado sobre el botón Salir');
   },

   addparamc: function(){
   	   	vistaParamc = Ext.create('CrudParam.view.VisParamc');
	    vistaParamc.setTitle('Añadir Parametro');				  	
   },

   editparamc: function(){
	    this.ActualizarParametro();
   },


   delparamc: function(){
		this.BorrarParametro();
	},

	ActualizaParamc: function(){
		VisViewport.down('gridpanel[name=gridparamm]').store.removeAll();
		VisViewport.down('gridpanel[name=gridparamc]').store.removeAll();
		VisViewport.down('gridpanel[name=gridparamc]').store.load();
	},

    addparamm: function(){
    	var selpar= VisViewport.down('gridpanel[name=gridparamc]').getSelectionModel().getSelection();
	    if(selpar.length === 0){
				Ext.Msg.alert('Error', 'Ningún parametro seleccionado, Seleccione parametro al cual desea agregar valores');
    	}else{
          vistaParamm = Ext.create('CrudParam.view.VisParamm');
	      vistaParamm.setTitle('Agregar Valor de Parametro');	
	    }
	},

   editparamm: function(){
		 this.ActualizarValorParametro();
  },

   delparamm: function(){
		this.BorrarVP();
	},

  ActualizaParamm: function(){
   	  VisViewport.down('gridpanel[name=gridparamm]').store.removeAll();
	  VisViewport.down('gridpanel[name=gridparamm]').store.load();
 },

   CopiarStoreModificados:function(store){
        console.log(store);
    	var modified=store.getModifiedRecords();
      
       if(!Ext.isEmpty(modified)){  
           var datos = [];
          /* store.clearFilter();*/
		  
		        for(var i=0;i<modified.length;i++){
                      datos.push(modified[i].data);
                 }
          
      }
     
       return datos;	
   },

	

    AdicionarParametro: function(){

		    var atributo=vistaParamc.down('textfield[name=txtAtributo]').getValue();
		    var descripcion=vistaParamc.down('textfield[name=txtDescripcion]').getValue();
		   /* var idestado=vistaParamc.down('combobox[name=cboestado]').getValue();  */
		   		
		    if(atributo==""){
                 Ext.Msg.alert('Advertencia', 'Debe ingresar un Cod. Parámetro');
		    }else if(descripcion==""){
                 Ext.Msg.alert('Advertencia', 'Ingrese una descripción para el parámetro');
		    }else if(this.buscarParametroEncabezado(atributo,1)){
                  Ext.Msg.alert('Advertencia', 'Código de parámetro ya ingresado');
		    }else{    
                 var me=this;
                 $.ajax({
			        type: "POST",
			        url : 'app/data/php/CrudParametros.php',
			      
			        data:{
			            action:'insertarpar',
			            atributo:atributo,
			            descripcion:descripcion
			        },
			        async:false,
			        success: function(response){	
			           VisViewport.down('gridpanel[name=gridparamm]').store.removeAll();
					   VisViewport.down('gridpanel[name=gridparamc]').store.load();
			           Ext.Msg.alert('Operación Exitosa','Parámetro Agregado Satisfactoriamente');
			           me.CerrarVentanaParamc();  
			          
			           /*var respSyncrono = Ext.decode(response); 
			           console.log(respSyncrono.razon);*/
			        },
			        failure: function(){
			            alert('el servidor no ha respondido la consulta');
			        }
               });
                                   
		    }
	},


	 ActualizarParametro: function(){
           
	 	    var records= VisViewport.down('gridpanel[name=gridparamc]').getSelectionModel().getSelection();
	 	  
	         if(records.length === 0){
				Ext.Msg.alert('Información', 'Por favor seleccione un elemento de la grilla');
			}else{
               if(records.length === 1){
               	       EditorEncabezado[0].startEdit(records[0],1);                                  
               }else{
               	    Ext.Msg.alert('Error', 'Más de un parámetro seleccionado');
               }
		    }	  

	},

		ActualizaGrillaParamc: function(){
			
		    var record= VisViewport.down('gridpanel[name=gridparamc]').getSelectionModel().getSelection();
		    var atri=record[0].get('atributo');
		    var descripcion=record[0].get('descripcion');
		   
		    	  var me=this;
                  $.ajax({
			        type: "POST",
			        url : 'app/data/php/CrudParametros.php',
			        data:{
			            action:'actualizarpar',
			            id:record[0].get('idparametro'),
			            descripcion:descripcion,
			            atributo:atri,
			            estado:'A'
			        },
			        async:false,
			        success: function(response){	
			         		          
			          /* VisViewport.down('gridpanel[name=gridparamm]').store.removeAll(); 
			           VisViewport.down('gridpanel[name=gridparamc]').store.removeAll(); */
			           VisViewport.down('gridpanel[name=gridparamc]').store.load();
			           Ext.Msg.alert('Operación Exitosa','Parámetro Modificado Satisfactoriamente');
			          
			        },
			        failure: function(){
			            alert('el servidor no ha respondido la consulta');
			        }
               });      	    
	},

	   buscarParametroEncabezado:function(campo,opcion){
		    var sw = false;
		    var storeEnc=VisViewport.down('gridpanel[name=gridparamc]').store;
		    Ext.each(storeEnc, function(recordenc) {
		        for(var i=0;i<recordenc.data.length;i++){
		        	if(opcion==1){
		        	   if(recordenc.data.items[i].data.atributo == campo){
		                  sw = true;
		                  return true;
		               }
		        	}else{
		        		if(recordenc.data.items[i].data.descripcion == campo){
		                    sw = true;
		                    return true;
		                }
		        	}
		            
		        }
		        return false;
		    });
		    return sw;
    },


    BorrarParametro: function(){
           
	 	    var delrecord= VisViewport.down('gridpanel[name=gridparamc]').getSelectionModel().getSelection();
	 	    var storeParamc= VisViewport.down('gridpanel[name=gridparamc]').store;
	         if(delrecord.length === 0){
				Ext.Msg.alert('Información', 'Por favor seleccione un elemento de la grilla');
			}else{
                
				Ext.Msg.show({
		                title : 'Confirmación',
		                msg : 'Está seguro de borrar el(los) parámetro(s) seleccionado(s)?',
		                buttons : Ext.Msg.YESNO,
		                icon : Ext.MessageBox.WARNING,
		                scope : this,
		                width : 450,
		                fn : function(btn, ev){
		                    if (btn == 'yes') {
		                        storeParamc.remove(delrecord);  
		                        var idparametro=delrecord[0].get('idparametro');
                               
		                        if(idparametro!="") {
		                       	     
                                      $.ajax({
								        type: "POST",
								        url : 'app/data/php/CrudParametros.php',
								        data:{
								            action:'eliminarpar',
								            id:idparametro
								        },
								        async:false,
								        success: function(response){	
								           Ext.Msg.alert('Operación Exitosa','Parámetro Removido Satisfactoriamente');
				                           VisViewport.down('gridpanel[name=gridparamm]').store.removeAll();
								             
								           /*var respSyncrono = Ext.decode(response); 
								           console.log(respSyncrono.razon);*/
								        },
								        failure: function(){
								            alert('el servidor no ha respondido la consulta');
								        }
					               });

	                             
		                       }
                            
		                    }
		                } 
	            });
		    }
   },

	AdicionarValorParametro: function(){
		  
		    var valor=vistaParamm.down('textfield[name=txtValor]').getValue();
		    var secuencia=vistaParamm.down('numberfield[name=txtSecuencia]').getValue();
		 
		    var selparam= VisViewport.down('gridpanel[name=gridparamc]').getSelectionModel().getSelection();
		    if(valor==""){
                  Ext.Msg.alert('Advertencia', 'Ingrese valor del parametro');
		    }else if(secuencia==""){
                  Ext.Msg.alert('Advertencia', 'Seleccione un orden');
		    }else if(this.buscarValorDetalle(valor)){
		              Ext.Msg.alert('Advertencia', 'Valor ya ha sido agregado');
		    }else{
                 var me=this;
                 $.ajax({
			        type: "POST",
			        url : 'app/data/php/CrudParametros.php',
			        data:{
			            action:'insertarvp',
			            id:selparam[0].get('idparametro'),
			            valor:valor,
			            orden:secuencia
			        },
			        async:false,
			        success: function(response){	
			           Ext.Msg.alert('Operación Exitosa','Valor Agregado Satisfactoriamente');
			           me.CerrarVentanaParamm();  
			           VisViewport.down('gridpanel[name=gridparamm]').store.removeAll();
					   VisViewport.down('gridpanel[name=gridparamm]').store.proxy.extraParams.idparametro=selparam[0].get('idparametro');
					   VisViewport.down('gridpanel[name=gridparamm]').store.load();
			           /*var respSyncrono = Ext.decode(response); 
			           console.log(respSyncrono.razon);*/
			        },
			        failure: function(){
			            alert('el servidor no ha respondido la consulta');
			        }
               });

                                  
		    }
	},
		 ActualizarValorParametro: function(){
		     var records= VisViewport.down('gridpanel[name=gridparamm]').getSelectionModel().getSelection();
	         if(records.length === 0){
				Ext.Msg.alert('Información', 'Por favor seleccione un elemento de la grilla');
			}else{
               if(records.length === 1){
               	      EditorDetalle[0].startEdit(records[0],0);		            
               }else{
               	   Ext.Msg.alert('Error', 'Más de un registro seleccionado');
               }
		    }	   
		  
	},

	ActualizaGrillaVP: function(){
            var record= VisViewport.down('gridpanel[name=gridparamm]').getSelectionModel().getSelection();
		    var valor=record[0].get('valor');
		    var secuencia=record[0].get('orden'); 
		   
                  var me=this;
                  $.ajax({
			        type: "POST",
			        url : 'app/data/php/CrudParametros.php',
			        data:{
			            action:'actualizarvp',
			            idvp:record[0].get('idvp'),
			            valor:valor,
			            orden:secuencia,
			            estadovp:'A'
			        },
			        async:false,
			        success: function(response){	
			         			            
			           VisViewport.down('gridpanel[name=gridparamm]').store.removeAll();
					   VisViewport.down('gridpanel[name=gridparamm]').store.proxy.extraParams.idparametro=record[0].get('idparam');
					   VisViewport.down('gridpanel[name=gridparamm]').store.load();
					   Ext.Msg.alert('Operación Exitosa','Valor Modificado Satisfactoriamente');
			           /*var respSyncrono = Ext.decode(response); 
			           console.log(respSyncrono.razon);*/
			        },
			        failure: function(){
			            alert('el servidor no ha respondido la consulta');
			        }
               });                              
		   
	},

	buscarValorDetalle:function(nombre){
		    var sw = false;
		    var storeDet=VisViewport.down('gridpanel[name=gridparamm]').store;
		    Ext.each(storeDet, function(recorddet) {
		        for(var i=0;i<recorddet.data.length;i++){
		            if(recorddet.data.items[i].data.valor == nombre){
		                sw = true;
		                return true;
		            }
		        }
		        return false;
		    });
		    return sw;
    },

    BorrarVP: function(){
           
	 	    var delrecord= VisViewport.down('gridpanel[name=gridparamm]').getSelectionModel().getSelection();
	 	    var storeParamm= VisViewport.down('gridpanel[name=gridparamm]').store;
	         if(delrecord.length === 0){
				Ext.Msg.alert('Información', 'Por favor seleccione un elemento de la grilla');
			}else{
				Ext.Msg.show({
		                title : 'Confirmación',
		                msg : 'Está seguro de borrar el(los) registro(s) seleccionado(s)?',
		                buttons : Ext.Msg.YESNO,
		                icon : Ext.MessageBox.WARNING,
		                scope : this,
		                width : 450,
		                fn : function(btn, ev){
		                    if (btn == 'yes') {
		                    	var idvp=delrecord[0].get('idvp');
		                        storeParamm.remove(delrecord);  
                                                               
		                       if(idvp!="") {
		                       	      $.ajax({
								        type: "POST",
								        url : 'app/data/php/CrudParametros.php',
								        data:{
								            action:'eliminarvp',
								            idvp:idvp
								        },
								        async:false,
								        success: function(response){	
								           Ext.Msg.alert('Operación Exitosa','Valor Removido Satisfactoriamente');
				                       								             
								           /*var respSyncrono = Ext.decode(response); 
								           console.log(respSyncrono.razon);*/
								        },
								        failure: function(){
								            alert('el servidor no ha respondido la consulta');
								        }
					               });
		                       }
                            
		                    }
		                }
	            });
		    }
		 },

  MuestraValorParametro: function(el,record, item, index,  e, Opts){
	
    	    var idparametro=record.get('idparametro');  
		    VisViewport.down('gridpanel[name=gridparamm]').store.removeAll();
		    VisViewport.down('gridpanel[name=gridparamm]').store.proxy.extraParams.idparametro=idparametro;
		    VisViewport.down('gridpanel[name=gridparamm]').store.load();
		   // VisViewport.down('gridpanel[name=gridparamm]').expand(true);
		   // VisViewport.down('toolbar[name=tbGridParamm]').enable(true);
			
	},	 

  
	CerrarVentanaParamc: function(el,e){
		    vistaParamc.close();
        
	},
	CerrarVentanaParamm: function(el,e){
		    vistaParamm.close();     
	}


});