Ext.define('CrudParam.view.VisParamm', {
    extend: 'Ext.window.Window',
    alias:'widget.visparamm',
    autoShow:true,
    closable:true,
    autoHeight:true,
    
    width: 500,
    layout:{ type:'hbox'},
    title: '',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
              {
                xtype:'container',
                padding:10,
                items:[
                  {
                        xtype: 'textfield',
                        width: 460,
                        //padding:10,
                        name:'txtValor', 
                        allowkBlank:false,
                        fieldLabel: 'Valor',
                        labelWidth:100,
                        blankText:'Este campo es requerido'
                  },
                  {
                        xtype: 'numberfield',
                        width: 460,
                        value:1,
                        //padding:10,
                        allowkBlank:false,
                        name:'txtSecuencia',
                        fieldLabel: 'Secuencia',
                        minValue: 1,
                        labelWidth:100,
                        blankText:'Este campo es requerido'
                  }/*,
             
                {
                    xtype: 'combobox',
                    width: 325,
                    padding:10,
                    editable:false,
                    name:'cboestado',
                    store: 'StoEstados',
                    displayField: 'descripcion',
                    valueField: 'valor',
                    fieldLabel: 'Estado',
                    labelWidth:100
                } */                
                ]
             }
            ],
            buttonAlign: 'center',
            buttons:[  
               {
                    xtype: 'button',
                    text: 'Aceptar'
                },
                {
                    xtype: 'button',                   
                    text: 'Cancelar'
                }  
            ]
        });

        me.callParent(arguments);
    }

});