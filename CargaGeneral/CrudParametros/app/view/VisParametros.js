Ext.define('CrudParam.view.VisParametros', {
     extend: 'Ext.container.Viewport',

     closable:true,
    alias:  'widget.visparametros',
    height: 483,
    width: 661,
    layout: {
        type: 'border'
    },
    xtype: 'dirparam',
    title: 'Parametros del Sistema',

    initComponent: function() {
        var me = this;
        EditorDetalle=[Ext.create('Ext.grid.plugin.RowEditing', {
                                            clicksToEdit: 2,
                                            saveBtnText  : 'Actualizar',
                                            cancelBtnText: 'Cancelar',  
                                            name:'rowEditingEnc'
        }),Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })];
         EditorEncabezado=[Ext.create('Ext.grid.plugin.RowEditing', {
                                            clicksToEdit: 2,
                                            saveBtnText  : 'Actualizar',
                                            cancelBtnText: 'Cancelar',  
                                            name:'rowEditingEnc'
       }),  Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })];

        Ext.applyIf(me, {
            items: [
              
                {
                    xtype: 'container',
                     layout: {
                        type: 'border'
                    },
                    region: 'center',
                    items: [
                        {
                            xtype: 'gridpanel',
                            store: 'StoParamc',
                            name:'gridparamc',
                            title:'Lista de parámetros',
                            region: 'center',
                            autoScroll: true,
                            flex: 6,
                            autoHeight: true,
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'idparametro',
									hideable: false,
                                    text: 'IdParametro',
                                    flex:1
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'atributo',
									hideable: false,
                                    text: 'Atributo',

                                    flex:1,
                                     editor: {
                                        xtype: 'textfield',
                                        allowBlank: false
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'descripcion',
									hideable: false,
                                    text: 'Descripción',
                                    flex:4,
                                     editor: {
                                        xtype: 'textfield',
                                        allowBlank: false
                                    }
                                },
                          
                                 {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'estado',
									hideable: false,
                                    text: 'Estado',
                                    flex:1,
                                    renderer: function(value) {
                                         if(value=='A'){
                                            return 'Activo'
                                         }else{
                                            return 'Inactivo'
                                         }
                                     }
                                }                              
                            ],
                            viewConfig: {
                                stripeRows: true,
                                enableTextSelection: true
                            },
                             plugins: EditorEncabezado,
                          /*  plugins:[
                                Ext.create('Core.ux.FilterRow',{
                                    remoteFilter:true
                                })
                            ],*/
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    dock: 'top',
                                    items: [
                                        {
                                            xtype: 'button',
                                            text: 'Adicionar',
                                            iconCls: 'add',
                                            action:'addparamc'
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Modificar',
                                            iconCls: 'edit',
                                            action:'editparamc'
                                        },
                                       
                                        {
                                            xtype: 'button',
                                            text: 'Eliminar',
                                            iconCls: 'delete',
                                            action: 'delparamc'
                                        },
                                         {
                                            xtype: 'button',
                                            text: 'Actualizar',
                                            iconCls: 'refresh',
                                            action: 'actparamc'
                                         }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'gridpanel',
                            store: 'StoParamm',
                            collapsible:true,
                           //resizable:true,
                           // collapsed:true,
                            title:'Lista de valores',
                            autoHeight: true,
                            region: 'south', 
                            name:'gridparamm',
                            autoScroll:true,
                            flex: 4,
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'idvp',
                                    text: 'IdVP',
								    hideable: false
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'idparam',
                                    text: 'IdParametro',
								    hideable: false,
                                    hidden:true
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'valor',
								    hideable: false,
                                    text: 'Valor',
                                    flex:5,
                                    editor: {
                                        xtype: 'textfield',
                                        allowBlank: false,
                                        blankText:'Este campo es requerido'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'orden',
									hideable: false,
                                    text: 'Orden',
                                    flex:1,
                                    editor: {
                                        xtype: 'numberfield',
                                        allowBlank: false,
                                        maxValue: 100000,
                                        minValue: 1,
                                        blankText:'Este campo es requerido'
                                    }
                                },
                                 {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'estadovp',
									hideable: false,
                                    flex: 1,
                                    text: 'Estado',
                                    renderer: function(value) {
                                         if(value=='A'){
                                            return 'Activo'
                                         }else{
                                            return 'Inactivo'
                                         }
                                   }
                                }
                            ],
                            plugins:EditorDetalle,
                            viewConfig: {

                            },
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                   // disabled:true,
                                    name:'tbGridParamm',
                                    dock: 'top',
                                    items: [
                                        {
                                            xtype: 'button',
                                            text: 'Adicionar',
                                            iconCls: 'add',
                                            action:'addparamm'
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Modificar',
                                            iconCls: 'edit',
                                            action:'editparamm'
                                        },
                                        
                                        {
                                            xtype: 'button',
                                            text: 'Eliminar',
                                            iconCls: 'delete',
                                            action:'delparamm'                        
                                        },
                                         {
                                            xtype: 'button',
                                            text: 'Actualizar',
                                            iconCls: 'refresh',
                                            action: 'actparamm'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});