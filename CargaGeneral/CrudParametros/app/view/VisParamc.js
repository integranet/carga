Ext.define('CrudParam.view.VisParamc', {
    extend: 'Ext.window.Window',
    alias:'widget.visparamc',
    autoShow:true,
    closable:true,
   // height: 190,
   autoHeight:true,
   // padding: 15,
    width: 500,
   layout:{ type:'hbox'},
    title: '',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [

            {
                xtype:'container',
                padding:10,
                items:[
                {
                    xtype: 'textfield',
                    width: 460,
                   // padding:10,
                    name:'txtAtributo', 
                    allowkBlank:false,
                   // flex:1,
                    fieldLabel: 'Atributo',
                    //labelWidth:100
                },
                {
                    xtype: 'textareafield',
                   width: 460,
                    //padding:10,
                    maxLength:300,
                    name:'txtDescripcion',
                    allowkBlank:false,
                   // flex:1,
                    fieldLabel: 'Descripcion',
                   // labelWidth:100
                }
                ]
            }
               
            ],
            buttonAlign: 'center',
            buttons:[  
               {
                    xtype: 'button',
                    text: 'Aceptar'
                },
                {
                    xtype: 'button',                   
                    text: 'Cancelar'
                }  
            ]
        });

        me.callParent(arguments);
    }

});