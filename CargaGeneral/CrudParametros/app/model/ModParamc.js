Ext.define('CrudParam.model.ModParamc',{
	extend		: 'Ext.data.Model',
	fields		: ['idparametro','atributo', 'descripcion','estado'],
	idProperty	: 'id'
});