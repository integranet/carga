Ext.define('sicarga.model.DetalleAsignacion',{
    extend: 'Ext.data.Model',
    fields: [
                'id_dtlle_asgncion',
                'id_asgncion',
                'tpo_rcrso',
                'id_rcrso',
                'estdo',
                'tpo_eqpo',
                'cpcdad',
                'nbre_usrio'
    ]
});