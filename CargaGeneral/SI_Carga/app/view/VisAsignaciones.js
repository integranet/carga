Ext.define('sicarga.view.VisAsignaciones', {
  extend: 'Ext.tab.Panel',
    iconCls:'report',
    title:'Asignaciones',
    alias:'widget.visasignaciones',
    tipo: '',

    initComponent: function() {
        var me = this;
        Ext.applyIf(me, {
            items:[
            {
                xtype:'visgridasignacion',
                titulo: 'Vehiculos',
                storeDetalle: Ext.create('sicarga.store.stoDetalleAsignacionVehiculo'),
                mystore: 'stoAsignacion'+me.tipo,
                nombre: 'grAsignacionVehiculo'+me.tipo,
                action:'grAsignacionVehiculo'+me.tipo,
                iconCls:'vehicle',
                collapsible: true,
                title:'Vehiculos',
                tipoasignacion: 'Vehiculo',
                listeners: {
                    activate: function(tab, eOpts) {
                        me.tabActivo(me.tipo,'Vehiculo');
                    }
                }
            },
            {
                xtype:'visgridasignacion',
                titulo: 'Buques',
                storeDetalle: Ext.create('sicarga.store.stoDetalleAsignacionBuque'),
                mystore: 'stoAsignacionBuque'+me.tipo,
                action:'grAsignacionBuque'+me.tipo,
                nombre: 'grAsignacionBuque'+me.tipo,
                iconCls:'buque',
                collapsible: true,
                title:'Buques',
                tipoasignacion: 'Buque',
                listeners: {
                    activate: function(tab, eOpts) {
                        //entorno['TabActivo'] = 'grAsignacionBuque'+me.tipo;
                        me.tabActivo(me.tipo,'Buque');
                    }
                }
            },
            {
                xtype:'visgridasignacion',
                titulo: 'Actividades',
                storeDetalle: Ext.create('sicarga.store.stoDetalleAsignacionActividad'),
                mystore: 'stoAsignacionActividad'+me.tipo,
                action:'grAsignacionActividad'+me.tipo,
                nombre: 'grAsignacionActividad'+me.tipo,
                iconCls:'actividades',
                collapsible: true,
                title:'Actividades',
                tipoasignacion: 'Actividad',
                listeners: {
                    activate: function(tab, eOpts) {
                        //entorno['TabActivo'] = 'grAsignacionActividad'+me.tipo;
                        me.tabActivo(me.tipo,'Actividad');
                    }
                }
            }
            ]
        });
        me.callParent(arguments);
    },

    tabActivo: function(tipo,tipoGrilla){
        if(entorno.TabGeneral=='General'){
            entorno['TabActivoGeneral'] = 'grAsignacion'+tipoGrilla+tipo;
        }else if(entorno.TabGeneral=='Granel'){
            entorno['TabActivoGranel'] = 'grAsignacion'+tipoGrilla+tipo;
        }
    }

});