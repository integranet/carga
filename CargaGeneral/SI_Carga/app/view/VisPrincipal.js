Ext.define('sicarga.view.VisPrincipal', {
  extend: 'Ext.container.Viewport',
    autoRender:true,
    alias:'widget.visprincipal',
    layout: {
        type: 'border'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
               
                {
                            xtype: 'tabpanel',
                            region:'center',
                            activeTab: 0,
                            name:'tpanel'
                           
                }
               
            ]
        });

        me.callParent(arguments);
    }

});