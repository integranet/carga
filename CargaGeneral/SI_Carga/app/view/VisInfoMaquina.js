Ext.define('sicarga.view.VisInfoMaquina', {
  extend: 'Ext.window.Window',
   iconCls:'machine',
   width:340,
   height:330,
   autoShow:true,
   modal:true,
   asignado: false,
    title:'Información de la maquina',
    alias:'widget.visinfomaquina',
    layout: {
        type: 'border'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [

                {
                    xtype:'form',
                    region:'center',
                    padding:5,
                    layout:{
                        type:'hbox'
                    },
                    items:[
                        {
                            xtype:'container',
                            flex:1,
                            padding:5,
                            items:[
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Equipo',
                                    readOnly:true,
                                    name:'cdgo_eqpo'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Cod.Operador',
                                    readOnly:true,
                                    name:'cdgo_oprdor_prtrio'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Nombre Opera',
                                    readOnly:true,
                                    name:'nmbre_clnte'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Operario',
                                    readOnly:true,
                                    name:'nmbre_oprrio'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Tipo Equipo',
                                    readOnly:true,
                                    name:'nmbre_tpo_eqpo'
                                },
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Capacidad',
                                    readOnly:true,
                                    name:'cpcdad'
                                },
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Codigo Asignacion',
                                    readOnly:true,
                                    name:'cdgo_asgncion',
                                    hidden: !me.asignado
                                },
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Hora Inicial Asg.',
                                    readOnly:true,
                                    name:'hr_in',
                                    hidden: !me.asignado
                                },
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Hora Final Asg.',
                                    readOnly:true,
                                    name:'hr_fin',
                                    hidden: !me.asignado
                                },
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Estado',
                                    readOnly:true,
                                    name:'estdo'
                                }
                                /*{
                                    xtype:'textfield',
                                    fieldLabel: 'Marca',
                                    readOnly:true,
                                    name:'mrca'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Color',
                                    readOnly:true,
                                    name:'clor'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Chasis',
                                    readOnly:true,
                                    name:'chsis'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Motor',
                                    readOnly:true,
                                    name:'mtor'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Serie',
                                    readOnly:true,
                                    name:'srie'
                                }*/
                            ]
                             
                        }/*,
                         {
                            xtype:'container',
                            flex:1,
                            padding:5,
                            items:[

                            {
                                    xtype:'textfield',
                                    fieldLabel: 'Modelo',
                                    readOnly:true,
                                    name:'mdlo'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Capacidad',
                                    readOnly:true,
                                    name:'cpcdad'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Seguro',
                                    readOnly:true,
                                    name:'sgro'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Fecha vencim',
                                    readOnly:true,
                                    name:'fcha_vncmnto'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Placa',
                                    readOnly:true,
                                    name:'plca'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Ultima Insp',
                                    readOnly:true,
                                    name:'fcha_ultma_insp'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Empresa Insp.',
                                    readOnly:true,
                                    name:'emprsa_inspccion'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Nom. inspector',
                                    readOnly:true,
                                    name:'nmbre_inspctor'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'En puerto',
                                    readOnly:true,
                                    name:'en_el_prto'
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Estado',
                                    readOnly:true,
                                    name:'estdo'
                                }]
                        }*/
                    ]
                }
               
            ]
        });

        me.callParent(arguments);
    }

});