/*Asignacion de vehiculos*/
Ext.define('sicarga.view.VisGridAsignacion', {
    extend: 'Ext.panel.Panel',
    alias:'widget.visgridasignacion',
    titulo: '',
    mystore:'',
    storeDetalle: '',
    nombre: '',
    entidad:'',//la entidad es el titulo de la primera columna
    tipoasignacion: '',
    layout:{
        type:'border'
    },
    initComponent: function() {
        var me = this;
        
        Ext.applyIf(me, {
            items:[
            {
                xtype:'gridpanel',
                name:me.nombre,
                //selType:'checkboxmodel',
                action:me.action,
                store:me.mystore,
                region:'center',
                flex: 0.7,
                plugins: [Ext.create('Core.ux.FilterRow',{remoteFilter:true})],
                columns: [
                /*{
                    xtype: 'gridcolumn',
                    dataIndex: 'id_asgncion',
                    text: '# Asignacion',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:1
                },*/
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'cdgo_asgncion',
                    text: 'Codigo Asignacion',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:2
                },
                /*{
                    xtype: 'gridcolumn',
                    dataIndex: 'tpo_asgncion',
                    text: 'Tipo Asignacion',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:2
                },*/
                {
                    xtype: 'gridcolumn',
                    /*xtype: 'datecolumn',*/
                    /*format: 'd/m/Y',*/
                    dataIndex: 'fcha_asgncion',
                    text: 'Fecha Asignacion',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:2
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'hr_incio',
                    text: 'Hora Inicial',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:2
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'hr_fn',
                    text: 'Hora Final',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:2
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'cdgo_zna',
                    text: 'Zona',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:2
                }
                ],
                viewConfig: {
                    getRowClass: function(record) {
                        if(record.data.clor == '59'){
                            return '';
                        }else if(record.data.clor == '60'){
                            return 'colorasignacionV';
                        }else if(record.data.clor == 'AM'){
                            return 'colorasignacionA';
                        }else if(record.data.clor == 'RJ'){
                            return 'colorasignacionR';
                        }else if(record.data.clor == '92'){
                            return 'colorasignacionS';
                        }
                    }
                },
                 bbar: [Ext.create('Ext.PagingToolbar', {
                    store: me.mystore,
                    displayInfo: true,
                    displayMsg: me.titulo+' {0} - {1} of {2}',
                    emptyMsg: "No hay "+me.titulo+" disponibles"
                }),
                 {
                    xtype: 'button',
                    text: 'Asignar Horas',
                    name: 'btnAsignarHorario',
                    iconCls: 'add'
                }]
            },
            {
                xtype: 'gridpanel',
                name: me.nombre+'Detalle',
                action: me.nombre+'Detalle',
                region: 'south',
                store: me.storeDetalle,
                flex: 0.3,
                columns: [
                            /*{
                                xtype: 'gridcolumn',
                                dataIndex: 'id_dtlle_asgncion',
                                text: 'Det Asignacion',
                                sortable:false,
                                menuDisabled:true,
                                minWidth:70,
                                flex:0.3
                            },*/
                            {
                                xtype: 'gridcolumn',
                                dataIndex: 'id_rcrso',
                                text: 'Recurso',
                                sortable:false,
                                menuDisabled:true,
                                minWidth:70,
                                flex:1
                            },
                            {
                                xtype: 'gridcolumn',
                                dataIndex: 'estdo',
                                text: 'Estado',
                                sortable:false,
                                menuDisabled:true,
                                minWidth:70,
                                flex:1  
                            },
                            {
                                xtype: 'gridcolumn',
                                dataIndex: 'nbre_usrio',
                                text: 'Nombre',
                                sortable:false,
                                menuDisabled:true,
                                minWidth:70,
                                flex:1  
                            },
                            {
                                xtype: 'gridcolumn',
                                dataIndex: 'tpo_eqpo',
                                text: 'Tipo Equipo',
                                sortable:false,
                                menuDisabled:true,
                                minWidth:70,
                                flex:1  
                            },
                            {
                                xtype: 'gridcolumn',
                                dataIndex: 'cpcdad',
                                text: 'Capacidad',
                                sortable:false,
                                menuDisabled:true,
                                minWidth:70,
                                flex:1  
                            }
                        ],
                bbar: [Ext.create('Ext.PagingToolbar', {
                     store: me.storeDetalle,
                     displayInfo: true,
                     displayMsg: 'Detalle de Asignacion de'+me.titulo+' {0} - {1} of {2}',
                     emptyMsg: "No hay Detalle de Asignacion de "+me.titulo+" disponibles"
                     
                 }),{
                    xtype: 'button',
                    text: 'Desasignar',
                    name: 'btnDesasignar',
                    iconCls: 'delete'
                }],
                viewConfig: {
                    
                }
            }
            ]
           
        });

        if(me.tipoasignacion == 'Vehiculo'){
            me.items[0].columns.unshift(
                {
                    xfilter: {disabled: true},
                    xtype:'actioncolumn',
                    width:25,
                    name:'btnDetalleAsigancion',
                    items: [{
                        icon: 'resources/images/buscar.png',
                        tooltip: 'Detalle de la Asignación',
                        handler: function(grid, rowIndex, colIndex) {
                            var rec = grid.getStore().getAt(rowIndex);
                            entorno.Controladores.ConPrincipal.masInformacionAsignacion(rec);
                        }
                }]});
            me.items[0].columns.push(
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'plca_vhclo',
                    text: 'Placa',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:2
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'contprod',
                    text: 'Contenido',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:4
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'oprcion',
                    text: 'Operacion',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:2
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'cdgo_tpo_mvmnto',
                    text: 'Tipo Movimiento',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:2
                });
        }else if(me.tipoasignacion == 'Buque'){
            me.items[0].columns.push(
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'mtrcla_mtnve',
                    text: 'Matricula',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:2
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'nmbre_mtnve',
                    text: 'Nombre',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:4
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'nmro_rclda',
                    text: 'Recalada',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:2
                });
        }else if(me.tipoasignacion == 'Actividad'){
            me.items[0].columns.push(
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'cnsctvo_mvmnto',
                    text: 'Matricula Carga',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:2
                },
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'nmbre_clnte',
                    text: 'Nombre Cliente',
                    sortable:false,
                    menuDisabled:true,
                    minWidth:70,
                    flex:4
                });
        }

        me.callParent(arguments);
    }

});