Ext.define('sicarga.view.VisInfoAsgVehiculo', {
  extend: 'Ext.window.Window',
   iconCls:'machine',
   width:360,
   height:380,
   autoShow:true,
   modal:true,
    title:'Detalle Autorización',
    alias:'widget.visinfoasgvehiculo',
    layout: {
        type: 'border'
    },
    data: {},

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [

                {
                    xtype:'form',
                    region:'center',
                    padding:5,
                    layout:{
                        type:'hbox'
                    },
                    items:[
                        {
                            xtype:'container',
                            flex:1,
                            padding:5,
                            items:[
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Numero Autorización',
                                    readOnly:true,
                                    name:'nmro_autrzcion',
                                    value: me.data.nmro_autrzcion
                                },
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Numero Solicitud',
                                    readOnly:true,
                                    name:'nmro_slctud',
                                    value: me.data.nmro_slctud
                                },
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Zona',
                                    readOnly:true,
                                    name:'cdgo_zna',
                                    value: me.data.cdgo_zna
                                },
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Matricula Motonave',
                                    readOnly:true,
                                    name:'mtrcla_mtnve',
                                    value: me.data.mtrcla_mtnve
                                },{
                                    xtype:'textfield',
                                    fieldLabel: 'Nombre Motonave',
                                    readOnly:true,
                                    name:'nmbre_mtnve',
                                    value: me.data.nmbre_mtnve
                                },
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Matricula Carga',
                                    readOnly:true,
                                    name: 'mtrcla_crga',
                                    value: me.data.mtrcla_crga
                                },
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Propietario',
                                    readOnly:true,
                                    name: 'nmbre_clnte',
                                    value: me.data.nmbre_clnte
                                },      
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Peso',
                                    readOnly:true,
                                    name:'pso',
                                    value: me.data.pso
                                },
                                {
                                    xtype:'textfield',
                                    fieldLabel: 'Cantidad',
                                    readOnly:true,
                                    name:'cntdad',
                                    value: me.data.cntdad
                                }
                            ]
                             
                        }
                    ]
                }
               
            ]
        });

        me.callParent(arguments);
    }

});