Ext.define('sicarga.view.VisCola', {
  extend: 'Ext.panel.Panel',
    iconCls:'report',
    titulo:'',
    tipo: '',
    alias:'widget.viscola',
    layout: {
        type: 'border'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            title: me.titulo,
            items:[
                    {
                xtype:'container',
                region:'center',
                layout:{
                    type:'border'
                },
                items: [
                
               {
                    xtype:'visgridrecurso',
                    mystore:'stoEquipoDisponible',
                    region:'center',
                    flex:1,
                    padding:5,
                    action:'gpGeneral',
                    name: 'gpGeneral',
                    titulo:'Equipos',
                    tieneactioncolumn: true,
                    datasIndex: ['cdgo_eqpo','nmbre_tpo_eqpo'],
                    textos: ['Codigo','Tipo']
                },
               {
                    xtype:'visgridrecurso',
                    mystore:'stoAuxiliarDisponible',
                    region:'south',
                    flex:1,
                    padding:5,
                    action:'gpAuxiliar',
                    name: 'gpAuxiliar',
                    titulo:'Personas',
                    datasIndex: ['username','nmbre_usrio'],
                    textos: ['Username','Nombre']
                }
               
            ]
            },
            {
                xtype: 'visasignaciones',
                flex:2.2,
                action:'lista',
                iconCls:'lista',
                region:'west',
                collapsible: true,
                title:'Lista De Asignaciones',
                tipo: me.tipo
            }

            ]
        });

        me.callParent(arguments);
    }

});