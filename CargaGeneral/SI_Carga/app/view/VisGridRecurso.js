Ext.define('sicarga.view.VisGridRecurso', {
    extend: 'Ext.panel.Panel',
    alias:'widget.visgridrecurso',
    titulo: '',
    nombre: '',
    mystore:'',
    entidad:'',
    action: '',
    asignado: false,
    tieneactioncolumn: false,
    tieneactionpersona: false,
    tienehorarios: false,
    datasIndex: [],
    textos: [],
    layout:{
        type:'border'
    },

    renderizado: function(value){
        if(value==false){
            return '<p></p>';
        }else{
            var color = entorno.Controladores.ConPrincipal.buscarParametro(56);
            return '<p style="background-color:'+color+';"> </p>';
        }
    },
    initComponent: function() {
        var me = this;
        Ext.applyIf(me, {
            title: me.titulo,
            items:[
                    {
                        xtype:'gridpanel',
                        action:me.action,
                        name: me.nombre,
                        store:me.mystore,
                        region:'center',
                        columnLines: true,
                        plugins: [Ext.create('Core.ux.FilterRow',{remoteFilter:true})],
                        columns: [
                    {
                        xtype: 'gridcolumn',
                        dataIndex: me.datasIndex[0],
                        text: me.textos[0],
                        sortable:false,
                        menuDisabled:true,
                        flex:2
                        //minWidth:100
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: me.datasIndex[1],
                        text: me.textos[1],
                        sortable:false,
                        menuDisabled:true,
                        //minWidth:100,
                        flex:2
                    }
                ],
                viewConfig: {

                },
                bbar: Ext.create('Ext.PagingToolbar', {
                    store: me.mystore,
                    displayInfo: true,
                    displayMsg: me.titulo+' {0} - {1} of {2}',
                    emptyMsg: "No hay "+me.titulo+" Asignadas"
                    
                })/*,
                dockedItems: [{
                        xtype: 'toolbar',
                        height: 30,
                        name: 'tbAnexo',
                        items: [
                        {
                            xtype: 'button',
                            cls: 'x-btn-text-icon',
                            iconCls: 'machine'
                        },
                        {
                            xtype:'label',
                            text:'  Maquinas',
                        }]
                    }]*/
            }
            ]
           
        });

        if(me.tieneactionpersona){
            me.items[0].columns.unshift({
                xfilter: {disabled: true},
                xtype:'actioncolumn',
                width:25,
                name:'btnInfomacion',
                items: [{
                    icon: 'resources/images/buscar.png',
                    tooltip: 'Informacion del Auxiliar',
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        entorno.Controladores.ConPrincipal.masInformacionAuxiliar(rec,me.asignado);
                    }
                }
                ]
            });
        }
    

        if(me.tieneactioncolumn){
            me.items[0].columns.unshift({
                xfilter: {disabled: true},
                xtype:'actioncolumn',
                width:25,
                name:'btnInfomacion',
                items: [{
                    icon: 'resources/images/buscar.png',
                    tooltip: 'Informacion de la maquina',
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        entorno.Controladores.ConPrincipal.masInformacionMaquina(rec,me.asignado);
                    }
                }
                ]
            });
            me.items[0].columns.push({
                        xtype: 'gridcolumn',
                        dataIndex: 'cpcdad',
                        text: 'Capacidad',
                        sortable:false,
                        menuDisabled:true,
                        //minWidth:100,
                        flex:2
                    });
        }
        if(me.tienehorarios){
            var ancho = 0.01;
            var filtro = { disabled: true };
            me.items[0].columns.push({
                        xtype: 'gridcolumn',
                        dataIndex: 'hora0',
                        text: '0',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        //width:35
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora1',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '1',
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                        //width:25
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora2',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '2',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora3',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '3',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora4',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '4',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora5',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '5',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora6',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '6',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora7',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '7',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora8',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '8',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora9',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '9',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora10',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '10',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora11',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '11',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora12',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '12',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora13',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '13',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora14',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '14',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora15',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '15',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora16',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '16',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora17',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '17',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora18',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '18',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora19',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '19',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora20',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '20',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora21',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '21',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora22',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '22',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    },{
                        xtype: 'gridcolumn',
                        dataIndex: 'hora23',
                        sortable:false,
                        menuDisabled:true,
                        resizable:false,
                        text: '23',
                        //width:25
                        flex: ancho,
                        xfilter: filtro,
                        renderer: function(value,metaData,record,rowIndex,colIndex,store){
                            return me.renderizado(value);
                        }
                    });
        }
        me.callParent(arguments);
    }

});