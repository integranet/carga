Ext.define('sicarga.view.VisFormAsignacion', {
  extend: 'Ext.window.Window',
   iconCls:'machine',
   y:100,
   width:565,
   height:135,
   autoShow:true,
   //modal:true,
    title:'Formulario de asignacion',
    alias:'widget.visformasignacion',
    layout: {
        type: 'border'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            modal: true,
            items: [

                {
                    xtype:'form',
                    region:'center',
                    padding:5,
                    layout:{
                        type:'hbox'
                    },
                    items:[
                        {
                            xtype:'container',
                            flex:1,
                            padding:5,
                            items:[
                                /*{
                                    xtype:'combobox',
                                    fieldLabel: 'Hora Inicio',
                                   // readOnly:true,
                                    name:'horaInicio',
                                    store: sicarga.Utilidades.obtenerStoreHoras(),
                                    valueField: 'hora',
                                    displayField: 'hora'
                                }*/
                                 {
                           xtype: 'timefield',
                           name: 'horaInicio',
                           format:'H:i',
                           allowBlank:false,
                           editable:false,
                           blankText:'Campo obligatorio',
                           fieldLabel: 'Hora Inicial',
                           minValue: '0:00:00',
                           maxValue: '23:59:00',
                           increment: 30
                           
                      }
                                ,{
                                    xtype:'combobox',
                                    fieldLabel: 'Zona',
                                    store: 'stoZona',
                                    valueField: 'cdgo_zna',
                                    displayField: 'nmbre_zna',
                                    //readOnly:true,
                                    name:'zona'
                                }
                            ]
                             
                        },
                         {
                            xtype:'container',
                            flex:1,
                            padding:5,
                            items:[

                                {
                                    /*xtype:'combobox',
                                    fieldLabel: 'Hora Final',
                                   //readOnly:true,
                                    name:'horaFinal',
                                    store: sicarga.Utilidades.obtenerStoreHoras(),
                                    valueField: 'hora',
                                    displayField: 'hora'*/
                                     xtype: 'timefield',
                           name: 'horaFinal',
                           format:'H:i',
                           allowBlank:false,
                           editable:false,
                           blankText:'Campo obligatorio',
                           fieldLabel: 'Hora Final',
                           minValue: '0:00:00',
                           maxValue: '23:59:00',
                           increment: 30
                                }/*,{
                                    xtype:'combobox',
                                    fieldLabel: 'Operario',
                                   // readOnly:true,
                                    name:'Operario'
                                }*/]
                        }
                    ]
                }
               
            ],
            dockedItems: [{
                        xtype: 'toolbar',
                        height: 30,
                        dock: 'bottom',
                        name: 'tbAnexo',
                        items: [
                        {
                            xtype: 'button',
                            text:'Aceptar',
                            name: 'btnAceptar',
                            //tooltip:'',
                            //action:'',
                            cls: 'x-btn-text-icon',
                            iconCls: 'icon-ok'
                        },{
                            xtype: 'button',
                            text:'Cancelar',
                            name: 'btnCancelar',
                            //tooltip:'',
                            //action:'',
                            cls: 'x-btn-text-icon',
                            iconCls: 'icon-cancelar'
                        }]
                    }]
        });

        me.callParent(arguments);
    }

});