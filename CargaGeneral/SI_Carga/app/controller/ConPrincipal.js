Ext.define('sicarga.controller.ConPrincipal',{
	 extend: 'Ext.app.Controller',
	 views:['VisPrincipal','VisGridAsignacion','VisCola','VisInfoMaquina',
	 'VisFormAsignacion','VisAsignaciones','VisGridRecurso','VisInfoAuxiliar','VisInfoAsgVehiculo'],
   	 stores: ['stoEquipoDisponible','stoAsignacion','stoEquipoDisponibleGranel','stoAuxiliarDisponibleGranel',
   	 'stoAuxiliarDisponible','stoAsignacionBuque','stoAsignacionActividad','stoDetalleAsignacionVehiculo',
   	 'stoDetalleAsignacionBuque','stoDetalleAsignacionActividad','stoEquipoAsignado','stoAuxiliarAsignado',
   	 'stoAsignacionBuqueGranel','stoAsignacionGranel','stoZona','stoAsignacionActividadGranel'],
	 init:function (){
	 		document.getElementById('Idbody').innerHTML="";
	 		
	 		//definimos el entorno:
	 		entorno={};
	 		//definimos los controladores:
	 		entorno['Controladores']={};
	 		entorno['Controladores']['ConPrincipal']=this;
	 		entorno['Controladores']['ConAsignacion']=this.getController('ConAsignacion');
	 		
	 		//Definimos las Vistas:
	 		//vista principal (Viewport):
	 		entorno['Vistas']={}
	 		entorno['Vistas']['VisPrincipal']=Ext.widget('visprincipal');
	 		

	 		entorno['TabGeneral'] = 'General';
			entorno['Vistas']['VisColaGeneral']=Ext.widget('viscola',{
				titulo: 'Cola de Tareas Carga General',
				tipo: '',
				listeners: {
                    activate: function(tab, eOpts) {
                    	entorno['TabGeneral'] = 'General';
                    }
                }
			});	           
			entorno['Vistas']['VisColaGranel']=Ext.widget('viscola',{
				titulo: 'Cola de Tareas Carga Granel',
				tipo: 'Granel',
				listeners: {
                    activate: function(tab, eOpts) {
                    	entorno['TabGeneral'] = 'Granel';
                    }
                }
			});

			entorno['Vistas']['VisHorarioAsignadoMaquinas'] = Ext.widget('visgridrecurso',{
				titulo: 'Equipos Asignados',
				nombre: 'grMaquinasAsignadas',
				action: 'grMaquinasAsignadas',
				mystore: 'stoEquipoAsignado',
				datasIndex: ['cdgo_eqpo','nmbre_tpo_eqpo'],
				textos: ['Codigo','Tipo'],
				tieneactioncolumn: true,
				tienehorarios: true,
				iconCls:'vehicle',
				asignado: true
			});
			entorno['Vistas']['VisHorarioAsignadoAuxiliares'] = Ext.widget('visgridrecurso',{
				titulo: 'Personas Asignadas',
				nombre: 'grAuxiliaresAsignadas',
				action: 'grAuxiliaresAsignadas',
				mystore: 'stoAuxiliarAsignado',
				datasIndex: ['username','nmbre_usrio'],
				textos: ['Username','Nombre'],
				tienehorarios: true,
				iconCls:'personas',
				asignado: true,
				tieneactionpersona: true
			});

			entorno.Vistas.VisPrincipal.down('tabpanel[name=tpanel]').add(entorno['Vistas']['VisColaGeneral']);          
			entorno.Vistas.VisPrincipal.down('tabpanel[name=tpanel]').setActiveTab(entorno['Vistas']['VisColaGeneral']); 
			entorno.Vistas.VisPrincipal.down('tabpanel[name=tpanel]').add(entorno['Vistas']['VisColaGranel']);
			entorno.Vistas.VisPrincipal.down('tabpanel[name=tpanel]').add(entorno['Vistas']['VisHorarioAsignadoMaquinas']);
			entorno.Vistas.VisPrincipal.down('tabpanel[name=tpanel]').add(entorno['Vistas']['VisHorarioAsignadoAuxiliares']);
			      
          	this.cargarParametros();
			sicarga.Utilidades.cargarVariables();
         	this.control({

	        })
  },

  

  masInformacionMaquina:function(record,asignado){
  	 var asignado = asignado || false;
  	 entorno.Vistas['VisInfoMaquina']  = Ext.widget('visinfomaquina',{
  	 	asignado: asignado
  	 });
  	 entorno.Vistas['VisInfoMaquina'].down('form').loadRecord(record);
  },

  masInformacionAuxiliar:function(record,asignado){
  	 var asignado = asignado || false;
  	 entorno.Vistas['VisInfoAuxiliar']  = Ext.widget('visinfoauxiliar',{
  	 	asignado: asignado
  	 });
  	 entorno.Vistas['VisInfoAuxiliar'].down('form').loadRecord(record);
  },

  cargarParametros:function(){
    	
    	Ext.Ajax.request({
					url: 'app/data/php/CargarParametros.php',
					
					success: function(response) {

						var resp = Ext.decode(response.responseText);
						entorno['parametros']=resp.data;
						var tiempoRefresh = entorno.Controladores.ConPrincipal.buscarParametro(62);
						var colorV = entorno.Controladores.ConPrincipal.buscarParametro(43);
						var colorA = entorno.Controladores.ConPrincipal.buscarParametro(44);
						var colorR = entorno.Controladores.ConPrincipal.buscarParametro(65);
						var colorS = entorno.Controladores.ConPrincipal.buscarParametro(93);
						sicarga.Utilidades.colorAsignacion(colorV,'colorasignacionV');
						sicarga.Utilidades.colorAsignacion(colorA,'colorasignacionA');
						sicarga.Utilidades.colorAsignacion(colorR,'colorasignacionR');
						sicarga.Utilidades.colorAsignacion(colorS,'colorasignacionS');
						sicarga.Utilidades.iniciarTaskManager(tiempoRefresh*1000);
					},
					failure: function() {
											
					}
		});
    },

    buscarParametro:function(value){
    	for (var i = 0; i < entorno.parametros.length; i++) {
    		if(entorno.parametros[i].IdValorParametro==value){
    			return entorno.parametros[i].Valor;
    		}
    	}
    	return value;
    },


    masInformacionAsignacion: function(record){
    	Ext.Ajax.request({
					url: 'app/data/php/selectDetalleAutorizacionAsgv.php',
					params: {
						codasignacion: record.data.cdgo_asgncion
					},
					method: 'POST',
					success: function(response) {
						var resp = Ext.decode(response.responseText);
						entorno.Vistas['VisInfoAsgVehiculo']  = Ext.widget('visinfoasgvehiculo',{
							data: resp.data[0]
						});
					},
					failure: function() {
						Ext.Msg.alert('Error','Error al realizar la operación');
					}
		});
    }
	       
});