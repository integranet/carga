Ext.define('sicarga.controller.ConAsignacion',{
	 extend: 'Ext.app.Controller',
	 init:function (){
         	this.control({
         		'viscola gridpanel[action=gpGeneral]':{
         			//itemdblclick: this.showVisAsignacion
              itemdblclick: this.AsignarMaquina
         		},

            'viscola gridpanel[name=grAsignacionVehiculo]':{
              itemdblclick: this.mostrarDetalleAsignacion
            },

            'viscola gridpanel[name=grAsignacionBuque]':{
              itemdblclick: this.mostrarDetalleAsignacion
            },

            'viscola gridpanel[name=grAsignacionActividad]':{
              itemdblclick: this.mostrarDetalleAsignacion
            },

            'viscola gridpanel[name=grAsignacionVehiculoGranel]':{
              itemdblclick: this.mostrarDetalleAsignacion
            },

            'viscola gridpanel[name=grAsignacionBuqueGranel]':{
              itemdblclick: this.mostrarDetalleAsignacion
            },

            'viscola gridpanel[name=grAsignacionActividadGranel]':{
              itemdblclick: this.mostrarDetalleAsignacion
            },

            'viscola button[name=btnDesasignar]':{
              click: this.desasignar
            },

            'viscola button[name=btnAsignarHorario]':{
              click: this.showVisAsignacion
            },

            'viscola gridpanel[action=gpAuxiliar]':{
              itemdblclick: this.AsignarAuxiliar
            },

            'visformasignacion button[name=btnAceptar]':{
              click: this.AsignarHorario
            },

            'visformasignacion button[name=btnCancelar]':{
              click: this.CancelarAsignacionHorario
            }

	        })
  	},

    desasignar: function(){
      var tabGeneral = entorno.TabGeneral;
      var tabActivo = this.tabActivo(tabGeneral);
      var grillaDetAsignacion = this.grillaAsignacionDetalle(tabGeneral,tabActivo);
      if(grillaDetAsignacion.getSelectionModel().selected.items.length == 0){
        Ext.Msg.alert('Informacion', 'Debe seleccionar el recurso');
      }else{
        var id_recurso = grillaDetAsignacion.getSelectionModel().selected.items[0].data.id_dtlle_asgncion;
        var id_asgncion = grillaDetAsignacion.getSelectionModel().selected.items[0].data.id_asgncion;
        Ext.Ajax.request({
        url: 'app/data/php/desasignarRecurso.php',
        params: {
          idrecurso: id_recurso,
          id_asgncion: id_asgncion
        },
        method: 'POST',
        success: function(response){
          grillaDetAsignacion.store.removeAll();
          grillaDetAsignacion.store.proxy.extraParams.id_asgncion = id_asgncion;
          grillaDetAsignacion.store.load();
        },
        failure: function(response){
          Ext.Msg.alert('Error','Error al realizar la operacion');
        }
      });
      }
    },

  	showVisAsignacion:function(){
      var tabGeneral = entorno.TabGeneral;
      var tabActivo = this.tabActivo(tabGeneral);
      var grillaAsignacion = this.grillaAsignacion(tabGeneral,tabActivo);
      var grillaDetAsignacion = this.grillaAsignacionDetalle(tabGeneral,tabActivo);
      if(grillaAsignacion.getSelectionModel().selected.items.length == 0){
        Ext.Msg.alert('Informacion', 'Debe seleccionar la asignacion');
      }else{
        var data = (grillaAsignacion.getSelectionModel().selected.items[0].data);
        if(data.cdgo_zna=="" && data.hr_incio=="" && data.hr_fn==""){
          entorno.Vistas['VisFormAsignacion']  = Ext.widget('visformasignacion');
          var fecha = new Date();
          var hora = fecha.getHours();
          entorno.Vistas.VisFormAsignacion.down('timefield[name=horaInicio]').setValue(hora+':00');
          entorno.Vistas.VisFormAsignacion.down('timefield[name=horaFinal]').setValue((hora+2)+':00');
        }else{
          entorno.Vistas['VisFormAsignacion']  = Ext.widget('visformasignacion');
          var fecha = new Date();
          var hora = fecha.getHours();
          entorno.Vistas.VisFormAsignacion.down('timefield[name=horaInicio]').setValue(data.hr_fn);
          entorno.Vistas.VisFormAsignacion.down('timefield[name=horaFinal]').setValue(data.hr_incio);
          entorno.Vistas.VisFormAsignacion.down('combobox[name=zona]').setValue(data.cdgo_zna);
        }
      }
  	},

    AsignarMaquina: function(el,record, item, index,  e, Opts){
      var tabGeneral = entorno.TabGeneral;
      var tabActivo = this.tabActivo(tabGeneral);
      var grillaAsignacion = this.grillaAsignacion(tabGeneral,tabActivo);
      var grillaDetAsignacion = this.grillaAsignacionDetalle(tabGeneral,tabActivo);
      if(grillaAsignacion.getSelectionModel().selected.items.length == 0){
        Ext.Msg.alert('Informacion', 'Debe seleccionar la asignacion');
      }else{
        var detAsignacion = Ext.create('sicarga.model.DetalleAsignacion',{
          id_asgncion: grillaAsignacion.getSelectionModel().selected.items[0].data.id_asgncion,
          tpo_rcrso: 41,
          id_rcrso: record.get('cdgo_eqpo'),
          estdo: 'A'
        });
        this.GuardarDetalleAsignacion(detAsignacion.data,grillaDetAsignacion);
      }
    },

    mostrarDetalleAsignacion: function(el,record, item, index,  e, Opts){
      var id_asgncion=record.get('id_asgncion');
      var tabGeneral = entorno.TabGeneral;
      var tabActivo = this.tabActivo(tabGeneral);
      var grilla = this.grillaAsignacionDetalle(tabGeneral,tabActivo);
      grilla.store.removeAll();
      grilla.store.proxy.extraParams.id_asgncion = id_asgncion;
      grilla.store.load();
    },

    AsignarAuxiliar: function(el,record, item, index,  e, Opts){
      var tabGeneral = entorno.TabGeneral;
      var tabActivo = this.tabActivo(tabGeneral);
      var grillaAsignacion = this.grillaAsignacion(tabGeneral,tabActivo);
      var grillaDetAsignacion = this.grillaAsignacionDetalle(tabGeneral,tabActivo);
      if(grillaAsignacion.getSelectionModel().selected.items.length == 0){
        Ext.Msg.alert('Informacion', 'Debe seleccionar la asignacion');
      }else{
        var detAsignacion = Ext.create('sicarga.model.DetalleAsignacion',{
          id_asgncion: grillaAsignacion.getSelectionModel().selected.items[0].data.id_asgncion,
          tpo_rcrso: 42,
          id_rcrso: record.get('username'),
          estdo: 'A'
        });
        this.GuardarDetalleAsignacion(detAsignacion.data,grillaDetAsignacion);
      }
    },


    GuardarDetalleAsignacion: function(data,grilla){
      Ext.Ajax.request({
        url: 'app/data/php/guardarDetalleAsignacion.php',
        params: {
          registro: Ext.encode(data)
        },
        method: 'POST',
        success: function(response){
          var resp = Ext.decode(response.responseText);
          if(resp.success){
            grilla.store.removeAll();
            grilla.store.proxy.extraParams.id_asgncion = data.id_asgncion;
            grilla.store.load();
          }else{
            Ext.Msg.alert('Informacion',resp.motivo);
          }
        },
        failure: function(response){
          grilla.store.removeAll();
          grilla.store.proxy.extraParams.id_asgncion = data.id_asgncion;
          grilla.store.load();
        }
      });
    },


    AsignarHorario: function(){
      var grillaMaquina = entorno.Vistas.VisColaGeneral.down('gridpanel[action=gpGeneral]');
      var tabGeneral = entorno.TabGeneral;
      var tabActivo = this.tabActivo(tabGeneral);
      var grillaAsignacion = this.grillaAsignacion(tabGeneral,tabActivo);

      if(grillaAsignacion.getSelectionModel().selected.items.length == 0){
        Ext.Msg.alert('Informacion','Debe seleccionar una asignacion');
      }else{
        var id_asignacion = grillaAsignacion.getSelectionModel().selected.items[0].data.id_asgncion;
        var horaInicial = entorno.Vistas.VisFormAsignacion.down('timefield[name=horaInicio]').getRawValue();
        var horaFinal = entorno.Vistas.VisFormAsignacion.down('timefield[name=horaFinal]').getRawValue();
        var zona = entorno.Vistas.VisFormAsignacion.down('combobox[name=zona]').getValue();

        var hr_in = entorno.Vistas.VisFormAsignacion.down('timefield[name=horaInicio]').getValue().getHours();
        var hr_fn = entorno.Vistas.VisFormAsignacion.down('timefield[name=horaFinal]').getValue().getHours();

        if(hr_fn <= hr_in){
          Ext.Msg.alert('Informacion','La hora final debe ser mayor a la inicial');
        }else if(zona==null || zona == ''){
          Ext.Msg.alert('Informacion','Debe Seleccionar una zona');
        }else{
          Ext.Ajax.request({
          url: 'app/data/php/asignarHorario.php',
          method: 'POST',
          params:{
            id_asgncion: id_asignacion,
            hr_inicio: horaInicial,
            hr_fin: horaFinal,
            zna: zona
          },
          success: function(response){
            var resp = Ext.decode(response.responseText);
            if(resp.success){
              Ext.Msg.alert('Informacion',resp.data);
            }else{
              Ext.Msg.alert('Informacion',resp.data);
            }
            entorno.Vistas.VisFormAsignacion.close();
          },
          failure: function(){
            Ext.Msg.alert('Error','Error al realizar la operacion');
            entorno.Vistas.VisFormAsignacion.close();
          }
        });
        }
      }
    },

    CancelarAsignacionHorario: function(){
      entorno.Vistas.VisFormAsignacion.close();
    },

    tabActivo: function(tipo){
      if(tipo=='General'){
        return entorno.TabActivoGeneral;
      }else if(tipo=='Granel'){
        return entorno.TabActivoGranel;
      }
    },
    grillaAsignacion: function(tipo,tabActivo){
      if(tipo=='General'){
        return entorno.Vistas.VisColaGeneral.down('gridpanel[name='+tabActivo+']');
      }else if(tipo=='Granel'){
        return entorno.Vistas.VisColaGranel.down('gridpanel[name='+tabActivo+']');
      }
    },
    grillaAsignacionDetalle: function(tipo,tabActivo){
      if(tipo=='General'){
        return entorno.Vistas.VisColaGeneral.down('gridpanel[name='+tabActivo+'Detalle]');
      }else if(tipo=='Granel'){
        return entorno.Vistas.VisColaGranel.down('gridpanel[name='+tabActivo+'Detalle]');
      }
    }
});