Ext.define('sicarga.store.stoDetalleAsignacionActividad',{
	extend: 'sicarga.store.storeDinamico',
    url: 'app/data/php/selectDetalleAsignacionActividad.php',
    model: 'sicarga.model.DetalleAsignacion',
    autoLoad: true,
    pageSize: 20,
    remoteSort: true,
    extraParams: {
    	id_asgncion: ''
    }
});