Ext.define('sicarga.store.stoAsignacionActividadGranel',{
	extend: 'sicarga.store.storeDinamico',
    url: 'app/data/php/selectAsignacionActividadGranel.php',
    model: 'sicarga.model.Asignacion', 
    /*autoLoad: {start: 0, limit: 20},*/
    autoLoad: false,
    pageSize: 20,
    remoteSort: true,
    extraParams: {
        condiciones: Ext.encode([
        "(cast(ASG.cdgo_asgncion as char(50)))",
        "to_char(ASG.fcha_asgncion,'%d/%m/%Y')",
        "ASG.hr_incio",
        "ASG.hr_fn",
        "ASG.cdgo_zna",
        "(cast(ASG.cnsctvo_mvmnto as char(50)))",
        "CLT.nmbre_clnte"])
    }
});