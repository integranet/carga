Ext.define('sicarga.store.stoDetalleAsignacionBuque',{
	extend: 'sicarga.store.storeDinamico',
    url: 'app/data/php/selectDetalleAsignacionBuque.php',
    model: 'sicarga.model.DetalleAsignacion',
    autoLoad: true,
    pageSize: 20,
    remoteSort: true,
    extraParams: {
    	id_asgncion: ''
    }
});