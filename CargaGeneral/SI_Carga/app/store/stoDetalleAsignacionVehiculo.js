Ext.define('sicarga.store.stoDetalleAsignacionVehiculo',{
	extend: 'sicarga.store.storeDinamico',
    url: 'app/data/php/selectDetalleAsignacionVehiculo.php',
    model: 'sicarga.model.DetalleAsignacion',
    autoLoad: true,
    pageSize: 20,
    remoteSort: true,
    extraParams: {
    	id_asgncion: ''
    }
});