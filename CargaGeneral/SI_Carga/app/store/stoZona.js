Ext.define('sicarga.store.stoZona',{
	extend: 'sicarga.store.storeDinamico',
    url: 'app/data/php/selectZona.php',
    model: 'sicarga.model.Zona', 
    /*autoLoad: {start: 0, limit: 20},*/
    autoLoad: true,
    pageSize: 20,
    remoteSort: true
});