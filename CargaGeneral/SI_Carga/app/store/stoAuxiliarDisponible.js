Ext.define('sicarga.store.stoAuxiliarDisponible',{
	extend: 'sicarga.store.storeDinamico',
    url: 'app/data/php/SelectAuxiliar.php',
    model: 'sicarga.model.AuxiliarDisponible', 
    autoLoad: {start: 0, limit: 15},
    pageSize: 15,
    remoteSort: true
});