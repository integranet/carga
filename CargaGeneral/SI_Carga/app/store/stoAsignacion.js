Ext.define('sicarga.store.stoAsignacion',{
	extend: 'sicarga.store.storeDinamico',
    url: 'app/data/php/selectAsignacion.php',
    model: 'sicarga.model.Asignacion', 
    /*autoLoad: {start: 0, limit: 20},*/
    autoLoad: false,
    pageSize: 20,
    remoteSort: true,
    extraParams: {
    	condiciones: Ext.encode([
    	"(cast(ASG.cdgo_asgncion as char(50)))",
    	"to_char(ASG.fcha_asgncion,'%d/%m/%Y')",
    	"ASG.hr_incio",
    	"ASG.hr_fn",
    	"ASG.cdgo_zna",
    	"CTR.plca_vhclo",
    	"obtner_cntndo(AUT.mtrcla_crga,AUT.cdgo_tpo_mvmnto,AUT.prfjo_cntndor,AUT.nmro_cntndor,AUT.estdo)",
    	"(CASE WHEN mdldad_entrda = 'LLE' THEN 'Descargue' WHEN mdldad_entrda = 'VAC' THEN 'Cargue' ELSE '' END)",
        "ASG.cdgo_tpo_mvmnto"])
    }
});