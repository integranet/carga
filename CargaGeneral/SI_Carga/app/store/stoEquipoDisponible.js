Ext.define('sicarga.store.stoEquipoDisponible',{
	extend: 'sicarga.store.storeDinamico',
    url: 'app/data/php/SelectEquipos.php',
    model: 'sicarga.model.EquipoDisponible', 
    autoLoad: {start: 0, limit: 15},
    pageSize: 15,
    remoteSort: true
});