Ext.define('sicarga.store.stoEquipoAsignado',{
	extend: 'sicarga.store.storeDinamico',
    url: 'app/data/php/SelectEquiposAsignados.php',
    model: 'sicarga.model.EquipoAsignado', 
    autoLoad: {start: 0, limit: 15},
    pageSize: 15,
    remoteSort: true
});