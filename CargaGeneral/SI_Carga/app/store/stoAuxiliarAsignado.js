Ext.define('sicarga.store.stoAuxiliarAsignado',{
	extend: 'sicarga.store.storeDinamico',
    url: 'app/data/php/SelectAuxiliarAsignado.php',
    model: 'sicarga.model.AuxiliarAsignado', 
    autoLoad: {start: 0, limit: 15},
    pageSize: 15,
    remoteSort: true
});