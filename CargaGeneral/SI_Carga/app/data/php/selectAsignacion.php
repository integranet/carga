<?php

include("conexion/conexion.php");
include("conexion/utilidades.php");

$busqueda="";

if(isset($_POST['search']) && isset($_POST['condiciones'])){
     $filtros = $_POST['search'];
     $condiciones = $_POST['condiciones'];
     $busqueda = construirFiltros($filtros,$condiciones);
}

$con = new Conexion($db_nombre, $db_usuario, $db_password);

/*ejecutar procedimiento almacenado traer equipos*/
$sql = "execute procedure in_treqpos()";
$result = $con->ejecutarConsulta($sql);

$sql = "select ASG.cdgo_asgncion FROM asgncnes ASG, asgncnes_eqpos ASQ, cntrolV CTR,autrzcion AUT
WHERE ASG.tpo_asgncion = 38 AND ASG.cdgo_asgncion = ASQ.cnsctvo
AND ASQ.cnsctvo_cntrolv = CTR.cnsctvo AND AUT.nmro_autrzcion = ASQ.nmro_autrzcion
AND ASG.cdgo_tpo_mvmnto in('ISM','IST','SSL','SSM','SST') and ASG.estdo in('59','60','92') ".$busqueda;


$result = $con->ejecutarConsulta($sql);

//$total = odbc_num_rows($result);

$i = 0;

while(odbc_fetch_row($result)) {
     $i++;
}

$sql = "select SKIP ".$_POST['start']." FIRST ".$_POST['limit']."     ASG.id_asgncion,
     ASG.cdgo_asgncion,
     ASG.tpo_asgncion,
     to_char(ASG.fcha_asgncion,'%d/%m/%Y') fcha_asgncion,
     ASG.hra_asgncion,
     ASG.tmpo_asgncion,
     ASG.cdgo_zna,
     ASG.fcha_atncion,
     ASG.hra_atncion,
     ASG.tmpo_atncion,
     ASG.estdo,
     ASG.cnsctvo_mvmnto,
     ASG.cdgo_tpo_mvmnto,
     ASG.nmro_rclda,
     ASG.cdgo_tpo_crga_d,
     ASG.cdgo_tpo_crga_e,
     ASG.hr_incio,
     ASG.hr_fn,
     CTR.plca_vhclo,
     obtner_cntndo(AUT.mtrcla_crga,AUT.cdgo_tpo_mvmnto,AUT.prfjo_cntndor,AUT.nmro_cntndor,AUT.estdo) contprod,
     obtner_nmbre_zna(ASG.cdgo_zna) nmbre_zna,obtner_estdo_clores(ASG.hr_incio,ASG.estdo,ASG.fcha_asgncion,ASG.tpo_asgncion) clor,
     (CASE WHEN mdldad_entrda = 'LLE' THEN 'Descargue' WHEN mdldad_entrda = 'VAC' THEN 'Cargue' ELSE '' END) oprcion
FROM asgncnes ASG, asgncnes_eqpos ASQ, cntrolV CTR,autrzcion AUT
WHERE ASG.tpo_asgncion = 38 AND ASG.cdgo_asgncion = ASQ.cnsctvo
AND ASQ.cnsctvo_cntrolv = CTR.cnsctvo AND AUT.nmro_autrzcion = ASQ.nmro_autrzcion
AND ASG.cdgo_tpo_mvmnto in('ISM','IST','SSL','SSM','SST') and ASG.estdo in('59','60','92') ".$busqueda;



$result = $con->ejecutarConsulta($sql);

while ($row=odbc_fetch_array($result))
{
     $contenido['id_asgncion'] = trim(utf8_encode($row['id_asgncion']));
     $contenido['cdgo_asgncion'] = trim(utf8_encode($row['cdgo_asgncion']));
     $contenido['tpo_asgncion'] = trim(utf8_encode($row['tpo_asgncion']));
     $contenido['fcha_asgncion'] = trim(utf8_encode($row['fcha_asgncion']));
     $contenido['hra_asgncion'] = trim(utf8_encode($row['hra_asgncion']));
     $contenido['tmpo_asgncion'] = trim(utf8_encode($row['tmpo_asgncion']));
     $contenido['cdgo_zna'] = trim(utf8_encode($row['cdgo_zna']));
     $contenido['fcha_atncion'] = trim(utf8_encode($row['fcha_atncion']));
     $contenido['hra_atncion'] = trim(utf8_encode($row['hra_atncion']));
     $contenido['tmpo_atncion'] = trim(utf8_encode($row['tmpo_atncion']));
     $contenido['estdo'] = trim(utf8_encode($row['estdo']));
     $contenido['cnsctvo_mvmnto'] = trim(utf8_encode($row['cnsctvo_mvmnto']));
     $contenido['cdgo_tpo_mvmnto'] = trim(utf8_encode($row['cdgo_tpo_mvmnto']));
     $contenido['nmbre_zna'] = trim(utf8_encode($row['nmbre_zna']));
     $contenido['clor'] = trim(utf8_encode($row['clor']));
     $contenido['plca_vhclo'] = trim(utf8_encode($row['plca_vhclo']));
     $contenido['contprod'] = trim(utf8_encode($row['contprod']));
     $contenido['oprcion'] = trim(utf8_encode($row['oprcion']));
     $contenido['hr_incio'] = trim(utf8_encode($row['hr_incio']));
     $contenido['hr_fn'] = trim(utf8_encode($row['hr_fn']));
         
     $data[] = $contenido;
    
}
 if(!isset ($data)) {
       $data = null;
 }
$con->cerrarConexion();
 
echo "{success:true,data:".json_encode($data).",total:".$i."}";


?>
