<?php


include("conexion/conexion.php");

$busqueda=""; 

$con = new Conexion($db_nombre, $db_usuario, $db_password);

$sqlf = "";
if(isset($_POST['search'])){
     /*{"cdgo_eqpo":"a","nmbre_tpo_eqpo":""}*/
     $filtros = json_decode($_POST['search'],true);
     if($filtros['cdgo_eqpo']!=''){
       $sqlf=$sqlf." and EQA.cdgo_eqpo like '%".$filtros['cdgo_eqpo']."%'  ";
    }
    if($filtros['nmbre_tpo_eqpo']!=''){
       $sqlf=$sqlf." and TPE.nmbre_tpo_eqpo like '%".$filtros['nmbre_tpo_eqpo']."%'  ";
    }
    if($filtros['cpcdad']!=''){
       $sqlf=$sqlf." and (Cast(EQA.cpcdad AS char(5))) like '%".$filtros['cpcdad']."%'";
    }

}


$sql="select distinct EQA.cdgo_eqpo
from eqpos_aprjos EQA,  clntes CLT  ,tpo_eqpo TPE, eqpos_dspnbles_v2 EQD,oprrios_eqpos OPQ
where EQD.estdo = 'A' and CLT.cdgo_clnte=EQA.cdgo_oprdor_prtrio and EQA.estdo='A' and EQA.cdgo_tpo_eqpo = TPE.cdgo_tpo_eqpo
and EQD.cdgo_eqpo = EQA.cdgo_eqpo and date(EQD.hra_incial) = today and OPQ.cdgo_oprrio = EQD.cdgo_oprrio
and in_esta_turno((EQD.hra_incial::datetime hour to hour::char(2))) = 1 ".$sqlf;


$result = $con->ejecutarConsulta($sql);

//$total = odbc_num_rows($result);

$i = 0;

while(odbc_fetch_row($result)) {
     $i++;
}



$sql="select SKIP ".$_POST['start']." FIRST ".$_POST['limit']." distinct EQA.cdgo_eqpo, EQA.cdgo_oprdor_prtrio, CLT.nmbre_clnte, EQA.arrnddo , TPE.nmbre_tpo_eqpo, EQA.mrca, EQA.clor, 
EQA.chsis, EQA.mtor, EQA.srie, EQA.mdlo, EQA.cpcdad, EQA.sgro , EQA.fcha_vncmnto, EQA.plca, EQA.fcha_ultma_insp,
EQA.emprsa_inspccion, 
EQA.nmbre_inspctor, EQA.en_el_prto, EQA.estdo,
OPQ.nmbre_oprrio
from eqpos_aprjos EQA,  clntes CLT  ,tpo_eqpo TPE, eqpos_dspnbles_v2 EQD, oprrios_eqpos OPQ
where EQD.estdo = 'A' and CLT.cdgo_clnte=EQA.cdgo_oprdor_prtrio and EQA.estdo='A' and EQA.cdgo_tpo_eqpo = TPE.cdgo_tpo_eqpo
and EQD.cdgo_eqpo = EQA.cdgo_eqpo and date(EQD.hra_incial) = today and OPQ.cdgo_oprrio = EQD.cdgo_oprrio 
and in_esta_turno((EQD.hra_incial::datetime hour to hour::char(2))) = 1 ";



$sql=$sql.$sqlf." order by cdgo_eqpo";


$result = $con->ejecutarConsulta($sql);

while ($row=odbc_fetch_array($result))
{
	  
     $contenido['cdgo_eqpo'] = trim(utf8_encode($row['cdgo_eqpo']));
     $contenido['cdgo_oprdor_prtrio'] = trim(utf8_encode($row['cdgo_oprdor_prtrio']));
     $contenido['nmbre_clnte'] = trim(utf8_encode($row['nmbre_clnte']));
     $contenido['arrnddo'] = trim(utf8_encode($row['arrnddo']));
     $contenido['nmbre_tpo_eqpo'] = trim(utf8_encode($row['nmbre_tpo_eqpo']));
     $contenido['mrca'] = trim(utf8_encode($row['mrca']));
     $contenido['clor'] = trim(utf8_encode($row['clor']));
     $contenido['chsis'] = trim(utf8_encode($row['chsis']));
     $contenido['mtor'] = trim(utf8_encode($row['mtor']));
     $contenido['srie'] = trim(utf8_encode($row['srie']));
     $contenido['mdlo'] = trim(utf8_encode($row['mdlo']));
     $contenido['cpcdad'] = trim(utf8_encode($row['cpcdad']));
     $contenido['sgro'] = trim(utf8_encode($row['sgro']));
     $contenido['fcha_vncmnto'] = trim(utf8_encode($row['fcha_vncmnto']));
     $contenido['plca'] = trim(utf8_encode($row['plca']));
     $contenido['fcha_ultma_insp'] = trim(utf8_encode($row['fcha_ultma_insp']));
     $contenido['emprsa_inspccion'] = trim(utf8_encode($row['emprsa_inspccion']));
     $contenido['nmbre_inspctor'] = trim(utf8_encode($row['nmbre_inspctor']));
     $contenido['en_el_prto'] = trim(utf8_encode($row['en_el_prto']));
     $contenido['estdo'] = trim(utf8_encode($row['estdo']));
     $contenido['nmbre_oprrio'] = trim(utf8_encode($row['nmbre_oprrio']));
    
   

     $data[] = $contenido;
    
}
 if(!isset ($data)) {
       $data = null;
 }
$con->cerrarConexion();
 
echo "{success:true,data:".json_encode($data).",total:".$i."}";


?>
