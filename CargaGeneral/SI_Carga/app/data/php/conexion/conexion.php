<?php
include("../../../../Configuracion/config.php");
class Conexion{

	private $conn;

	function Conexion($dsn,$usuario,$password){
		try{
			$this->conn = odbc_connect($dsn, $usuario, $password);
		}catch(Exception $e){
			return $e->getMessage();
		}
	}

	public function ejecutarConsulta($sql) {
        try {
        	$result = odbc_exec($this->conn,$sql);
        	return $result;
        } catch (Exception $e) {
        	return $e->getMessage();
        }
    }
    
    public function cerrarConexion(){
    	odbc_close($this->conn);
    }


}
?>