<?php


include("conexion/conexion.php");

$busqueda=""; 

$con = new Conexion($db_nombre, $db_usuario, $db_password);


$sqlf = "";
if(isset($_POST['search'])){
     /*{"cdgo_eqpo":"a","nmbre_tpo_eqpo":""}*/
     $filtros = json_decode($_POST['search'],true);
     if($filtros['cdgo_eqpo']!=''){
       $sqlf=$sqlf." and cdgo_eqpo like '%".$filtros['cdgo_eqpo']."%'  ";
    }
    if($filtros['nmbre_tpo_eqpo']!=''){
       $sqlf=$sqlf." and TPE.nmbre_tpo_eqpo like '%".$filtros['nmbre_tpo_eqpo']."%'  ";
    }
    if($filtros['cpcdad']!=''){
       $sqlf=$sqlf." and (Cast(cpcdad AS char(5))) like '%".$filtros['cpcdad']."%'";
    }
}

$sql="select EQA.cdgo_eqpo
from eqpos_aprjos EQA, dtlle_asgnciones DAS, clntes, tpo_eqpo TPE, asgncnes ASG, eqpos_dspnbles_v2 EQD,oprrios_eqpos OPQ
where DAS.id_rcrso = EQA.cdgo_eqpo and DAS.tpo_rcrso = 41 and DAS.estdo = 'A'
and cdgo_clnte=EQD.cdgo_oprdor_prtrio and EQA.estdo='A'
and EQA.cdgo_tpo_eqpo = TPE.cdgo_tpo_eqpo
and DAS.id_asgncion = ASG.id_asgncion
and EQD.cdgo_eqpo = DAS.id_rcrso and date(EQD.hra_incial) = today and OPQ.cdgo_oprrio = EQD.cdgo_oprrio and ASG.estdo <> 61  and ASG.fcha_asgncion = today 
and in_esta_turno((EQD.hra_incial::datetime hour to hour::char(2))) = 1 ".$sqlf;



$result = $con->ejecutarConsulta($sql);

//$total = odbc_num_rows($result);

$j = 0;

while(odbc_fetch_row($result)) {
     $j++;
}



$sql="select SKIP ".$_POST['start']." FIRST ".$_POST['limit']." EQA.cdgo_eqpo, EQD.cdgo_oprdor_prtrio, nmbre_clnte, arrnddo , TPE.nmbre_tpo_eqpo, mrca, clor, chsis, 
mtor, srie, mdlo, cpcdad, sgro , EQA.fcha_vncmnto, plca, fcha_ultma_insp, emprsa_inspccion, nmbre_inspctor, en_el_prto,
EQA.estdo,
EQD.hra_incial::datetime hour to hour::char(2)::int hr_incio,
EQD.hra_fnal::datetime hour to hour::char(2)::int hr_fn,
ASG.hr_incio hr_in,ASG.hr_fn hr_fin,ASG.cdgo_asgncion,ASG.fcha_asgncion,
OPQ.nmbre_oprrio
from eqpos_aprjos EQA, dtlle_asgnciones DAS, clntes, tpo_eqpo TPE, asgncnes ASG, eqpos_dspnbles_v2 EQD, oprrios_eqpos OPQ
where DAS.id_rcrso = EQA.cdgo_eqpo and DAS.tpo_rcrso = 41 and DAS.estdo = 'A'
and cdgo_clnte=EQD.cdgo_oprdor_prtrio and EQA.estdo='A'
and EQA.cdgo_tpo_eqpo = TPE.cdgo_tpo_eqpo
and DAS.id_asgncion = ASG.id_asgncion
and EQD.cdgo_eqpo = DAS.id_rcrso and date(EQD.hra_incial) = today and OPQ.cdgo_oprrio = EQD.cdgo_oprrio and ASG.estdo <> 61  and ASG.fcha_asgncion = today 
and in_esta_turno((EQD.hra_incial::datetime hour to hour::char(2))) = 1".$sqlf;



$result = $con->ejecutarConsulta($sql);

while ($row=odbc_fetch_array($result))
{
	  
     $contenido['cdgo_eqpo'] = trim(utf8_encode($row['cdgo_eqpo']));
     $contenido['cdgo_oprdor_prtrio'] = trim(utf8_encode($row['cdgo_oprdor_prtrio']));
     $contenido['nmbre_clnte'] = trim(utf8_encode($row['nmbre_clnte']));
     $contenido['arrnddo'] = trim(utf8_encode($row['arrnddo']));
     $contenido['nmbre_tpo_eqpo'] = trim(utf8_encode($row['nmbre_tpo_eqpo']));
     $contenido['mrca'] = trim(utf8_encode($row['mrca']));
     $contenido['clor'] = trim(utf8_encode($row['clor']));
     $contenido['chsis'] = trim(utf8_encode($row['chsis']));
     $contenido['mtor'] = trim(utf8_encode($row['mtor']));
     $contenido['srie'] = trim(utf8_encode($row['srie']));
     $contenido['mdlo'] = trim(utf8_encode($row['mdlo']));
     $contenido['cpcdad'] = trim(utf8_encode($row['cpcdad']));
     $contenido['sgro'] = trim(utf8_encode($row['sgro']));
     $contenido['fcha_vncmnto'] = trim(utf8_encode($row['fcha_vncmnto']));
     $contenido['plca'] = trim(utf8_encode($row['plca']));
     $contenido['fcha_ultma_insp'] = trim(utf8_encode($row['fcha_ultma_insp']));
     $contenido['emprsa_inspccion'] = trim(utf8_encode($row['emprsa_inspccion']));
     $contenido['nmbre_inspctor'] = trim(utf8_encode($row['nmbre_inspctor']));
     $contenido['en_el_prto'] = trim(utf8_encode($row['en_el_prto']));
     $contenido['estdo'] = trim(utf8_encode($row['estdo']));
     $contenido['cdgo_asgncion'] = trim(utf8_encode($row['cdgo_asgncion']));
     $contenido['fcha_asgncion'] = trim(utf8_encode($row['fcha_asgncion']));
     $contenido['hr_in'] = trim(utf8_encode($row['hr_in']));
     $contenido['hr_fin'] = trim(utf8_encode($row['hr_fin']));
     $contenido['nmbre_oprrio'] = trim(utf8_encode($row['nmbre_oprrio']));
     for ($i=0; $i <= 23 ; $i++) {
          if($row['hr_fn']!='' && $row['hr_incio']!=''){
               if(($row['hr_fn']*1) >= $i && ($row['hr_incio']*1)<=$i ){
                    $contenido['hora'.$i] = true;
               }else{
                    $contenido['hora'.$i] = false;
               }
          }else{
               $contenido['hora'.$i] = false;
          }
     }

     $data[] = $contenido;
    
}
 if(!isset ($data)) {
       $data = null;
 }
$con->cerrarConexion();
 
echo "{success:true,data:".json_encode($data).",total:".$j."}";


?>
