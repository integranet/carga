Ext.define('sicarga.clases.Utilidades.js',{
	alternateClassName:'sicarga.Utilidades',
	singleton:true,

	iniciarTaskManager: function(intervalo){
        intervalo  = intervalo || 60000;
		Ext.TaskManager.start({
			run: function(){
				entorno.Vistas.VisColaGeneral.down('gridpanel[name=grAsignacionVehiculo]').store.load({
					params: {
                        
					}
				});
                entorno.Vistas.VisColaGeneral.down('gridpanel[name=grAsignacionBuque]').store.load({
                    params: {
                        
                    }
                });
                entorno.Vistas.VisColaGeneral.down('gridpanel[name=grAsignacionActividad]').store.load({
                    params: {
                        
                    }
                });

                /*Granel*/
                entorno.Vistas.VisColaGranel.down('gridpanel[name=grAsignacionVehiculoGranel]').store.load({
                    params: {
                        
                    }
                });
                entorno.Vistas.VisColaGranel.down('gridpanel[name=grAsignacionBuqueGranel]').store.load({
                    params: {
                        
                    }
                });
                
                entorno.Vistas.VisColaGranel.down('gridpanel[name=grAsignacionActividadGranel]').store.load({
                    params: {
                        
                    }
                });
			},
			interval: intervalo,
            onError: function(){
                Ext.MessageBox.alert('Error','Error iniciando timer');
            }/*,
			duration: 600000*/
		});
	},

	cargarVariables: function(){
    	Ext.Ajax.request({
    		url: 'app/data/php/CargarVariables.php',
    		success:function(response){
    			var resp = Ext.decode(response.responseText);
    			entorno['Variables'] = resp.data;
                var tiempoRefresh = sicarga.Utilidades.buscarVariable('EQ0010') * 1000;
                //sicarga.Utilidades.iniciarTaskManager(tiempoRefresh);
    		},
    		failure: function(){
    			console.log('Error cargando variables');
    		}
    	});
    },

    buscarVariable: function(cdgo_vrble,username){
    	username  = username || '';
    	for (var i = 0; i < entorno.Variables.length; i++) {
    		if(entorno.Variables[i].cdgo_vrble==cdgo_vrble && entorno.Variables[i].username == username){
    			return entorno.Variables[i].vlor;
    		}
    	}
    	return '';
    },

    colorAsignacion: function(color,nombreClase){
        color = color || "#fffec7";
        nombreClase = nombreClase || "colorasignacion";
        var heads = document.getElementsByTagName('head')[0];
        var estilo = document.createElement('style');
        var css = "."+nombreClase+" > td { background:"+color+" !important; }";
        estilo.type = 'text/css';
        if(estilo.styleSheet){
            estilo.styleSheet.cssText = css;
        }else{
            estilo.appendChild(document.createTextNode(css));
        }
        document.getElementsByTagName('head')[0].appendChild(estilo);
    },

    obtenerStoreHoras: function(){
        var stoHoras = Ext.create('Ext.data.Store',{
            fields: ['hora'],
            data: [
                    {
                        'hora': '0'
                    },
                    {
                        'hora': '1'
                    },
                    {
                        'hora': '2'
                    },
                    {
                        'hora': '3'
                    },
                    {
                        'hora': '4'
                    },
                    {
                        'hora': '5'
                    },
                    {
                        'hora': '6'
                    },
                    {
                        'hora': '7'
                    },
                    {
                        'hora': '8'
                    },
                    {
                        'hora': '9'
                    },
                    {
                        'hora': '10'
                    },
                    {
                        'hora': '11'
                    },
                    {
                        'hora': '12'
                    },
                    {
                        'hora': '13'
                    },
                    {
                        'hora': '14'
                    },
                    {
                        'hora': '15'
                    },
                    {
                        'hora': '16'
                    },
                    {
                        'hora': '17'
                    },
                    {
                        'hora': '18'
                    },
                    {
                        'hora': '18'
                    },
                    {
                        'hora': '19'
                    },
                    {
                        'hora': '20'
                    },
                    {
                        'hora': '21'
                    },
                    {
                        'hora': '22'
                    },
                    {
                        'hora': '23'
                    },
            ]
        });
        return stoHoras;
    }

});