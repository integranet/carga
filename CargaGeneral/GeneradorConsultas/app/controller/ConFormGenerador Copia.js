Ext.define('Reporteador.controller.ConFormGenerador',{
	 extend: 'Ext.app.Controller',
	   	
	 init:function (){
	 	
         	this.control({
             'visformgenerador combobox[name=cbotpoasign]':{
                 select: this.AlSeleccionarReporte
              },
              'visformgenerador button[action=IrACrudTablas]':{
                 click: this.MostrarCrudTablasReporte
              },
         	  	'visformgenerador button[action=newquery]':{
	            	click: this.NuevaConsulta
	            }
	            ,'visformgenerador button[action=openquery]':{
                click: this.MostrarReportesConfigurados
              }
              ,'vistablasgen':{
                close: this.AlCerrarPestañaTablasReporte
              }
              ,'visformgenerador button[action=savequery]':{
	            	click: this.GuardarConsulta
	            }
	            ,'visformgenerador button[action=executequery]':{
	            	click: this.EjecutarConsulta
	            }
              ,'visformgenerador button[action=sqldinam]':{
	            	click: this.MostrarSqlDinamico
	            }
	            ,'visformgenerador button[action=irAConfiguracion]':{
	            	click: this.MostrarVentanaConfiguracion
	            }
	            ,'visformgenerador button[action=moveup]':{
	            	click: this.MoverCampoArriba
	            }
	             ,'visformgenerador button[action=movedown]':{
	            	click: this.MoverCampoAbajo
	            }
	            ,'visformgenerador button[action=moveright]':{
	            	click: this.MoverCamposDerecha
	            } 
	            ,'visformgenerador gridpanel[name=gridColumnas1]':{
	            	itemdblclick: this.MoverCamposDerecha
	            }
	            ,'visformgenerador button[action=moveleft]':{
	            	click: this.MoverCamposIzquierda
	            }
	            ,'visformgenerador gridpanel[name=gridColumnas2]':{
	            	itemdblclick: this.MoverCamposIzquierda
	            }
	            ,'visformgenerador button[action=movetodosrigth]':{
	            	click: this.MoverTodosDerecha
	            }
	            ,'visformgenerador button[action=movetodosleft]':{
	            	click: this.MoverTodosIzquierda
	            }
	          /*  ,'visformgenerador checkboxfield[name=chkFunciones]':{
	            	change: this.CargaGrillaFuncionesCampos
	            }
	           /* ,'visformgenerador combobox[name=cboFunciones]':{
	            	expand: this.FiltraComboFunciones
	            }	*/
	           /* ,'visformgenerador checkboxfield[name=chkOrden]':{
	            	change: this.CargaGrillaOrdenCampos
	            }*/
	            ,'visformgenerador checkboxfield[name=chkAgrupar]':{
	            	change: this.CargaGrillaAgruparCampos
	            }
              	         	            
	        })
   },  

     /*Otras Funciones*/
     AlSeleccionarReporte:function(){
         this.NuevaConsulta();
         this.TraerTablabase();
     },

     TraerTablabase:function(){
           var me=this;
           var TipoReporte=entorno['Vistas']['VisFormGenerador'].down('combobox[name=cbotpoasign]').getValue();
           Ext.Ajax.request({
                  type: "POST",
                  url : 'app/data/php/ObtenerInfoTablasReporte.php',
                  params: {
                      action:'traetablabase',
                      tiporeporte:TipoReporte
                  },                                    
                  success: function(response){
                   try{
                          var resp = Ext.decode(response.responseText); 
                          if(resp.success){
                              entorno['TablaBase']=resp.tablabase;  
                              if(resp.idtablabase!=null){
                                   me.cargarTablasMaestras(resp.idtablabase);
                              }                                                       
                          }
                    }catch(e){
                          Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
                    }

                  },
                  failure: function(){
                      Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
                  }
          });
     },

    buscarTablasMaestras:function(value,op){

            for (var i = 0; i < entorno.TablasMaestras.length; i++) {
                if(entorno.TablasMaestras[i].Valor==value){
                  if(op==1){
                      return entorno.TablasMaestras[i].CampoBase;
                  }else{
                      return entorno.TablasMaestras[i].CampoMaestra;
                  }                  
                }
            }
        return value;
    },

    cargarTablasMaestras:function(idtablabase){
      
        Ext.Ajax.request({
            url: 'app/data/php/ObtenerInfoTablasReporte.php',
            params: {
                      action:'traetablasmaestras',
                      tablabase:idtablabase
             },     
            success: function(response) {

              var resp = Ext.decode(response.responseText);
              entorno['TablasMaestras']=resp.data;
            
            },
            failure: function() {
                   Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));        
            }
      });
    },

    LlenaGrillaCampos:function(){
            var me=this;
            for (var i = 0; i < entorno.CamposConsulta.length; i++) {
                if(entorno.CamposConsulta[i].Tipo=="69"){
                   entorno.Controladores.ConReportesConf.CargarColumnas(entorno.CamposConsulta[i].Campo);           
                }else if(entorno.CamposConsulta[i].Tipo=="70"){
                   me.LLenaGrillaFunciones(entorno.CamposConsulta[i].Campo,entorno.CamposConsulta[i].Valor);
                }else if(entorno.CamposConsulta[i].Tipo=="71"){
                    me.LLenaGrillaFiltros(entorno.CamposConsulta[i].Campo,entorno.CamposConsulta[i].Valor);
                }
            }       
    },


      ObtenerCamposConsulta:function(idconsulta){
       var me=this;
        Ext.Ajax.request({
            url: 'app/data/php/ObtenerCamposConsulta.php',
            params: {
                 idconsulta:idconsulta
             },     
            success: function(response) {

              var resp = Ext.decode(response.responseText);
              entorno['CamposConsulta']=resp.data;
              me.LlenaGrillaCampos();
              
            },
            failure: function() {
                   Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));        
            }
      });
    },

     AlCerrarPestañaTablasReporte:function(panel,eOpts){
         entorno['Vistas']['VisCrudTablasRep']=null;
     },

      MostrarCrudTablasReporte:function(){
        if(entorno['Vistas']['VisCrudTablasRep']==null){
               entorno['Vistas']['VisCrudTablasRep']=Ext.create('Reporteador.view.VisTablasGenerador');
               entorno['Vistas']['VisPrincipal'].down('tabpanel[name=tpanel]').add(entorno['Vistas']['VisCrudTablasRep']);
               entorno['Vistas']['VisPrincipal'].down('tabpanel[name=tpanel]').setActiveTab(entorno['Vistas']['VisCrudTablasRep']);      
        }else{
                entorno['Vistas']['VisPrincipal'].down('tabpanel[name=tpanel]').setActiveTab(entorno['Vistas']['VisCrudTablasRep']);      
        }
          
      },
       //Funciones Fieldset Columnas

        getSelectedRowIndex:function(grid) {
               
		      var r = grid.getSelectionModel().getSelection();
		      var s = grid.getStore();
		      return s.indexOf(r[0]);

       },

        DevolverIndiceFila:function(store,campo){                
           
           var indice = [];
          for(var j in campo){
              Ext.each(store, function(record) {
                    for(var i=0;i<record.data.length;i++){
                        var abreviatura=record.data.items[i].data.abreviatura;
                        if(String(abreviatura.toLowerCase())==String(campo[j].toLowerCase())){
                            indice.push(i);                           
                        }
                    }
                });
        }
            return indice;
     },

     SeparaFunciones:function(funciones){
            var datos = [];
           
            for(var i in funciones){
               if (funciones[i].indexOf("(")!=-1){
                 datos.push(funciones[i].split("("));
                 //datos.join(funciones[i].split("("));
               }             
            }
            //console.log(datos);
            return datos;
     },

     TraerNombreFuncion:function(tipo){
            var funcion="";
            if(tipo==1){
               funcion="MAX";
            }
            if(tipo==2){
              funcion="MIN";
            }
            if(tipo==3){
              funcion="AVG";
            }
            if(tipo==4){
              funcion="SUM";
            }
            if(tipo==5){
              funcion="COUNT";
            }
            return funcion;
     },

     CargaArregloFunciones:function(tFunciones){
        
         var arrFunc = null;
         for(var k in tFunciones){                 
               arrFunc = tFunciones[k][1].split(") ");              
               tFunciones[k][1] = arrFunc[0];
               tFunciones[k][2] = arrFunc[1];
               arrFunc = null;                           
         }
          //console.log(tFunciones);
          return tFunciones;
     },

      LLenaGrillaFunciones:function(campo,valor){
              var storegrilla1=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas1]').store;
              var storeFunciones=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]').store;
            
             Ext.each(storegrilla1, function(recordgrid1) {
                   for(var i=0;i<recordgrid1.data.length;i++){
                         var nomcampo=recordgrid1.data.items[i].data.nomtabla+"."+recordgrid1.data.items[i].data.nomcampo;
                          
                         if(String(nomcampo.toLowerCase())==String(campo.toLowerCase())){
                            // console.log(abreviatura);
                           var total=storeFunciones.getCount();  
                           storeFunciones.insert(total,Ext.create('Reporteador.model.ModColumnas', {
                               nomtabla:recordgrid1.data.items[i].data.nomtabla,
                               nomcampo:recordgrid1.data.items[i].data.nomcampo,
                               abreviatura:recordgrid1.data.items[i].data.abreviatura,
                               tipodato:recordgrid1.data.items[i].data.tipodato,
                               longitud:recordgrid1.data.items[i].data.longitud,
                               funcion:recordgrid1.data.items[i].data.funcion,
                               tipofuncion:valor,
                               agrupacion:recordgrid1.data.items[i].data.agrupacion
                           }));
                           /*storegrilla1.remove(storegrilla1.getAt(i));
                           i=i-1;*/
                         }         
                   } 
                   storeFunciones.commitChanges();                            
          }); 

      },

       CargaArregloFiltros:function(tFiltros){
        
        var arrFilt = [];        
       for(var k in tFiltros){     
              // console.log(tFiltros[k]);    
               arrFilt.push(tFiltros[k].split("="));                                
         }
           return arrFilt;
     },

      LLenaGrillaFiltros:function(campo,valor){
              
           var storeFiltros=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFiltros]').store;
         
                  Ext.each(storeFiltros, function(recordfilt) {
                    for(var i=0;i<recordfilt.data.length;i++){
                         var nomcampo=recordfilt.data.items[i].data.nomtabla+"."+recordfilt.data.items[i].data.nomcampo;
                  
                         if(String(nomcampo.toLowerCase())==String(campo.toLowerCase())){
                              storeFiltros.getAt(i).set('criterio',valor);
                              storeFiltros.commitChanges();
                          }                        
                    }
                  }); 
               
      },


       MoverCampoArriba:function(el,e,opts) {
           
           var grilla=el.up('gridpanel');
           var rowIndex = this.getSelectedRowIndex(grilla);
           var storegrilla=grilla.store; 
		       var selrecords=grilla.getSelectionModel().getSelection(); 
              if(selrecords.length==1){     
		           if(rowIndex!=-1 && rowIndex!=0){
		           	     var record=storegrilla.getAt(rowIndex); 
		           	     var indice=rowIndex-1;                 
		                 storegrilla.insert(indice,Ext.create('Reporteador.model.ModColumnas', {
  						       nomtabla:record.get('nomtabla'),
  						       nomcampo:record.get('nomcampo'),
  						       abreviatura:record.get('abreviatura'),
  						       tipodato:record.get('tipodato'),
  						       longitud:record.get('longitud'),
  						       funcion:record.get('funcion'),
  						       agrupacion:record.get('agrupacion')
						}));
		                storegrilla.remove(record);
		                grilla.getSelectionModel().select(indice);
		           }
		       }

       },
 
       MoverCampoAbajo:function(el,e,opts) {
           //var grilla2=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]');
           var grilla=el.up('gridpanel');
          
           var rowIndex = this.getSelectedRowIndex(grilla);
           var storegrilla=grilla.store; 
           var selrecords=grilla.getSelectionModel().getSelection(); 
           if(selrecords.length==1){
	           if(rowIndex!=-1 && rowIndex!=storegrilla.getCount()-1){
	           	    
	           	     var record=storegrilla.getAt(rowIndex);
	           	     var indice=rowIndex+1; 
	           	     storegrilla.remove(record);               
	                 storegrilla.insert(indice,Ext.create('Reporteador.model.ModColumnas', {
  					       nomtabla:record.get('nomtabla'),
  					       nomcampo:record.get('nomcampo'),
  					       abreviatura:record.get('abreviatura'),
  					       tipodato:record.get('tipodato'),
  					       longitud:record.get('longitud'),
  					       funcion:record.get('funcion'),
  					       agrupacion:record.get('agrupacion')
					}));
	                grilla.getSelectionModel().select(indice);
	              
	           }
	      }
       },       
   

       MoverCamposDerecha:function() {
            //Se habilita botón Consultar
             entorno['Vistas']['VisFormGenerador'].down('button[action=executequery]').setDisabled(false);

            var storegrilla1=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas1]').store;
            var storegrilla2=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store;
            var selrecords=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas1]').getSelectionModel().getSelection();
              for(var i = 0; i < selrecords.length; i++){
                  storegrilla1.remove(selrecords[i]);
                  var total=storegrilla2.getCount();
                  storegrilla2.insert(total,Ext.create('Reporteador.model.ModColumnas', {
  				        nomtabla:selrecords[i].get('nomtabla'),
  				        nomcampo:selrecords[i].get('nomcampo'),
  				        abreviatura:selrecords[i].get('abreviatura'),
  				        tipodato:selrecords[i].get('tipodato'),
  				        longitud:selrecords[i].get('longitud'),
  				        funcion:selrecords[i].get('funcion'),
  				        agrupacion:selrecords[i].get('agrupacion')
				}));
              }
            this.CargaGrillaFuncionesCampos();     
	          this.CargaGrillaOrdenCampos();
	          this.CargaGrillaAgruparCampos();
       },

       
       MoverCamposIzquierda:function() {
            var storegrilla1=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas1]').store;
            var storegrilla2=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store;
            var selrecords=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').getSelectionModel().getSelection();
              for(var i = 0; i < selrecords.length; i++){
                  storegrilla2.remove(selrecords[i]);
                  var total=storegrilla1.getCount();  
                  storegrilla1.insert(total,Ext.create('Reporteador.model.ModColumnas', {
  				        nomtabla:selrecords[i].get('nomtabla'),
  				        nomcampo:selrecords[i].get('nomcampo'),
  				        abreviatura:selrecords[i].get('abreviatura'),
  				        tipodato:selrecords[i].get('tipodato'),
  				        longitud:selrecords[i].get('longitud'),
  				        funcion:selrecords[i].get('funcion'),
  				        agrupacion:selrecords[i].get('agrupacion')
				}));
              }
            this.CargaGrillaFuncionesCampos();     
	          this.CargaGrillaOrdenCampos();
	          this.CargaGrillaAgruparCampos();
       },
 
       MoverTodosDerecha:function() {

        //Se habilita botón Consultar
        entorno['Vistas']['VisFormGenerador'].down('button[action=executequery]').setDisabled(false);

        var storegrilla1=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas1]').store;
        var storegrilla2=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store;
        var Mask = new Ext.LoadMask(entorno['Vistas']['VisFormGenerador'], {msg:"Cargando, Por favor espere..."});
        Mask.show()
	 
		 Ext.each(storegrilla1, function(recordgrid1) {

              for(var i=0;i<recordgrid1.data.length;i++){
                   var total=storegrilla2.getCount();  
                   storegrilla2.insert(total,Ext.create('Reporteador.model.ModColumnas', {
      					       nomtabla:recordgrid1.data.items[i].data.nomtabla,
      					       nomcampo:recordgrid1.data.items[i].data.nomcampo,
      					       abreviatura:recordgrid1.data.items[i].data.abreviatura,
      					       tipodato:recordgrid1.data.items[i].data.tipodato,
      					       longitud:recordgrid1.data.items[i].data.longitud,
      					       funcion:recordgrid1.data.items[i].data.funcion,
      					       agrupacion:recordgrid1.data.items[i].data.agrupacion
			             }));
                       storegrilla1.remove(storegrilla1.getAt(i));
                       i=i-1;
                   } 
                            
	        });		   
	        this.CargaGrillaFuncionesCampos();     
	        this.CargaGrillaOrdenCampos();
	        this.CargaGrillaAgruparCampos();
	        Mask.hide(); 
       },

       MoverTodosIzquierda:function() {
            var storegrilla1=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas1]').store;
            var storegrilla2=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store;
	          var Mask = new Ext.LoadMask(entorno['Vistas']['VisFormGenerador'], {msg:"Cargando, Por favor espere..."});
            Mask.show();
	        Ext.each(storegrilla2, function(recordgrid2) {
                   for(var i=0;i<recordgrid2.data.length;i++){
                       var total=storegrilla1.getCount();  
                       storegrilla1.insert(total,Ext.create('Reporteador.model.ModColumnas', {
      					       nomtabla:recordgrid2.data.items[i].data.nomtabla,
      					       nomcampo:recordgrid2.data.items[i].data.nomcampo,
      					       abreviatura:recordgrid2.data.items[i].data.abreviatura,
      					       tipodato:recordgrid2.data.items[i].data.tipodato,
      					       longitud:recordgrid2.data.items[i].data.longitud,
      					       funcion:recordgrid2.data.items[i].data.funcion,
      					       agrupacion:recordgrid2.data.items[i].data.agrupacion
			          }));
                       storegrilla2.remove(storegrilla2.getAt(i));
                       i=i-1;
                   }
            });
             entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]').store.removeAll();
             entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridAgrupaciones]').store.removeAll();
             entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridOrden]').store.removeAll();  
            Mask.hide();
       },

      // Funciones fieldset Operaciones
      CopiarStore:function(store){
           
	    store.clearFilter();
      var datos = [];
   
     Ext.each(store, function(record) {
          for(var i=0;i<record.data.length;i++){
               datos.push({
                'idcampo':record.data.items[i].data.idcampo,
                'nomtabla':record.data.items[i].data.nomtabla,
                'nomcampo':record.data.items[i].data.nomcampo,
                'abreviatura':record.data.items[i].data.abreviatura,
                'tipodato':record.data.items[i].data.tipodato,
                'select':record.data.items[i].data.select,
                'vfuncion':record.data.items[i].data.vfuncion,
                'vfiltro':record.data.items[i].data.vfiltro,
                'vorden':record.data.items[i].data.vorden,
                'agrupamiento':record.data.items[i].data.agrupamiento          
               
            });
        }
     });

     return datos;
 },

    CopiarInfoStoreOrdenColumnas:function(store){
           
     
      store.removeAll();
      var selrecords=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').getSelectionModel().getSelection();
          
         if(selrecords.length>0){
            for(var i = 0; i < selrecords.length; i++){
               var total=store.getCount();
               store.insert(total,Ext.create('Reporteador.model.ModColumnas', {
                 nomtabla:selrecords[i].data.nomtabla,
                 nomcampo:selrecords[i].data.nomcampo,
                 abreviatura:selrecords[i].data.abreviatura,
                 tipodato:selrecords[i].data.tipodato,
                 longitud:selrecords[i].data.longitud,
                 funcion:selrecords[i].data.funcion,
                 agrupacion:selrecords[i].data.agrupacion
           }));
        }
      }      
     },


     /*  CargaGrillaFuncionesCampos:function(){
         
          var storeFunciones=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]').store;
          var estadocheckfunciones = entorno['Vistas']['VisFormGenerador'].down('checkboxfield[name=chkFunciones]').getValue();  
          if(estadocheckfunciones){
              this.CopiarInfoStoreColumnas(storeFunciones);
          }else{
           	  storeFunciones.removeAll();
          }
            
       },*/

       FiltraComboFunciones:function(el){

           var grillaFunciones=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]');
           var tipodato=grillaFunciones.selModel.lastSelected.data.tipodato;
          
            if(tipodato!="INTEGER"){
                el.store.filter(function(r) {
                    var value = r.get('IdValorParametro');
                    return (value == 1 || value == 2 || value == 5);
                });
           }else{
                  el.store.clearFilter();                
           }
           
       },


    /*   CargaGrillaOrdenCampos:function(){
                  
          var storeOrdenamiento=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridOrden]').store;
         // var estadocheckOrden = entorno['Vistas']['VisFormGenerador'].down('checkboxfield[name=chkOrden]').getValue();  
          //Habilita RadioGroup para especificar si el orden sera Ascendente o Descendente
         if(estadocheckOrden){
              entorno['Vistas']['VisFormGenerador'].down('radiogroup[name=radioOrden]').setDisabled(false);
              //Devuelve todos los checkboxes a su estado original
              entorno['Vistas']['VisFormGenerador'].down('radiogroup[name=radioOrden]').reset();
              this.CopiarInfoStoreOrdenColumnas(storeOrdenamiento);
         }else{
         	//Deshabilita RadioGroup Ordenamiento Campos
         	 entorno['Vistas']['VisFormGenerador'].down('radiogroup[name=radioOrden]').setDisabled(true);
             storeOrdenamiento.removeAll();
         }
    
       },*/

       /*CargaGrillaAgruparCampos:function(){
        
          var storeAgrupaciones=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridAgrupaciones]').store;
          var estadocheckAgrupamiento = entorno['Vistas']['VisFormGenerador'].down('checkboxfield[name=chkAgrupar]').getValue();  
          if(estadocheckAgrupamiento){
              this.CopiarInfoStoreColumnas(storeAgrupaciones);
           }else{
           	  storeAgrupaciones.removeAll();
           }
       },*/

       // Funciones ToolBar
       CargaArregloTablas:function(){
           var aCampos = [];
           var tCampos,storeCampos;
        
         storeCampos=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store;
         Ext.Array.include(aCampos,entorno['TablaBase']);
         for (var i =0; i < storeCampos.getCount(); i++){
              tCampos = storeCampos.getAt(i).data;
              Ext.Array.include(aCampos,tCampos.nomtabla);
         }
            return aCampos;
       },
      //Funciones para obtener las cadenas a ser guardadas al momento de pulsar sobre el botono Guardar Consulta
      HayFuncionesAgregadas:function(){
              var sw=false;
               var storeFunciones=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]').store;
               Ext.each(storeFunciones, function(recordfunc) {
                    for(var i=0;i<recordfunc.data.length;i++){       
                         var tipofuncion=recordfunc.data.items[i].data.valor;
                         if(tipofuncion!=""){
                             sw=true;
                             return true;
                          }else{
                             sw=false;
                          }                       
                    }
               }); 
               return sw;            

      },

      LlenaArregloColumnas:function(abreviatura){
          var arrColumnas = [];
          arrColumnas.push(abreviatura);
      },

      ArmaCadenaFiltros:function(){
         
           var filtros="";  
           var storeFiltros=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFiltros]').store;
               Ext.each(storeFiltros, function(recordfilt) {
                    for(var i=0;i<recordfilt.data.length;i++){
                         var campo=recordfilt.data.items[i].data.nomtabla+"."+recordfilt.data.items[i].data.nomcampo;
                         var criterio=recordfilt.data.items[i].data.criterio;
                         if(criterio!=""){
                              filtros=filtros+campo+"="+criterio+"|";
                          }                        
                    }
               }); 
               return filtros;             
      },

      ArmaCadenaCampos:function(){
            storeCampos=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store;
            var campos="";
            Ext.each(storeCampos, function(record) {
              for(var i=0;i<record.data.length;i++){
                  var abreviatura=record.data.items[i].data.abreviatura;
                  campos=campos+abreviatura+"|";
              }           
           });  
           return campos;            
       },

       ArmaCadenaFunciones:function(){         
           var funciones="";  
           var me=this;   
           var storeFunciones=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]').store;
               Ext.each(storeFunciones, function(recordfunc) {
                    for(var i=0;i<recordfunc.data.length;i++){
                         var nombretabla=recordfunc.data.items[i].data.nomtabla;
                         var nombrecampo=recordfunc.data.items[i].data.nomcampo;
                         var tipofuncion=recordfunc.data.items[i].data.tipofuncion;
                         if(tipofuncion!=""){
                             var funcion=me.TraerNombreFuncion(tipofuncion);
                             funciones=funciones+funcion+"("+nombretabla+"."+nombrecampo+") "+nombrecampo+"|";
                          }else{
                            funciones=funciones+nombretabla+"."+nombrecampo+"|";
                          }                       
                    }
               }); 
               return funciones;             
      },
   
        ArmaCadenaAgrupamiento:function(){
           var gridAgrupamiento=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridAgrupaciones]');
           var storeAgrupamiento=gridAgrupamiento.store;
        
           var agrupamiento="";
           Ext.each(storeAgrupamiento, function(record) {
                  for(var i=0;i<record.data.length;i++){
                        agrupamiento=agrupamiento+record.data.items[i].data.abreviatura+"|";
                  }
            });         
           return agrupamiento;            
       },

     
        ArmaCadenaOrdenamiento:function(){
           var gridOrden=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridOrden]');
       
           var selrecords=gridOrden.getSelectionModel().getSelection();
           var ordenamiento="";
           if(selrecords.length>0){
               for(var i = 0; i < selrecords.length; i++){
                  ordenamiento=ordenamiento+selrecords[i].data.abreviatura+"|";
               }
           }else{
             ordenamiento="";
           } 
           return ordenamiento;            
       },

          removeLastCaracter:function (cadena)
          {
              if (cadena.substring(cadena.length-1) == ",")
              {
                  cadena = cadena.substring(0, cadena.length-1);
              }

              return cadena;
          },  

         ArmarAgrupamiento:function(){
           
            var estadocheckAgrupamiento = entorno['Vistas']['VisFormGenerador'].down('checkboxfield[name=chkAgrupar]').getValue();  
            var storeAgrupamiento="",HayFuncion=false;
             
             HayFuncion=this.HayFuncionesAgregadas();
            
             if(HayFuncion){
                entorno['Vistas']['VisFormGenerador'].down('checkboxfield[name=chkAgrupar]').setValue(true);  
             }
             var agrupamiento=" group by ",campoAgrupacion="";         
             if(estadocheckAgrupamiento){
                  storeAgrupamiento=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridAgrupaciones]').store;
                
                  var me=this;
                  Ext.each(storeAgrupamiento, function(record) {
                   if(storeAgrupamiento.getCount()>0){
                        for(var i=0;i<record.data.length;i++){
                           var nombretabla=record.data.items[i].data.nomtabla;
                           var nombrecampo=record.data.items[i].data.nomcampo;
                           campoAgrupacion=nombretabla+"."+nombrecampo;    
                           agrupamiento=agrupamiento+campoAgrupacion;
                       
                          if(i<(record.data.length-1)&&campoAgrupacion!=""){
                            agrupamiento=agrupamiento+",";
                         }
                       }                                
                   }else{
                     agrupamiento="";
                   } 
                 });

             }else{
                  agrupamiento="";
            } 
         
          // agrupamiento=me.removeLastCaracter(agrupamiento);
           return agrupamiento;            
       },


       ArmarOrdenamiento:function(){

           var gridOrden=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridOrden]');
           var storeOrden=gridOrden.store;
           var radiosel=entorno['Vistas']['VisFormGenerador'].down('radiofield[name=rb]').inputValue;
           var orden;
         
           if(radiosel==2){
             orden=" DESC";
           }else{
             orden=" ASC"; 
           }
          
           var ordenamiento=" order by ";
            Ext.each(storeOrden, function(record) {
             if(storeOrden.getCount()>0){
                  for(var i=0;i<record.data.length;i++){
                    ordenamiento=ordenamiento+record.data.items[i].data.nomtabla+"."+record.data.items[i].data.nomcampo+orden;
                    if(i<(record.data.length-1)){
                      ordenamiento=ordenamiento+",";
                   }
                 }                                
             }else{
             	 ordenamiento="";
             } 
           });
           return ordenamiento;            
       },

        ArmarFiltros:function(){
           var storeFiltros="",filtro="";  
           storeFiltros=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFiltros]').store;
           Ext.each(storeFiltros, function(record) {
	            for(var i=0;i<record.data.length;i++){
	              	 var criterio= record.data.items[i].data.valor;
                 	 var nombretabla=record.data.items[i].data.nomtabla;
	                 var nombrecampo=record.data.items[i].data.nomcampo;
	                 var tipodato=record.data.items[i].data.tipodato;
	            	   var operador="",op="";
                                    
  	            	 if(criterio!=""){
                       op=criterio.substring(0, 2);
                       if(op==">="||op=="<="||op=="!="){
                          operador=op;
                          criterio=criterio.split(op).join("");
                       }else{
                           op=criterio.substring(0, 1);
                           if(op==">"||op=="<"||op=="="){
                              operador=op;
                              criterio=criterio.split(op).join("");
                           }else{
                              operador="=";
                           }
                       }
                                            
                       if(tipodato=="DATE"){
                           op=criterio.search(":");
                           if(op!=-1){
                              operador=" Between ";
                              criterio=criterio.split(":").join("' and '");
                           }
                          
                       }

    	            	 	if(tipodato!="INTEGER"){
                        criterio=criterio.split(";").join("' or "+nombretabla+"."+nombrecampo+"='");
    	            	 		filtro=filtro+" AND ("+nombretabla+"."+nombrecampo+operador+"'"+criterio+"') "; 
    	            	 	}else{
                        criterio=criterio.split(";").join(" or "+nombretabla+"."+nombrecampo+"=");
                        filtro=filtro+" AND ("+nombretabla+"."+nombrecampo+operador+""+criterio+") ";
    	            	 	}                   
                          
  	            	 }
	            }
	        });
           return filtro;
       },

      ArmarSelect:function(){

          var storeCampos="",storeFunciones="";
         // var estadocheckfunciones = entorno['Vistas']['VisFormGenerador'].down('checkboxfield[name=chkFunciones]').getValue();  
          var gridCampos=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]');
          storeCampos=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store;
           var selrecords=gridCampos.getSelectionModel().getSelection();
           var select="select ",campo="";
           var me=this;
           var arrColumnas = "";    
           Ext.each(storeCampos, function(record) {
              for(var i=0;i<record.data.length;i++){
                    var nomcolumna = record.data.items[i].data.abreviatura.split(" ").join("_");
                    nomcolumna=nomcolumna.split(".").join("");
                    campo=me.ArmarCampos(record.data.items[i].data.nomtabla,record.data.items[i].data.nomcampo,0);
                    
                    arrColumnas=arrColumnas+nomcolumna;
                    select=select+campo;
                                 
                     if(i<(record.data.length-1)){
                       select=select+",";
                       arrColumnas=arrColumnas+",";
                    }                  
               }
          });
            
                storeFunciones=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]').store;
                Ext.each(storeFunciones, function(record) {
                    for(var i=0;i<record.data.length;i++){
                      
                       var tipofuncion=record.data.items[i].data.valor;
                       var nombretabla=record.data.items[i].data.nomtabla;
                       var nombrecampo=record.data.items[i].data.nomcampo;
                       var abreviatura=record.data.items[i].data.abreviatura;
                       var nomcolumna = abreviatura.split(" ").join("_");
                       nomcolumna=nomcolumna.split(".").join("");
                   ;
                       if(tipofuncion!=""){
                         campo=me.ArmarCampos(nombretabla,nombrecampo,tipofuncion);
                          nomfuncion=me.TraerNombreFuncion(tipofuncion);
                          if(storeCampos.getCount()>0){
                             select=select+","+campo;
                             arrColumnas=arrColumnas+","+nomfuncion+"_"+nomcolumna;
                          }else{
                             if(i==0){
                                 select=select+campo;
                                 arrColumnas=arrColumnas+nomfuncion+"_"+nomcolumna;
                             }else{
                                  select=select+","+campo;
                                  arrColumnas=arrColumnas+","+nomfuncion+"_"+nomcolumna;
                             }
                          }
                           
                       }else{
                         campo="";
                       } 
                    }
                });
             
           entorno['Arreglo']['Columnas']=arrColumnas;

           
           return select;

      },

       ArmarFrom:function(){
       	   var aTablas = [];
       	   var from=" FROM ",where=" WHERE 1=1 ",campobase="",campomaestra="";
	         aTablas=this.CargaArregloTablas();
	       for(var i in aTablas){
    			   from=from+aTablas[i];
             if(aTablas[i]!=entorno['TablaBase']){
                 campobase=this.buscarTablasMaestras(aTablas[i],1);
                 campomaestra=this.buscarTablasMaestras(aTablas[i],2);
                 where=where+" AND "+entorno['TablaBase']+"."+campobase+"="+aTablas[i]+"."+campomaestra+"";
             }            
    			   if(i<(aTablas.length-1)){
	               	  from=from+",";
	           }
		     }
		    return from+where;
	       	       
       },

       ArmarCampos:function(nomtabla,nomcampo,tipofuncion){
              var funcion="",campo="";

           	  if(tipofuncion==0){
                 campo=nomtabla+"."+nomcampo;
              }else{
              	 funcion=this.TraerNombreFuncion(tipofuncion);
	           	   campo=funcion+"("+nomtabla+"."+nomcampo+") "+funcion+"_"+nomcampo;
              }
              return campo;
          
       },

       ArmarConsulta:function(){
       	 /* var estadocheckAgrupamiento = entorno['Vistas']['VisFormGenerador'].down('checkboxfield[name=chkAgrupar]').getValue();      
       	  var estadocheckOrden = entorno['Vistas']['VisFormGenerador'].down('checkboxfield[name=chkOrden]').getValue();  */
       	             
	        var select="select ",from="",filtro="",campo="",grupo="", order="";
	        var me=this;
              select=me.ArmarSelect();
              filtro=me.ArmarFiltros();
              from=me.ArmarFrom();
              order=me.ArmarOrdenamiento();
              grupo=me.ArmarAgrupamiento();

         
	          /*  if(estadocheckOrden){
                   	order=me.ArmarOrdenamiento();
                } */
              
                 
                                
                               
                var consulta=select+from+filtro+grupo+order;
               
                return consulta;
                /*console.log("Select: "+select);
                console.log("From:" +from);
  	            console.log("Filtros:" +filtro);
                console.log("Agrupamiento: "+grupo);
  	            console.log("Ordenamiento: "+order);*/	           
        
         
        
       },

        NuevaConsulta:function(sw,records){
          records = records || {};
          sw  = sw || false;
          var me=this;
             //Se recargan la grilla de Filtros y Columna de campos
             entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFiltros]').store.removeAll();
             entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas1]').store.removeAll();
             
            //Se limpian Campos Grilla Columna 2,Funciones, Ordenamiento y Agrupamiento
             entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store.removeAll();
        	   entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]').store.removeAll();
             entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridAgrupaciones]').store.removeAll();
             entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridOrden]').store.removeAll();  
             //Se deseleccionan los checkboxes
            /* entorno['Vistas']['VisFormGenerador'].down('checkboxfield[name=chkFunciones]').setValue(false);  
             entorno['Vistas']['VisFormGenerador'].down('checkboxfield[name=chkOrden]').setValue(false);  */
             entorno['Vistas']['VisFormGenerador'].down('checkboxfield[name=chkAgrupar]').setValue(false);  
             
             //Se cargan grilla filtros y campos para el reporte
             var TipoReporte=entorno['Vistas']['VisFormGenerador'].down('combobox[name=cbotpoasign]').getValue();
            /* entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFiltros]').store.extraParams.TipoReporte=TipoReporte;
             entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFiltros]').store.load();*/
             entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas1]').store.extraParams.TipoReporte=TipoReporte;
             entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas1]').store.load(function(){
            
                 
                 if(sw==true){
                    //Se guardan el codigo y el titulo de la consulta a cargar
                    entorno['Consulta']['Codigo']=records[0].data.codigo;
                    entorno['Consulta']['Titulo']=records[0].data.titulo;
                    //Habilita botón Consultar
                    entorno['Vistas']['VisFormGenerador'].down('button[action=executequery]').setDisabled(false);
                    entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store.extraParams.idconsulta=(records[0].data.codigo);
                    entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store.load();
                    entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]').store.extraParams.idconsulta=(records[0].data.codigo);
                    entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]').store.load();
                    entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFiltros]').store.extraParams.idconsulta=(records[0].data.codigo);
                    entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFiltros]').store.load();
                    entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridAgrupaciones]').store.extraParams.idconsulta=(records[0].data.codigo);
                    entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridAgrupaciones]').store.load();
                    entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridOrden]').store.extraParams.idconsulta=(records[0].data.codigo);
                    entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridOrden]').store.load(function(){
                        var storeOrden=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridOrden]').store;
                        if(storeOrden.getCount()>0){
                            var orden=storeOrden.getAt(0).data.vorden;
                            var radioOrden=entorno['Vistas']['VisFormGenerador'].down('radiofield[name=rb]');
                            if(orden=="DESC"){
                               radioOrden.setValue(2);
                               radioOrden.inputValue=2;
                            }else{
                               radioOrden.setValue(1);
                               radioOrden.inputValue=1;
                            }
                        }
                    });
                   // me.ObtenerCamposConsulta(records[0].data.codigo);
                 }else{
                      //Se reinician valores de variable codigo y titulo de la consulta
                      entorno['Consulta']['Codigo']="";
                      entorno['Consulta']['Titulo']="";
                      //Deshabilita botón Consultar
                      entorno['Vistas']['VisFormGenerador'].down('button[action=executequery]').setDisabled(true);
                                         
                 }
             });
        },
	
    MostrarReportesConfigurados:function(){
        var TipoReporte=entorno['Vistas']['VisFormGenerador'].down('combobox[name=cbotpoasign]').getValue();
        if(TipoReporte!=""){   
            var mask = Ext.Msg.wait( "Espere por favor", "Cargando", null );
		        entorno['Vistas']['VisRepConfigurados']=Ext.widget('visreportesconf');
            entorno['Vistas']['VisRepConfigurados'].down('gridpanel[action=dobleclick]').store.proxy.extraParams.search='{"codigo":"","titulo":""}';
		        entorno['Vistas']['VisRepConfigurados'].down('gridpanel[action=dobleclick]').store.load(function(){
						     mask.close();
		        }); 
       
        }else{
                Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(50));
          }              
     },

        EjecutarConsulta:function(){
           this.ArmarConsulta();          
           //console.log(entorno['Arreglo']['Columnas']);
           //console.log(entorno['Vistas']['VisFormGenerador'].down('container[name=formulario]'));
           //var consulta="select cdgo_zna from asgncnes_eqpos";
           var consulta= this.ArmarConsulta();   
           console.log(consulta);
          // window.open("http://yata/smWebReporteDinamico/ReporteDinamico.aspx?dato="+consulta);
        /*   entorno['Vistas']['VisRepoGen']=Ext.create('Reporteador.view.VisReporteGenerado');
           entorno['Vistas']['VisRepoGen'].down('component[name=visadic]').autoEl.src= entorno['Controladores']['ConPrincipal'].buscarParametro(55)+"?dato="+consulta+"&columnas="+entorno['Arreglo']['Columnas'];   
           entorno['Vistas']['VisPrincipal'].down('tabpanel[name=tpanel]').add(entorno['Vistas']['VisRepoGen']);
           entorno['Vistas']['VisPrincipal'].down('tabpanel[name=tpanel]').setActiveTab(entorno['Vistas']['VisRepoGen']);  */    
         
         /*  document.getElementById("dato").value=consulta;
           document.getElementById("Reporte").submit();*/
          // newDialog = window.open('http://yata/smWebReporteDinamico/ReporteDinamico.aspx', 'about:blank', "_form");
            //entorno['Vistas']['VisFormGenerador'].down('form[name=frmCamposConsulta]').target='_blank';
            //entorno['Vistas']['VisFormGenerador'].down('form[name=frmCamposConsulta]').method='POST';
            //entorno['Vistas']['VisFormGenerador'].down('form[name=frmCamposConsulta]').action='http://yata/smWebReporteDinamico/ReporteDinamico.aspx';
            //entorno['Vistas']['VisFormGenerador'].down('form[name=frmCamposConsulta]').submit();
           /** entorno['Vistas']['VisFormGenerador'].down('form[name=frmCamposConsulta]').submit
            ({
                 url: 'http://yata/smWebReporteDinamico/ReporteDinamico.aspx',      
                 waitMsg: 'Enviando datos reporte...',
            params:{
                 dato:'Hola Mundo'
            },
            success: function(opts, response) {
                 window.open(response);
            },
            failure: function(){
                      Ext.Msg.alert('el servidor no ha respondido la consulta');
              }
      });*/
           // window.open("http://yata/smWebReporteDinamico/ReporteDinamico.aspx", "_blank");
        },

        GuardarConsulta:function(){

            var codigo=entorno.Consulta.Codigo;
            var titulo=entorno.Consulta.Titulo;
            var storefiltros= entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFiltros]').store;
            var storecampos=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store;
            var storefunciones=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]').store;
            var storeorden=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridOrden]').store;
            var storeagrupamiento=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridAgrupaciones]').store;
            var datosSelect = Ext.encode(this.CopiarStore(storecampos));
            var datosFiltros = Ext.encode(this.CopiarStore(storefiltros));
            var datosFunciones = Ext.encode(this.CopiarStore(storefunciones));
            var datosAgrupamiento = Ext.encode(this.CopiarStore(storeagrupamiento));
            var datosOrdenamiento = Ext.encode(this.CopiarStore(storeorden));
            if(codigo==""){
                Ext.Msg.show({
                    title : 'Confirmación',
                    msg : entorno['Controladores']['ConPrincipal'].buscarParametro(33),
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.WARNING,
                    scope : this,
                    width : 450,
                    fn : function(btn, ev){
                        if (btn == 'yes') {
                            entorno['Vistas']['VisTituloConsulta']=Ext.widget('vistituloconsulta'); 
                            entorno['Vistas']['VisTituloConsulta'].show();  
                        }
                    } 
                });
            }else{
                Ext.Msg.show({
                    title : 'Confirmación',
                    msg : entorno['Controladores']['ConPrincipal'].buscarParametro(32) + " "+titulo +"?",
                    buttons : Ext.Msg.YESNO,
                    icon : Ext.MessageBox.WARNING,
                    scope : this,
                    width : 450,
                    fn : function(btn, ev){
                        if (btn == 'yes') {
                            Ext.Ajax.request({
                              type: "POST",
                              url : 'app/data/php/CrudReportesConf.php',
                              params: {
                                  action:'actreporte',
                                  codigo:codigo,
                                  titulo:titulo,
                                  filtros:filtros,
                                  columnas:campos,
                                  funciones:funciones,
                                  ordenamiento:orden,
                                  agrupamiento:agrupamiento

                              },                                    
                              success: function(response){
                                 var resp = Ext.decode(response.responseText);  
                                 if(resp.success){
                                      Ext.Msg.alert('Operación Exitosa', entorno['Controladores']['ConPrincipal'].buscarParametro(25));
                                 }else{
                                      Ext.Msg.alert('Error', entorno['Controladores']['ConPrincipal'].buscarParametro(35));
                                 }                          
                                        
                              },
                              failure: function(){
                                  Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
                              }
                          });
                        }
                    } 
                });
                 
            }
        },

		    MostrarSqlDinamico:function(){
           var TipoReporte=entorno['Vistas']['VisFormGenerador'].down('combobox[name=cbotpoasign]').getValue();
           if(TipoReporte!=""){
              entorno['Vistas']['VisSqlDinamico']=Ext.widget('vissqldinamico');	
              entorno['Vistas']['VisSqlDinamico'].show();  
           }else{
                Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(50));
           }  

	    	},

		MostrarVentanaConfiguracion:function(){
          
           var TipoReporte=entorno['Vistas']['VisFormGenerador'].down('combobox[name=cbotpoasign]').getValue();
           if(TipoReporte!=""){
                entorno['Vistas']['VisConfiguracion']=Ext.widget('visconfiguracion'); 
                entorno['Vistas']['VisConfiguracion'].down('combobox[name=tabla]').store.extraParams.TipoReporte=TipoReporte;
                entorno['Vistas']['VisConfiguracion'].show();
           }else{
                Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(50));
           }            

		}
    


});