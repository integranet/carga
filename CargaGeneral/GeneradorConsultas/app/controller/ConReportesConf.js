Ext.define('Reporteador.controller.ConReportesConf', {
	extend: 'Ext.app.Controller',

	init: function() {

		this.control({

			'visreportesconf gridpanel[action=dobleclick]': {
				itemdblclick: this.CargarConsulta
			}
            ,'visreportesconf button[action=clickCargar]': {
				click: this.CargarConsulta
			}
			,'visreportesconf button[action=clickEliminar]': {
				click: this.EliminarConsulta
			}
			,'visreportesconf button[action=clickCerrar]': {
				click: this.CerrarVentanaRepoConf
			}		
			
		});

	},

	CargarConsulta:function(){
        
          var selrecord= entorno['Vistas']['VisRepConfigurados'].down('gridpanel[action=dobleclick]').getSelectionModel().getSelection();
	 	  //console.log(selrecord);
	 	    if(selrecord.length>0) {
	 	    	 entorno.Controladores.ConFormGenerador.NuevaConsulta(true,selrecord);      
	        }else{
	        	Ext.Msg.alert('Advertencia', entorno['Controladores']['ConPrincipal'].buscarParametro(20));
	        }
	},    

	CargarColumnas:function(columnas){
          var storegrilla1=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas1]').store;
          var storegrilla2=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store;  
            
             Ext.each(storegrilla1, function(recordgrid1) {
                   for(var i=0;i<recordgrid1.data.length;i++){
                         var campo=recordgrid1.data.items[i].data.nomtabla+"."+recordgrid1.data.items[i].data.nomcampo;
                          
                         if(String(campo.toLowerCase())==String(columnas.toLowerCase())){
                       	    // console.log(abreviatura);
                           var total=storegrilla2.getCount();  
                           storegrilla2.insert(total,Ext.create('Reporteador.model.ModColumnas', {
      					       nomtabla:recordgrid1.data.items[i].data.nomtabla,
      					       nomcampo:recordgrid1.data.items[i].data.nomcampo,
      					       abreviatura:recordgrid1.data.items[i].data.abreviatura,
      					       tipodato:recordgrid1.data.items[i].data.tipodato,
      					       longitud:recordgrid1.data.items[i].data.longitud,
      					       funcion:recordgrid1.data.items[i].data.funcion,
      					       agrupacion:recordgrid1.data.items[i].data.agrupacion
			               }));
                           /*storegrilla1.remove(storegrilla1.getAt(i));
                           i=i-1;*/
                         }         
                   }                             
	        });		   
            
          
	}, 

    EliminarConsulta:function(){
        this.BorrarConsulta();
	},


    BorrarConsulta: function(){
	        var delrecord= entorno['Vistas']['VisRepConfigurados'].down('gridpanel[action=dobleclick]').getSelectionModel().getSelection();
	 	    if(delrecord.length==0) {
				Ext.Msg.alert('Información',  entorno['Controladores']['ConPrincipal'].buscarParametro(18));
			}else{
                
				Ext.Msg.show({
		                title : 'Confirmación',
		                msg :  entorno['Controladores']['ConPrincipal'].buscarParametro(31)+": "+delrecord[0].data.titulo,
		                buttons : Ext.Msg.YESNO,
		                icon : Ext.MessageBox.WARNING,
		                scope : this,
		                width : 450,
		                fn : function(btn, ev){
		                    if (btn == 'yes') {
		                     		                       	                                           
                                      Ext.Ajax.request({
										type: "POST",
								        url : 'app/data/php/CrudReportesConf.php',
										params: {
											action:'eliminarepo',
								            codigo:delrecord[0].data.codigo
										},
                                    
								        success: function(response){
								           var resp = Ext.decode(response.responseText);	
								           if(resp.success){
								           	    entorno['Vistas']['VisRepConfigurados'].down('gridpanel[action=dobleclick]').store.proxy.extraParams.search='{"codigo":"","titulo":""}';
		                                        entorno['Vistas']['VisRepConfigurados'].down('gridpanel[action=dobleclick]').store.load(function(){
                                                }); 
                                                entorno.Controladores.ConFormGenerador.NuevaConsulta();
								           	    Ext.Msg.alert('Operación Exitosa', entorno['Controladores']['ConPrincipal'].buscarParametro(22));
								           }else{
								           	    Ext.Msg.alert('Error', entorno['Controladores']['ConPrincipal'].buscarParametro(23));
								           }								          
				                          
								        },
								        failure: function(){
								            Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
								        }
					               });	                                                         
		                    }
		                }
	            });
		    }
   },

    CerrarVentanaRepoConf:function(){
       entorno['Vistas']['VisRepConfigurados'].close();
	}


});