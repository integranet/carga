Ext.define('Reporteador.controller.ConTituloConsulta', {
	extend: 'Ext.app.Controller',

	init: function() {

		this.control({

			'vistituloconsulta button[action=clickAceptar]': {
				click: this.GrabarConsulta
			}
            ,'vistituloconsulta button[action=clickCancelar]': {
				click: this.CerrarVentanaTitulo
			}
			
		});

	},
  
     GrabarConsulta:function(){
        var codigo=entorno.Consulta.Codigo;
        var titulo=entorno['Vistas']['VisTituloConsulta'].down('textfield[name=TxtTitulo]').getValue();
        var TipoReporte=entorno['Vistas']['VisFormGenerador'].down('combobox[name=cbotpoasign]').getValue();
        var storefiltros= entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFiltros]').store;
        var storecampos=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store;
        var storefunciones=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]').store;
        var storeorden=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridOrden]').store;
        var storeagrupamiento=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridAgrupaciones]').store;
        var datosSelect = Ext.encode(entorno.Controladores.ConFormGenerador.CopiarStore(storecampos));
        var datosFiltros = Ext.encode(entorno.Controladores.ConFormGenerador.CopiarStore(storefiltros));
        var datosFunciones = Ext.encode(entorno.Controladores.ConFormGenerador.CopiarStore(storefunciones));
        var datosAgrupamiento = Ext.encode(entorno.Controladores.ConFormGenerador.CopiarStore(storeagrupamiento));
        var datosOrdenamiento = Ext.encode(entorno.Controladores.ConFormGenerador.CopiarStore(storeorden));
           
       if(titulo==""){
          Ext.Msg.alert('Advertencia',entorno['Controladores']['ConPrincipal'].buscarParametro(34));
       }else{

           Ext.Ajax.request({
                  type: "POST",
                  url : 'app/data/php/CrudReportesConf.php',
                  params: {
                      action:'insreporte',
                      codigo:codigo,
                      titulo:titulo,
                      tiporeporte:TipoReporte,
                      filtros:datosFiltros,
                      columnas:datosSelect,
                      funciones:datosFunciones,
                      ordenamiento:datosOrdenamiento,
                      agrupamiento:datosAgrupamiento
                  },                                    
                  success: function(response){
                     var resp = Ext.decode(response.responseText);  
                     if(resp.success){
                          entorno.Controladores.ConFormGenerador.CargarGrillasReporte(resp.codigo);
                          Ext.Msg.alert('Operación Exitosa', entorno['Controladores']['ConPrincipal'].buscarParametro(25));
                     }else{
                          Ext.Msg.alert('Error', entorno['Controladores']['ConPrincipal'].buscarParametro(35));
                     }      
                  },
                  failure: function(){
                      Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
                  }
              });
       }
    },

    CerrarVentanaTitulo:function(){
       entorno['Vistas']['VisTituloConsulta'].close();
	}


});