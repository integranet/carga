Ext.define('Reporteador.controller.ConConfiguracion', {
	extend: 'Ext.app.Controller',

	init: function() {

		this.control({
            
           'visconfiguracion combobox[name=tabla]': {
				select: this.CargaComboCampos
			}
			,'vislistacampos gridpanel[action=dobleclick]':{
	             itemdblclick: this.SeleccionarCampoTabla
	        }
	        ,'vislistacampos button[action=clickAceptar]':{
	            	click: this.SeleccionarCampoTabla
	         }
	        ,'vislistacampos button[action=clickCancelar]':{
	            	click: this.CerrarVentanaListaCampos
	        }
            ,'visconfiguracion combobox[name=campo]': {
				expand: this.MostrarListaCampos
			}
			,'visconfiguracion button[action=newquery]': {
				click: this.NuevoCampo
			}
			,'visconfiguracion button[action=editquery]': {
				click: this.EditarCampo
			}
			,'visconfiguracion button[action=savequery]': {
				click: this.GuardarCampo
			}
			,'visconfiguracion button[action=undo]': {
				click: this.DeshacerCambios
			}
			,'visconfiguracion button[action=delquery]': {
				click: this.EliminarCampo
			}
			,'visconfiguracion button[action=executequery]': {
				click: this.EjecutarConsulta
			}		
			,'visconfiguracion menu': {
				click: this.BuscarCampos
			}
			,'visbusquedacampos gridpanel[action=dobleclick]':{
	            	itemdblclick: this.SeleccionarCampo
	        }
	        ,'visbusquedacampos button[action=clickAceptar]':{
	            	click: this.SeleccionarCampoBoton
	        }
	         ,'visbusquedacampos button[action=clickCancelar]':{
	            	click: this.AlCerrarVentanaBusquedaCampos
	        }
		    ,'visconfiguracion button[action=firstrecord]': {
				click: this.IrAPrimerRegistro
			}
			,'visconfiguracion button[action=prevrecord]': {
				click: this.IrARegistroAnterior
			}
			,'visconfiguracion button[action=nextrecord]': {
				click: this.IrARegistroSiguiente
			}
			,'visconfiguracion button[action=lastrecord]': {
				click: this.IrAUltimoRegistro
			}
			,'visconfiguracion button[action=cerrarventanaconfig]': {
				click: this.CerrarVentanaConfiguracion
			}
			
		});

	},
	//Funciones Campos Formulario
	CargaComboCampos:function(){

         var tabla=entorno['Vistas']['VisConfiguracion'].down('combobox[name=tabla]').getValue();
         entorno['Vistas']['VisConfiguracion'].down('combobox[name=campo]').store.extraParams.Tabla=String(tabla.toLowerCase());
         entorno['Vistas']['VisConfiguracion'].down('combobox[name=campo]').store.load(function(){
         	 entorno['Vistas']['VisConfiguracion'].down('combobox[name=campo]').setValue("");
	         entorno['Vistas']['VisConfiguracion'].down('combobox[name=tipodato]').setValue("");
	         entorno['Vistas']['VisConfiguracion'].down('textfield[name=abreviatura]').setValue("");
	         entorno['Vistas']['VisConfiguracion'].down('textfield[name=longitud]').setValue("");
         });
	},

	MostrarListaCampos: function(el,e){
		
		el.collapse();
		var tabla=entorno['Vistas']['VisConfiguracion'].down('combobox[name=tabla]').getValue();
	    /*Verifica si ha sido seleccionada alguna tabla */
      
       if(tabla!=""){
         /*Verifica si la ventana ya ha sido renderizada */
         if(entorno['Vistas']['VisListaCampos']==null){
        	entorno['Vistas']['VisListaCampos'] = Ext.create('Reporteador.view.VisListaCampos');
            entorno['Vistas']['VisListaCampos'].show();
            entorno['Vistas']['VisListaCampos'].on('close',function(){
          	   entorno['Controladores']['ConPrincipal'].destruirObjeto('VisListaCampos');
            },this);
			
	       // entorno['Vistas']['VisListaPaises'].down('gridpanel[action=dobleclick]').store.proxy.extraParams.idparametro=4;
			entorno['Vistas']['VisListaCampos'].down('gridpanel[action=dobleclick]').store.proxy.extraParams.search='{"Valor":""}';
			entorno['Vistas']['VisListaCampos'].down('gridpanel[action=dobleclick]').store.load(function(){
					//mask.close();
			});
	    }
      }else{
      	  Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(49));
      }
	},

	SeleccionarCampoTabla:function(){
        var el = entorno['Vistas']['VisListaCampos'];
	    var selrecords= el.down('gridpanel[action=dobleclick]').getSelectionModel().getSelection();
	   
	    if(selrecords.length === 0){
				Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(18));
	    }else{
	    	    
		        entorno['Vistas']['VisConfiguracion'].down('combobox[name=campo]').setValue(selrecords[0].get('columna'));
		        entorno['Vistas']['VisConfiguracion'].down('combobox[name=tipodato]').setValue(selrecords[0].get('tipodato'));      
		        entorno['Vistas']['VisConfiguracion'].down('textfield[name=longitud]').setValue(selrecords[0].get('longitud'));      
		        entorno['Vistas']['VisListaCampos'].close();
	    }
	},

   CerrarVentanaListaCampos:function(){
		 entorno['Vistas']['VisListaCampos'].close();
   },
   

	//Funciones Botones
    HabilitaCampos:function(estado,op){
        if(op!=1){
        	  entorno['Vistas']['VisConfiguracion'].down('combobox[name=tabla]').setReadOnly(!estado);
              entorno['Vistas']['VisConfiguracion'].down('combobox[name=campo]').setReadOnly(!estado);
        }else{
              entorno['Vistas']['VisConfiguracion'].down('combobox[name=tabla]').setReadOnly(estado);
              entorno['Vistas']['VisConfiguracion'].down('combobox[name=campo]').setReadOnly(estado);
         }
       
        /* entorno['Vistas']['VisConfiguracion'].down('combobox[name=tipodato]').setReadOnly(estado);
         entorno['Vistas']['VisConfiguracion'].down('textfield[name=longitud]').setReadOnly(estado);*/
         entorno['Vistas']['VisConfiguracion'].down('textfield[name=abreviatura]').setReadOnly(estado);
         entorno['Vistas']['VisConfiguracion'].down('combobox[name=filtro]').setReadOnly(estado);
         entorno['Vistas']['VisConfiguracion'].down('combobox[name=funcion]').setReadOnly(estado);
         entorno['Vistas']['VisConfiguracion'].down('combobox[name=agrupacion]').setReadOnly(estado);
    },

     LimpiaCampos:function(){

         entorno['Vistas']['VisConfiguracion'].down('combobox[name=tabla]').setValue("");
         entorno['Vistas']['VisConfiguracion'].down('combobox[name=campo]').setValue("");
         entorno['Vistas']['VisConfiguracion'].down('combobox[name=tipodato]').setValue("");
         entorno['Vistas']['VisConfiguracion'].down('textfield[name=abreviatura]').setValue("");
         entorno['Vistas']['VisConfiguracion'].down('textfield[name=longitud]').setValue("");
         entorno['Vistas']['VisConfiguracion'].down('combobox[name=filtro]').setValue("");
         entorno['Vistas']['VisConfiguracion'].down('combobox[name=funcion]').setValue("");
         entorno['Vistas']['VisConfiguracion'].down('combobox[name=agrupacion]').setValue("");
    },
   
    HabilitaControles:function(){
       
	        entorno['Vistas']['VisConfiguracion'].down('button[action=savequery]').setDisabled(true); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=undo]').setDisabled(true); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=editquery]').setDisabled(false); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=delquery]').setDisabled(false); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=search]').setDisabled(false); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=executequery]').setDisabled(false); 
	        if(entorno['Stores']['StoListaCampos'].getCount()!=1){
		        entorno['Vistas']['VisConfiguracion'].down('button[action=firstrecord]').setDisabled(false); 
		        entorno['Vistas']['VisConfiguracion'].down('button[action=prevrecord]').setDisabled(false); 
		        entorno['Vistas']['VisConfiguracion'].down('button[action=nextrecord]').setDisabled(false); 
		        entorno['Vistas']['VisConfiguracion'].down('button[action=lastrecord]').setDisabled(false); 
            }else{
            	entorno['Vistas']['VisConfiguracion'].down('button[action=firstrecord]').setDisabled(true); 
		        entorno['Vistas']['VisConfiguracion'].down('button[action=prevrecord]').setDisabled(true); 
		        entorno['Vistas']['VisConfiguracion'].down('button[action=nextrecord]').setDisabled(true); 
		        entorno['Vistas']['VisConfiguracion'].down('button[action=lastrecord]').setDisabled(true); 
            }	       
    },

    DeshabilitaControles:function(){
           
	        entorno['Vistas']['VisConfiguracion'].down('button[action=savequery]').setDisabled(false); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=undo]').setDisabled(false); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=editquery]').setDisabled(true); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=delquery]').setDisabled(true); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=search]').setDisabled(true); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=executequery]').setDisabled(true); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=firstrecord]').setDisabled(true); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=prevrecord]').setDisabled(true); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=nextrecord]').setDisabled(true); 
	        entorno['Vistas']['VisConfiguracion'].down('button[action=lastrecord]').setDisabled(true); 
    },



    CargarCamposTodos:function(){
                    
            entorno['Stores']['StoListaCampos']=entorno['Controladores']['ConPrincipal'].getStore('stoTotalCampos');
            var Mask = new Ext.LoadMask(entorno['Vistas']['VisConfiguracion'], {msg:entorno['Controladores']['ConPrincipal'].buscarParametro(20)});
            Mask.show();
            var me=this;
             entorno['Stores']['StoListaCampos'].extraParams.TipoReporte=entorno['Vistas']['VisFormGenerador'].down('combobox[name=cbotpoasign]').getValue();
             entorno['Stores']['StoListaCampos'].load(function(){
             	
             	if(entorno['Stores']['StoListaCampos'].getCount()>0){
             		me.HabilitaCampos(true);
             		entorno['Vistas']['VisConfiguracion'].down('button[action=editquery]').setDisabled(false);
             		entorno['Vistas']['VisConfiguracion'].down('button[action=delquery]').setDisabled(false); 
             		entorno['Vistas']['VisConfiguracion'].down('button[action=executequery]').setDisabled(false);  
                   	entorno['Vistas']['VisConfiguracion'].down('form').loadRecord(entorno['Stores']['StoListaCampos'].data.first());
             	    if(entorno['Stores']['StoListaCampos'].getCount()!=1){
		             	    entorno['Vistas']['VisConfiguracion'].down('button[action=nextrecord]').setDisabled(false);
		             	    entorno['Vistas']['VisConfiguracion'].down('button[action=lastrecord]').setDisabled(false);
             	    }else{
             	    	    entorno['Vistas']['VisConfiguracion'].down('button[action=prevrecord]').setDisabled(true);
		             	    entorno['Vistas']['VisConfiguracion'].down('button[action=firstrecord]').setDisabled(true);
             	    	    entorno['Vistas']['VisConfiguracion'].down('button[action=nextrecord]').setDisabled(true);
		             	    entorno['Vistas']['VisConfiguracion'].down('button[action=lastrecord]').setDisabled(true);
             	    }             	
                    entorno['Vistas']['VisConfiguracion'].down('textfield[name=indice]').setValue(0);
             	}
             	Mask.hide();
             
             });
    },

	NuevoCampo:function(){
		entorno['Configuracion']['EsNuevo']=true;
		this.LimpiaCampos();
        this.HabilitaCampos(false,1);
        this.DeshabilitaControles();
	},

    EditarCampo:function(){
      this.HabilitaCampos(false,2);
      this.DeshabilitaControles();
	},

	GuardarCampo:function(){
         var form = entorno['Vistas']['VisConfiguracion'].down('form').getForm();
		 var TipoReporte=entorno['Vistas']['VisFormGenerador'].down('combobox[name=cbotpoasign]').getValue();
		 var estadotabla = entorno['Vistas']['VisConfiguracion'].down('combobox[name=tabla]').readOnly;
		 var estadocampo= entorno['Vistas']['VisConfiguracion'].down('combobox[name=campo]').readOnly;
         var accion="",me=this;

         if(estadotabla && estadocampo){
            accion='editarcampo';
         }else{
            accion='guardarcampo';
         }
       //  var idempresa=form.findField('idempresa').getRawValue(); 
		  if(form.isValid()){
            
               form.submit({

				url: 'app/data/php/CrudCampos.php',
				waitMsg:entorno['Controladores']['ConPrincipal'].buscarParametro(37),
				submitEmptyText:false,
				params: {
					action:accion,
					tiporeporte:TipoReporte
				},
				success: function(opts, response) {

					if (this.result.success) {
						entorno['Vistas']['VisConfiguracion'].show();
						me.CargarCamposTodos();
					    //me.InsertarCampoStoreColumnas();                   
						var msg=Ext.Msg.alert('Advertencia',entorno['Controladores']['ConPrincipal'].buscarParametro(28));
						/*Funcion usada para colocar alert delante de ventana modal*/
						Ext.defer(function () {
						    msg.toFront();
					    }, 50);
					   
						//entorno['Vistas']['VisCrudEmpresas'].down('gridpanel[name=gridcrudempresas]').store.load();
					}else{
						Ext.Msg.alert('Resultado',entorno['Controladores']['ConPrincipal'].buscarParametro(29));
					}

			    },failure: function(){
			            Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
			    }
	    	 });
	     	
			
		}else{
			 Ext.Msg.alert('Advertencia',entorno['Controladores']['ConPrincipal'].buscarParametro(27));
		}
  
	},

	DeshacerCambios:function(){
		if(entorno['Configuracion']['EsNuevo']){
		 	 this.LimpiaCampos();
		} 
      
        this.HabilitaCampos(true,1);
        this.HabilitaControles();
	},

	EjecutarConsulta:function(){
      console.log("Ha pulsado en el boton Ejecutar Consulta");
	},

	EliminarCampo:function(){
       this.BorrarCampo();
	},

   BuscarCampos:function(el,item,e,eOpts){
       if(item.text==entorno['Controladores']['ConPrincipal'].buscarParametro(16)){
            this.CargarCamposTodos();
       }
       if(item.text==entorno['Controladores']['ConPrincipal'].buscarParametro(17)){
            
            var mask = Ext.Msg.wait( "Espere por favor", "Cargando", null );
		    entorno['Vistas']['VisBusquedaCampos']=Ext.widget('visbusquedacampos');
		    var tiporeporte=entorno['Vistas']['VisFormGenerador'].down('combobox[name=cbotpoasign]').getValue();
		    entorno['Vistas']['VisBusquedaCampos'].down('gridpanel[action=dobleclick]').store.proxy.extraParams.TipoReporte=tiporeporte;
            entorno['Vistas']['VisBusquedaCampos'].down('gridpanel[action=dobleclick]').store.proxy.extraParams.search='{"tabla":"","campo":"","abreviatura":""}';
		    entorno['Vistas']['VisBusquedaCampos'].down('gridpanel[action=dobleclick]').store.load(function(){
						mask.close();
		    });

       }
	},


	  BorrarCampo: function(){
           
	 	     var idcamporeporte= entorno['Vistas']['VisConfiguracion'].down('textfield[name=idcamporeporte]').getValue();
	 	    //var storeEmpresa= entorno['Vistas']['VisCrudEmpresas'].down('gridpanel[name=gridcrudempresas]').store;
	         if(idcamporeporte == ""){
				Ext.Msg.alert('Información',  entorno['Controladores']['ConPrincipal'].buscarParametro(19));
			}else{
                
				Ext.Msg.show({
		                title : 'Confirmación',
		                msg :  entorno['Controladores']['ConPrincipal'].buscarParametro(21),
		                buttons : Ext.Msg.YESNO,
		                icon : Ext.MessageBox.WARNING,
		                scope : this,
		                width : 450,
		                fn : function(btn, ev){
		                    if (btn == 'yes') {
		                      //  storeEmpresa.remove(delrecord);  
		                                                      
		                        if(idcamporeporte!="") {
		                       	    var me=this;                                     
                                    Ext.Ajax.request({
										type: "POST",
								        url : 'app/data/php/CrudCampos.php',
										params: {
											action:'eliminarcampo',
								            idcamporeporte:idcamporeporte
										},                                    
								        success: function(response){
								           var resp = Ext.decode(response.responseText);	
								           if(resp.success){
								           	    me.RemoverFromStoreColumnas();
                                               // me.IrARegistroSiguiente();
                                                me.CargarCamposTodos();
                                                Ext.Msg.alert('Operación Exitosa', entorno['Controladores']['ConPrincipal'].buscarParametro(22));
								           }else{
								           	   // Ext.Msg.alert('Error', entorno['Controladores']['ConPrincipal'].buscarParametro(30));
								           	   Ext.Msg.alert('Error', resp.razon);
								           }						          
				                          
								        },
								        failure: function(){
								            Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
								        }
					                });	                             
		                       }
                            
		                    }
		                } 
	            });
		    }
   },

	SeleccionarCampo: function( el, record, item, index,  e, Opts){
	     // entorno['Vistas']['VisFormEmpresas'].down('combobox[name=sector]').setValue(record.get('IdValorParametro'));
	      this.LimpiaCampos();
	      entorno['Vistas']['VisConfiguracion'].down('form').loadRecord(record);
	     // var formCampos = entorno['Vistas']['VisConfiguracion'].down('form').getForm();
	      entorno['Vistas']['VisBusquedaCampos'].close();
	},

	 SeleccionarCampoBoton: function( el, record, item, index,  e, Opts){

	 	var selrecord= entorno['Vistas']['VisBusquedaCampos'].down('gridpanel[action=dobleclick]').getSelectionModel().getSelection();
	   
	    if(selrecord.length === 0){
				Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(18));
	    }else{
		      //entorno['Vistas']['VisFormEmpresas'].down('combobox[name=sector]').setValue(selrecord[0].get('IdValorParametro'));
		      this.LimpiaCampos();
		      entorno['Vistas']['VisConfiguracion'].down('form').loadRecord(selrecord[0]);
		      entorno['Vistas']['VisBusquedaCampos'].close();
	    }
	},

   AlCerrarVentanaBusquedaCampos:function(){
		 entorno['Vistas']['VisBusquedaCampos'].close();
   },
   
    InsertarCampoStoreColumnas:function(){
         var nomtabla = entorno['Vistas']['VisConfiguracion'].down('combobox[name=tabla]').getValue();
		 var nomcampo= entorno['Vistas']['VisConfiguracion'].down('combobox[name=campo]').getValue();
		 var tipodato= entorno['Vistas']['VisConfiguracion'].down('combobox[name=tipodato]').getValue();
         var abreviatura=entorno['Vistas']['VisConfiguracion'].down('textfield[name=abreviatura]').getValue();
         var longitud=entorno['Vistas']['VisConfiguracion'].down('textfield[name=longitud]').getValue();
         var filtro=entorno['Vistas']['VisConfiguracion'].down('combobox[name=filtro]').getValue();
         var funcion=entorno['Vistas']['VisConfiguracion'].down('combobox[name=funcion]').getValue();
         var agrupacion=entorno['Vistas']['VisConfiguracion'].down('combobox[name=agrupacion]').getValue();
        
         var storegrilla1=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas1]').store;
               
          var total=storegrilla1.getCount();
          storegrilla1.insert(total,Ext.create('Reporteador.model.ModColumnas', {
			        nomtabla:nomtabla,
			        nomcampo:nomcampo,
			        abreviatura:abreviatura,
			        tipodato:tipodato,
			        longitud:longitud,
			        funcion:funcion,
			        agrupacion:agrupacion
          }));
    },

    RemoverFromStoreColumnas:function(){
            var nomtabla = entorno['Vistas']['VisConfiguracion'].down('combobox[name=tabla]').getValue();
		    var nomcampo= entorno['Vistas']['VisConfiguracion'].down('combobox[name=campo]').getValue();
            var storegrilla1=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas1]').store;

	        Ext.each(storegrilla1, function(recordgrid1) {
                   for(var i=0;i<recordgrid1.data.length;i++){
                   	  if(recordgrid1.data.items[i].data.nomtabla==nomtabla && recordgrid1.data.items[i].data.nomcampo==nomcampo){
                   	  	  storegrilla1.remove(storegrilla1.getAt(i));
                   	  	  i=i-1;
                   	  }                     
                   }
            });
    },

	IrAPrimerRegistro:function(){

      this.LimpiaCampos();
      entorno['Vistas']['VisConfiguracion'].down('form').loadRecord(entorno['Stores']['StoListaCampos'].data.first());
      entorno['Vistas']['VisConfiguracion'].down('button[action=nextrecord]').setDisabled(false);
      entorno['Vistas']['VisConfiguracion'].down('button[action=lastrecord]').setDisabled(false);
      entorno['Vistas']['VisConfiguracion'].down('button[action=prevrecord]').setDisabled(true);
      entorno['Vistas']['VisConfiguracion'].down('button[action=firstrecord]').setDisabled(true);
	  entorno['Vistas']['VisConfiguracion'].down('textfield[name=indice]').setValue(0);

	},

    IrARegistroAnterior:function(){

      this.LimpiaCampos();
      var indice=entorno['Vistas']['VisConfiguracion'].down('textfield[name=indice]').getValue()-1;
      entorno['Vistas']['VisConfiguracion'].down('form').loadRecord(entorno['Stores']['StoListaCampos'].data.items[indice]);
	  entorno['Vistas']['VisConfiguracion'].down('textfield[name=indice]').setValue(indice);
            	   
	   if(indice==0){    
		      entorno['Vistas']['VisConfiguracion'].down('button[action=nextrecord]').setDisabled(false);
		      entorno['Vistas']['VisConfiguracion'].down('button[action=lastrecord]').setDisabled(false);
		      entorno['Vistas']['VisConfiguracion'].down('button[action=prevrecord]').setDisabled(true);
		      entorno['Vistas']['VisConfiguracion'].down('button[action=firstrecord]').setDisabled(true);
       }else{
       	      entorno['Vistas']['VisConfiguracion'].down('button[action=nextrecord]').setDisabled(false);
		      entorno['Vistas']['VisConfiguracion'].down('button[action=lastrecord]').setDisabled(false);
		      entorno['Vistas']['VisConfiguracion'].down('button[action=prevrecord]').setDisabled(false);
		      entorno['Vistas']['VisConfiguracion'].down('button[action=firstrecord]').setDisabled(false);
       }

   	},

	IrAUltimoRegistro:function(){

      this.LimpiaCampos();
      entorno['Vistas']['VisConfiguracion'].down('form').loadRecord(entorno['Stores']['StoListaCampos'].getAt(entorno['Stores']['StoListaCampos'].getCount()-1));
 	  entorno['Vistas']['VisConfiguracion'].down('button[action=prevrecord]').setDisabled(false);
      entorno['Vistas']['VisConfiguracion'].down('button[action=firstrecord]').setDisabled(false);
      entorno['Vistas']['VisConfiguracion'].down('button[action=nextrecord]').setDisabled(true);
      entorno['Vistas']['VisConfiguracion'].down('button[action=lastrecord]').setDisabled(true);
      entorno['Vistas']['VisConfiguracion'].down('textfield[name=indice]').setValue(entorno['Stores']['StoListaCampos'].getCount()-1); 
 
 	},

	IrARegistroSiguiente:function(){

       this.LimpiaCampos();
       var indice=entorno['Vistas']['VisConfiguracion'].down('textfield[name=indice]').getValue()+1;
       entorno['Vistas']['VisConfiguracion'].down('form').loadRecord(entorno['Stores']['StoListaCampos'].data.items[indice]);
	   entorno['Vistas']['VisConfiguracion'].down('textfield[name=indice]').setValue(indice);
      
       if(indice==entorno['Stores']['StoListaCampos'].getCount()-1){
            entorno['Vistas']['VisConfiguracion'].down('button[action=prevrecord]').setDisabled(false);
            entorno['Vistas']['VisConfiguracion'].down('button[action=firstrecord]').setDisabled(false);
            entorno['Vistas']['VisConfiguracion'].down('button[action=nextrecord]').setDisabled(true);
            entorno['Vistas']['VisConfiguracion'].down('button[action=lastrecord]').setDisabled(true);
       }else{
       	     entorno['Vistas']['VisConfiguracion'].down('button[action=prevrecord]').setDisabled(false);
             entorno['Vistas']['VisConfiguracion'].down('button[action=firstrecord]').setDisabled(false);
             entorno['Vistas']['VisConfiguracion'].down('button[action=nextrecord]').setDisabled(false);
             entorno['Vistas']['VisConfiguracion'].down('button[action=lastrecord]').setDisabled(false);
       }
       
    },   

	CerrarVentanaConfiguracion:function(){
		 entorno['Vistas']['VisConfiguracion'].close(); 
	}	

});