Ext.define('CrudEmp.controller.ConCrudEmpresas',{
	 extend: 'Ext.app.Controller',
	   	
	 init:function (){
	 	
         	this.control({
                 'viscrudempresas button[action=addempresa]':{
	            	click: this.addEmpresa
	            },
	             'viscrudempresas button[action=editempresa]':{
	            	click: this.editEmpresa
	            },
	             'viscrudempresas button[action=delempresa]':{
	            	click: this.delEmpresa
	            },
	             'viscrudempresas button[action=actgrilla]':{
	            	click: this.actGrillaEmpresa
	            },
	            'viscrudempresas button[action=adminanexo]':{
	            	click: this.MostrarGrillaAnexos
	            },
	            'viscrudempresas button[action=irparam]':{
	            	click: this.MostrarParametros
	            }


	        })
   },  

   addEmpresa:function(){
   	 
   	   entorno['Vistas']['VisFormEmpresas']=Ext.create('CrudEmp.view.VisFormEmpresas',{
										 	
	  });
	  entorno['Vistas']['VisFormEmpresas'].show();
   },

   editEmpresa:function(){
   	 
      var records= entorno['Vistas']['VisCrudPrincipal'].down('gridpanel[name=gridcrudempresas]').getSelectionModel().getSelection();
	   
	    if(records.length === 0){
				Ext.Msg.alert('Información', entorno['Controladores']['ConCrudPrincipal'].buscarParametro(19));
	    }else{
               if(records.length === 1){
               	    entorno['Vistas']['VisFormEmpresas']=Ext.create('CrudEmp.view.VisFormEmpresas',{
										 	
	                });
                   this.llenaFormEmpresa(entorno['Vistas']['VisFormEmpresas'],records);
                    entorno['Vistas']['VisFormEmpresas'].show();
                  /*  entorno['Vistas']['VisFormEmpresas']=Ext.create('CrudEmp.view.VisFormEmpresas',{
										 	
					 });
				   	entorno['Vistas']['VisFormEmpresas'].show();*/
               }else{
               	    Ext.Msg.alert('Error', 'Más de una empresa seleccionada');
               }
		}	 	
   	
   },
   delEmpresa:function(){
   	 	   this.BorrarEmpresa();
   },
   actGrillaEmpresa:function(){
    	entorno['Vistas']['VisCrudEmpresas'].down('gridpanel[name=gridcrudempresas]').store.load();
   },

    llenaFormEmpresa:function(el,record){
    	 
                var ruta=entorno['Controladores']['ConCrudPrincipal'].buscarParametro(26);
                var nomarchivo=entorno['Controladores']['ConCrudPrincipal'].buscarParametro(34);

                el.down('textfield[name=idempresa]').setValue(record[0].get('idempresa'));
                el.down('textfield[name=nit]').setValue(record[0].get('nit'));
                el.down('textfield[name=razonsocial]').setValue(record[0].get('razon'));
		        el.down('combobox[name=sector]').setValue(record[0].get('idsector'));
		        el.down('textfield[name=email]').setValue(record[0].get('mail'));
		        el.down('textfield[name=telefono]').setValue(record[0].get('tel'));
		        el.down('textfield[name=contacto]').setValue(record[0].get('contacto'));
		        el.down('textfield[name=sitioweb]').setValue(record[0].get('sitio'));
		        el.down('textfield[name=direccion]').setValue(record[0].get('direccion')); 
		        el.down('textfield[name=URLinformacionAdicional]').setValue(record[0].get('urlinfoadic'));
		        el.down('textfield[name=archivologo]').setValue(record[0].get('logo'));   
		        
		        if(record[0].get('logo')!=""){
		              el.down('image[name=logoEmpresa]').setSrc(ruta+record[0].get('logo'));
		        }else{
		        	  el.down('image[name=logoEmpresa]').setSrc(ruta+nomarchivo);   
				} 						       
		      
			    if(record[0].get('actividad')!=""){
			       	  el.down('htmleditor[name=ActividadPrincipal]').setValue(record[0].get('actividad'));
			    }

			    if(record[0].get('productos')!=""){
          	    	   el.down('htmleditor[name=Productos]').setValue(record[0].get('productos'));
			    }

			    if(record[0].get('beneficios')!=""){
                   	 el.down('htmleditor[name=Beneficios]').setValue(record[0].get('beneficios'));
			    }			   

	},


   MostrarGrillaAnexos:function(){
       var selrecord= entorno['Vistas']['VisCrudEmpresas'].down('gridpanel[name=gridcrudempresas]').getSelectionModel().getSelection();
       if(selrecord.length === 0){
				Ext.Msg.alert('Información', entorno['Controladores']['ConCrudPrincipal'].buscarParametro(19));
	   }else{
           entorno['Stores']['StoAnexos']=Ext.create('CrudEmp.store.stoAnexoEmpresa'); 
	       entorno['Vistas']['VisGrillaAnexos']=Ext.create('CrudEmp.view.VisGrillaAnexos',{
				name:selrecord[0].get('idempresa'),
                myStoreAnexo:entorno['Stores']['StoAnexos']
		  });
        
	      entorno['Vistas']['VisGrillaAnexos'].down('gridpanel[name=gdAnexo]').store.proxy.extraParams.IdFuente = selrecord[0].get('idempresa');
		  entorno['Vistas']['VisGrillaAnexos'].down('gridpanel[name=gdAnexo]').store.load();
		  entorno['Vistas']['VisGrillaAnexos'].show();
	  }
							
		  
   },
   MostrarParametros:function(){
   	 var UrlParam=entorno['Controladores']['ConCrudPrincipal'].buscarParametro(33);
     window.open(UrlParam, "_blank");
   },

    BorrarEmpresa: function(){
           
	 	    var delrecord= entorno['Vistas']['VisCrudEmpresas'].down('gridpanel[name=gridcrudempresas]').getSelectionModel().getSelection();
	 	    var storeEmpresa= entorno['Vistas']['VisCrudEmpresas'].down('gridpanel[name=gridcrudempresas]').store;
	         if(delrecord.length === 0){
				Ext.Msg.alert('Información',  entorno['Controladores']['ConCrudPrincipal'].buscarParametro(19));
			}else{
                
				Ext.Msg.show({
		                title : 'Confirmación',
		                msg :  entorno['Controladores']['ConCrudPrincipal'].buscarParametro(18),
		                buttons : Ext.Msg.YESNO,
		                icon : Ext.MessageBox.WARNING,
		                scope : this,
		                width : 450,
		                fn : function(btn, ev){
		                    if (btn == 'yes') {
		                        storeEmpresa.remove(delrecord);  
		                        var idempresa=delrecord[0].get('idempresa');
                               
		                        if(idempresa!="") {
		                       	                                           
                                      Ext.Ajax.request({
										type: "POST",
								        url : 'app/data/php/CrudEmpresas.php',
										params: {
											action:'eliminaremp',
								            id:idempresa
										},
                                    
								        success: function(response){
								           var resp = Ext.decode(response.responseText);	
								           if(resp.success){
                                                Ext.Msg.alert('Operación Exitosa', entorno['Controladores']['ConCrudPrincipal'].buscarParametro(17));
								           }else{
								           	    Ext.Msg.alert('Error', entorno['Controladores']['ConCrudPrincipal'].buscarParametro(30));
								           }								          
				                          
								        },
								        failure: function(){
								            alert('el servidor no ha respondido la consulta');
								        }
					               });

	                             
		                       }
                            
		                    }
		                } 
	            });
		    }
   }
   
});