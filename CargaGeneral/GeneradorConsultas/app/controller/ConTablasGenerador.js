Ext.define('Reporteador.controller.ConTablasGenerador', {
	extend: 'Ext.app.Controller',

	init: function() {
        var me=this;
      
        //Control Sin Selector
      
       /* entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasbase]').editingPlugin.on('edit',function(){
        		me.ActualizaInfoTablasBase();});

        entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').editingPlugin.on('edit',function(){
        		me.ActualizaInfoTablasMaestras();});  */

		this.control({
           
			'vistablasgen gridpanel[name=gridtablasbase]': {
				itemclick: this.MuestraTablasMaestras
			},
			'vistablasgen button[action=addtblbase]': {
	                click: this.addtablabase
	        },
	        'vistablasgen button[action=edittblbase]': {
	                click: this.edittablabase
	        },	           
	        'vistablasgen button[action=deltblbase]': {
	                click: this.deltablabase
	        },
	        'vistablasgen button[action=actgridbase]': {
	                click: this.ActualizaTablaBase
	        },
	        'vistablasgen button[action=addtblmaestra]': {
	                click: this.addtablamaestra
            },
	        'vistablasgen button[action=edittblmaestra]': {
	                click: this.edittablamaestra
	        },	            
	        'vistablasgen button[action=deltblmaestra]': {
	                click: this.deltablamaestra
	        },
	        'vistablasgen button[action=actgridmaestra]': {
	                click: this.ActualizaTablaMaestra
	        }  
         			
		});

	},
    

	   addtablabase: function(){
	   	   this.AdicionaTablabase();	   	  
	   },

	   edittablabase: function(){
		    this.HabilitaCamposTablaBase();	
	   },

	   deltablabase: function(){
			this.BorraTablaBase();
		},

		ActualizaTablaBase: function(){
			 entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').store.removeAll();
			 entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasbase]').store.removeAll();
			 entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasbase]').store.load();
		},

	    addtablamaestra: function(){
	    	this.AdicionaTablaMaestra();
		},

	   edittablamaestra: function(){
		   this.HabilitaCamposTablaMaestra();
	   },

	   deltablamaestra: function(){
			this.BorraTablaMaestra();
		},

	  ActualizaTablaMaestra: function(){
	   	var selEnc= entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasbase]').getSelectionModel().getSelection();
	    entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').store.removeAll();
	    if(selEnc.length != 0){
	    	    entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').store.proxy.extraParams.idtablaenc=selEnc[0].get('idtablaenc');
			    entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').store.load();
    	}	   	 
	 },

	 AdicionaTablabase:function(){
        //Cancelamos la edicion
         entorno['Editores']['EditorEncabezado'][0].cancelEdit();
 
		//agregamos un nuevo registro al comienzo 
		entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasbase]').store.insert(0, Ext.create('Reporteador.model.ModTablasEnc'));
				
		entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasbase]').getSelectionModel().select(0);
        var selrecords= entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasbase]').getSelectionModel().getSelection();
		//Habilitamos la edicion para el registro recien añadido	
		entorno['Editores']['EditorEncabezado'][0].startEdit(selrecords[0],1);
	 },
       
       HabilitaCamposTablaBase: function(){
           
	 	    var selrecords= entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasbase]').getSelectionModel().getSelection();
	 	  
	        if(selrecords.length === 0){
				Ext.Msg.alert('Información', entorno['Controladores']['ConPrincipal'].buscarParametro(18));
			}else{
                if(selrecords.length === 1){
               	       entorno['Editores']['EditorEncabezado'][0].startEdit(selrecords[0],1);                                  
                }else{
               	    Ext.Msg.alert('Error',entorno['Controladores']['ConPrincipal'].buscarParametro(51));
               }
		    }	
	},

	ActualizaInfoTablasBase:function(){
              
                 var records= entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasbase]').getSelectionModel().getSelection();
		  	   
		    	 var me=this;
                 Ext.Ajax.request({
			        type: "POST",
			        url : 'app/data/php/CrudTablasReporte.php',
			        params:{
			            action:'acttablabase',
			            idtablaenc:records[0].get('idtablaenc'),
			            tablabase:records[0].get('tablabase'),
			            tiporeporte:records[0].get('idtiporeporte'),
			            idestado:records[0].get('idestado')
			        },
			     
			        success: function(response){
			             	
			            var resp = Ext.decode(response.responseText); 
			           
			        	 if(resp.success){ 
			        	 	  me.ActualizaTablaBase();
	                          Ext.Msg.alert('Operación Exitosa', entorno['Controladores']['ConPrincipal'].buscarParametro(54));       
	                     }else{
	                          Ext.Msg.alert('Error', entorno['Controladores']['ConPrincipal'].buscarParametro(35));
	                     } 			          
			        },
			        failure: function(){
			             Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
			        }
               });    	    
	},

	BorraTablaBase:function(){
            var delrecords= entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasbase]').getSelectionModel().getSelection();
	 	    var storeTablaBase= entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasbase]').store;
	        if(delrecords.length === 0){
				Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(18));
			}else{
                var me=this;
				Ext.Msg.show({
		                title : 'Confirmación',
		                msg :entorno['Controladores']['ConPrincipal'].buscarParametro(53),
		                buttons : Ext.Msg.YESNO,
		                icon : Ext.MessageBox.WARNING,
		                scope : this,
		                width : 450,
		                fn : function(btn, ev){
		                    if (btn == 'yes') {
		                        storeTablaBase.remove(delrecords);  
		                        var idtablaenc=delrecords[0].get('idtablaenc');
                               
		                        if(idtablaenc!="") {
		                       	     Ext.Ajax.request({
                 
								        type: "POST",
								        url : 'app/data/php/CrudTablasReporte.php',
								        params:{
								            action:'deltablabase',
								            idtablaenc:idtablaenc
								        },								        
								        success: function(response){	
								        	 var resp = Ext.decode(response.responseText); 
								        	 if(resp.success){ 
						                          Ext.Msg.alert('Operación Exitosa', entorno['Controladores']['ConPrincipal'].buscarParametro(22));
						                           me.ActualizaTablaBase();
						                     }else{
						                          Ext.Msg.alert('Error', entorno['Controladores']['ConPrincipal'].buscarParametro(35));
						                     } 								           
								        },
								        failure: function(){
								            Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
								        }
					               });

	                             
		                       }
                            
		                    }
		                } 
	            });
		    }
	},

	 AdicionaTablaMaestra:function(){
        //Cancelamos la edicion
         entorno['Editores']['EditorDetalle'][0].cancelEdit();
          var selrecordsenc= entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasbase]').getSelectionModel().getSelection();
	        if(selrecordsenc.length === 0){
				Ext.Msg.alert('Información', entorno['Controladores']['ConPrincipal'].buscarParametro(52));
			}else{
               if(selrecordsenc.length === 1){
               	   if(selrecordsenc[0].get('idtablaenc')!=""){
               	      	//agregamos un nuevo registro al comienzo 
						entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').store.insert(0, Ext.create('Reporteador.model.ModTablasDet', {
  						       idtablaenc:selrecordsenc[0].get('idtablaenc')}));
								
						entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').getSelectionModel().select(0);
				        var selrecords= entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').getSelectionModel().getSelection();
						//Habilitamos la edicion para el registro recien añadido					
				 		entorno['Editores']['EditorDetalle'][0].startEdit(selrecords[0],1);
                    }
               }else{
               	    Ext.Msg.alert('Error',entorno['Controladores']['ConPrincipal'].buscarParametro(51));
               }
		    }	
	
	 },

    HabilitaCamposTablaMaestra: function(){
		    var selrecords= entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').getSelectionModel().getSelection();
	        if(selrecords.length === 0){
				Ext.Msg.alert('Información', entorno['Controladores']['ConPrincipal'].buscarParametro(18));
			}else{
               if(selrecords.length === 1){
               	      entorno['Editores']['EditorDetalle'][0].startEdit(selrecords[0],0);		            
               }else{
               	    Ext.Msg.alert('Error',entorno['Controladores']['ConPrincipal'].buscarParametro(51));
               }
		    }	   
		  
	},

	ActualizaInfoTablasMaestras:function(){
       
        var records= entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').getSelectionModel().getSelection();
	  
                var me=this;
                Ext.Ajax.request({
			        type: "POST",
			        url : 'app/data/php/CrudTablasReporte.php',
			        params:{
			            action:'acttablamaestra',
			            idtabladet:records[0].get('idtabladet'),
			            idtablaenc:records[0].get('idtablaenc'),
			            tablamaestra:records[0].get('tablamaestra'),
                        campotblbase:records[0].get('campotblbase'),
                        campotblmaestra:records[0].get('campotblmaestra'),
			            idestado:records[0].get('idestado')
			        },
			        success: function(response){
                           var resp = Ext.decode(response.responseText);   
			         	    if(resp.success){ 
			         		  	   me.ActualizaTablaMaestra();
		                           Ext.Msg.alert('Operación Exitosa',entorno['Controladores']['ConPrincipal'].buscarParametro(54));
		                    }else{
		                          Ext.Msg.alert('Error', entorno['Controladores']['ConPrincipal'].buscarParametro(35));
		                    } 
			        },
			        failure: function(){
			            Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
			        }
               });               
	},

	 BorraTablaMaestra: function(){
           
	 	    var delrecords= entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').getSelectionModel().getSelection();
	 	    var storeTablaMaestra= entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').store;
	        if(delrecords.length === 0){
				Ext.Msg.alert('Información', entorno['Controladores']['ConPrincipal'].buscarParametro(18));
			}else{
				var me=this;
				Ext.Msg.show({
		                title : 'Confirmación',
		                msg : entorno['Controladores']['ConPrincipal'].buscarParametro(53),
		                buttons : Ext.Msg.YESNO,
		                icon : Ext.MessageBox.WARNING,
		                scope : this,
		                width : 450,
		                fn : function(btn, ev){
		                    if (btn == 'yes') {
		                    	var idtabladet=delrecords[0].get('idtabladet');
		                        storeTablaMaestra.remove(delrecords);  
                                                               
		                       if(idtabladet!="") {
		                       	     Ext.Ajax.request({
								        type: "POST",
								        url : 'app/data/php/CrudTablasReporte.php',
								        params:{
								            action:'deltablamaestra',
								            idtabladet:idtabladet
								        },								    
								        success: function(response){
								             var resp = Ext.decode(response.responseText); 
								        	 if(resp.success){ 
						                          Ext.Msg.alert('Operación Exitosa', entorno['Controladores']['ConPrincipal'].buscarParametro(22));
						                        
						                     }else{
						                          Ext.Msg.alert('Error', entorno['Controladores']['ConPrincipal'].buscarParametro(35));
						                     } 			
								         
								        },
								        failure: function(){
								            Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
								        }
					               });
		                       }
                            
		                    }
		                }
	            });
		    }
		 },

      MostrarCamposTablaBase:function(){
          console.log("entro al selector");
      },

      MuestraTablasMaestras: function(el,record, item, index,  e, Opts){
	
    	    var idtablaenc=record.get('idtablaenc');  
		    entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').store.removeAll();
		    entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').store.proxy.extraParams.idtablaenc=idtablaenc;
		    entorno['Vistas']['VisCrudTablasRep'].down('gridpanel[name=gridtablasmaestras]').store.load();
	  }

});