Ext.define('Reporteador.controller.ConSqlDinamico', {
	extend: 'Ext.app.Controller',

	init: function() {

		this.control({
            
            'vissqldinamico button[action=newquery]': {
				click: this.NuevaConsulta
			}
			,'vissqldinamico button[action=editquery]': {
				click: this.EditarConsulta
			}
			,'vissqldinamico button[action=savequery]': {
				click: this.GuardarConsulta
			}
			,'vissqldinamico button[action=undo]': {
				click: this.DeshacerCambios
			}
			,'vissqldinamico button[action=delquery]': {
				click: this.EliminarConsulta
			}
			,'vissqldinamico button[action=executequery]': {
				click: this.EjecutarConsulta
			}	
			,'vissqldinamico menu': {
				click: this.BuscarConsulta
			}	
		   ,'visbusquedasql gridpanel[action=dobleclick]':{
	            	itemdblclick: this.SeleccionarSql
	        }
	        ,'visbusquedasql button[action=clickAceptar]':{
	            	click: this.SeleccionarSqlBoton
	        }
	         ,'visbusquedasql button[action=clickCancelar]':{
	            	click: this.AlCerrarVentanaBusquedaSql
	        }
		    ,'vissqldinamico button[action=firstrecord]': {
				click: this.IrAPrimerRegistro
			}
			,'vissqldinamico button[action=prevrecord]': {
				click: this.IrARegistroAnterior
			}
			,'vissqldinamico button[action=nextrecord]': {
				click: this.IrARegistroSiguiente
			}
			,'vissqldinamico button[action=lastrecord]': {
				click: this.IrAUltimoRegistro
			}
			,'vissqldinamico button[action=cerrarsqldinamico]': {
				click: this.CerrarVentanaSqlDinamico
			}
			
		});

	},


	//Funciones Botones

    HabilitaCampos:function(estado){

         //entorno['Vistas']['VisSqlDinamico'].down('textfield[name=codigo]').setReadOnly(estado);
         entorno['Vistas']['VisSqlDinamico'].down('textfield[name=titulo]').setReadOnly(estado);
         entorno['Vistas']['VisSqlDinamico'].down('textfield[name=query]').setReadOnly(estado);
        
    },

     LimpiaCampos:function(){

         entorno['Vistas']['VisSqlDinamico'].down('textfield[name=codigo]').setValue("");
         entorno['Vistas']['VisSqlDinamico'].down('textfield[name=titulo]').setValue("");
         entorno['Vistas']['VisSqlDinamico'].down('textfield[name=query]').setValue("");
    },

    HabilitaControles:function(op){
            
            entorno['Vistas']['VisSqlDinamico'].down('button[action=newquery]').setDisabled(false); 
            entorno['Vistas']['VisSqlDinamico'].down('button[action=editquery]').setDisabled(false); 
            entorno['Vistas']['VisSqlDinamico'].down('button[action=savequery]').setDisabled(true); 
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=undo]').setDisabled(true); 
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=delquery]').setDisabled(false); 
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=search]').setDisabled(false); 
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=executequery]').setDisabled(false); 
	        if(entorno['Stores']['StoListaConsultas'].getCount()!=1){
		        entorno['Vistas']['VisSqlDinamico'].down('button[action=firstrecord]').setDisabled(false); 
		        entorno['Vistas']['VisSqlDinamico'].down('button[action=prevrecord]').setDisabled(false); 
		        entorno['Vistas']['VisSqlDinamico'].down('button[action=nextrecord]').setDisabled(false); 
		        entorno['Vistas']['VisSqlDinamico'].down('button[action=lastrecord]').setDisabled(false); 
            }else{
            	entorno['Vistas']['VisSqlDinamico'].down('button[action=firstrecord]').setDisabled(true); 
		        entorno['Vistas']['VisSqlDinamico'].down('button[action=prevrecord]').setDisabled(true); 
		        entorno['Vistas']['VisSqlDinamico'].down('button[action=nextrecord]').setDisabled(true); 
		        entorno['Vistas']['VisSqlDinamico'].down('button[action=lastrecord]').setDisabled(true); 
            }
    },

    DeshabilitaControles:function(){
           
            entorno['Vistas']['VisSqlDinamico'].down('button[action=newquery]').setDisabled(true); 
            entorno['Vistas']['VisSqlDinamico'].down('button[action=editquery]').setDisabled(true);
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=savequery]').setDisabled(false); 
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=undo]').setDisabled(false); 
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=delquery]').setDisabled(true); 
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=search]').setDisabled(true); 
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=executequery]').setDisabled(true); 
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=firstrecord]').setDisabled(true); 
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=prevrecord]').setDisabled(true); 
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=nextrecord]').setDisabled(true); 
	        entorno['Vistas']['VisSqlDinamico'].down('button[action=lastrecord]').setDisabled(true); 
    },


    CargarConsultaTodos:function(){
          
            entorno['Stores']['StoListaConsultas']=entorno['Controladores']['ConPrincipal'].getStore('stoTotalConsultas');
            var Mask = new Ext.LoadMask(entorno['Vistas']['VisSqlDinamico'], {msg:entorno['Controladores']['ConPrincipal'].buscarParametro(20)});
            Mask.show();
            var me=this;
             entorno['Stores']['StoListaConsultas'].extraParams.TipoReporte=entorno['Vistas']['VisFormGenerador'].down('combobox[name=cbotpoasign]').getValue();
             entorno['Stores']['StoListaConsultas'].load(function(){
             	
             	if(entorno['Stores']['StoListaConsultas'].getCount()>0){
             		me.HabilitaCampos(true);  
             		entorno['Vistas']['VisSqlDinamico'].down('button[action=editquery]').setDisabled(false);
             		entorno['Vistas']['VisSqlDinamico'].down('button[action=delquery]').setDisabled(false); 
             		entorno['Vistas']['VisSqlDinamico'].down('button[action=executequery]').setDisabled(false);  
                   	entorno['Vistas']['VisSqlDinamico'].down('form').loadRecord(entorno['Stores']['StoListaConsultas'].data.first());
             	    if(entorno['Stores']['StoListaConsultas'].getCount()!=1){
		             	    entorno['Vistas']['VisSqlDinamico'].down('button[action=nextrecord]').setDisabled(false);
		             	    entorno['Vistas']['VisSqlDinamico'].down('button[action=lastrecord]').setDisabled(false);
             	    }else{
             	    	    entorno['Vistas']['VisSqlDinamico'].down('button[action=prevrecord]').setDisabled(true);
		             	    entorno['Vistas']['VisSqlDinamico'].down('button[action=firstrecord]').setDisabled(true);
             	    	    entorno['Vistas']['VisSqlDinamico'].down('button[action=nextrecord]').setDisabled(true);
		             	    entorno['Vistas']['VisSqlDinamico'].down('button[action=lastrecord]').setDisabled(true);
             	    }
             	    entorno['Vistas']['VisSqlDinamico'].down('textfield[name=indice]').setValue(0);
             	}
             	Mask.hide();
             
             });           
    
    },

	NuevaConsulta:function(){

           entorno['SqlDinamico']['EsNuevo']=true;
           this.HabilitaCampos(false);
           this.DeshabilitaControles();
           entorno['Vistas']['VisSqlDinamico'].down('textfield[name=codigo]').setValue('');
           entorno['Vistas']['VisSqlDinamico'].down('textfield[name=titulo]').setValue('');
           entorno['Vistas']['VisSqlDinamico'].down('textareafield[name=query]').setValue('');
           /*Ext.Ajax.request({
                  type: "POST",
                  url : 'app/data/php/TraeCodigoConsulta.php',
                                          
                  success: function(response){
                     var resp = Ext.decode(response.responseText); 
                     if(resp.success){
                           entorno['Vistas']['VisSqlDinamico'].down('textfield[name=codigo]').setValue(resp.codigo);
                           entorno['Vistas']['VisSqlDinamico'].down('textfield[name=titulo]').setValue('');
                           entorno['Vistas']['VisSqlDinamico'].down('textareafield[name=query]').setValue('');
                     }else{
                     	   entorno['Vistas']['VisSqlDinamico'].down('textfield[name=codigo]').setValue(1);
                     }    
                  },
                  failure: function(){
                      Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
                  }
              });*/
            
	},

    EditarConsulta:function(){

       entorno['SqlDinamico']['EsNuevo']=false;
       this.HabilitaCampos(false);
       this.DeshabilitaControles();

	},

	GuardarConsulta:function(){
       
		 var form = entorno['Vistas']['VisSqlDinamico'].down('form').getForm();
		 var estadocodigo = entorno.SqlDinamico.EsNuevo;
		 
         var accion="";
        
         if(estadocodigo){
            accion='guardarquery';
         }else{
            accion='editarquery';
         }
          var me=this;      
		  if(form.isValid()){
            
               form.submit({

				url: 'app/data/php/CrudQuery.php',
				waitMsg: entorno['Controladores']['ConPrincipal'].buscarParametro(36),
				submitEmptyText:false,
				params: {
					action:accion
				},
				success: function(opts, response) {

					if (this.result.success) {
						entorno['Vistas']['VisSqlDinamico'].show();
					    me.CargarConsultaTodos();                  
						var msg=Ext.Msg.alert('Advertencia',entorno['Controladores']['ConPrincipal'].buscarParametro(25));
						/*Funcion usada para colocar alert delante de ventana modal*/
						Ext.defer(function () {
						    msg.toFront();
					    }, 50);
						//entorno['Vistas']['VisCrudEmpresas'].down('gridpanel[name=gridcrudempresas]').store.load();
					} else {
						Ext.Msg.alert('Resultado',entorno['Controladores']['ConPrincipal'].buscarParametro(26));
					}

			    },failure: function(){
			              Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
			    }
	    	 });
	     	
			
		}else{
			 Ext.Msg.alert('Advertencia',entorno['Controladores']['ConPrincipal'].buscarParametro(27));
		}
  
	},

	DeshacerCambios:function(){
		 if(entorno['SqlDinamico']['EsNuevo']){
		 	   this.LimpiaCampos();
		 }      
         this.HabilitaCampos(true);
         this.HabilitaControles();
	},

	EjecutarConsulta:function(){
	      var consulta=entorno['Vistas']['VisSqlDinamico'].down('textareafield[name=query]').getValue();
	      this.CerrarVentanaSqlDinamico();
	      entorno['Arreglo']['Columnas']="";
	      entorno.Controladores.ConFormGenerador.ExecuteQuery(consulta);
	},

	EliminarConsulta:function(){
	        this.BorrarConsulta();
	},

	BuscarConsulta:function(el,item,e,eOpts){
       if(item.text==entorno['Controladores']['ConPrincipal'].buscarParametro(16)){
           // console.log("Ha pulsado en el boton Consultar todos los registro");
            this.CargarConsultaTodos();
            
       }
       if(item.text==entorno['Controladores']['ConPrincipal'].buscarParametro(17)){
            
            var mask = Ext.Msg.wait( "Espere por favor", "Cargando", null );
		    entorno['Vistas']['VisBusquedaSql']=Ext.widget('visbusquedasql');
		    entorno['Vistas']['VisBusquedaSql'].down('gridpanel[action=dobleclick]').store.proxy.extraParams.TipoReporte=entorno['Vistas']['VisFormGenerador'].down('combobox[name=cbotpoasign]').getValue();
            entorno['Vistas']['VisBusquedaSql'].down('gridpanel[action=dobleclick]').store.proxy.extraParams.search='{"codigo":"","titulo":""}';
		    entorno['Vistas']['VisBusquedaSql'].down('gridpanel[action=dobleclick]').store.load(function(){
						mask.close();
		    });

            //  entorno['Vistas']['VisBusquedaSql'].show();  
       }
	},

	  BorrarConsulta: function(){
           
	 	    var delrecord= entorno['Vistas']['VisSqlDinamico'].down('textfield[name=codigo]').getValue();
	 	    //var storeEmpresa= entorno['Vistas']['VisCrudEmpresas'].down('gridpanel[name=gridcrudempresas]').store;
	         if(delrecord == ""){
				Ext.Msg.alert('Información',  entorno['Controladores']['ConPrincipal'].buscarParametro(19));
			}else{
                
				Ext.Msg.show({
		                title : 'Confirmación',
		                msg :  entorno['Controladores']['ConPrincipal'].buscarParametro(21),
		                buttons : Ext.Msg.YESNO,
		                icon : Ext.MessageBox.WARNING,
		                scope : this,
		                width : 450,
		                fn : function(btn, ev){
		                    if (btn == 'yes') {
		                      //  storeEmpresa.remove(delrecord);  
		                                                      
		                        if(delrecord!="") {
		                       	      var me=this;                                    
                                      Ext.Ajax.request({
										type: "POST",
								        url : 'app/data/php/CrudQuery.php',
										params: {
											action:'eliminarquery',
								            codigo:delrecord
										},
                                    
								        success: function(response){
								           var resp = Ext.decode(response.responseText);	
								           if(resp.success){
								           	    me.CargarConsultaTodos();
                                                Ext.Msg.alert('Operación Exitosa', entorno['Controladores']['ConPrincipal'].buscarParametro(22));
								           }else{
								           	    Ext.Msg.alert('Error', entorno['Controladores']['ConPrincipal'].buscarParametro(23));
								           }								          
				                          
								        },
								        failure: function(){
								            Ext.Msg.alert(entorno['Controladores']['ConPrincipal'].buscarParametro(24));
								        }
					               });

	                             
		                       }
                            
		                    }
		                } 
	            });
		    }
   },
        
    SeleccionarSql: function( el, record, item, index,  e, Opts){
	     // entorno['Vistas']['VisFormEmpresas'].down('combobox[name=sector]').setValue(record.get('IdValorParametro'));
         
	      this.LimpiaCampos();
	      entorno['Vistas']['VisSqlDinamico'].down('button[action=editquery]').setDisabled(false); 
	      entorno['Vistas']['VisSqlDinamico'].down('button[action=delquery]').setDisabled(false); 
          entorno['Vistas']['VisSqlDinamico'].down('button[action=executequery]').setDisabled(false);  
	      entorno['Vistas']['VisSqlDinamico'].down('form').loadRecord(record);
	      entorno['Vistas']['VisBusquedaSql'].close();
	},

	 SeleccionarSqlBoton: function( el, record, item, index,  e, Opts){

	 	var selrecord= entorno['Vistas']['VisBusquedaSql'].down('gridpanel[action=dobleclick]').getSelectionModel().getSelection();
	    
	    if(selrecord.length === 0){
				Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(18));
	    }else{
		      //entorno['Vistas']['VisFormEmpresas'].down('combobox[name=sector]').setValue(selrecord[0].get('IdValorParametro'));
		      this.LimpiaCampos();
		      entorno['Vistas']['VisSqlDinamico'].down('button[action=editquery]').setDisabled(false); 
		      entorno['Vistas']['VisSqlDinamico'].down('button[action=delquery]').setDisabled(false); 
              entorno['Vistas']['VisSqlDinamico'].down('button[action=executequery]').setDisabled(false);  
		      entorno['Vistas']['VisSqlDinamico'].down('form').loadRecord(selrecord[0]);
		      entorno['Vistas']['VisBusquedaSql'].close();
	    }
	},
   AlCerrarVentanaBusquedaSql:function(){
		 entorno['Vistas']['VisBusquedaSql'].close();
   },

	IrAPrimerRegistro:function(){
    
      this.LimpiaCampos();
      entorno['Vistas']['VisSqlDinamico'].down('form').loadRecord(entorno['Stores']['StoListaConsultas'].data.first());
      entorno['Vistas']['VisSqlDinamico'].down('button[action=nextrecord]').setDisabled(false);
      entorno['Vistas']['VisSqlDinamico'].down('button[action=lastrecord]').setDisabled(false);
      entorno['Vistas']['VisSqlDinamico'].down('button[action=prevrecord]').setDisabled(true);
      entorno['Vistas']['VisSqlDinamico'].down('button[action=firstrecord]').setDisabled(true);
	  entorno['Vistas']['VisSqlDinamico'].down('textfield[name=indice]').setValue(0);

	},

    IrARegistroAnterior:function(){
      
      this.LimpiaCampos();
      var indice=entorno['Vistas']['VisSqlDinamico'].down('textfield[name=indice]').getValue()-1;
      entorno['Vistas']['VisSqlDinamico'].down('form').loadRecord(entorno['Stores']['StoListaConsultas'].data.items[indice]);
	  entorno['Vistas']['VisSqlDinamico'].down('textfield[name=indice]').setValue(indice);   
      if(indice==0){    
		      entorno['Vistas']['VisSqlDinamico'].down('button[action=nextrecord]').setDisabled(false);
		      entorno['Vistas']['VisSqlDinamico'].down('button[action=lastrecord]').setDisabled(false);
		      entorno['Vistas']['VisSqlDinamico'].down('button[action=prevrecord]').setDisabled(true);
		      entorno['Vistas']['VisSqlDinamico'].down('button[action=firstrecord]').setDisabled(true);
       }else{
       	     
       	      entorno['Vistas']['VisSqlDinamico'].down('button[action=nextrecord]').setDisabled(false);
		      entorno['Vistas']['VisSqlDinamico'].down('button[action=lastrecord]').setDisabled(false);
		      entorno['Vistas']['VisSqlDinamico'].down('button[action=prevrecord]').setDisabled(false);
		      entorno['Vistas']['VisSqlDinamico'].down('button[action=firstrecord]').setDisabled(false);
      }

  
   	},

	IrAUltimoRegistro:function(){
     
      this.LimpiaCampos();
      entorno['Vistas']['VisSqlDinamico'].down('form').loadRecord(entorno['Stores']['StoListaConsultas'].getAt(entorno['Stores']['StoListaConsultas'].getCount()-1));
 	  entorno['Vistas']['VisSqlDinamico'].down('button[action=prevrecord]').setDisabled(false);
      entorno['Vistas']['VisSqlDinamico'].down('button[action=firstrecord]').setDisabled(false);
      entorno['Vistas']['VisSqlDinamico'].down('button[action=nextrecord]').setDisabled(true);
      entorno['Vistas']['VisSqlDinamico'].down('button[action=lastrecord]').setDisabled(true);
 	  entorno['Vistas']['VisSqlDinamico'].down('textfield[name=indice]').setValue(entorno['Stores']['StoListaConsultas'].getCount()-1); 
 	
 	},

	IrARegistroSiguiente:function(){
    
      this.LimpiaCampos();
      var indice=entorno['Vistas']['VisSqlDinamico'].down('textfield[name=indice]').getValue()+1;
      entorno['Vistas']['VisSqlDinamico'].down('form').loadRecord(entorno['Stores']['StoListaConsultas'].data.items[indice]);
	  entorno['Vistas']['VisSqlDinamico'].down('textfield[name=indice]').setValue(indice);
      
      if(indice==entorno['Stores']['StoListaConsultas'].getCount()-1){
            entorno['Vistas']['VisSqlDinamico'].down('button[action=prevrecord]').setDisabled(false);
            entorno['Vistas']['VisSqlDinamico'].down('button[action=firstrecord]').setDisabled(false);
            entorno['Vistas']['VisSqlDinamico'].down('button[action=nextrecord]').setDisabled(true);
            entorno['Vistas']['VisSqlDinamico'].down('button[action=lastrecord]').setDisabled(true);
       }else{
       	     entorno['Vistas']['VisSqlDinamico'].down('button[action=prevrecord]').setDisabled(false);
             entorno['Vistas']['VisSqlDinamico'].down('button[action=firstrecord]').setDisabled(false);
             entorno['Vistas']['VisSqlDinamico'].down('button[action=nextrecord]').setDisabled(false);
             entorno['Vistas']['VisSqlDinamico'].down('button[action=lastrecord]').setDisabled(false);
       }  
	
	},

	CerrarVentanaSqlDinamico:function(){
		 entorno['Vistas']['VisSqlDinamico'].close(); 
	}	

});