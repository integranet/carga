Ext.define('Reporteador.controller.ConPrincipal',{
	 extend: 'Ext.app.Controller',
	 views:['VisFormGenerador','VisPrincipal','VisReportesConfigurados','VisSqlDinamico','VisConfiguracion','VisListaCampos',
	 'VisBusquedaSql','VisBusquedaCampos','VisTituloConsulta','VisTablasGenerador','VisReporteGenerado','VisGridHorario','VisColaMaquinas','VisColaAuxiliares'],
   	 models: ['ModFiltros','ModColumnas','ModReportesConf','ModParametros','ModBuscarSql','ModBuscarCampos','ModCamposTabla','ModTablasEnc','ModTablasDet'],
	 stores: ['stoFiltros','stoColumnasIzq','stoColumnasDer','stoFunciones','stoCboFuncion','stoRepoConf','stoCboTablasRep','stoEstadoTblMaestra','stoTablasEnc',
	 'stoCboCampos','stoOrdenCampos','stoAgruparCampos','stoCboTipoDato','stoCboTieneFiltro','stoCboTieneFuncion','stoCboTipoAsign','stoCboTieneAgrup',
	 'stoBuscarSql','stoBuscarCampos','stoTotalConsultas','stoTotalCampos','stoEstadoTblbase','stoTablasDet','stoCampostblMaestra','stoCampostblBase'],
	 init:function (){
	 		document.getElementById('Idbody').innerHTML='';
	 		
	 		//definimos el entorno:
	 		entorno={};
	 		//definimos los controladores:
	 		entorno['Controladores']={};
	 		entorno['Controladores']['ConPrincipal']=this;
	 		entorno['Controladores']['ConFormGenerador']=this.getController('ConFormGenerador');
	 		entorno['Controladores']['ConReportesConf']=this.getController('ConReportesConf');
	 		entorno['Controladores']['ConTablasGenerador']=this.getController('ConTablasGenerador');
	 		//Definimos las Vistas:
	 		//vista principal (Viewport):
	 		entorno['Vistas']={}
	 		entorno['Vistas']['VisPrincipal']=Ext.widget('visprincipal');
	 		
	 		//vista ventana Configuracion de Tablas y Campos:
	 		entorno['Vistas']['VisFormGenerador']=Ext.widget('visformgenerador');
        	entorno['Vistas']['VisPrincipal'].down('tabpanel[name=tpanel]').add(entorno['Vistas']['VisFormGenerador']);
		    entorno['Vistas']['VisPrincipal'].down('tabpanel[name=tpanel]').setActiveTab(entorno['Vistas']['VisFormGenerador']);
		//	entorno['Vistas']['VisCrudPrincipal'].down('tabpanel').setActiveTab(entorno['Vistas']['VisCrudEmpresas']);       	
		    //vista de Busqueda Querys y Campos:
			entorno['Vistas']['VisBusquedaSql']=null;
            entorno['Vistas']['VisBusquedaCampos']=null;
          

            /*Variables del sistema*/
            entorno['Consulta']={};  
            entorno['Editores']={};
            entorno['Arreglo']={};                 
            entorno['Consulta']['Codigo']="";
            entorno['Consulta']['Titulo']="";
            entorno['Arreglo']['Columnas']="";
            /*Se usa para determinar la accion a ejecutar al momento de guardar una
            consulta desde la vista de Sql Dinamico*/
            entorno['SqlDinamico']={};                    
            entorno['SqlDinamico']['EsNuevo']=false;
            entorno['Configuracion']={};                    
            entorno['Configuracion']['EsNuevo']=false;
            /*Stores*/
            entorno['Stores']={};
		/*	entorno['Vistas']['VisColaMaquinas']=Ext.widget('viscolamaquinas');	           
			entorno['Vistas']['VisColaAuxiliares']=Ext.widget('viscolaauxiliares');	

			entorno.Vistas.VisPrincipal.down('tabpanel[name=tpanel]').add(entorno['Vistas']['VisColaMaquinas']);          
			entorno.Vistas.VisPrincipal.down('tabpanel[name=tpanel]').setActiveTab(entorno['Vistas']['VisColaMaquinas']); 
			entorno.Vistas.VisPrincipal.down('tabpanel[name=tpanel]').add(entorno['Vistas']['VisColaAuxiliares']);          
			*/ 
           this.cargarParametros();
         	this.control({

	        })
  },

   
    buscarParametro:function(value){

            for (var i = 0; i < entorno.parametros.length; i++) {
                if(entorno.parametros[i].IdValorParametro==value){
                    return entorno.parametros[i].Valor;
                }
            }
        return value;
    },

    cargarParametros:function(){
    	
    	Ext.Ajax.request({
					url: 'app/data/php/CargarParametros.php',
					
					success: function(response) {

						var resp = Ext.decode(response.responseText);
						entorno['parametros']=resp.data;
					
					},
					failure: function() {
											
					}
		});
    },

  	destruirObjeto:function(name){
		
		entorno.Vistas[''+name] = null;
		
	}

	       
});