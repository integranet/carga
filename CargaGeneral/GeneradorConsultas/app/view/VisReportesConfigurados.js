Ext.define('Reporteador.view.VisReportesConfigurados', {
    extend: 'Ext.window.Window',
    alias: 'widget.visreportesconf',
    autoShow:true,
    height: 250,
    autoScroll:true,
    width: 450,
    modal:true,
    title: 'Reportes Configurados',
    layout:{
        type:'border'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
           
                items: [
                {
                    xtype: 'gridpanel',
                    height:'100%',
                    /*width:'100%',*/
                    action:'dobleclick',
                    store: 'stoRepoConf',
                    autoScroll: true,
                    title: '',
                    region:'center',
                    columns: [
                         {
                            xtype: 'gridcolumn',
                            dataIndex: 'codigo',
                            flex: 1,
                            hideable:false,
                            text: 'Código'
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'titulo',
                            flex: 3,
                            hideable:false,
                            text: 'Titulo'
                        }
                    ],
                    viewConfig: {

                    },
                    plugins:[
                        Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })
                    ]
                  }
              ],
            buttons:[
                {
                    text:'Cargar Consulta',
                    action:'clickCargar',
                    iconCls:'aceptar'
                }
                ,{
                    text:'Eliminar Consulta',
                    action:'clickEliminar',
                    iconCls:'delete'
                },
                '->',
                {
                    text:'Cerrar',
                    action:'clickCerrar',
                    iconCls:'cancel'
                }

            ]
        });

        me.callParent(arguments);
    }

});