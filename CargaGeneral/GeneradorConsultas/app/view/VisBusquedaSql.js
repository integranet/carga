Ext.define('Reporteador.view.VisBusquedaSql', {
    extend: 'Ext.window.Window',
    alias: 'widget.visbusquedasql',
    autoShow:true,
    height: 250,
    autoScroll:true,
    width: 450,
    modal:true,
    title: 'Búsqueda de SQLs',
    layout:{
        type:'border'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
           
                items: [
                {
                    xtype: 'gridpanel',
                    height:'100%',
                    /*width:'100%',*/
                    action:'dobleclick',
                    store: 'stoBuscarSql',
                    autoScroll: true,
                    title: '',
                    region:'center',
                    columns: [
                         {
                            xtype: 'gridcolumn',
                            dataIndex: 'codigo',
                            flex: 1,
                            text: 'Código'
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'titulo',
                            flex: 5,
                            text: 'Titulo'
                        }
                    ],
                    viewConfig: {

                    },
                    plugins:[
                        Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })
                    ]
                  }
              
            ],
            buttons:[
                {
                    text:'Aceptar',
                    action:'clickAceptar',
                    iconCls:'aceptar'
                },{
                    text:'Cancelar',
                    action:'clickCancelar',
                    iconCls:'cancel'
                }

            ]
        });

        me.callParent(arguments);
    }

});