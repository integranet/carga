Ext.define('Reporteador.view.VisBusquedaCampos', {
    extend: 'Ext.window.Window',
    alias: 'widget.visbusquedacampos',
    autoShow:true,
    height: 250,
    autoScroll:true,
    width: 450,
    modal:true,
    title: 'Búsqueda de Campos',
    layout:{
        type:'border'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
           
                items: [
                {
                    xtype: 'gridpanel',
                    height:'100%',
                    /*width:'100%',*/
                    action:'dobleclick',
                    store: 'stoBuscarCampos',
                    autoScroll: true,
                    title: '',
                    region:'center',
                    columns: [
                         {
                            xtype: 'gridcolumn',
                            dataIndex: 'tabla',
                            flex: 1,
                            text: 'Tabla'
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'campo',
                            flex: 1,
                            text: 'Campo'
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'abreviatura',
                            flex: 3,
                            text: 'Abreviatura'
                        }
                    ],
                    viewConfig: {

                    },
                    plugins:[
                        Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })
                    ]
                  }
              ],
            buttons:[
                {
                    text:'Aceptar',
                    action:'clickAceptar',
                    iconCls:'aceptar'
                },{
                    text:'Cancelar',
                    action:'clickCancelar',
                    iconCls:'cancel'
                }

            ]
        });

        me.callParent(arguments);
    }

});