Ext.define('Reporteador.view.VisTituloConsulta', {
    extend: 'Ext.window.Window',
    alias: 'widget.vistituloconsulta',
    modal:true,
    height: 120,
    resizable:false,
    padding: '',
    width: 565,
    title: 'Titulo de la Consulta',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
             items: [
                {
                    xtype: 'form',
                    frame:true,
                    height: 110,
                    width: 560,

            items: [
                {
                    xtype: 'container',
                    height: 105,
                    padding: 10,
                    width: 548,
                    items: [
                      
                        {
                            xtype: 'textfield',
                            width: 501,
                            fieldLabel: 'Titulo',
                            name:'TxtTitulo',
                            labelWidth: 60
                        }
                    ]
                }
                 ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    width: 400,
                    dock: 'bottom',
                    items: [
                        '->',
                        {
                            xtype: 'button',
                            action:'clickAceptar',
                            text:'Aceptar',
                            iconCls:'aceptar'
                        },                    
                       
                        {
                            xtype: 'button',
                            action:'clickCancelar',
                            text:'Cancelar',
                            iconCls:'cancel'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});