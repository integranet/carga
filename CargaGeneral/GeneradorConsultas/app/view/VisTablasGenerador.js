Ext.define('Reporteador.view.VisTablasGenerador', {
      extend: 'Ext.panel.Panel',
    
      closable:true,
      alias:  'widget.vistablasgen',
  
       layout: {
            type: 'border'
        },
        title: 'Parametrización Tablas Reporte',

    initComponent: function() {
        var me = this;
        entorno['Editores']['EditorEncabezado']=[Ext.create('Ext.grid.plugin.RowEditing', {
                                            clicksToEdit: 2,
                                            errorSummary:false,  
                                            saveBtnText  : 'Actualizar',
                                            cancelBtnText: 'Cancelar',                                         
                                            name:'rowEditingEnc',
                                            listeners: {
                                              edit: function(editor, e, eOpts) {
                                                 entorno.Controladores.ConTablasGenerador.ActualizaInfoTablasBase();
                                              }
                                           }
       }),  Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
       })];
        entorno['Editores']['EditorDetalle']=[Ext.create('Ext.grid.plugin.RowEditing', {
                                            clicksToEdit: 2,
                                            errorSummary:false,
                                            saveBtnText  : 'Actualizar',
                                            cancelBtnText: 'Cancelar',   
                                            name:'rowEditingDet',
                                            listeners: {
                                              edit: function(editor, e, eOpts) {
                                                 entorno.Controladores.ConTablasGenerador.ActualizaInfoTablasMaestras();
                                              }
                                           }
        }),Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
        })];
        
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    region:'center',
                    layout: {
                        type: 'border'
                    },
                    items: [
                        {
                            xtype: 'gridpanel',
                            title: 'Tablas Base',
                            autoHeight: true,
                            store: 'stoTablasEnc',
                            flex: 6,
                            region: 'center',
                            name:'gridtablasbase',
                            autoScroll: true,
                           
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'idtablaenc',
                                    text: 'Id',
                                    flex:1,
                                    hideable: false,
                                    hidden:true
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'tablabase',
                                    text: 'Tabla Base',
                                    hideable: false,
                                    flex:2,
                                    editor: {
                                        xtype: 'textfield',
                                        allowBlank: false
                                    }                                
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'idtiporeporte',
                                    text: 'Tipo Reporte',
                                    hideable: false,
                                    flex:1,
                                    editor: {
                                        xtype: 'combobox',
                                        editable:false,                                                            
                                        triggerAction: 'all',
                                        selectOnTab: true,
                                        store:'stoCboTipoAsign',
                                        displayField: 'Valor',
                                        valueField: 'IdValorParametro',
                                        allowBlank: false
                                    },
                                    renderer: function(value) {
                                        return entorno['Controladores']['ConPrincipal'].buscarParametro(value);
                                   }   
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'idestado',
                                    text: 'Estado',
                                    hideable: false,
                                    flex:1,
                                    editor: {
                                        xtype: 'combobox',
                                        editable:false,                                                            
                                        triggerAction: 'all',
                                        selectOnTab: true,
                                        store:'stoEstadoTblbase',
                                        displayField: 'Valor',
                                        valueField: 'IdValorParametro',
                                        allowBlank: false
                                    },                                   
                                    renderer: function(value) {
                                        return entorno['Controladores']['ConPrincipal'].buscarParametro(value);
                                    }                      
                                }
                            ],
                            viewConfig: {
                                  stripeRows: true,
                                  enableTextSelection: true
                            },
                            plugins:entorno['Editores']['EditorEncabezado'],
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    dock: 'top',
                                    items: [
                                        {
                                            xtype: 'button',
                                            text: 'Adicionar',
                                            iconCls: 'add',
                                            action:'addtblbase'
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Modificar',
                                            iconCls: 'edit',
                                            action:'edittblbase'
                                        },
                                       
                                        {
                                            xtype: 'button',
                                            text: 'Eliminar',
                                            iconCls: 'delete',
                                            action: 'deltblbase'
                                        },
                                         {
                                            xtype: 'button',
                                            text: 'Actualizar',
                                            iconCls: 'refresh',
                                            action: 'actgridbase'
                                         }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'gridpanel',
                           // height: 146,
                            title: 'Tablas Maestras',
                            store: 'stoTablasDet',
                            name:'gridtablasmaestras',
                            autoScroll: true,
                            autoHeight: true,
                            flex: 4,
                            region: 'south',
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'idtabladet',
                                    text: 'Id',
                                    flex:1,
                                    hideable: false,
                                    hidden:true
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'tablamaestra',
                                    text: 'Tabla Maestra',
                                    hideable: false,
                                    flex:1.5,
                                    editor: {
                                        xtype: 'textfield',
                                        allowBlank: false
                                    }    
                                },   
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'campotblbase',
                                    text: 'Campo Tabla Base',
                                    hideable: false,
                                    flex:2,
                                    editor: {
                                        xtype: 'combobox',
                                       // editable:false,                                                            
                                        triggerAction: 'all',
                                        selectOnTab: true,
                                        store:'stoCampostblBase',
                                        displayField: 'columna',
                                        valueField: 'columna',
                                        allowBlank: false/*,
                                        listeners:{           
                                          beforeedit: function(editor, e, eOpts) {
                                               entorno.Controladores.ConTablasGenerador.MostrarCamposTablaBase();
                                           }                           
                                        }*/
                                    }  
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'campotblmaestra',
                                    text: 'Campo Tabla Maestra',
                                    hideable: false,
                                    flex:2,
                                     editor: {
                                        xtype: 'combobox',
                                        //editable:false,                                                            
                                        triggerAction: 'all',
                                        selectOnTab: true,
                                        store:'stoCampostblMaestra',
                                        displayField: 'columna',
                                        valueField: 'columna',
                                        allowBlank: false/*,
                                        listeners:{           
                                          beforeedit: function(editor, e, eOpts) {
                                               entorno.Controladores.ConTablasGenerador.MostrarCamposTablaBase();
                                           }                           
                                        }*/
                                    }        
                                },                            
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'idestado',
                                    text: 'Estado',
                                    hideable: false,
                                    flex:1,
                                    editor: {
                                        xtype: 'combobox',
                                        editable:false,                                                            
                                        triggerAction: 'all',
                                        selectOnTab: true,
                                        store:'stoEstadoTblMaestra',
                                        displayField: 'Valor',
                                        valueField: 'IdValorParametro',
                                        allowBlank: false
                                    },
                                    renderer: function(value) {
                                        return entorno['Controladores']['ConPrincipal'].buscarParametro(value);
                                   }   
                                }
                            ],
                            viewConfig: {
                                 stripeRows: true,
                                enableTextSelection: true
                            },
                            plugins:entorno['Editores']['EditorDetalle'],
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                   // disabled:true,
                                    name:'tbGridMaestra',
                                    dock: 'top',
                                    items: [
                                        {
                                            xtype: 'button',
                                            text: 'Adicionar',
                                            iconCls: 'add',
                                            action:'addtblmaestra'
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'Modificar',
                                            iconCls: 'edit',
                                            action:'edittblmaestra'
                                        },
                                        
                                        {
                                            xtype: 'button',
                                            text: 'Eliminar',
                                            iconCls: 'delete',
                                            action:'deltblmaestra'                        
                                        },
                                         {
                                            xtype: 'button',
                                            text: 'Actualizar',
                                            iconCls: 'refresh',
                                            action: 'actgridmaestra'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});