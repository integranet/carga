Ext.define('Reporteador.view.VisConfiguracion', {
    extend: 'Ext.window.Window',
    alias: 'widget.visconfiguracion',
    modal:true,
    resizable:false,
    height: 280,
    width: 500,
    title: 'Configuración de Tablas y Campos',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    frame:true,
                    height: 250,
                    width: 490,
                items: [
                {
                    xtype: 'container',
                    height: 125,
                    width: 470,
                    layout: {
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'container',
                            height: 120,
                            padding: 5,
                            width: 226,
                            layout: {
                                align: 'stretch',
                                padding: 10,
                                type: 'vbox'
                            },
                            flex: 1,
                            items: [
                                 {
                                    xtype: 'numberfield',
                                    fieldLabel: 'Indice',
                                    name:'indice',
                                    labelWidth: 50,
                                    hidden:true,
                                    readOnly:true
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'IdCampoReporte',
                                    name:'idcamporeporte',
                                    labelWidth: 50,
                                    hidden:true,
                                    readOnly:true
                                },
                                {
                                    xtype: 'combobox',
                                    name:'tabla',
                                    fieldLabel:'Tabla',
                                    emptyText:'Seleccione la tabla',
                                    allowBlank:false,
                                    editable:false,                                                        
                                    triggerAction: 'all',
                                    store:'stoCboTablasRep',
                                    displayField: 'Valor',
                                    valueField: 'IdValorParametro',
                                    labelWidth: 50,
                                    readOnly:true
                                },
                                {
                                    xtype: 'combobox',
                                    name:'campo',
                                    fieldLabel:'Campo',
                                    emptyText:'Seleccione el campo',
                                    allowBlank:false,
                                    editable:false,                                                        
                                    triggerAction: 'all',
                                    store:'stoCboCampos',
                                    displayField: 'columna',
                                    valueField: 'columna',
                                    labelWidth: 50,
                                    readOnly:true
                                },
                                {
                                    xtype: 'combobox',
                                    fieldLabel: 'Tipo de dato',
                                    store:'stoCboTipoDato',
                                    displayField: 'valor',
                                    valueField: 'id',
                                    editable:false,
                                    name:'tipodato',
                                    labelWidth: 50,
                                    readOnly:true
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            padding: 10,
                            width: 250,
                            layout: {
                                align: 'stretch',
                                padding: 5,
                                type: 'vbox'
                            },
                            flex: 1,
                            items: [
                                {
                                    xtype: 'label',
                                    height: 30,
                                    text: ''
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Abreviatura',
                                    allowBlank:false,
                                    name:'abreviatura',
                                    labelWidth: 70,
                                    readOnly:true
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Longitud',
                                    name:'longitud',
                                    labelWidth: 70,
                                    readOnly:true
                                }
                            ]
                        }
                    ]
                },
                 {
                    xtype: 'container',
                    height: 80,
                    padding: 10,
                    width: 470,
                    layout: {
                        padding: 10,
                        type: 'hbox'
                    },
                    flex:1,
                    items: [
                        {
                            xtype: 'fieldset',
                            height: 60,
                            width: 450,
                             layout: {
                                padding: 2,
                                type: 'hbox'
                            }, 
                            flex:1,
                            title: '',
                            items: [
                                    {
                                        xtype: 'combobox',
                                        fieldLabel: 'Filtro',
                                        allowBlank:false,
                                        store:'stoCboTieneFiltro',
                                        displayField: 'Valor',
                                        valueField: 'IdValorParametro',
                                        editable:false,
                                        name:'filtro',
                                        labelWidth: 40,
                                        readOnly:true,
                                        flex:1
                                    },
                                    {
                                        xtype: 'combobox',
                                        fieldLabel: '   Función',
                                        allowBlank:false,
                                        store:'stoCboTieneFuncion',
                                        displayField: 'Valor',
                                        valueField: 'IdValorParametro',
                                        editable:false,
                                        name:'funcion',
                                        labelWidth: 60,
                                        readOnly:true,
                                        flex:1.2
                                    },
                                    {
                                        xtype: 'combobox',
                                        fieldLabel: '   Agrupación',
                                        allowBlank:false,
                                        store:'stoCboTieneAgrup',
                                        displayField: 'Valor',
                                        valueField: 'IdValorParametro',
                                        editable:false,
                                        name:'agrupacion',
                                        labelWidth: 75,
                                        readOnly:true,
                                        flex:1.5
                                    }
                           ]}
               
                    ]
                }
                   ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    width: 400,
                    dock: 'bottom',
                    items: [
                        {
                          xtype:'label',
                          flex:1
                        },
                        {
                            xtype: 'button',
                            action:'newquery',
                            tooltip:'Nuevo Campo',
                            iconCls:'new'
                        },
                        {
                            xtype: 'button',
                            action:'editquery',
                            tooltip:'Editar Campo',
                            disabled:true,
                            iconCls:'open'
                        },
                        {
                            xtype: 'button',
                            action:'savequery',
                            tooltip:'Grabar Campo',
                            disabled:true,
                            iconCls:'save'
                        },
                        {
                            xtype: 'button',
                            action:'undo',
                            tooltip:'Deshacer',
                            disabled:true,
                            iconCls:'undo'
                        },
                        {
                            xtype: 'button',
                            action:'delquery',
                            tooltip:'Borrar Campo',
                            disabled:true,
                            iconCls:'papelera'
                        },
                        {
                            xtype: 'button',
                            action:'executequery',
                            tooltip:'Ejecutar Consulta',
                            disabled:true,
                            iconCls:'impresion'
                        },
                        {
                            xtype: 'button',
                            action:'search',
                            tooltip:'Buscar',
                            iconCls: 'buscar',
                            menu: {                          
                                 showSeparator: false,          
                                 items: [{
                                   text: entorno['Controladores']['ConPrincipal'].buscarParametro(16)
                                 },
                                 {
                                   text: entorno['Controladores']['ConPrincipal'].buscarParametro(17)
                                 }]
                            }
                        },
                        {
                            xtype: 'button',
                            action:'firstrecord',
                            tooltip:'Primer Registro',
                            disabled:true,
                            iconCls:'first'
                        },
                        {
                            xtype: 'button',
                            action:'prevrecord',
                            tooltip:'Registro Anterior',
                            disabled:true,
                            iconCls:'previous'
                        },
                        {
                            xtype: 'button',
                            action:'nextrecord',
                            tooltip:'Registro Siguiente',
                            disabled:true,
                            iconCls:'next'
                        },
                        {
                            xtype: 'button',
                            action:'lastrecord',
                            tooltip:'Ultimo Registo',
                            disabled:true,
                            iconCls:'last'
                        },
                        {
                            xtype: 'button',
                            action:'cerrarventanaconfig',
                            tooltip:'Salir',
                            iconCls:'cancel'
                        },
                        {
                          xtype:'label',
                          flex:1
                        },
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});