Ext.define('CrudEmp.view.VisCrudEmpresas', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.viscrudempresas',
    height: 250,
    width: 400,
    name:'',
    modal:true,
    region:'center',
    layout: {
        type: 'border'
    },
   
    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'gridpanel',
                    title: 'Empresas',
                    store: 'StoCrudEmpresas',
                    name:'gridcrudempresas',
                    region: 'center',
                    columns: [
                         {
                            xtype: 'gridcolumn',
                            dataIndex: 'idempresa',
                            text: 'IdEmpresa',
                            hidden:true,
                            hideable:false
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'nit',
                            text: 'Nit',
                            hideable:false,
                            flex:1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'razon',
                            text: 'Razón Social',
                            hideable:false,
                            flex:2
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'mail',
                            text: 'Email',
                            hideable:false,
                            flex:2

                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'direccion',
                            text: 'Dirección',
                            hideable:false,
                            flex:1
                        },
                         {
                            xtype: 'gridcolumn',
                            dataIndex: 'tel',
                            text: 'Teléfono',
                            hideable:false,
                            flex:1
                        }, 
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'contacto',
                            text: 'Contacto',
                            hideable:false,
                            flex:2
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'sitio',
                            text: 'Sitio Web',
                            hideable:false,
                            flex:2
                        }
                       
                    ],
                    plugins:[Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })],
                    viewConfig: {
                          stripeRows: true,
                          enableTextSelection: true
                    },
                    dockedItems: [
                    {
                        xtype: 'toolbar',
                        dock: 'top',
                        height:30,
                        items: [{
                                    xtype:'button',
                                    action:'addempresa',
                                    name:'btnAdicionar',
                                    iconCls: 'add',
                                    text:'Adicionar'
                              },{
                                    xtype: 'button',
                                    text: 'Modificar',
                                    iconCls: 'edit',
                                    action:'editempresa'
                              },{
                                            xtype: 'button',
                                            text: 'Eliminar',
                                            iconCls: 'delete',
                                            action: 'delempresa'
                              },
                              {
                                    xtype:'button',
                                    action:'adminanexo',
                                    name:'btnAdmAnexo',
                                    iconCls: 'add',
                                    text:'Administrar Anexos'
                              },

                              ,{
                                            xtype: 'button',
                                            text: 'Refrescar',
                                            iconCls: 'refresh',
                                            action: 'actgrilla'
                              },
                              {
                                            xtype: 'button',
                                            text: 'Ir a Parametros',
                                            iconCls: 'ir',
                                            action: 'irparam'
                              }]
                }
            ],
            
                }
            ]
        });

        me.callParent(arguments);
 
    }

});