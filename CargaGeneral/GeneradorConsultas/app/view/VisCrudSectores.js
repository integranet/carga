Ext.define('CrudEmp.view.VisCrudSectores', {
    extend: 'Ext.window.Window',
    alias:'widget.viscrudsectores',
    autoShow:true,
    height: 250,
    autoScroll:true,
    width: 450,
    modal:true,
    title: 'Sectores',
    layout:{
        type:'border'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'gridpanel',
                    height:'100%',
                    /*width:'100%',*/
                    action:'dobleclick',
                    store: 'StoCrudSectores',
                    autoScroll: true,
                    title: '',
                    region:'center',
                    columns: [
                         {
                            xtype: 'gridcolumn',
                            dataIndex: 'IdValorParametro',
                            flex: 1,
                            text: 'IdSector',
                            hidden:true,
                            hideable:true
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'Valor',
                            flex: 5,
                            text: 'Sector'
                        }
                    ],
                    viewConfig: {

                    },
                    plugins:[
                        Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })
                    ]
                }
            ],
            buttons:[
                {
                    text:'Aceptar',
                    action:'clickAceptar',
                    iconCls:'aceptar'
                },{
                    text:'Cancelar',
                    action:'clickCancelar',
                    iconCls:'cancel'
                }

            ]
        });

        me.callParent(arguments);
    }

});