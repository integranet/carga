Ext.define('Reporteador.view.VisListaCampos', {
    extend: 'Ext.window.Window',
    alias:'widget.vislistacampos',
    //autoShow:true,
    autoScroll:true,
    height: 250,
    width: 544,
    title: 'Campos',
    modal:true,
    layout:{
        type:'border'
    },
   /* layout:{
        type:'vbox',
        align:'stretch'
    },*/

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'gridpanel',
                    height:'100%',
                    /*width:'100%',*/
                    action:'dobleclick',
                    store: 'stoCboCampos',
                    autoScroll: true,
                    title: '',
                    region:'center',
                    columns: [
                         {
                            xtype: 'gridcolumn',
                            dataIndex: 'columna',
                            flex: 1,
                            text: 'IdCampo',
                            hidden:true
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'columna',
                            flex: 5,
                            text: 'Campo'
                        }
                    ],
                    viewConfig: {

                    },
                    plugins:[
                        Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })
                    ]
                }
            ],
             buttons:[
                {
                    text:'Aceptar',
                    action:'clickAceptar',
                    iconCls:'aceptar'
                },{
                    text:'Cancelar',
                    action:'clickCancelar',
                    iconCls:'cancel'
                }

            ]
        });

        me.callParent(arguments);
    }

});