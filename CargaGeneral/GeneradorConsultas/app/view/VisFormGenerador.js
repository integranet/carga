Ext.define('Reporteador.view.VisFormGenerador', {
       
    extend: 'Ext.form.Panel',
    alias: 'widget.visformgenerador',
    iconCls:'report',
    region:'center',
    layout: {
        type: 'border'
    },
    //bodyPadding: 10,
    title: 'Estadisticas de asignación de Equipos',

    initComponent: function() {
        var me = this;
         Ext.apply(Ext.form.VTypes, {
              filtro : function(value, field) {
                  return /^(=|>=|<=|>|<|<>)?[\w]+$/.test(value);                
              },
              filtroText : 'La condicion ingresada no es válida',
              filtroMask : /^(=|>=|<=|>|<|<>)?[\w]+$/            
        },{
             phone: function(value,field){  
                  return value.replace(/[ \-\(\)]/g,'').length >= 7;  
             },  
            phoneText: 'Número de teléfono erróneo, asegurese que el número tenga 7 digitos minimo',  
            phoneMask: /[ \d\-\(\)]/ 
        });
      /*   Ext.apply(Ext.form.VTypes, {
              validacionNumero : function(value, field) {
               return /[0-9]/.test(value);
              },
              validacionNumeroText : 'Los datos ingresado no son válidos. Solo números',
              validacionNumeroMask : /[0-9]/i            
        },{
             filtro: function(value,field){  
                  return /^(=|>=|<=|>|<|!=)?([0-9a-zA-Z]/.test(value);
             },  
            filtroText: 'El filtro ingresado no es valido', //mensaje de error  
            filtroMask: /^(=|>=|<=|>|<|!=)?([0-9a-zA-Z]/  //regexp para filtrar los caracteres permitidos  
        });*/
       
        Ext.applyIf(me, {

            items: [
                {
                   xtype:'container',
                   html:'<form id="Reporte" style="display:none" method="POST" target="_blank" action="http://yata/smWebReporteDinamico/ReporteDinamico.aspx"> <input type="text" id="dato" name="dato" value="hola  mundo"> </form>',
                    name:'formulario'
                },
                {
                    xtype: 'container',
                    layout: {
                        type: 'border'
                    },
                    region: 'center',
                    items: [
                        {
                            xtype: 'container',
                            flex:3,
                            layout: {
                               
                                type: 'border'
                            },
                            region: 'center',
                            items: [
                      
                                {
                                 /*   xtype: 'panel',
                                   // width: 285,
                                    title: '',
                                    flex: 1,
                                    items: [
                                        {*/
                                            xtype: 'fieldset',
                                            flex:1,
                                           //height: '95%',
                                           layout: {
                                                type: 'border'
                                            },
                                            region:'center',
                                            title: 'Filtros',
                                            items: [
                                                {
                                                    xtype: 'gridpanel',
                                                    name:'gridFiltros',
                                                    store:'stoFiltros',
                                                    //title: 'My Grid Panel',
                                                    flex:1,
                                                    region: 'center',
                                                    columns: [
                                                        {
                                                            xtype: 'gridcolumn',
                                                            dataIndex:'abreviatura',
                                                            flex:1,
                                                            text: 'Campos Disponibles'
                                                        },
                                                        {
                                                            xtype: 'gridcolumn',
                                                            flex:1,
                                                            dataIndex: 'vfiltro',
                                                            text: 'Criterios',
                                                             editor: {
                                                                xtype: 'textfield',
                                                                name:'TxtCriterios'/*,
                                                                vtype:'filtro'*/
                                                             }
                                                        },
                                                        {
                                                            xtype: 'actioncolumn'
                                                            ,width:25
                                                            , items: [{ // Delete button
                                                                icon: '../resources/images/delete.png'
                                                                , handler: function(grid, rowIndex, colindex) {
                                                                    var storeFiltro=grid.store;
                                                                    storeFiltro.getAt(rowIndex).set('vfiltro','');
                                                                    storeFiltro.filter(
                                                                        {filterFn: function(item) { return item.get("vfiltro") != ""; }}
                                                                    );                                                                   
                                                                } 
                                                            }]
                                                    }
                                                        
                                                    ],
                                                    plugins:[
                                                        Ext.create('Ext.grid.plugin.CellEditing', {
                                                            clicksToEdit: 2
                                                        }) 
                                                    ],
                                                    viewConfig: {
                                                         plugins: {
                                                              ptype: 'gridviewdragdrop',
                                                              dragGroup: 'DDGroup',
                                                              dropGroup: 'DDGroup'
                                                        },
                                                         listeners: {
                                                              beforedrop: function(node, data, overModel, dropPosition, dropFunction, eOpts ) {
                                                                    if(data.records[0].get("permitefiltro")==1){
                                                                        var storeFiltro=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFiltros]').store;
                                                                        if(!entorno.Controladores.ConFormGenerador.CampoYaAgregado(storeFiltro,data.records[0].get("idcampo"))){
                                                                           return true;
                                                                        }else{
                                                                           Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(100));
                                                                           return false;
                                                                        }

                                                                    }else{
                                                                         Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(95));
                                                                         return false;
                                                                    }                                                                                                             
                                                              }
                                                          }
                                                    }
                                                }
                                        /*    ]
                                        }*/
                                    ]
                                },
                                {
                                   /* xtype: 'panel',
                                    width: 150,
                                    title: '',
                                    flex: 1,
                                    items: [
                                        {*/
                                            xtype: 'fieldset',
                                            flex:1,
                                            layout: {
                                                type: 'border'
                                            },
                                            region:'south',
                                            title: 'Columnas',
                                            items: [
                                                {
                                                    xtype: 'gridpanel',
                                                    title: '',
                                                    store:'stoColumnasDer',
                                                    name:'gridColumnas2',
                                                    flex: 1.2,
                                                    hideHeaders:true,                                                  
                                                    autoHeigth:true,
                                                    region: 'center',
                                                    selModel: new Ext.selection.RowModel({mode:'SINGLE'}),
                                                    columns: [
                                                        {
                                                            xtype: 'gridcolumn',
                                                            dataIndex: 'abreviatura',
                                                            text: 'Campos',
                                                            flex:1
                                                        },
                                                        {
                                                            xtype: 'actioncolumn'
                                                            ,width:25
                                                            , items: [{ // Delete button
                                                                icon: '../resources/images/delete.png'
                                                                , handler: function(grid, rowIndex, colindex) {
                                                                      var storeColumna=grid.store;
                                                                      storeColumna.getAt(rowIndex).set('select',0);
                                                                      storeColumna.filter(
                                                                         {filterFn: function(item) { return item.get("select") == 1; }}
                                                                      ); 
                                                                } 
                                                            }]
                                                        }
                                                        
                                                    ],
                                                    viewConfig: {
                                                         plugins: {
                                                              ptype: 'gridviewdragdrop',
                                                              dragGroup: 'DDGroup',
                                                              dropGroup: 'DDGroup'
                                                          },
                                                          listeners: {
                                                               beforedrop: function(node, data, overModel, dropPosition, dropFunction, eOpts ) {
                                                                        var storeColumnas=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridColumnas2]').store;
                                                                        if(!entorno.Controladores.ConFormGenerador.CampoYaAgregado(storeColumnas,data.records[0].get("idcampo"))){
                                                                           return true;
                                                                        }else{
                                                                           Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(100));
                                                                           return false;
                                                                        }                                                                                                          
                                                               },
                                                                drop: function(node, data, dropRec, dropPosition) {
                                                                    data.records[0].set('select',1);
                                                                    if(entorno['Vistas']['VisFormGenerador'].down('button[action=executequery]').isDisabled()){
                                                                              entorno['Vistas']['VisFormGenerador'].down('button[action=executequery]').setDisabled(false);
                                                                    }                                                               
                                                                }
                                                         }
                                                    },
                                                    dockedItems: [
                                                        {
                                                    xtype: 'toolbar',
                                                    //height: 10,

                                                    region:'center',
                                                    //flex:0.2,
                                                    width:27,
                                                    vertical: true,
                                                    dock: 'left',
                                                    items:[
                                                        {
                                                            xtype:'label',
                                                            text:'',
                                                            flex:0.5

                                                        },{
                                                            xtype:'button',
                                                            text:'',
                                                            action:'moveup',
                                                            iconCls:'up'

                                                        }, {
                                                            xtype:'button',
                                                            text:'',
                                                            action:'movedown',
                                                            iconCls:'down'
                                                        },/*{
                                                            xtype:'label',
                                                            text:'',
                                                            flex:0.2

                                                        },

                                                         {
                                                            xtype:'button',
                                                            text:'',
                                                            action:'moveright',
                                                            iconCls:'next'

                                                        }, {
                                                            xtype:'button',
                                                            text:'',
                                                            action:'moveleft',
                                                            iconCls:'previous'

                                                        },{
                                                            xtype:'button',
                                                            text:'',
                                                            action:'movetodosrigth',
                                                            iconCls:'last'

                                                        },
                                                         {
                                                            xtype:'button',
                                                            text:'',
                                                            action:'movetodosleft',
                                                            iconCls:'first'

                                                        },*/ 
                                                         {
                                                            xtype:'label',
                                                            text:'',
                                                            flex:0.5

                                                        }
                                                    ]
                                                }
                                                    ]
                                                    
                                                },

                                                {
                                                    xtype: 'gridpanel',
                                                    store:'stoColumnasIzq',
                                                    name:'gridColumnas1',
                                                    title: '',
                                                    hideHeaders:true,
                                                    flex: 1,
                                                    region: 'west',
                                                    selModel: new Ext.selection.RowModel({mode:'SINGLE'}),
                                                    columns: [
                                                        {
                                                            xtype: 'gridcolumn',
                                                            dataIndex: 'abreviatura',
                                                            text: 'Campos',
                                                            flex:1
                                                        }
                                                        
                                                        ],
                                                    viewConfig: {
                                                         allowCopy:true,
                                                          copy:true,
                                                          plugins: {
                                                              ptype: 'gridviewdragdrop',
                                                              dragGroup: 'DDGroup',
                                                              dropGroup: 'DDGroup',
                                                              enableDrop:false
                                                          }
                                                    }
                                                   

                                                }
                                         /*   ]
                                        }*/
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            //width: 150,
                            layout: {
                                align: 'stretch',
                                type: 'vbox'
                            },
                            flex:3,
                            region: 'east',
                            items: [
                                {/*
                                    xtype: 'panel',
                                 //   height: 165,
                                    width: 150,
                                    title: '',
                                    flex: 1,,,,,,,
                                    items: [
                                        {*/
                                            xtype: 'fieldset',
                                            flex:1,
                                            //padding:2,
                                            layout: {
                                                type: 'border'
                                            },
                                            title: 'Operaciones',
                                            items: [
                                                {
                                                    xtype: 'gridpanel',
                                                    //height: 150,
                                                    name:'gridFunciones',
                                                    store:'stoFunciones',
                                                    flex:1.5,
                                                    title: '',
                                                    region: 'north',
                                                    columns: [
                                                        {
                                                            xtype: 'gridcolumn',
                                                            dataIndex: 'abreviatura',
                                                            text: 'Campos Disp.',
                                                            flex:1
                                                        },
                                                        {
                                                            xtype: 'gridcolumn',
                                                            dataIndex: 'vfuncion',
                                                            text: 'Función',
                                                            flex:1,
                                                             editor: {
                                                                xtype: 'combobox',
                                                                name:'cboFunciones',
                                                                editable:false,
                                                             /*   typeAhead: true,*/
                                                                triggerAction: 'all',
                                                                selectOnTab: true,
                                                                store:'stoCboFuncion',
                                                                displayField: 'Valor',
                                                                valueField: 'IdValorParametro',
                                                               listeners: {
                                                                 expand:function(el,eOpts){
                                                                     entorno.Controladores.ConFormGenerador.FiltraComboFunciones(el)
                                                                 }
                                                                 
                                                               }                                         
                                                              
                                                              
                                                            },
                                                            renderer: function(value,metadata,record){
                                                                //Aqui fueron alambrados los valores del filtro funciones
                                                               return entorno['Controladores']['ConPrincipal'].buscarParametro(value);
                                                               /* if (value === 1) {
                                                                     return 'Máximo'
                                                                }else if(value==2){
                                                                     return 'Mínimo'    
                                                                }else if(value==3){
                                                                     return 'Promedio'    
                                                                }else if(value==4){
                                                                      return 'Sumar'    
                                                                }else if(value==5){
                                                                     return 'Contar'    
                                                                }*/
                                                                       
                                                          } 
                                                        },
                                                        {
                                                            xtype: 'actioncolumn'
                                                            ,width:25
                                                            , items: [{ // Delete button
                                                                icon: '../resources/images/delete.png'
                                                                , handler: function(grid, rowIndex, colindex) {
                                                                        var storeFunciones=grid.store;
                                                                        storeFunciones.getAt(rowIndex).set('vfuncion','');
                                                                        storeFunciones.filter(
                                                                            {filterFn: function(item) { return item.get("vfuncion") != ""; }}
                                                                        ); 
                                                                } 
                                                            }]
                                                        }
                                                        
                                                    ],
                                                     plugins:[
                                                        Ext.create('Ext.grid.plugin.CellEditing', {
                                                            clicksToEdit: 1
                                                        }) 
                                                     ],
                                                    viewConfig: {
                                                        plugins: {
                                                              ptype: 'gridviewdragdrop',
                                                              dragGroup: 'DDGroup',
                                                              dropGroup: 'DDGroup'
                                                          },
                                                          listeners: {
                                                              beforedrop: function(node, data, overModel, dropPosition, dropFunction, eOpts ) {
                                                                    if(data.records[0].get("permitefuncion")==1){                                                                      
                                                                        var storefunciones=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridFunciones]').store;
                                                                        if(!entorno.Controladores.ConFormGenerador.CampoYaAgregado(storefunciones,data.records[0].get("idcampo"))){
                                                                             if(entorno['Vistas']['VisFormGenerador'].down('button[action=executequery]').isDisabled()){
                                                                                entorno['Vistas']['VisFormGenerador'].down('button[action=executequery]').setDisabled(false);
                                                                             } 
                                                                             return true;
                                                                        }else{
                                                                           Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(100));
                                                                           return false;
                                                                        }                                                                        
                                                                    }else{
                                                                         Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(94));
                                                                         return false;
                                                                    }                                                                                                             
                                                              }
                                                          }
                                                   
                                                    },
                                                    dockedItems: [
                                                     {
                                                        xtype:'toolbar',
                                                        width:26,
                                                        dock: 'left',
                                                        vertical:true,
                                                        items:[
                                                        {
                                                            xtype:'label',
                                                            text:'',
                                                            flex:1

                                                        }
                                                    ]
                                                    },
                                                        {
                                                            xtype: 'toolbar',
                                                            dock: 'top',
                                                            items: [
                                                                {
                                                                    xtype: 'label',
                                                                    padding:'2 5 2 5',
                                                                    text:' Funciones ' 
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },                                             
                                                {
                                                    xtype: 'gridpanel',
                                                    //height: 150,
                                                    name:'gridAgrupaciones',
                                                    store:'stoAgruparCampos',
                                                    flex:1,
                                                    hideHeaders:true,
                                                    title: '',
                                                    region: 'center',
                                                    selModel: new Ext.selection.RowModel({mode:'SINGLE'}),
                                                    columns: [
                                                        {
                                                            xtype: 'gridcolumn',
                                                            dataIndex: 'abreviatura',
                                                            text: 'Campos',
                                                            flex:1
                                                        },
                                                        {
                                                            xtype: 'actioncolumn'
                                                            ,width:25
                                                            , items: [{ // Delete button
                                                                icon: '../resources/images/delete.png'
                                                                , handler: function(grid, rowIndex, colindex) {
                                                                    var storeAgrupamiento=grid.store;
                                                                    storeAgrupamiento.getAt(rowIndex).set('agrupamiento',0);
                                                                    storeAgrupamiento.filter(
                                                                         {filterFn: function(item) { return item.get("agrupamiento") == 1; }}
                                                                      ); 
                                                                } 
                                                            }]
                                                        }
                                                    ],
                                                    viewConfig: {
                                                        plugins: {
                                                              ptype: 'gridviewdragdrop',
                                                              dragGroup: 'DDGroup',
                                                              dropGroup: 'DDGroup'
                                                        },
                                                        listeners: {
                                                              beforedrop: function(node, data, overModel, dropPosition, dropFunction, eOpts ) {
                                                                    if(data.records[0].get("permiteagrupacion")==1){
                                                                        var storeagrupamiento=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridAgrupaciones]').store;
                                                                        if(!entorno.Controladores.ConFormGenerador.CampoYaAgregado(storeagrupamiento,data.records[0].get("idcampo"))){
                                                                           data.records[0].set('agrupamiento',1);
                                                                           return true;
                                                                        }else{
                                                                           Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(100));
                                                                           return false;
                                                                        }
                                                                          
                                                                    }else{
                                                                         Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(96));
                                                                         return false;
                                                                    }                                                                                                             
                                                              }
                                                        }
                                                    },
                                                    dockedItems: [
                                                    {
                                                        xtype:'toolbar',
                                                        width:26,
                                                        dock: 'left',
                                                        vertical:true,
                                                        items:[
                                                        {
                                                            xtype:'label',
                                                            text:'',
                                                            flex:1

                                                        }, {
                                                            xtype:'button',
                                                            text:'',
                                                            action:'moveup',
                                                            iconCls:'up'

                                                        }, {
                                                            xtype:'button',
                                                            text:'',
                                                            action:'movedown',
                                                            iconCls:'down'
                                                        },
                                                         {
                                                            xtype:'label',
                                                            text:'',
                                                            flex:1

                                                        }
                                                    ]
                                                    },
                                                        {
                                                            xtype: 'toolbar',
                                                            dock: 'top',
                                                            items: [
                                                                {
                                                                    xtype: 'checkboxfield',
                                                                    name:'chkAgrupar',
                                                                    boxLabel: 'Agrupar',
                                                                    labelWidth: 45
                                                                }
                                                            ]
                                                        }/*,
                                                        {
                                                            xtype: 'toolbar',
                                                            height: 10,
                                                            width: 20,
                                                            vertical: true,
                                                            dock: 'left',
                                                            items: [
                                                               
                                                            ]
                                                        }*/
                                                    ]
                                                },
                                                   {
                                                    xtype: 'gridpanel',
                                                    title: '',
                                                    name:'gridOrden',
                                                    store:'stoOrdenCampos',
                                                    flex:1,
                                                    hideHeaders:true,
                                                    region: 'south',
                                                    selModel: new Ext.selection.RowModel({mode:'SINGLE'}),
                                                    columns: [
                                                        {
                                                            xtype: 'gridcolumn',
                                                            dataIndex: 'abreviatura',
                                                            text: 'Campos',
                                                            flex:1
                                                        },
                                                        {
                                                            xtype: 'actioncolumn'
                                                            ,width:25
                                                            , items: [{ // Delete button
                                                                icon: '../resources/images/delete.png'
                                                                , handler: function(grid, rowIndex, colindex) {
                                                                    var storeOrden=grid.store;
                                                                    storeOrden.getAt(rowIndex).set('vorden','');
                                                                    storeOrden.filter(
                                                                        {filterFn: function(item) { return item.get("vorden") != ""; }}
                                                                    ); 
                                                                } 
                                                            }]
                                                        }
                                                       
                                                    ],
                                                    viewConfig: {
                                                       plugins: {
                                                              ptype: 'gridviewdragdrop',
                                                              dragGroup: 'DDGroup',
                                                              dropGroup: 'DDGroup'
                                                        },
                                                         listeners: {
                                                              beforedrop: function(node, data, overModel, dropPosition, dropFunction, eOpts ) {
                                                                        var storeOrden=entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridOrden]').store;
                                                                        if(!entorno.Controladores.ConFormGenerador.CampoYaAgregado(storeOrden,data.records[0].get("idcampo"))){
                                                                           return true;
                                                                        }else{
                                                                           Ext.Msg.alert('Información',entorno['Controladores']['ConPrincipal'].buscarParametro(100));
                                                                           return false;
                                                                        }                                                                                                          
                                                               },
                                                                drop: function(node, data, overModel, dropPosition, eOpts) {
                                                                    var radiosel=entorno['Vistas']['VisFormGenerador'].down('radiogroup[name=radioOrden]').getValue().rb;
                                                                    // var radiosel=entorno['Vistas']['VisFormGenerador'].down('radiofield[name=rb]').inputValue;
                                                                   //  var rowIndex = entorno.Controladores.ConFormGenerador.getSelectedRowIndex(entorno['Vistas']['VisFormGenerador'].down('gridpanel[name=gridOrden]'));
                                                                    
                                                                     if(radiosel==2){
                                                                         data.records[0].set('vorden','DESC');
                                                                     }else{
                                                                         data.records[0].set('vorden','ASC');
                                                                     }                                          
                                                                }
                                                         }
                                                    },
                                                    dockedItems: [
                                                    {
                                                        xtype:'toolbar',
                                                        width:26,
                                                        dock: 'left',
                                                        vertical:true,
                                                        items:[
                                                        {
                                                            xtype:'label',
                                                            text:'',
                                                            flex:1

                                                        }, {
                                                            xtype:'button',
                                                            text:'',
                                                            action:'moveup',
                                                            iconCls:'up'

                                                        }, {
                                                            xtype:'button',
                                                            text:'',
                                                            action:'movedown',
                                                            iconCls:'down'
                                                        },
                                                         {
                                                            xtype:'label',
                                                            text:'',
                                                            flex:1

                                                        }
                                                    ]
                                                    },
                                                        {
                                                            xtype: 'toolbar',
                                                            dock: 'top',
                                                            items: [
                                                                {
                                                                    xtype: 'label',
                                                                    padding:'2 5 2 5',
                                                                    text:'Orden' 
                                                                },
                                                                {
                                                                    xtype: 'radiogroup',
                                                                    name:'radioOrden',
                                                                    columns: 2,
                                                                    disabled:false,
                                                                    //flex:1,
                                                                    items: [
                                                                        { 
                                                                          boxLabel: 'Ascendente',
                                                                          name: 'rb',
                                                                          inputValue: '1',
                                                                          checked: true,
                                                                          width:100,
                                                                          listeners:{  
                                                                              change : function(el,newvalue,oldvalue,eOpts){ 
                                                                                   if (el.getValue()) { // checked.
                                                                                         entorno.Controladores.ConFormGenerador.ActualizaGrillaOrdenCampos("ASC")// or "no" 
                                                                                   }
                                                                              }
                                                                          }
                                                                        },
                                                                        { 
                                                                          boxLabel: 'Descendente',
                                                                          name: 'rb',
                                                                          inputValue: '2',
                                                                          width:100,
                                                                          listeners:{  
                                                                              change : function(el,newvalue,oldvalue,eOpts){ 
                                                                                   if (el.getValue()) { // checked.
                                                                                       entorno.Controladores.ConFormGenerador.ActualizaGrillaOrdenCampos("DESC")// or "no" 
                                                                                   }
                                                                                
                                                                              }
                                                                          }
                                                                        }                                                                        
                                                                    ]
                                                                }/*,
                                                                {
                                                                    xtype: 'label',
                                                                    flex:1
                                                                }*/
    
                                                                /*{
                                                                    xtype: 'radiofield',
                                                                    boxLabel: 'Ascendente',
                                                                    labelWidth: 60
                                                                },
                                                                {
                                                                    xtype: 'radiofield',
                                                                    boxLabel: 'Descendente',
                                                                    labelWidth: 65
                                                                }*/
                                                            ]
                                                        }
                                                    ]
                                                },
                                         /*   ]
                                        }*/
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],

             dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'bottom',
                            items: [
                                {
                                    xtype: 'textfield',
                                    name:'TxtCodigo',
                                    hidden:true
                                }, '->',
                                {
                                    xtype: 'button',
                                    iconCls:'ayuda'
                                },
                                {
                                    xtype: 'button',
                                    action:'irAConfiguracion',
                                    iconCls:'configuracion',
                                    text: 'Configurar'
                                },
                                {
                                    xtype: 'button',
                                    action:'newquery',
                                    iconCls:'new',
                                    text: 'Nuevo'
                                },
                                {
                                    xtype: 'button',
                                    action:'openquery',
                                    iconCls:'open',
                                    text: 'Abrir Consulta'
                                },
                                {
                                    xtype: 'button',
                                    action:'savequery',
                                    iconCls:'save',
                                    text: 'Grabar Consulta'
                                },
                                {
                                    xtype: 'button',
                                    action:'executequery',
                                    disabled:true,
                                    iconCls:'consultar',
                                    text: 'Consultar'
                                },
                                {
                                    xtype: 'button',
                                    iconCls:'sql',
                                    action:'sqldinam',
                                    text: 'SQL Dinámico'
                                },
                                {
                                    xtype: 'button',
                                    iconCls:'cancel',
                                    text: 'Salir'
                                }
                            ]
                        },
                         {
                            xtype: 'toolbar',
                            dock: 'top',
                            items: [
                                {
                                    xtype: 'combobox',
                                    name:'cbotpoasign',
                                    fieldLabel:'Tipo de Asignacion',
                                    emptyText:'Seleccione el tipo de asignación',
                                    editable:false,                                                        
                                    triggerAction: 'all',
                                    store:'stoCboTipoAsign',
                                    value:'',
                                    displayField: 'Valor',
                                    valueField: 'IdValorParametro'
                                },
                                {
                                    xtype: 'button',
                                    action:'IrACrudTablas',
                                    iconCls:'ir',
                                    text: 'Ir a Crud de Tablas Reporte'
                                }
                            ]
                        }
          ]
        });

        me.callParent(arguments);
    }

});