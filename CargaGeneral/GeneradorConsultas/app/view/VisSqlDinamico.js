Ext.define('Reporteador.view.VisSqlDinamico', {
    extend: 'Ext.window.Window',
    alias: 'widget.vissqldinamico',
    modal:true,
    height: 232,
    resizable:false,
    padding: '',
    width: 565,
    title: 'SQL Dinámico',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
             items: [
                {
                    xtype: 'form',
                    frame:true,
                    height: 225,
                    width: 560,

            items: [
                {
                    xtype: 'container',
                    height: 218,
                    padding: 10,
                    width: 548,
                    items: [
                        {
                            xtype: 'numberfield',
                            fieldLabel: 'Indice',
                            name:'indice',
                            labelWidth: 50,
                            hidden:true,
                            readOnly:true
                        },
                        {
                            xtype: 'textfield',
                            width: 212,
                            fieldLabel: 'Código',
                            name:'codigo',
                            allowBlank:true,                           
                            labelWidth: 60,
                            readOnly:true,
                            hidden:true
                        },
                        {
                            xtype: 'textfield',
                            width: 501,
                            fieldLabel: 'Titulo',
                            name:'titulo',
                            allowBlank:false,
                            blankText :'Este campo es requerido',
                            labelWidth: 60,
                            readOnly:true
                        },
                        {
                            xtype: 'textareafield',
                            height: 123,
                            width: 501,
                            fieldLabel: 'Consulta',
                            name:'query',
                            allowBlank:false,
                            blankText:'Este campo es requerido',
                            labelWidth: 60,
                            readOnly:true
                        }
                    ]
                }
                 ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    width: 400,
                    dock: 'bottom',
                    items: [
                        {
                          xtype:'label',
                          flex:1
                        },
                        {
                            xtype: 'button',
                            action:'newquery',
                            tooltip:'Nueva Consulta',
                            iconCls:'new'
                        },
                        {
                            xtype: 'button',
                            action:'editquery',
                            tooltip:'Editar Consulta',
                            disabled:true,
                            iconCls:'open'
                        },
                        {
                            xtype: 'button',
                            action:'savequery',
                            tooltip:'Grabar Consulta',
                            disabled:true,
                            iconCls:'save'
                        },
                        {
                            xtype: 'button',
                            action:'undo',
                            tooltip:'Deshacer',
                            disabled:true,
                            iconCls:'undo'
                        },
                        {
                            xtype: 'button',
                            action:'delquery',
                            tooltip:'Borrar Consulta',
                            disabled:true,
                            iconCls:'papelera'
                        },
                        {
                            xtype: 'button',
                            action:'executequery',
                            tooltip:'Ejecutar Consulta',
                            disabled:true,
                            iconCls:'impresion'
                        },
                        {
                            xtype: 'button',
                            action:'search',
                            tooltip:'Buscar',
                            iconCls: 'buscar',
                            menu: {                          
                                 showSeparator: false,          
                                 items: [{
                                   text: entorno['Controladores']['ConPrincipal'].buscarParametro(16)
                                 },
                                 {
                                   text: entorno['Controladores']['ConPrincipal'].buscarParametro(17)
                                 }]
                            }
                        },
                        {
                            xtype: 'button',
                            action:'firstrecord',
                            tooltip:'Primer Registro',
                            disabled:true,
                            iconCls:'first'
                        },
                        {
                            xtype: 'button',
                            action:'prevrecord',
                            tooltip:'Registro Anterior',
                            disabled:true,
                            iconCls:'previous'
                        },
                        {
                            xtype: 'button',
                            action:'nextrecord',
                            tooltip:'Registro Siguiente',
                            disabled:true,
                            iconCls:'next'
                        },
                        {
                            xtype: 'button',
                            action:'lastrecord',
                            tooltip:'Ultimo Registo',
                            disabled:true,
                            iconCls:'last'
                        },
                        {
                            xtype: 'button',
                            action:'cerrarsqldinamico',
                            tooltip:'Salir',
                            iconCls:'cancel'
                        },
                        {
                          xtype:'label',
                          flex:1
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});