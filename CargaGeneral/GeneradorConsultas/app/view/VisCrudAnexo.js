Ext.define('CrudEmp.view.VisCrudAnexo', {
    extend: 'Ext.window.Window',
    alias: 'widget.viscrudanexo',
    title: 'Cargar nuevo Anexo',
    width: 410,
    autoHeight: true,
    bodyPadding: 10,
    modal: true,
    padre:'',
    Identificacion:0,
    Entidad:0,
    mystore:'',
    // frame: true,
    //  renderTo: Ext.getBody(),

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
    items: [{
        xtype: 'form',
        bodyPadding: 10,
        height: 170,
        items: [
        {
            xtype: 'combobox',
            fieldLabel: 'Tipo de Anexo',
            editable: false,
            store: 'stoTipoAnexo',
            labelWidth: 90,
            width: 350,
            allowBlank: false,
            //queryMode :  'local',
            displayField: 'Valor',
            valueField: 'IdValorParametro',
            name: 'IdTipoAnexo'
        }
        ,
        {
            xtype: 'filefield',
            name: 'anexo',
            fieldLabel: 'Anexo',
            labelWidth: 90,
            width: 350,
            msgTarget: 'side',
            hidden:true,
            allowBlank: true,
            //anchor: '100%',
            buttonText: 'Seleccione Archivo...'
        }, {
            xtype: 'textfield',
            fieldLabel: 'Url',
            hidden:true,

            labelWidth: 90,
            width: 350,
            allowBlank:true,
            name: 'url'
        }
        , {
            xtype: 'textareafield',
            fieldLabel: 'Descripcion',
            labelWidth: 90,
            width: 350,
            hidden:true,
            allowBlank:false,
            name: 'Descripcion'
        }, {
            xtype: 'textfield',
            fieldLabel: 'Identificacion',
            value:me.Identificacion,
            hidden:true,

            name: 'Identificacion' //solo es para submit
        }, {
            xtype: 'textfield',
            fieldLabel: 'Entidad',
            value:me.Entidad,
            hidden:true,
            name: 'Entidad' //solo es para submit
        }],

        buttons: [{
            text: 'Cargar',
            action: 'clickCargarAnexo',
            cls: 'x-btn-text-icon',
            iconCls: 'icon-agregar'
        }]
    }]
});

        me.callParent(arguments);
    }

});
