Ext.define('Reporteador.view.VisColaMaquinas', {
  extend: 'Ext.panel.Panel',
    iconCls:'machine',
    title:'Cola de tareas maquinas',
    alias:'widget.viscolamaquinas',
    layout: {
        type: 'border'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype:'visgridhorario',
                    icono:'machine',
                    region:'center',
                    padding:5,
                    titulo:'Carga General',
                    entidad:'Cod. Maquina'
                },
               {
                    xtype:'visgridhorario',
                    
                    region:'south',
                    flex:1,
                    padding:5,
                    titulo:'Carga Granel',
                    entidad:'Cod. Maquina'
                }
               
            ]
        });

        me.callParent(arguments);
    }

});