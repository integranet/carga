Ext.define('Reporteador.view.VisGridHorario', {
    extend: 'Ext.panel.Panel',
    alias:'widget.visgridhorario',
    titulo: '',
    entidad:'',//la entidad es el titulo de la primera columna
    layout:{
        type:'border'
    },
    initComponent: function() {
        var me = this;
        
        Ext.applyIf(me, {
            items:[
            {
                xtype:'gridpanel',
                title:me.titulo,
                region:'center',
                columns: [
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: me.entidad,
                    //flex:2,
                    width:80
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: 'Estado',
                    //flex:0.5,
                    width:45

                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '0:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '1:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '2:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '3:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '4:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '5:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '6:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '7:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '8:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '9:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '10:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '11:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '12:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '13:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '14:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '15:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '16:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '17:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '18:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '19:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '20:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '21:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '22:00',
                    flex:1,
                },{
                    xtype: 'gridcolumn',
                    dataIndex: 'string',
                    text: '23:00',
                    flex:1,
                }
                ],
                viewConfig: {

                }
            },
            {
                xtype:'panel',
                flex:0.3,
                iconCls:'lista',
                region:'east',
                collapsible: true,
                title:'Lista De Asignaciones'

            }
            ]
           
        });

        me.callParent(arguments);
    }

});