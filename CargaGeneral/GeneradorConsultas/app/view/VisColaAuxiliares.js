Ext.define('Reporteador.view.VisColaAuxiliares', {
  extend: 'Ext.panel.Panel',
   iconCls:'auxiliar',
    title:'Cola de tareas auxiliares',
    alias:'widget.viscolaauxiliares',
    layout: {
        type: 'border'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype:'visgridhorario',
                    region:'center',
                    padding:5,
                    titulo:'Carga General',
                    entidad:'Cod. Auxiliar'
                },
               {
                    xtype:'visgridhorario',
                    region:'south',
                    flex:1,
                    padding:5,
                    titulo:'Carga Granel',
                    entidad:'Cod. Auxiliar'
                }
               
            ]
        });

        me.callParent(arguments);
    }

});