<?php 


if(isset($_POST['action']) && !empty($_POST['action'])) {
    $action = $_POST['action'];
    switch($action) {
        case 'eliminarcampo' : EliminarCampo();break;
        case 'guardarcampo' : GuardarCampo();break;
        case 'editarcampo' : ActualizarCampo();break;
        // ...etc...
    }
}


function EliminarCampo() {

   //include("conexion/config.php");
   include("conexion/conexion.php");

   if (isset($_POST['idcamporeporte'])) {
      $idcamporeporte = trim($_POST['idcamporeporte']);

       $con = new Conexion($db_nombre, $db_usuario, $db_password);
       /*Se verifica si el campo es usado en alguna consulta*/
       $sqltotal = " select count(*) total from det_cnslta where id_cmpo_rprte= ".$idcamporeporte;
       $result= $con -> ejecutarConsulta($sqltotal);
       $rowtotal = odbc_fetch_array($result);
       $total = $rowtotal['total'];
       if($total>0){
           echo trim(json_encode(array('data' => null,'razon'=> 'No puede eliminarse el campo porque tiene consultas relacionadas', 'success' => false, 'total' => 0)));
       }else{  
           $sqldelcampo=" delete From def_cmpos_rprte Where id_cmpo_rprte=".$idcamporeporte;
           $rs =  $con -> ejecutarConsulta($sqldelcampo);
           if($rs){         
              echo trim(json_encode(array('data' => null,'razon'=> 'Campo eliminado exitosamente' ,'success' => true, 'total' => 0)));
           }else{
              echo trim(json_encode(array('data' => null, 'razon'=> 'Ocurrió un error al eliminar el campo' ,'success' => false, 'total' => 0)));
           }    
       }
      $con -> cerrarConexion();
    }else{
          echo trim(json_encode(array('data' => null,'razon'=> 'El campo indicado no pudo ser eliminado', 'success' => false, 'total' => 0)));
    }

  
}

function GuardarCampo(){
      
       //include("conexion/config.php");
       include("conexion/conexion.php");

       $con = new Conexion($db_nombre, $db_usuario, $db_password);

       if(isset($_POST['tabla'])){
            $tabla = trim($_POST['tabla']);
        }
        if(isset($_POST['campo'])){
            $campo = trim($_POST['campo']);
        }
        if(isset($_POST['abreviatura'])){
            $abreviatura = trim(utf8_decode($_POST['abreviatura']));
        }
        if(isset($_POST['tiporeporte'])){
            $tiporeporte = trim($_POST['tiporeporte']);
        }
        if(isset($_POST['tipodato'])){
            $tipodato = trim($_POST['tipodato']);
        }      
         if(isset($_POST['longitud'])){
            $longitud = trim($_POST['longitud']);
        }
        if(isset($_POST['filtro'])){
            $filtro = trim($_POST['filtro']);
        }
        if(isset($_POST['funcion'])){
            $funcion = trim($_POST['funcion']);
        }       
        if(isset($_POST['agrupacion'])){
            $agrupacion = trim($_POST['agrupacion']);
        }          
        $usuario=1;         

            $sqlinscamporep =  " insert into def_cmpos_rprte(NMBRE_TBLA,NMBRE_CMPO,uso_rprte,ABRVTRA,id_tpo_rprte,tpo_dto,lngtud,fltro,fncion,agrpcion,id_estdo_cmpo)
              values('$tabla', '$campo',$usuario, '$abreviatura',$tiporeporte,'$tipodato','$longitud','$filtro','$funcion','$agrupacion',78)";
             
               $rs =  $con -> ejecutarConsulta($sqlinscamporep);

               if($rs){ 
                 echo trim(json_encode(array('data' => null, 'success' => true, 'total' => 0)));
              }else{
                     echo trim(json_encode(array('data' => null, 'success' => false, 'total' => 0)));
              }

           $con -> cerrarConexion();
    }


function ActualizarCampo(){
    
       //include("conexion/config.php");
       include("conexion/conexion.php");

       $con = new Conexion($db_nombre, $db_usuario, $db_password);
 
    
      if(isset($_POST['tabla'])){
            $tabla = trim($_POST['tabla']);
        }
        if(isset($_POST['campo'])){
            $campo = trim($_POST['campo']);
        }
         if(isset($_POST['abreviatura'])){
            $abreviatura = trim(utf8_decode($_POST['abreviatura']));
        }
        if(isset($_POST['tiporeporte'])){
            $tiporeporte = trim($_POST['tiporeporte']);
        }
        if(isset($_POST['tipodato'])){
            $tipodato = trim($_POST['tipodato']);
        }      
         if(isset($_POST['longitud'])){
            $longitud = trim($_POST['longitud']);
        }
        if(isset($_POST['filtro'])){
            $filtro = trim($_POST['filtro']);
        }
        if(isset($_POST['funcion'])){
            $funcion = trim($_POST['funcion']);
        }       
        if(isset($_POST['agrupacion'])){
            $agrupacion = trim($_POST['agrupacion']);
        }        

        $sqlupdcamporep = " update def_cmpos_rprte set ABRVTRA='$abreviatura', TPO_DTO='$tipodato', LNGTUD='$longitud',
        FLTRO ='$filtro',FNCION ='$funcion' ,AGRPCION ='$agrupacion' where NMBRE_TBLA ='$tabla' and NMBRE_CMPO='$campo' 
        and id_tpo_rprte=$tiporeporte";  
        $rs =  $con -> ejecutarConsulta($sqlupdcamporep);  

                 if($rs){                  
                     echo trim(json_encode(array('data' => null, 'success' => true, 'total' => 0)));
                  }else{
                     echo trim(json_encode(array('data' => null, 'success' => false, 'total' => 0)));
                  }   
              $con -> cerrarConexion();       
         }
         

?>