<?php
  if(isset($_POST['action']) && !empty($_POST['action'])) {
    $action = $_POST['action'];

    switch($action) {
        case 'cargarselect':CargarSelect();break;
        case 'cargarfiltros':CargarFiltros();break;
        case 'cargarfunciones':CargarFunciones();break;
        case 'cargaragrup':CargarAgrupamiento();break;
        case 'cargarorden':CargarOrdenamiento();break;
        // ...etc...
    }
}
     function CargarSelect() {
      
        if (isset($_POST['idconsulta'])) {
              $idconsulta = $_POST['idconsulta']; 
              $sqltraerselect = "select dr.nmbre_tbla, dr.nmbre_cmpo, dr.abrvtra,dr.tpo_dto,dc.id_cmpo_rprte,dc.slct,dc.agrpcion, 
              dc.vlor_fltro, dc.vlor_fncion, dc.vlor_orden from def_cmpos_rprte dr,det_cnslta dc where dr.id_cmpo_rprte
              =dc.id_cmpo_rprte and dc.slct=1 and dc.id_estdo_cnslta=76 and dc.id_enc_cnslta=".$idconsulta;
              EjecutarConsulta($sqltraerselect);
       }else{
           echo json_encode(array('success' => false,'data' => null,'total' => 0));
       }  
  }

  function CargarFiltros() {
      
        if (isset($_POST['idconsulta'])) {
              $idconsulta = $_POST['idconsulta']; 
              $sqltraerfilt = " select dr.nmbre_tbla, dr.nmbre_cmpo,dr.abrvtra,dr.tpo_dto,dc.id_cmpo_rprte,dc.slct,dc.agrpcion, 
              dc.vlor_fltro, dc.vlor_fncion, dc.vlor_orden from def_cmpos_rprte dr,det_cnslta dc where dr.id_cmpo_rprte=
              dc.id_cmpo_rprte and dc.vlor_fltro != ''  and dc.id_estdo_cnslta=76 and dc.id_enc_cnslta=".$idconsulta;
              EjecutarConsulta($sqltraerfilt);
       }else{
           echo json_encode(array('success' => false,'data' => null,'total' => 0));
       }  
  }

  function CargarFunciones() {
      
        if (isset($_POST['idconsulta'])) {
              $idconsulta = $_POST['idconsulta']; 
              $sqltraerfunc = "select dr.nmbre_tbla, dr.nmbre_cmpo, dr.abrvtra,dr.tpo_dto,dc.id_cmpo_rprte,dc.slct,dc.agrpcion, 
              dc.vlor_fltro, dc.vlor_fncion, dc.vlor_orden from def_cmpos_rprte dr, det_cnslta dc where 
              dr.id_cmpo_rprte=dc.id_cmpo_rprte and dc.vlor_fncion != '' and dc.id_estdo_cnslta=76 and 
              dc.id_enc_cnslta=".$idconsulta;
              EjecutarConsulta($sqltraerfunc);
       }else{
           echo json_encode(array('success' => false,'data' => null,'total' => 0));
       }  
  }

  function CargarAgrupamiento() {
      
        if (isset($_POST['idconsulta'])) {
              $idconsulta = $_POST['idconsulta']; 
              $sqltraeragrup = "select dr.nmbre_tbla, dr.nmbre_cmpo, dr.abrvtra,dr.tpo_dto,dc.id_cmpo_rprte,dc.slct,dc.agrpcion, 
              dc.vlor_fltro, dc.vlor_fncion, dc.vlor_orden from def_cmpos_rprte dr, det_cnslta dc where dr.id_cmpo_rprte=
              dc.id_cmpo_rprte and dc.agrpcion=1 and dc.id_estdo_cnslta=76 and dc.id_enc_cnslta=".$idconsulta;
              EjecutarConsulta($sqltraeragrup);
       }else{
           echo json_encode(array('success' => false,'data' => null,'total' => 0));
       }  
  }

    function CargarOrdenamiento() {
      
        if (isset($_POST['idconsulta'])) {
              $idconsulta = $_POST['idconsulta']; 
              $sqltraerselect = "select dr.nmbre_tbla, dr.nmbre_cmpo, dr.abrvtra,dr.tpo_dto,dc.id_cmpo_rprte,dc.slct,dc.agrpcion, 
              dc.vlor_fltro, dc.vlor_fncion, dc.vlor_orden from def_cmpos_rprte dr, det_cnslta dc where dr.id_cmpo_rprte
              =dc.id_cmpo_rprte and dc.vlor_orden != ''  and dc.id_estdo_cnslta=76 and dc.id_enc_cnslta=".$idconsulta;
              EjecutarConsulta($sqltraerselect);
       }else{
           echo json_encode(array('success' => false,'data' => null,'total' => 0));
       }  
  }


  function EjecutarConsulta($consulta) {

     // include("conexion/config.php");     
      include("conexion/conexion.php");  
       
      $con = new Conexion($db_nombre, $db_usuario, $db_password);    
     
      $result = $con -> ejecutarConsulta($consulta);    
      while ($row=odbc_fetch_array($result))
      {

           $contenido['idcampo'] = ltrim(rtrim(utf8_encode($row['id_cmpo_rprte'])));
           $contenido['nomtabla'] = ltrim(rtrim(utf8_encode($row['nmbre_tbla'])));
           $contenido['nomcampo'] = ltrim(rtrim(utf8_encode($row['nmbre_cmpo'])));
           $contenido['abreviatura'] = ltrim(rtrim(utf8_encode($row['abrvtra'])));
           $contenido['tipodato'] = ltrim(rtrim(utf8_encode($row['tpo_dto'])));
           $contenido['select'] =ltrim(rtrim(utf8_encode($row['slct'])));
           $contenido['vfiltro'] =ltrim(rtrim(utf8_encode($row['vlor_fltro'])));
           $contenido['vfuncion'] =ltrim(rtrim(utf8_encode($row['vlor_fncion'])));
           $contenido['vorden'] =ltrim(rtrim(utf8_encode($row['vlor_orden'])));
           $contenido['agrupamiento'] =ltrim(rtrim(utf8_encode($row['agrpcion'])));
       
           $data[] = $contenido;
          
      } 
        
        if(!isset ($data)) {
           $data = null;
        }
      
            echo json_encode(array('success' => true,'data' => $data,'total' => count($data)));
            $con -> cerrarConexion();           
   }



?>