<?php

if(isset($_POST['action']) && !empty($_POST['action'])) {
      $action = $_POST['action'];
      switch($action) {
          case 'instablabase' : ActualizaTablasBase();break;
          case 'acttablabase' : ActualizaTablasBase();break;
          case 'deltablabase' : EliminarTablaBase();break;
          case 'instablamaestra' : ActualizaTablasMaestras();break;
          case 'acttablamaestra' : ActualizaTablasMaestras();break;
          case 'deltablamaestra' : EliminarTablaMaestra();break;
          // ...etc...
      }
 
  }
   
  
 /*Funcion que cambia el estado de los parametros a inactivo*/
function EliminarTablaBase() {
   //include("conexion/config.php");
    
   include("conexion/conexion.php");   

   if (isset($_POST['idtablaenc'])) {
      $idtablaenc = $_POST['idtablaenc'];   
      $con = new Conexion($db_nombre, $db_usuario, $db_password);
      $sqldeldet = "delete from det_tbla_rprte where id_enc_tbla_rep=" .$idtablaenc;
      $result = $con -> ejecutarConsulta($sqldeldet); 

      $sqldelenc = "delete from enc_tbla_rprte where id_enc_tbla_rep= " .$idtablaenc;
        
      if($con -> ejecutarConsulta($sqldelenc)){
            echo "{success:true,razon:'Tabla eliminada exitosamente'}";
      }else{
             echo "{success:false,razon:'Ocurrió un error al eliminar la tabla'}";
      }    
      
       $con -> cerrarConexion();

  }else{
      echo "{success:false,razon:'Ocurrió un error al eliminar la tabla'}";
  }

}


/*Funcion para insertar nuevos parametros en tabla dir_parametros*/
function ActualizaTablasBase() {
       
      //include("conexion/config.php");
       
      include("conexion/conexion.php");

             
     if (isset($_POST['idtablaenc'])) {
            $idtablaenc = $_POST['idtablaenc'];
            $con = new Conexion($db_nombre, $db_usuario, $db_password);
             if (isset($_POST['tablabase'])) {
                $tablabase = $_POST['tablabase'];
              }
              if (isset($_POST['tiporeporte'])) {
                  $tiporeporte = utf8_decode($_POST['tiporeporte']);
              } 
              if (isset($_POST['idestado'])) {
                  $idestado = utf8_decode($_POST['idestado']);
              } 
            if($idtablaenc==""){      
              $sqlinstablabase = "insert into enc_tbla_rprte(tbla_bse,id_tpo_rprte,id_estdo_enc_tbla_rprte)
              values('" . $tablabase . "'," . $tiporeporte . "," . $idestado . ")";         
                     
               if($con -> ejecutarConsulta($sqlinstablabase)){
                        echo "{success:true,razon:'Tablas actualizadas exitosamente'}";
               }else{
                       echo "{success:false,razon:'Ocurrió un error al realizar operacion'}";
                }
           
          }else{
                 $sqlacttablabase = "update enc_tbla_rprte set tbla_bse = '" . $tablabase . "', id_tpo_rprte = '" . $tiporeporte . "', 
                 id_estdo_enc_tbla_rprte = '" . $idestado . "' where id_enc_tbla_rep = " . $idtablaenc;
                                  
                if($con -> ejecutarConsulta($sqlacttablabase)){
                        echo "{success:true,razon:'Tablas actualizadas exitosamente'}";
                }else{
                       echo "{success:false,razon:'Ocurrió un error al realizar operacion'}";
                }       
           
          }
          $con -> cerrarConexion();

    }else{
      echo "{success:false,razon:'Ocurrió un error al realizar operación'}";
    }

}


/*Funcion para insertar nuevos valores de parametros en tabla dir_valorparametros*/
function ActualizaTablasMaestras() {
        
        //include("conexion/config.php");
         
        include("conexion/conexion.php");

        if (isset($_POST['idtabladet'])) {
               $idtabladet = $_POST['idtabladet'];
                $con = new Conexion($db_nombre, $db_usuario, $db_password);
                if (isset($_POST['idtablaenc'])) {
                    $idtablaenc = utf8_decode($_POST['idtablaenc']);
                }
                if (isset($_POST['tablamaestra'])) {
                    $tablamaestra = utf8_decode($_POST['tablamaestra']);
                }  
                if (isset($_POST['campotblbase'])) {
                    $campotblbase = utf8_decode($_POST['campotblbase']);
                } 
                if (isset($_POST['campotblmaestra'])) {
                    $campotblmaestra = utf8_decode($_POST['campotblmaestra']);
                }  
                 if (isset($_POST['idestado'])) {
                    $idestado = utf8_decode($_POST['idestado']);
                }                
             if($idtabladet==""){
                $sqlinstablamaestra = "insert into det_tbla_rprte(tbla_mstra,id_enc_tbla_rep,cmpo_tbla_bse,cmpo_tbla_mstra,id_estdo_det_tbla_rprte)
                values('" . $tablamaestra . "','" . $idtablaenc . "','" . $campotblbase . "','" . $campotblmaestra . "',".$idestado.")";
                
                  if($con -> ejecutarConsulta($sqlinstablamaestra)){
                          echo "{success:true,razon:'Tablas actualizadas exitosamente'}";
                  }else{
                         echo "{success:false,razon:'Error al realizar operacion'}";
                  }       
                
            }else{
                   $sqlacttablamaestra = "update det_tbla_rprte set tbla_mstra = '" . $tablamaestra . "', 
                   cmpo_tbla_bse='".$campotblbase."',cmpo_tbla_mstra='".$campotblmaestra."', 
                   id_estdo_det_tbla_rprte = '" . $idestado . "' where id_det_tbla_rep = " . $idtabladet;
                                    
                  if($con -> ejecutarConsulta($sqlacttablamaestra)){
                          echo "{success:true,razon:'Tablas actualizadas exitosamente'}";
                  }else{
                         echo "{success:false,razon:'Error al realizar operacion'}";
                  }          
            }
             $con -> cerrarConexion();
    }else{
        echo "{success:false,razon:'Ocurrió un error al realizar operación'}";
    }
       
}



/*Funcion que cambia el estado de los valores de parametros a inactivo*/
function EliminarTablaMaestra() {
        
        //include("conexion/config.php");
         
        include("conexion/conexion.php");

        if (isset($_POST['idtabladet'])) {
             $idtabladet = $_POST['idtabladet'];
             $con = new Conexion($db_nombre, $db_usuario, $db_password);
             $sqldelmaestra = "delete from det_tbla_rprte where id_det_tbla_rep= " .$idtabladet;
        
             if($con -> ejecutarConsulta($sqldelmaestra)){
                  echo "{success:true,razon:'Tabla removida exitosamente'}";
             }else{
                   echo "{success:false,razon:'Ocurrió un error al eliminar la tabla'}";
             }            
              $con -> cerrarConexion();
         }else{
               echo "{success:false,razon:'Error al realizar operacion'}";
        }
         
}      


?>