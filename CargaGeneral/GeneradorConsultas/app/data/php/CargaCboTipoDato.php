<?php  
    header("Content-Type: text/plain");   
  
    echo "{ 
        total:9, 
        data:[{ 
            id: '0', 
            valor: 'CHAR'
        },{ 
            id: '1', 
            valor: 'SMALLINT' 
            
        },{ 
            id: '2', 
            valor: 'INTEGER' 
            
        },{ 
            id: '4', 
            valor: 'SMALLFLOAT'
        },{ 
            id: '5', 
            valor: 'DECIMAL'
        },{ 
            id: '6', 
            valor: 'SERIAL' 
            
        },{ 
            id: '7', 
            valor: 'DATE' 
            
        },{ 
            id: '8', 
            valor: 'MONEY' 
          
        },{ 
            id: '10', 
            valor: 'DATETIME'
        },{ 
            id: '11', 
            valor: 'BYTE' 
            
        },{ 
            id: '12', 
            valor: 'TEXT' 
            
        },{ 
            id: '13', 
            valor: 'VARCHAR' 
          
        },{ 
            id: '14', 
            valor: 'INTERVAL'
        },{ 
            id: '256', 
            valor: 'CHAR'
        },{ 
            id: '257', 
            valor: 'SMALLINT' 
            
        },{ 
            id: '258', 
            valor: 'INTEGER' 
            
        },{ 
            id: '260', 
            valor: 'SMALLFLOAT' 
          
        },{ 
            id: '261', 
            valor: 'DECIMAL'
        },{ 
            id: '262', 
            valor: 'SERIAL'
        },{ 
            id: '263', 
            valor: 'DATE'
        },{ 
            id: '264', 
            valor: 'MONEY'
        },{ 
            id: '266', 
            valor: 'DATETIME' 
            
        },{ 
            id: '267', 
            valor: 'BYTE' 
            
        },{ 
            id: '268', 
            valor: 'TEXT' 
          
        },{ 
            id: '269', 
            valor: 'VARCHAR'
        },{ 
            id: '270', 
            valor: 'INTERVAL'
        }] 
    }";  
?>  