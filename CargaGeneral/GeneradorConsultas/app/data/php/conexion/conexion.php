<?php
/*
$server = '192.168.4.115';
$user = 'test';
$pwd = 'sprb07';
$db = 'aspa21b';
$dsn = 'informix://test:sprb07@192.168.4.115/aspa21b';
*/
include("../../../../Configuracion/config.php");
class Conexion{

	private $conn;

	function Conexion($dsn,$usuario,$password){
		try{
			$this->conn = odbc_connect($dsn, $usuario, $password);
		}catch(Exception $e){
			return $e->getMessage();
		}
	}

	public function ejecutarConsulta($sql) {
        try {
        	$result = odbc_exec($this->conn,$sql);
        	return $result;
        } catch (Exception $e) {
        	return $e->getMessage();
        }
    }
    
    public function cerrarConexion(){
    	odbc_close($this->conn);
    }


}
?>