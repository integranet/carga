<?php

if(isset($_POST['rutaArchivo'])){
    $RutaArchivo = $_POST['rutaArchivo'];   
    /*Se obtiene y recorre una lista con todos los archivos con extension txt 
    dentro del directorio especificado*/
    foreach (glob($RutaArchivo."*.txt") as $nombre_archivo) {
        //Se eliminan los archivos con extension txt dentro del directorio indicado
        unlink($nombre_archivo);
    }
  //  unlink($RutaArchivo);	
  echo json_encode(array(
      'success' => true,
      'razon'=> 'archivo eliminado exitosamente'
   ));
	
}else{
	echo json_encode(array(
      'success' => false,
      'razon'=> 'no fue posible eliminar el archivo'
   ));
}

?>