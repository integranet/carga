<?php

if(isset($_POST['action']) && !empty($_POST['action'])) {
      $action = $_POST['action'];
      switch($action) {
          case 'traetablabase' : TraerTablaBase();break; 
          case 'traetablasmaestras' : TraerTablasMaestras();break;        
          // ...etc...
      }
 
  }   
  
 /*Funcion Para obtener la tabla base de un reporte dado*/
function TraerTablaBase() {
  
  include("conexion/conexion.php");   

   if (isset($_POST['tiporeporte'])) {
      $tiporeporte = $_POST['tiporeporte'];   
      $con = new Conexion($db_nombre, $db_usuario, $db_password);
      $sqltraetblbase = "select id_enc_tbla_rep,tbla_bse from enc_tbla_rprte where id_estdo_enc_tbla_rprte=45 and id_tpo_rprte=".$tiporeporte;
    
     $result = $con -> ejecutarConsulta($sqltraetblbase);     
        
      if($result){
            $row = odbc_fetch_array($result);
            $tablabase = $row['tbla_bse'];
            $idtablabase = $row['id_enc_tbla_rep'];      
            echo json_encode(array('success' => true,'tablabase' => $tablabase,'idtablabase' => $idtablabase,'total' => count($tablabase)));
      }else{
           echo json_encode(array('success' => false,'tablabase' => null,'idtablabase' => null,'total' => 0));
      }    
      
       $con -> cerrarConexion();

  }else{
       echo json_encode(array('success' => false,'tablabase' => null,'idtablabase' => null,'total' => 0));
  }

}

function TraerTablasMaestras(){
   //include("conexion/config.php");
  include("conexion/conexion.php");   

   if (isset($_POST['tablabase'])) {
      $tablabase = $_POST['tablabase']; 

      $con = new Conexion($db_nombre, $db_usuario, $db_password);

      $query = "select * from det_tbla_rprte WHERE id_estdo_det_tbla_rprte=47 and id_enc_tbla_rep=".$tablabase;
     
      $result= $con -> ejecutarConsulta($query);

         $data = array();
          while ($row=odbc_fetch_array($result))
          {
               $contenido['Id'] = utf8_encode($row['id_det_tbla_rep']);
               $contenido['Valor'] = utf8_encode($row['tbla_mstra']);
               $contenido['CampoBase'] = utf8_encode($row['cmpo_tbla_bse']);
               $contenido['CampoMaestra'] = utf8_encode($row['cmpo_tbla_mstra']);
                            
               $data[] = $contenido;              
          }
          //$result = null;

        $con -> cerrarConexion();

        echo json_encode(array('data' => $data, 'success' => true, 'total' => count($data)));

  }else{
       echo json_encode(array('data' => null,'success' => false,'total' => 0));
  }
}

?>