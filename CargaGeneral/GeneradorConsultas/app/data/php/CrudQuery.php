<?php



if(isset($_POST['action']) && !empty($_POST['action'])) {
    $action = $_POST['action'];
    switch($action) {
        case 'eliminarquery' : EliminarQuery();break;
        case 'guardarquery' : GuardarQuery();break;
        case 'editarquery' : ActualizarQuery();break;
        // ...etc...
    }
}



function EliminarQuery() {

   //include("conexion/config.php");
   include("conexion/conexion.php");


   if (isset($_POST['codigo'])) {
      $codigo = trim($_POST['codigo']);
      
      $con = new Conexion($db_nombre, $db_usuario, $db_password);
      $sqldelsql = " delete From rprtes_sql_dnmcos Where cdgo_sql=$codigo ";
     
      $rs= $con -> ejecutarConsulta($sqldelsql);

       if($rs){         
           echo trim(json_encode(array('data' => null, 'success' => true, 'total' => 0)));
       }else{
          echo trim(json_encode(array('data' => null, 'success' => false, 'total' => 0)));
       }
     
        $con -> cerrarConexion();
  }else{
     echo trim(json_encode(array('data' => null, 'success' => false, 'total' => 0)));
  }
}

function GuardarQuery(){
      
       //include("conexion/config.php");
       include("conexion/conexion.php");
    
       $con = new Conexion($db_nombre, $db_usuario, $db_password);

       if(isset($_POST['codigo'])){
            $codigo = trim($_POST['codigo']);
        }
        if(isset($_POST['titulo'])){
            $titulo = trim($_POST['titulo']);
        }
        if(isset($_POST['query'])){
           $query = html_entity_decode(trim($_POST['query']), ENT_QUOTES, "UTF-8");
           // $query = trim($_POST['query']);
        }    
        session_start();  
        //$user=$_SESSION['idusuario']; 
        /*Obtiene el codigo con que va a guardarse la nuevo consulta. Falta agregar clausula
        where por el campo uso_rprte*/
       /*  $sqlnucodigo = "select max(cdgo_sql) as maximo from gnrdor_rprte_sql ";
         $result= $con -> ejecutarConsulta($sqlnucodigo);
         $row = odbc_fetch_array($result);
         $nucodigo = $row['maximo']+1;   */
         $hoy = date("Y-m-d");   
         $fecAct = new DateTime(trim(utf8_encode($hoy)));
         $fechaHoy=date_format($fecAct, 'Y-m-d H:i:s');      
         /*Falta agregar campos  uso_rprte, fcha_mdfccion dentro del insert*/
         $sqlinsquery = " insert into rprtes_sql_dnmcos(ttlo_sql, exprsion_sql,fcha_mdfccion)
         values ('$titulo', '$query','$fechaHoy')"; 
         $rs= $con->ejecutarConsulta($sqlinsquery);   
         if($rs){                  
             echo trim(json_encode(array('success' => true)));
          }else{
             echo trim(json_encode(array('success' => false)));
          }
        $con->cerrarConexion();
}


function ActualizarQuery(){
      
       //include("conexion/config.php");
       include("conexion/conexion.php");

       if(isset($_POST['codigo'])){
            $codigo = trim($_POST['codigo']);
            $con = new Conexion($db_nombre, $db_usuario, $db_password);
        if(isset($_POST['titulo'])){
            $titulo = trim($_POST['titulo']);
        }
        if(isset($_POST['query'])){
            $query = html_entity_decode(trim($_POST['query']), ENT_QUOTES, "UTF-8");
            //$query = trim($_POST['query']);
        } 
         //Se obtiene la fecha de modificacion 
         $hoy = date("Y-m-d");   
         $fecAct = new DateTime(trim(utf8_encode($hoy)));
         $fechaHoy=date_format($fecAct, 'Y-m-d H:i:s');       

         /*Falta agregar campos  uso_rprte, fcha_mdfccion dentro del update*/
         $sqlupdquery = " update rprtes_sql_dnmcos set ttlo_sql='$titulo', 
         exprsion_sql='$query', fcha_mdfccion='$fechaHoy' where cdgo_sql=$codigo";
        
                 $rs=$con->ejecutarConsulta($sqlupdquery);
                 if($rs){                  
                     echo trim(json_encode(array('data' => null, 'success' => true, 'total' => 0)));
                  }else{
                     echo trim(json_encode(array('data' => null, 'success' => false, 'total' => 0)));
                  }  

            $con->cerrarConexion();

       }else{

            echo trim(json_encode(array('data' => null, 'success' => false, 'total' => 0)));
       }  
 }       


?>