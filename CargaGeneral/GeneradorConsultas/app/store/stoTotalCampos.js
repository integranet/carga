Ext.define('Reporteador.store.stoTotalCampos',{
	extend: 'Reporteador.store.storeDinamico',
    url: 'app/data/php/ListarCampos.php',
    model: 'Reporteador.model.ModBuscarCampos', 
    autoLoad:false,
    extraParams:{
     TipoReporte:''
    }
});