Ext.define('Reporteador.store.stoColumnasDer',{
	extend: 'Reporteador.store.storeDinamico',
    url: 'app/data/php/CargaGrillasConsulta.php',
    model: 'Reporteador.model.ModColumnas',
    autoLoad:false,
    extraParams:{
      idconsulta:'',
      action:'cargarselect'
    }
});