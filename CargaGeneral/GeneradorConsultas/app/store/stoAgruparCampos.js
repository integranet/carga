Ext.define('Reporteador.store.stoAgruparCampos',{
	extend: 'Reporteador.store.storeDinamico',
    url: 'app/data/php/CargaGrillasConsulta.php',
    model: 'Reporteador.model.ModColumnas',
    autoLoad:false,
    extraParams:{
      idconsulta:'',
      action:'cargaragrup'
    }
});