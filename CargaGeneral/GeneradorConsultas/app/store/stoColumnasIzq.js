Ext.define('Reporteador.store.stoColumnasIzq',{
	extend: 'Reporteador.store.storeDinamico',
    url: 'app/data/php/CargarGrillaColumnas.php',
    model: 'Reporteador.model.ModColumnas', 
    autoLoad:false,
    extraParams:{
     TipoReporte:''
    }
});