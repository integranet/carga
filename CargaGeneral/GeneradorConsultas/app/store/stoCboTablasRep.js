Ext.define('Reporteador.store.stoCboTablasRep',{
	extend: 'Reporteador.store.storeDinamico',
    url: 'app/data/php/CargaTablasReporte.php',
    model: 'Reporteador.model.ModParametros',
    autoLoad:true,
    extraParams:{
      TipoReporte:''
    }
});