Ext.define('Reporteador.store.stoRepoConf',{
	extend: 'Reporteador.store.storeDinamico',
    url: 'app/data/php/CargaReportes.php',
    model: 'Reporteador.model.ModReportesConf',
    autoLoad:false,
    extraParams:{
      TipoReporte:''
    }
});