Ext.define('Reporteador.store.stoTotalConsultas',{
	extend: 'Reporteador.store.storeDinamico',
    url: 'app/data/php/ListarConsultas.php',
    model: 'Reporteador.model.ModBuscarSql', 
    autoLoad:false,
    extraParams:{
     TipoReporte:''
    }
});