Ext.define('Reporteador.store.stoCboCampos',{
	extend: 'Reporteador.store.storeDinamico',
    url: 'app/data/php/CargaCamposTabla.php',
    model: 'Reporteador.model.ModCamposTabla',
    autoLoad:false,
    extraParams:{
      Tabla:''
    }
});