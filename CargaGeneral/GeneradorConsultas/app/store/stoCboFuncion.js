Ext.define('Reporteador.store.stoCboFuncion',{
	extend: 'Reporteador.store.storeDinamico',
    url: 'app/data/php/SelectParametro.php',
    model: 'Reporteador.model.ModParametros',
    autoLoad:true,
    extraParams:{
       IdParametro:1
    }
});