Ext.define('Reporteador.store.stoTablasDet',{
	extend: 'Reporteador.store.storeDinamico',
    url: 'app/data/php/CargaTablasMaestras.php',
    model: 'Reporteador.model.ModTablasDet', 
    autoLoad:false,
    extraParams:{
      idtablaenc:''
    }
});