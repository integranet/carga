Ext.define('Reporteador.store.stoBuscarCampos',{
	extend: 'Reporteador.store.storeDinamico',
    url: 'app/data/php/ListarCampos.php',
    model: 'Reporteador.model.ModBuscarCampos',
    autoLoad:false
});