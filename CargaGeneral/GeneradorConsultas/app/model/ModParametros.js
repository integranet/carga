Ext.define('Reporteador.model.ModParametros',{
    extend: 'Ext.data.Model',
   fields: [
      'IdParametro',
      'IdValorParametro',
      'Valor',
      'Orden'
      ]
});