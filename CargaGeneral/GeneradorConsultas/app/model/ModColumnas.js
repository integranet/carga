Ext.define('Reporteador.model.ModColumnas',{
    extend: 'Ext.data.Model',
   fields: [
      'idcampo',
      'nomtabla',
      'nomcampo',
      'abreviatura',
      'tipodato',
      'longitud',
      'permitefuncion',
      'permitefiltro',
      'permiteagrupacion',
      'select',
      'agrupamiento',
      'vfiltro',
      'vfuncion',
      'vorden',      
      'tipofuncion'
      ]
});