Ext.define('Reporteador.model.ModReportesConf',{
	extend		: 'Ext.data.Model',
	fields		: ['codigo', 'titulo','usuario',
	'filtros','columnas','funciones','orden','agrupacion']
});