Ext.define('Reporteador.model.ModBuscarCampos',{
	extend		: 'Ext.data.Model',
	fields		: ['idcamporeporte','tabla', 'campo','abreviatura',
	'tipodato','longitud','filtro','funcion','agrupacion']
});