Ext.Loader.setConfig(
    {
        enabled: true
    }
);


Ext.application({
    
    name: 'Reporteador',
    appFolder: 'app',
    controllers: ['ConPrincipal','ConFormGenerador','ConTablasGenerador','ConReportesConf','ConConfiguracion','ConSqlDinamico','ConTituloConsulta']
});
