<?php
//Hecho por: Yulibeth
//odbc:israel
//xml:israel

class bd {
    private $con;
    function bd($bd="",$uid="",$pwd="") {
        if($bd=="") {
            $config = simplexml_load_file("../config/web.config");
            $strings=$config->xpath("//configuration/connectionStrings/add");
            for($i=0;
            $i<count($strings);
            $i++) {
                if($strings[$i]['name']=='SmSecure')$bd=$strings[$i]['value'];
            }
        }
        $bd=str_replace("ODBC;","",$bd);
        
        $bd=str_replace("odbc;","",$bd);
        $this->con = odbc_connect($bd,$uid,$pwd);
    }
    public function consulta($sql) {
        $res=odbc_exec($this->con,$sql);
        return $res;
    }
    public function close() {
        odbc_close($this->con);
    }
    public function validateDecimals($number) {
        if(ereg('^[0-9]+\.[0-9]$', $number))
            return true;
        else
            return false;
    }
    public function desparametro($cod,$val) {
        $sql="select descripcion from pgSigis.dbo.detParametros where codParametro='$cod' and valor=$val";
        $reg=odbc_fetch_array($this->consulta($sql));
        return $reg["descripcion"];
    }

    public function crearthumb ( $pathToImages, $pathToThumbs, $thumbWidth ) {

        // abrimos el directorio
        $dir = opendir( $pathToImages );
        //Se utiliza un Prefijo para la creación de los Thumbs
        $PreThumbs = "thumb_";
        // bucle en busca de archivos jpg
        $sw = 0;
        while (false !== ($fname = readdir( $dir ))) {
            $info = pathinfo($pathToImages . $fname);

            // continua unicamente si la imagen es jpg
            if( strtolower($info['extension']) == 'jpg' ) {
                //echo "Creando thumbnail para {$fname}";
                $img = imagecreatefromjpeg( "{$pathToImages}{$fname}" );
            }else {
                if( strtolower($info['extension']) == 'gif' ) {
                    $img = imagecreatefromgif( "{$pathToImages}{$fname}" );
                }else {
                    if( strtolower($info['extension']) == 'png' ) {
                        $img = imagecreatefrompng( "{$pathToImages}{$fname}" );
                    }else {
                        $sw = 1;
                    }
                }
            }
            if ($sw == 0) {
                // carga y devuelve las medidas de la imagen
                $width = imagesx( $img );
                $height = imagesy( $img );
                /*// calcula el tamaño
                $new_width = $thumbWidth;
                $new_height = floor( $height * ( $thumbWidth / $width ) );
                */
                $new_width = $thumbWidth;
                $new_height = $thumbWidth;
                // crea una nueva imagen de manera temporal
                $tmp_img = imagecreatetruecolor( $new_width, $new_height );
                // copia y dedimensiona la nueva imagen en la temporal que hemos creado
                imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
                //Pregunto si existe el Archivo en la Carpeta de Thumbs para decidir si crearlo o NO
                if (!file_exists($pathToThumbs.$PreThumbs.$fname)) {
                    // guarda el thumbnail en un archivo
                    imagejpeg( $tmp_img, "{$pathToThumbs}{$PreThumbs}{$fname}" );
                }
            }
            $sw = 0;
        }
        // cierra el directorio
        closedir( $dir );
    }

}
?>