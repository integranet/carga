Ext.define('auxiliar.view.VisAuxiliar', {
    extend: 'Ext.panel.Panel',
    autoRender:true,
    iconCls:'auxiliar',
    region:'center',
    title:'Personas',
    alias:'widget.visauxiliar',
    layout: {
        type: 'border'
    },

    initComponent: function() {
        var me = this;
         var fe = Ext.create('Ext.grid.feature.GroupingSummary', {
                groupHeaderTpl: 'Fecha: {name} ({rows.length} Persona{[values.rows.length > 1 ? "s" : ""]})',
                hideGroupedHeader: true
            });
        Ext.applyIf(me, {
            items: [
               
                {
                    xtype: 'gridpanel',
                    region:'north',
                    store:'stoAuxiliar',
                    flex:1,
                    name:'gridUsuarios',
                     plugins: [
                        Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })
                    ],
                    columns: [
                        { 
                            text: 'Codigo',
                            flex:1,
                            dataIndex: 'username' 
                        },
                        { 
                            text: 'Nombre',
                            flex:1,
                            dataIndex: 'nmbre_usrio' 
                        }
                        
                    ],
                    bbar: Ext.create('Ext.PagingToolbar', {
                            store: 'stoAuxiliar',
                            displayInfo: true,
                            displayMsg: 'Personas {0} - {1} de {2}',
                            emptyMsg: "No hay Personas",
                            items:[
                              /*   {
                                    xtype:'label',
                                    text:'',
                                    flex:1
                                },
                                {
                                    xtype:'button',
                                    iconCls:'icon-up'

                                },
                                {
                                    xtype:'button',
                                    iconCls:'icon-down'

                                },
                                {
                                    xtype:'label',
                                    text:'',
                                    flex:1
                                }*/
                            ]
                        })   
                },
               
                {
                    xtype: 'gridpanel',
                    region:'center',
                    flex:1,
                     iconCls:'auxiliar',
                    title:'Personas disponibles',
                    features:[fe],
                    store:'stoAuxiliarDisponible',
                    name:'gridDisponible',
                    bbar: Ext.create('Ext.PagingToolbar', {
                            store: 'stoAuxiliarDisponible',
                            displayInfo: true,
                            displayMsg: 'Personas disponibles {0} - {1} de {2}',
                            emptyMsg: "No hay Personas disponibles",
                            items:[
                                { xtype: 'tbseparator' },
                                {
                                    xtype:'checkbox',
                                    boxLabel  : 'Solo hoy',
                                    name      : 'isToday',
                                    checked:'true'
                                },
                                { xtype: 'tbseparator' },
                                {
                                    xtype:'checkbox',
                                    boxLabel  : 'Solo Activos',
                                    name      : 'isActivo',
                                    checked:'true'
                                }, { xtype: 'tbseparator' },
                                {
                                    xtype:'button',
                                    action:'clickCancelar',
                                    iconCls:'icon-cancelar',
                                    text:'Terminar disponibilidad'
                                }, {
                                    xtype:'button',
                                    action:'clickEditar',
                                    iconCls:'icon-edit',
                                    text:'Corregir zona'
                                },{
                                    xtype:'label',
                                    html:'<table width="100" height="20" border="0"><tr class="colorDiurnoCELL"><td >'+me.textTurno1+'</td><td class="colorNocturnoCELL" >'+me.textTurno2+'</td></table>',
                                    width:110
                                }
                                
                            ]
                        }),  
                    plugins: [
                        Ext.create('Core.ux.FilterRow',{
                            remoteFilter:true
                        })
                    ],
                    columns: [
                        { 
                            text: 'Codigo',
                            flex:1,
                            dataIndex: 'username' 
                        },
                        { 
                            text: 'Nombre',
                            flex:1,
                            dataIndex: 'nmbre_usrio' 
                        },{ 
                            text: 'Zona',
                            flex:1,
                            dataIndex: 'cdgo_zna'
                        },{ 
                            text: 'Fecha',
                            flex:1,
                            dataIndex: 'fcha'
                        },{ 
                            text: 'Hora Inicial',
                            flex:1,
                            dataIndex: 'hra_incial'
                        },{ 
                            text: 'Hora Final',
                            flex:1,
                            dataIndex: 'hra_fnal'
                        },
                        { 
                            text: 'Estado',
                            flex:1,
                            dataIndex: 'estdo' 
                        },
                        { 
                            text: 'Tipo turno',
                            flex:1,
                            hidden:true,
                            dataIndex: 'turno' 
                        }
                        
                    ],viewConfig: {
                    getRowClass: function(record) {

                        if(record.data.turno == '101'){
                            return 'ColorDiurno';
                        }else if(record.data.turno == '102'){
                            return 'ColorNocturno';
                        }else{
                            return '';
                        }
                    }
                },
                    
                           
                }
               
            ]
        });

        me.callParent(arguments);
    }

});