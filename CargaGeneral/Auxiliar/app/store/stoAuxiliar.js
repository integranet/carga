Ext.define('auxiliar.store.stoAuxiliar',{
	extend: 'auxiliar.store.storeDinamico',
    url: 'app/data/php/SelectAuxiliar.php',
    model: 'auxiliar.model.Auxiliar', 
    autoLoad: {start: 0, limit: 15},
    pageSize: 15,
    remoteSort: true,
});