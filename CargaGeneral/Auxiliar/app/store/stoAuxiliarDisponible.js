Ext.define('auxiliar.store.stoAuxiliarDisponible',{
	extend: 'auxiliar.store.storeDinamico',
    url: 'app/data/php/SelectAuxiliaresDisponible.php',
    model: 'auxiliar.model.AuxiliarDisponible', 
    autoLoad: {start: 0, limit: 15},
    extraParams: {hoy: true, estado:true},
    pageSize: 15,
    groupDir:'ASC',
    //sortInfo:{field: 'hra_incial', direction: "DESC"},
    groupField: 'fcha',
});