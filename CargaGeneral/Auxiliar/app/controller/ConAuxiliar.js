Ext.define('auxiliar.controller.ConAuxiliar',{
	extend: 'Ext.app.Controller',
	
    init:function (){
	 		
     	this.control({
        'visauxiliar checkbox[name=isToday]':{
           change:this.CargarDisponibles
        },
        'visauxiliar checkbox[name=isActivo]':{
           change:this.CargarActivos
        },
         'visauxiliar button[action=clickCancelar]':{
           click:this.CancelarDisponibles
        },
        'visauxiliar button[action=clickEditar]':{
           click:this.EditarDisponibles
        },
        /* 'visauxiliar button[action=clickEditarOperario]':{
           click:this.cambiarOperario
        },*/
        'visauxiliar gridpanel[name=gridUsuarios]':{
           itemdblclick:this.MostrarFormulario
        },
        'visformauxiliar button[action=clickCancelar]':{
            click:this.Cancelar
        },
       
        'visformauxiliar button[action=clickDisponerAuxiliar]':{
            click:this.Disponer
        },
        'visformauxiliar button[action=clickCambiarZona]':{
            click:this.updateZona
        },
         'visformauxiliar timefield[name=hra_incial]':{
            select:this.ValidarHoraInicial
        },

      })
  	},

    clearTime:function( field, value, eOpts ){
      
        entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').clearValue();
        entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_incial]').clearValue();
        entorno['Vistas']['VisFormAuxiliar'].down('datefield[name=fchaFinal]').setValue(value);

    },

    MostrarFormulario:function(){
      me = this;
       var records = entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridUsuarios]").getSelectionModel().getSelection();
       if(records.length>=1){
           entorno['Vistas']['VisFormAuxiliar']=Ext.create('auxiliar.view.VisFormAuxiliar',{
             incremento: entorno['Controladores']['ConPrincipal'].buscarParametro(82),
             
             FnHoraFinal:me.ValidarHoraFinal,
             FnHoraInicial:me.ValidarHoraInicial
           });
           
           entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridDisponible]").store.load();
           entorno['Vistas']['VisFormAuxiliar'].show();
           entorno['Vistas']['VisFormAuxiliar'].down('form').getForm().loadRecord(records[0]);
           

           
       }
  	},

	  CargarDisponibles:function(el, newValue, oldValue, eOpts){
        entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridDisponible]").store.proxy.extraParams.hoy=newValue;
       
        entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridDisponible]").store.load();

    },
    
    CargarActivos:function(el, newValue, oldValue, eOpts){
        entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridDisponible]").store.proxy.extraParams.estado=newValue;
       
        entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridDisponible]").store.load();

    },

    Cancelar:function(){
        entorno['Vistas']['VisFormAuxiliar'].close();
        entorno['Vistas']['VisFormAuxiliar']=null;

    },

    Disponer:function(){
        var form= entorno['Vistas']['VisFormAuxiliar'].down('form').getForm();
        if(form.isValid()){
          
              Ext.Ajax.request({
                url: 'app/data/php/insertDisponibilidad.php',
                //waitMsg: 'Registrando',
                params:{
                  username:entorno['Vistas']['VisFormAuxiliar'].down('textfield[name=username]').getValue(),
                  nmbre_usrio:entorno['Vistas']['VisFormAuxiliar'].down('textfield[name=nmbre_usrio]').getValue(),
                  hra_incial:entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_incial]').getRawValue(),
                  hra_fnal:entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').getRawValue(),
                  cdgo_zna:entorno['Vistas']['VisFormAuxiliar'].down('combobox[name=cdgo_zna]').getValue(),
                  fcha:entorno['Vistas']['VisFormAuxiliar'].down('datefield[name=fcha]').getRawValue(),
                  fchaFinal:entorno['Vistas']['VisFormAuxiliar'].down('datefield[name=fchaFinal]').getRawValue()

                },
                success:function(response){
                  
                  resp = Ext.decode(response.responseText)
                    if(resp.sw){
                      
                        entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridDisponible]").store.load();
                        entorno['Vistas']['VisFormAuxiliar'].close();
                        entorno['Vistas']['VisFormAuxiliar']=null;
                    }else{

                        var mens =entorno['Controladores']['ConPrincipal'].buscarParametro(resp.error);
                        var m = Ext.Msg.alert("informacion",mens);
                        m.toFront();
                    }
                },
                failure:function(){
                  
                        var mens =entorno['Controladores']['ConPrincipal'].buscarParametro(68);
                        var m = Ext.Msg.alert("informacion",mens);
                        m.toFront();
                       
                }
              }           
              );
        }
    },

    CancelarDisponibles:function(){
        var records = entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridDisponible]").getSelectionModel().getSelection();
       if(records.length>=1){
           if(records[0].data.estdo!='I'){
              Ext.Ajax.request({
                url: 'app/data/php/updateAuxiliarDisponible.php',
                
                params:{
                  sw:3,
                  data:Ext.encode(records[0].data)

                },
                success:function(response){
                  
                  resp = Ext.decode(response.responseText)
                    if(resp.sw){
                      
                        entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridDisponible]").store.load();
                        
                    }else{

                        var mens =entorno['Controladores']['ConPrincipal'].buscarParametro(68);
                        var m = Ext.Msg.alert("informacion",mens);
                        m.toFront();
                    }
                },
                failure:function(){
                  
                        var mens =entorno['Controladores']['ConPrincipal'].buscarParametro(68);
                        var m = Ext.Msg.alert("informacion",mens);
                        m.toFront();
                       
                }
              });
           }else{
              Ext.Msg.alert("informacion",entorno['Controladores']['ConPrincipal'].buscarParametro(105));
           }
           
           
       }else{
          Ext.Msg.alert("informacion",entorno['Controladores']['ConPrincipal'].buscarParametro(105));
       }
    },


 EditarDisponibles:function(){
        var records = entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridDisponible]").getSelectionModel().getSelection();
       if(records.length>=1){
           if(records[0].data.estdo!='I'){
                entorno['Vistas']['VisFormAuxiliar']=Ext.create('auxiliar.view.VisFormAuxiliar',{
                   update:true,
                   title:'Corregir Zona - '+records[0].data.nmbre_usrio,
                   height:110
                });
               
                 entorno['Vistas']['VisFormAuxiliar'].show();
                 entorno['Vistas']['VisFormAuxiliar'].down('form').getForm().loadRecord(records[0]);

           }else{
              Ext.Msg.alert("informacion",entorno['Controladores']['ConPrincipal'].buscarParametro(105));
           }
           
           
       }else{
          Ext.Msg.alert("informacion",entorno['Controladores']['ConPrincipal'].buscarParametro(105));
       }
    },

    cambiarOperario:function(){
      var records = entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridDisponible]").getSelectionModel().getSelection();
      if(records.length>=1){
          entorno['Vistas']['VisFormAuxiliar']=Ext.create('auxiliar.view.VisFormAuxiliar',{
             update:true,
             title:'Cambiar operario',
             height:110
          });
           
           
           entorno['Vistas']['VisFormAuxiliar'].show();
           
        
      }else{
          Ext.Msg.alert("informacion",entorno['Controladores']['ConPrincipal'].buscarParametro(80));
       }
    },

    updateZona:function(){
      var records = entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridDisponible]").getSelectionModel().getSelection();
      if(records.length>=1){
          var zona = entorno['Vistas']['VisFormAuxiliar'].down('combobox[name=cdgo_zna]');
          if(zona.isValid()){
              records[0].data.cdgo_zna=zona.value;
              Ext.Ajax.request({
                url: 'app/data/php/updateAuxiliarDisponible.php',
                
                params:{
                  sw:2,
                  data:Ext.encode(records[0].data),
                  
                },
                success:function(response){
                  
                  resp = Ext.decode(response.responseText)
                    if(resp.sw){
                      
                        entorno['Vistas']['VisAuxiliar'].down("gridpanel[name=gridDisponible]").store.load();
                        entorno['Vistas']['VisFormAuxiliar'].close();
                        entorno['Vistas']['VisFormAuxiliar']=null;
                        
                    }else{

                        var mens =entorno['Controladores']['ConPrincipal'].buscarParametro(resp.error);
                        var m = Ext.Msg.alert("informacion",mens);
                        m.toFront();
                    }
                },
                failure:function(){
                  
                        var mens =entorno['Controladores']['ConPrincipal'].buscarParametro(68);
                        var m = Ext.Msg.alert("informacion",mens);
                        m.toFront();
                       
                }
              });
          }
      }
    },

    ValidarHoraInicial:function(value, field){
    
                           /*   try {*/
                                    h1 =  entorno['Controladores']['ConPrincipal'].buscarParametro(90);
                                    h2 = entorno['Controladores']['ConPrincipal'].buscarParametro(91);
  
                                    hh1 = 1*h1.substring(0,2);
                                    mm1 = 1*h1.substring(3,5);
                                  
                                    hh2 = 1*h2.substring(0,2);
                                    mm2 = 1*h2.substring(3,5);
                                   
                                    var hi = entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_incial]').getValue().getHours();
                                    var mi = entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_incial]').getValue().getMinutes();
                                    entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').clearValue();
                                    
                                    if(hi<=hh1){
                                     
                                        if (hi==hh1){
                                            if(mi<mm1){
                                               
                                                 entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').getStore().data.items[hh1]);
                                                entorno['Vistas']['VisFormAuxiliar'].down('label[name=lbTurno]').setText("Turno:    Nocturno"+" ( "+h2+" a "+h1+" )");
                                            }else{
                                               
                                                entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').getStore().data.items[hh2]);
                                                entorno['Vistas']['VisFormAuxiliar'].down('label[name=lbTurno]').setText("Turno:    Diurno"+" ( "+h1+" a "+h2+" )");
                                            }
                                        }else{
                                           
                                            entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').getStore().data.items[hh1]);
                                            entorno['Vistas']['VisFormAuxiliar'].down('label[name=lbTurno]').setText("Turno:    Nocturno"+" ( "+h2+" a "+h1+" )");
                                        } 
                                        
                                    }else{
                                        if(hi>=hh2){

                                            if (hi==hh2){
                                                if(mi<mm2){
                                                   
                                                    entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').getStore().data.items[hh2]);
                                                    entorno['Vistas']['VisFormAuxiliar'].down('label[name=lbTurno]').setText("Turno:    Diurno"+" ( "+h1+" a "+h2+" )");
                                                }else{
                                                  
                                                    entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').getStore().data.items[hh1]);
                                                    entorno['Vistas']['VisFormAuxiliar'].down('label[name=lbTurno]').setText("Turno:    Nocturno"+" ( "+h2+" a "+h1+" )");
                                                }
                                            }else{
                                               
                                                entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').getStore().data.items[hh1]);
                                                entorno['Vistas']['VisFormAuxiliar'].down('label[name=lbTurno]').setText("Turno:    Nocturno"+" ( "+h2+" a "+h1+" )");
                                            } 
                                            
                                        }else{
                                           
                                            entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').select(entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').getStore().data.items[hh2]);
                                            entorno['Vistas']['VisFormAuxiliar'].down('label[name=lbTurno]').setText("Turno:    Diurno"+" ( "+h1+" a "+h2+" )");
                                        }
                                    }

                                  
                                    //return true;
                         /*     }catch(e){
                                 
                                    return true;
                                }*/
                                
                            },

    ValidarHoraFinal:function(value, field){


      if(value!=""){
             /*  try {*/
                              
                                var dia = entorno['Vistas']['VisFormAuxiliar'].down('datefield[name=fcha]').getValue();
                                entorno['Vistas']['VisFormAuxiliar'].down('datefield[name=fchaFinal]').setValue(dia);

                                    h1 =  entorno['Controladores']['ConPrincipal'].buscarParametro(90);
                                    h2 = entorno['Controladores']['ConPrincipal'].buscarParametro(91);
  
                                    hh1 = 1*h1.substring(0,2);
                                    mm1 = 1*h1.substring(3,5);
                                  
                                    hh2 = 1*h2.substring(0,2);
                                    mm2 = 1*h2.substring(3,5);

                                var hi = entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_incial]').getValue().getHours();
                                var hf = entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').getValue().getHours();
                                var mi = entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_incial]').getValue().getMinutes();
                                var mf = entorno['Vistas']['VisFormAuxiliar'].down('timefield[name=hra_fnal]').getValue().getMinutes();

                                    if(hi<=hh1){
                                        if (hi==hh1){
                                            if(mi<mm1){
                                                sw=false;
                                            }else{
                                                sw=true;
                                            }
                                        }else{
                                            sw=false;
                                        } 
                                        
                                    }else{
                                        if(hi>=hh2){

                                            if (hi==hh2){
                                                if(mi<mm2){
                                                    sw=true;
                                                }else{
                                                    sw=false;
                                                }
                                            }else{
                                                sw=false;
                                            } 
                                            
                                        }else{
                                            sw=true;
                                        }
                                    }

                                    if(sw){

                                        var text = ""+entorno['Controladores']['ConPrincipal'].buscarParametro(81)+" ( "+h1+" a "+h2+" )";

                                            if(hi<=hf){
                                                if(hi==hf){
                                                    if(mi<mf){
                                                        
                                                        return true;
                                                    }else{

                                                        return text;
                                                    }
                                                }else{
                                                   if(hf<=hh2){
                                                         if(hf==hh2){
                                                            if(mf<=mm2){
                                                                return true;
                                                            }else{
                                                                return text;
                                                            }
                                                         }else{
                                                            return true;
                                                         }
                                                        
                                                            
                                                    }else{
                                                            return text;
                                                    }
                                                }
                                            }else{
                                                return text;
                                            }
                                    }else{
                                            var text = ""+entorno['Controladores']['ConPrincipal'].buscarParametro(81)+" ( "+h2+" a "+h1+" )";
 
                                            if (hi>=hh2){

                                                if(hf>=hi){
                                                    if(hf==hi){
                                                        if(mi<mf){
                                                       
                                                            return true;
                                                        }else{

                                                            return text;
                                                        }
                                                    }else{
                                                     
                                                        return true;
                                                    }
                                                }else{
                                                    if(hf<=hh1){
                                                        if(hf==hh1){
                                                            if(mf<=mm1){
                                                              
                                                                entorno['Vistas']['VisFormAuxiliar'].down('datefield[name=fchaFinal]').setValue(dia.Add("D", 1));
                                                                return true;

                                                            }else{

                                                                return text;
                                                            }
                                                        }else{
                                                          
                                                            entorno['Vistas']['VisFormAuxiliar'].down('datefield[name=fchaFinal]').setValue(dia.Add("D", 1));
                                                            return true;
                                                        }
                                                    }else{
                                                        return text;
                                                    }
                                                }
                                            }else{

                                                if(hf<=hh1){
                                                        if(hf==hh1){
                                                            if(mf<=mm1){
                                                                if(hf>=hi){
                                                                     if(hf==hi){
                                                                        if(mi<mf){
                                                                         
                                                                            entorno['Vistas']['VisFormAuxiliar'].down('datefield[name=fchaFinal]').setValue(dia);
                                                                            return true;
                                                                        }else{

                                                                            return text;
                                                                        }
                                                                    }else{
                                                                      
                                                                        entorno['Vistas']['VisFormAuxiliar'].down('datefield[name=fchaFinal]').setValue(dia);
                                                                        return true;
                                                                    }
                                                                }else{
                                                                    return text;
                                                                }

                                                                
                                                            }else{

                                                                return text;
                                                            }
                                                        }else{
                                                          if(hf>=hi){
                                                                     if(hf==hi){
                                                                        if(mi<mf){
                                                                         
                                                                            entorno['Vistas']['VisFormAuxiliar'].down('datefield[name=fchaFinal]').setValue(dia);
                                                                            return true;
                                                                        }else{

                                                                            return text;
                                                                        }
                                                                    }else{
                                                                      
                                                                        entorno['Vistas']['VisFormAuxiliar'].down('datefield[name=fchaFinal]').setValue(dia);
                                                                        return true;
                                                                    }
                                                                }else{
                                                                    return text;
                                                                }
                                                        }
                                                }else{
                                                    return text;
                                                }
                                            }

                                           
                                    }
                                    

                              /*  }catch(e){
                                    return "error "+entorno['Controladores']['ConPrincipal'].buscarParametro(81);
                                }*/

        }else{
            //return true;
        }                                
    }
});