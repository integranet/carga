Ext.define('auxiliar.controller.ConPrincipal',{
	extend: 'Ext.app.Controller',
	views:['VisPrincipal','VisAuxiliar','VisFormAuxiliar'],
	stores: ['stoAuxiliar','stoAuxiliarDisponible','stoZona'],
    init:function (){
	 		document.getElementById('Idbody').innerHTML="";
	 		
	 		//definimos el entorno:
	 		this.cargarParametros();


	 		
         	this.control({

	        })
  	},

  	IniciarEntorno:function(parametros){
  			entorno ={};

  			// Parametros SI:
  			entorno['parametros']=parametros;
  			
  			// Controladores del sistema:
  			entorno['Controladores']={};
  			entorno['Controladores']['ConPrincipal']=this;

      this.colorTurnoROW(this.buscarParametro(103),"colorDiurno");
      this.colorTurnoROW(this.buscarParametro(104),"colorNocturno");

      this.colorTurnoCELL(this.buscarParametro(103),"colorDiurnoCELL");
      this.colorTurnoCELL(this.buscarParametro(104),"colorNocturnoCELL");


  			// Vistas de Usuario:
  			entorno['Vistas']={};
  			
        entorno['Vistas']['VisPrincipal']=Ext.widget('visprincipal');
       
        entorno['Vistas']['VisAuxiliar']=Ext.widget('visauxiliar',{
            textTurno1:this.buscarParametro(101),
            textTurno2:this.buscarParametro(102)

        });
       
        entorno['Vistas']['VisPrincipal'].add(entorno['Vistas']['VisAuxiliar']);



  	},
	  
    cargarParametros:function(){
    	me=this;
    	Ext.Ajax.request({
					url: 'app/data/php/CargarParametros.php',
					
					success: function(response) {

						var resp = Ext.decode(response.responseText);
						
						me.IniciarEntorno(resp.data);
					
					},
					failure: function() {
											
					}
		});
    },

    buscarParametro:function(value){
    	for (var i = 0; i < entorno.parametros.length; i++) {
    		if(entorno.parametros[i].IdValorParametro==value){
    			return entorno.parametros[i].Valor;
    		}
    	}
    	return value;
    },

     colorTurnoROW: function(color,nombreClase){
        color = color || "#fffec7";
        nombreClase = nombreClase || "colorasignacion";
        var heads = document.getElementsByTagName('head')[0];
        var estilo = document.createElement('style');
        
        var css = "."+nombreClase+" > td { background:"+color+" !important; } ";
        estilo.type = 'text/css';
        if(estilo.styleSheet){
            estilo.styleSheet.cssText = css;
        }else{
            estilo.appendChild(document.createTextNode(css));
        }
        document.getElementsByTagName('head')[0].appendChild(estilo);
    },

    colorTurnoCELL: function(color,nombreClase){
        color = color || "#fffec7";
        nombreClase = nombreClase || "colorasignacion";
        var heads = document.getElementsByTagName('head')[0];
        var estilo = document.createElement('style');
        
        var css = "."+nombreClase+"  { background:"+color+" !important; text-align:center !important; font-size:12px !important; } ";
        estilo.type = 'text/css';
        if(estilo.styleSheet){
            estilo.styleSheet.cssText = css;
        }else{
            estilo.appendChild(document.createTextNode(css));
        }
        document.getElementsByTagName('head')[0].appendChild(estilo);
    },

	       
});