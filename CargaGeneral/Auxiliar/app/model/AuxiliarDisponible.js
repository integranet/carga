Ext.define('auxiliar.model.AuxiliarDisponible',{
    extend: 'Ext.data.Model',
   fields: [
      'username',
      'nmbre_usrio',
      'cdgo_zna',
      'estdo',
      'fcha',
      'hra_incial',
      'hra_fnal',
      'turno'
      ]
});