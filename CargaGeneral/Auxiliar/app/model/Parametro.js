Ext.define('auxiliar.model.Parametro',{
    extend: 'Ext.data.Model',
   fields: [
        'IdValorParametro'
        ,'IdParametro'
        ,'Valor'
        ,'Orden'
      ]
});